//
//  Settings+CoreDataProperties.m
//  
//
//  Created by Juli on 25/11/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Settings+CoreDataProperties.h"

@implementation Settings (CoreDataProperties)

@dynamic applicationBackground;
@dynamic catalogueButtonBackground;
@dynamic generalButtonsBackground;
@dynamic headerViewBackground;
@dynamic homeScreenButtons;
@dynamic logoImageBinary;
@dynamic navigationBarBackground;
@dynamic pickerBackground;
@dynamic searchBarBackground;
@dynamic tableCellHeaderBackground;
@dynamic myaccount_button;
@dynamic orders_button;
@dynamic products_button;
@dynamic promotions_button;
@dynamic template_button;
@dynamic segmantbarBackground;
@dynamic datePickerBackground;

@end

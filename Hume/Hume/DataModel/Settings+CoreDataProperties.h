//
//  Settings+CoreDataProperties.h
//  
//
//  Created by Juli on 25/11/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Settings.h"

NS_ASSUME_NONNULL_BEGIN
/**
 @brief: A catagory class for Settings.
         Added Theming realted all properties like view backgound color,image, different buttons types.
 @property:
 - NSString *applicationBackground;
 - NSString *catalogueButtonBackground;
 - NSString *generalButtonsBackground;
 - NSString *headerViewBackground;
 - NSString *homeScreenButtons;
 - NSString *logoImageBinary;
 - NSString *navigationBarBackground;
 - NSString *pickerBackground;
 - NSString *searchBarBackground;
 - NSString *tableCellHeaderBackground;
 - NSString *myaccount_button;
 - NSString *orders_button;
 - NSString *products_button;
 - NSString *promotions_button;
 - NSString *template_button;
 - NSString *segmantbarBackground;
 - NSString *datePickerBackground;

 */
@interface Settings (CoreDataProperties)

//Properties
@property (nullable, nonatomic, retain) NSString *applicationBackground;
@property (nullable, nonatomic, retain) NSString *catalogueButtonBackground;
@property (nullable, nonatomic, retain) NSString *generalButtonsBackground;
@property (nullable, nonatomic, retain) NSString *headerViewBackground;
@property (nullable, nonatomic, retain) NSString *homeScreenButtons;
@property (nullable, nonatomic, retain) NSString *logoImageBinary;
@property (nullable, nonatomic, retain) NSString *navigationBarBackground;
@property (nullable, nonatomic, retain) NSString *pickerBackground;
@property (nullable, nonatomic, retain) NSString *searchBarBackground;
@property (nullable, nonatomic, retain) NSString *tableCellHeaderBackground;
@property (nullable, nonatomic, retain) NSString *myaccount_button;
@property (nullable, nonatomic, retain) NSString *orders_button;
@property (nullable, nonatomic, retain) NSString *products_button;
@property (nullable, nonatomic, retain) NSString *promotions_button;
@property (nullable, nonatomic, retain) NSString *template_button;
@property (nullable, nonatomic, retain) NSString *segmantbarBackground;
@property (nullable, nonatomic, retain) NSString *datePickerBackground;

@end

NS_ASSUME_NONNULL_END

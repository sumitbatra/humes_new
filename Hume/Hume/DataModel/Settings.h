//
//  Settings.h
//  
//
//  Created by Juli on 25/11/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN
/**
 @brief: Settings is a NSManagedObject subclass. Can perform data storage oprations realted to theme
 @function:
 insertWithJSONData
      -  Can preserve a theme setting object by passing a setting dictionary in database.
 saveSettings
      -  Will enumrate each object for collecting different backgound color values.
 getTheameSettings
      -  Start the opration for fetching theme parameteres from DB.
*/
@interface Settings : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
+(Settings *)insertWithJSONData:(NSDictionary*)dataDictionary;
+(Settings *)saveSettings:(Settings *)settngObj withDictionary:(NSDictionary*)dataDictionary;
+(Settings *)getTheameSettings;

@end

NS_ASSUME_NONNULL_END

#import "Settings+CoreDataProperties.h"

//
//  Settings.m
//  
//
//  Created by Juli on 25/11/15.
//
//

#import "Settings.h"
#import "AppDelegate.h"
#import "NSDictionary+KeyExists.h"

@implementation Settings

// Insert code here to add functionality to your managed object subclass

/**
 @brief:Can preserve a theme setting object by passing a setting dictionary in database.
 */
+(Settings *)insertWithJSONData:(NSDictionary*)dataDictionary{
	
		NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Settings" inManagedObjectContext:[APP_DELEGATE managedObjectContext]];
		NSFetchRequest *request = [[NSFetchRequest alloc] init];
		[request setEntity:entityDesc];
		NSArray *arraySettings = [[APP_DELEGATE managedObjectContext] executeFetchRequest:request error:nil];
		if ([arraySettings count] > 0) {
		
			Settings *settingObj = [arraySettings lastObject];
			[self saveSettings:settingObj withDictionary:dataDictionary];
			return settingObj;
		}
		else {
		 Settings *settingObj = [NSEntityDescription insertNewObjectForEntityForName:@"Settings"
															 inManagedObjectContext:[APP_DELEGATE managedObjectContext]];
			
			[self saveSettings:settingObj withDictionary:dataDictionary];
			return settingObj;
		}
	return nil;
}


/**
 @brief:Will enumrate each object for collecting different backgound color values.
 */
+(Settings *)saveSettings:(Settings *)settngObj withDictionary:(NSDictionary*)dataDictionary {

	if (![dataDictionary valueForKeyIsNull:@"ApplicationBackground"]) {
		settngObj.applicationBackground = [dataDictionary valueForKey:@"ApplicationBackground"];
	}
	if (![dataDictionary valueForKeyIsNull:@"CatalogueButtonBackground"]) {
		settngObj.catalogueButtonBackground = [dataDictionary valueForKey:@"CatalogueButtonBackground"];
	}
	if (![dataDictionary valueForKeyIsNull:@"GeneralButtonsBackground"]) {
		settngObj.generalButtonsBackground = [dataDictionary valueForKey:@"GeneralButtonsBackground"];
	}
	if (![dataDictionary valueForKeyIsNull:@"HeaderViewBackground"]) {
		settngObj.headerViewBackground = [dataDictionary valueForKey:@"HeaderViewBackground"];
	}
	if (![dataDictionary valueForKeyIsNull:@"HomeScreenButtons"]) {
		settngObj.homeScreenButtons = [dataDictionary valueForKey:@"HomeScreenButtons"];
	}
	if (![dataDictionary valueForKeyIsNull:@"LogoImageBinary"]) {
		settngObj.logoImageBinary = [dataDictionary valueForKey:@"LogoImageBinary"];
	}
	if (![dataDictionary valueForKeyIsNull:@"NavigationBarBackground"]) {
		settngObj.navigationBarBackground = [dataDictionary valueForKey:@"NavigationBarBackground"];
	}
	if (![dataDictionary valueForKeyIsNull:@"PickerBackground"]) {
		settngObj.pickerBackground = [dataDictionary valueForKey:@"PickerBackground"];
	}
	if (![dataDictionary valueForKeyIsNull:@"SearchBarBackground"]) {
		settngObj.searchBarBackground = [dataDictionary valueForKey:@"SearchBarBackground"];
	}
	if (![dataDictionary valueForKeyIsNull:@"TableCellHeaderBackground"]) {
		settngObj.tableCellHeaderBackground = [dataDictionary valueForKey:@"TableCellHeaderBackground"];
	}
	if (![dataDictionary valueForKeyIsNull:@"myaccount_button"]) {
		settngObj.myaccount_button = [dataDictionary valueForKey:@"myaccount_button"];
	}
	if (![dataDictionary valueForKeyIsNull:@"orders_button"]) {
		settngObj.orders_button = [dataDictionary valueForKey:@"orders_button"];
	}
	if (![dataDictionary valueForKeyIsNull:@"products_button"]) {
		settngObj.products_button = [dataDictionary valueForKey:@"products_button"];
	}
	if (![dataDictionary valueForKeyIsNull:@"promotions_button"]) {
		settngObj.promotions_button = [dataDictionary valueForKey:@"promotions_button"];
	}
	if (![dataDictionary valueForKeyIsNull:@"template_button"]) {
		settngObj.template_button = [dataDictionary valueForKey:@"template_button"];
	}
	if (![dataDictionary valueForKeyIsNull:@"SegmantbarBackground"]) {
		settngObj.segmantbarBackground = [dataDictionary valueForKey:@"SegmantbarBackground"];
	}
	if (![dataDictionary valueForKeyIsNull:@"DatePickerBackground"]) {
		settngObj.datePickerBackground = [dataDictionary valueForKey:@"DatePickerBackground"];
	}
	[[APP_DELEGATE managedObjectContext] save:nil];
	return settngObj;
}
/**
 @brief: Start the opration for fetching theme parameteres from DB.
 */
+(Settings *)getTheameSettings{
	
	NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Settings" inManagedObjectContext:[APP_DELEGATE managedObjectContext]];
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setEntity:entityDesc];
	
	NSArray *arraySettings = [[APP_DELEGATE managedObjectContext] executeFetchRequest:request error:nil];
	
	if ([arraySettings count] > 0) {
		
		Settings *settngObj = [arraySettings lastObject];
		return settngObj;
	}
	return nil;
}

@end

//
//  APIManager.m
//  Hume
//
//  Created by User on 17/02/16.
//  Copyright © 2016 Damco. All rights reserved.
//

#import "APIManager.h"

/**
 
 @description APIManager is a singleton class. Anyone can use this iOS class to perform network related operations. This class exhibits methods to download/upload data from/to the server, to download/upload images from/to the server.
 
 */

static APIManager *apiManager = nil;
static NSURLSession *session = nil;
@interface APIManager() {
    NSURL *baseUrl;
}
+(NSURLSession *)uRLSessionWithConfiguration:(NSURLSessionConfiguration *) sessionConfiguration;
@end

@implementation APIManager

/**
 @brief This function returns the singleton object of APIManager class.
 */
+(APIManager*)sharedAPIManager{

    // Singleton Pattern
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        apiManager = [[APIManager alloc] init];
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        session = [APIManager uRLSessionWithConfiguration:sessionConfiguration];
    });
    
    return apiManager;
}

/**
 @brief This function creates & returns NSURLSession object using defaultSessionConfiguration.
 @param sessionConfiguration, object of NSURLSessionConfiguration.
 @return NSURLSession, returns an object of NSURLSession.
 */
+(NSURLSession *)uRLSessionWithConfiguration:(NSURLSessionConfiguration *) sessionConfiguration{
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    return  urlSession;
}

/**
 @brief This function make a data request to server using the provided url.
 @param url, url of resource.
 @param block, a completion block to be executed upon successful request completion
 */

-(void)dataRequestWithUrlString:(NSString*)urlString requestType:(RequestType) type withParameters:(NSDictionary*) parameters onCompletion:(void (^) (BOOL success, NSData * response, NSString* errorMessage))block{
    
    NSString * accessToken = [[NSUserDefaults standardUserDefaults] valueForKey:ACCESSTOKEN];
    
    if (accessToken.length > 0){
        session.configuration.HTTPAdditionalHeaders =  @{@"Access_Token" : accessToken};
    }
    
    baseUrl = [NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"defaultUrl"]];
    
    NSAssert(urlString.length > 0, @"url should not be nil");
    NSAssert(parameters.count > 0 , @"Paramerts cannot be nil");
    
    NSMutableURLRequest *mutableRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString relativeToURL:baseUrl]];
    mutableRequest.HTTPMethod = type == kRequestTypePOST? @"POST" : @"GET";
    mutableRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:nil];
    
    NSAssert(mutableRequest != nil, @"request should not be nil");
    
   NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:mutableRequest completionHandler:^(NSData *data, NSURLResponse * urlResponse, NSError * error){
       
       if (data != nil){
           if (block){
               block(YES,data,nil);
               NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
               NSLog(@"response %@", response);
           }else{
               block(NO, nil, [error localizedDescription]);
           }
       }
    }];
    
    [dataTask resume];
}

@end

//
//  APIResponse.m
//  Hume
//
//  Created by user on 13/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "APIResponse.h"

@implementation APIResponse

/**
 @brief: Creating api response objectect with default values..
 */
+(APIResponse *)modelObjecttWithDictionary:(id)dic
{
   // NSString *str = [[NSString alloc]initWithData:dic encoding:NSUTF8StringEncoding];
    //NSDictionary *jsonResponseDic = [NSJSONSerialization  JSONObjectWithData:(NSData *)dic options:NSJSONReadingAllowFragments error:nil];
    APIResponse *instance = [[APIResponse alloc]initWithDictionary:dic];
    return instance;
    
}

/**
 @brief: Setting apiresponse all keys with response dictionry.
 */
-(id)initWithDictionary:(NSDictionary *)dic
{
    self = [super init];
    NSLog(@"Response Dic = %@",dic);
    
    if ( dic  != nil || dic!=[NSNull null] || dic!= NULL || dic!=[NSNull class])
    {
        if (self && [dic isKindOfClass:[NSDictionary class]])
        {
            if ([dic valueForKey:@"data"])
            {
                //NSDictionary *tempDic = [dic valueForKey:@"data"];
                self.isSuccess = [[dic valueForKey:@"statusCode"] integerValue];
                self.message = [dic valueForKey:@"message"];
                if (self.isSuccess == 1)
                {
                   self.data = [dic valueForKey:@"data"];
                }else{
                    
                    //For error
                    self.errorMessage = (NSString *)[dic valueForKey:@"data"];
                    
                }
            }
        }
        
        
        
    }
    
    return self;
}

@end

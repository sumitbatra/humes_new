//
//  APIManager.h
//  Hume
//
//  Created by User on 17/02/16.
//  Copyright © 2016 Damco. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum  RequestType{
    kRequestTypePOST,
    kRequestTypeGET
}RequestType;

@interface APIManager : NSObject
+(APIManager*) sharedAPIManager;
-(void)dataRequestWithUrlString:(NSString*)urlString requestType:(RequestType) type withParameters:(NSDictionary*) parameters onCompletion:(void (^) (BOOL success, NSData * response, NSString* errorMessage))block;
@end

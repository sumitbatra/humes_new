//
//  APIResponse.h
//  Hume
//
//  Created by user on 13/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @brief: APIResponse is generic server response class with properties
 
 @property:
     - NSInteger isSuccess;
     
     - id data;
     
     - NSString *errorMessage;
     
     - NSString *message;
 
 @discussion: Can parse server response in APIResponse class.

 */

@interface APIResponse : NSObject
{
    
    
}

@property(nonatomic,assign) NSInteger isSuccess;
@property(nonatomic,retain) id data;
@property(nonatomic,assign) NSString *errorMessage;
@property(nonatomic,assign) NSString *message;

+(APIResponse *)modelObjecttWithDictionary:(NSDictionary *)dic;
-(id)initWithDictionary:(NSDictionary *)dic;





@end

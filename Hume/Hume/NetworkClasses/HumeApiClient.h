//
//  HumeApiClient.h
//  Hume
//
//  Created by user on 06/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPSessionManager.h>
/**
 @brief: HumeApiClient is a Networking subclass inheritated from AFHTTPSessionManager for setting up default url
      - creating shared instance.
 @function: 
      - SharedClient
      - AddAccessTokenInHeader
 
 */
@interface HumeApiClient : AFHTTPSessionManager

+(HumeApiClient *)SharedClient;
-(void)AddAccessTokenInHeader;
@end

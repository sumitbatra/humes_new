//
//  HumeApiClient.m
//  Hume
//
//  Created by user on 06/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "HumeApiClient.h"

@implementation HumeApiClient

/**
 @brief: Creating sharedinstnce with default server url.
 
 @return: HumeApiClient shared instance.
 */
+(HumeApiClient *)SharedClient
{
    static HumeApiClient *sharedClient = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
       // sharedClient = [[self alloc]initWithBaseURL:[NSURL URLWithString:kHumeBaseURLString]];
        //As per CR Updated by
        sharedClient = [[self alloc]initWithBaseURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"defaultUrl"]]];

    });
    return sharedClient;
}


/**
 @brief: 
 - Create & initialize a default serail intializer
 - Initializing a default url
 
 @return: instancetype
 */
-(instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if (!self)
    {
        return nil;
    }
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    self.requestSerializer =  [AFJSONRequestSerializer serializer];
    //[self.requestSerializer setValue:@"03BC/MDfvRdwQ1WdIaTDAQ==" forHTTPHeaderField:@"Access_Token"];
    return self;
}

/**
 @brief: Adding access token to header.
 */
-(void)AddAccessTokenInHeader{
    
    //[[NSUserDefaults standardUserDefaults] valueForKey:ACCESSTOKEN];
    
    [self.requestSerializer setValue: [[NSUserDefaults standardUserDefaults] valueForKey:ACCESSTOKEN] forHTTPHeaderField:@"Access_Token"];
}
@end


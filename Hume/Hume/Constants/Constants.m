//
//  Constants.m
//  Hume
//
//  Created by User on 12/30/14.
//  Copyright (c) 2014 Damco. All rights reserved.
//

#import "Constants.h"

@implementation Constants



//****** Networking URLs  starts****************
//NSString * const kHumeBaseURLString = @"http://172.29.100.129:8090/api";

//NSString * const kHumeBaseURLString = @"http://172.29.100.107/Prompt/api";   // Use this for local


//NSString * const kHumeBaseURLString = @"http://210.215.73.233:8082/prompt/api";   //live(ex-staging) client server
NSString * const kHumeBaseURLString = @"http://210.215.73.233:8083/promptstaging/api";   //07 - Oct (ex-staging for testing)

//NSString * const kHumeBaseURLString = @"http://210.215.160.210:8083/promptstaging";   //client staging
//NSString * const kHumeBaseURLString = @"http://210.215.73.204:8108/promptstaging";   //client staging
//NSString * const kHumeBaseURLString = @"http://210.215.160.210:8083/promptstaging";   //client server(new staging - TEST)
	//NSString * const kHumeBaseURLString = @"http://210.215.160.210:8082/prompt/api";   //client server(LIVE - DO NOT TEST)


//NEW IP Address 16 feb 
//NSString * const kHumeBaseURLString = @"http://172.29.18.91:8090/api"; // outdated

//NSString * const kHumeBaseURLString = @"http://demo52.damcogroup.com:8889/api";  // outdated
NSString * const kHumeBaseURLStringStage = @"http://demo11.damcogroup.com/Prompt/api";   // Use this for client review




NSString * const kHumeAPIKey = @"";
NSString * const kLoginUser = @"user/loginuser";
NSString * const kChangePassword = @"user/ChangeUserPassword";
NSString * const kLogOutUser = @"user/Logoutuser";
NSString * const kCustomerDetailSMyAccount = @"Customer";
NSString * const kUpdateCustomer = @"Customer/UpdateCustomer";
NSString * const kUpdateContact =  @"Customer/UpdateContact";
NSString * const kUpdateToBillAddress = @"Customer/UpdateBillToAddress";

NSString *const kRegister = @"User/InsertUserRegister";

//172.29.100.107/Prompt/api/product/ItemCrossRefDetail?itemNo=CW1000&custNO=14136&itemUOM=CTN

#pragma mark Products
NSString * const kProductListingAll = @"product/ProductListingALL";
NSString * const kProductListing = @"product/ProductListing?pageno=%@&record=%@";
NSString * const kProductDetail = @"product/Productdetail?item=%@";
NSString * const kProductPrice = @"product/ProductPrice";
NSString *const kSearchProduct = @"product/SearchProduct?search=%@";
NSString *const kGetBasePrice = @"product/GetItemUnitOfMeasurePrice?Item=%@&UOM=%@";
NSString *const kGetItemUOMs = @"product/GetItemUOM?item=%@&customerNo=%@";

#pragma mark Order 
NSString * const kOrders = @"salesorder";
NSString * const kSearchOrderNo = @"salesorder/ListSalesOrderByOrderNo?orderno=";
NSString * const kOrderSalesDeatils = @"salesorder/ListSalesLines?orderno=";
NSString * const kOrderDeleteProduct = @"salesorder/DeleteSaleOrderLines";
NSString * const kOrderUpdateProduct = @"salesorder/UpdateSaleOrderLines";
NSString * const kOrderUpdateSaleOrderStatus = @"salesorder/UpdateSaleOrderStatus";
NSString * const kOrderAddNewProduct = @"salesorder/InsertSaleOrderLines";

#pragma mark Product
NSString * const kOrderDeleteSaleOrder = @"salesorder/DeleteSaleOrder";
NSString * const kOrderUpdateSaleOrder = @"salesorder/UpdateSaleOrder";
NSString * const kOrderAddNewSaleOrder = @"salesorder/InsertSaleOrder";

#pragma mark Shipping Address
NSString * const kShippingAddress = @"ShiptoAddress";
NSString * const kUpdateShipToAddress = @"ShiptoAddress/UpdateShiptoAddress";
NSString * const kAddShipToAddress = @"ShiptoAddress/AddShiptoAddress";
NSString * const kPrimaryShpiptoAddress = @"ShiptoAddress/PrimaryShiptoAddress";


#pragma mark Promotion
NSString * const kDiscountsList = @"promo/GetPromoDiscountProductList";
NSString * const kUpSellsList = @"promo/GetPromoUpSellProductList";
NSString * const kMultiBuyList = @"promo/GetPromoMultiBuyProductList";
NSString * const kRewardsList = @"promo/GetPromoRewardsProductList";

NSString * const kGetAllPromotions = @"promo/GetPromoProductListALL";

#pragma mark Pantry
NSString * const kPantryList = @"pantry";
NSString * const kPantryItemSearch = @"pantry/pantryItemsBySearch?search=";
NSString * const kPantryInsertItemIntoPantry = @"pantry/InsertPantryLines";
NSString * const kPantryItemRemove = @"pantry/RemoveItemFromPantryLine";
NSString * const kPantryItemUpdate = @"Pantry/UpdatePantryLines";
NSString * const kInsertSaleOrderLinesfromPantry = @"salesorder/InsertSaleOrderLinesfromPantry";

NSString * const kGetPromoDiscountOnProduct = @"promo/GetPromoDiscount?itemno=";



NSString *const ACCESSTOKEN = @"ACCESSTOKEN";
NSString *const kForgotPassWord = @"user/forgotpassword";



//****** Networking URLs  ENDS****************



//****** Alert Messages ************

NSString * const kStandardErrorMessage = @"An error occurred while connecting to the server.";
NSString * const kServerError = @"Server Error";

//***************************************
NSString * const FONT_BOLD = @"MyriadPro-BoldCond";

NSString * const FONT_COND = @"MyriadPro-Cond";

NSString * const FONT_REGULAR = @"MyriadPro-Regular";

NSString * const MY_ACCOUNT_TITLE = @"MY ACCOUNT";
NSString * const ORDERS_TITLE = @"ORDERS";
NSString * const SHIPPING_ADD_TITLE = @"SHIPPING ADDRESS";
NSString * const EDIT_ADD_TITLE = @"EDIT ADDRESS";
NSString * const NEW_SHIPPING_ADD_TITLE = @"NEW SHIP ADDRESS";
NSString * const BILLING_ADD_TITLE = @"BILLING ADDRESS";
NSString * const EDIT_CUSTOMER_TITLE = @"   EDIT CUSTOMER INFO";
NSString * const CUSTOMERNOKEY = @"CUSTOMERNO";
//NSString * const CUSTOMERIMAGEKEY= @"CUSTOMERIMAGEKEY";
NSString * const CUSTOMEREDITABLE = @"CUSTOMEREDITABLE";
NSString * const PRODUCT_DETAIL = @"PRODUCT DETAIL";
NSString * const PANTRYLIST = @"PANTRY LIST";

NSString * const SUCCESSMESSAGE = @"Address has been updated successfully";
NSString * const NEWADDRESSSUCCESSMSG = @"New shipping address has been added";
NSString * const UPDATEPROFILE = @"Profile has been updated successfully";

NSString * const EDITORDERS = @"ORDER DETAILS";

int const MY_ACCOUNT_TITLE_FONT_SIZE = 18;

//***************************DefaultColours*********************

NSString * const kapplicationBackground = @"#002365";
NSString * const kcatalogueButtonBackground = @"#FC6A31";
NSString * const kgeneralButtonsBackground = @"#002365";
NSString * const kheaderViewBackground = @"#1F4E79";
NSString * const khomeScreenButtons = @"#45AC38";
NSString * const klogoImageBinary = @"";
NSString * const knavigationBarBackground = @"#002365";
NSString * const kpickerBackground = @"#5B9BD5";
NSString * const ksearchBarBackground = @"#1F4E79";
NSString * const ktableCellHeaderBackground = @"#5B9BD5";
NSString * const kmyaccount_button = @"#EA361A";
NSString * const korders_button = @"#FECC02";
NSString * const kproducts_button = @"#FB4268";
NSString * const kpromotions_button = @"#F87A31";
NSString * const ktemplate_button = @"#80D60D";
NSString * const ksegmantbarBackground = @"#1F4E79";
NSString * const kdatePickerBackground = @"#5B9BD5";


//Key value constants.
 NSString * const keyStatusCode = @"statusCode";
 NSString * const keyUsername = @"Username";
 NSString * const keyPassword = @"Password";
 NSString * const keyTitle = @"Title";
 NSString * const keyFirstName = @"FirstName";
 NSString * const keyLastName = @"LastName";
 NSString * const keyCompanyName = @"CompanyName";
 NSString * const keyAddressLine1 = @"AddressLine1";
 NSString * const keyAddressLine2 = @"AddressLine2";
 NSString * const keyCity = @"City";
 NSString * const keyState = @"State";
 NSString * const keyPostCode = @"PostCode";
 NSString * const keyEmail = @"Email";
 NSString * const keyPhoneNumber = @"PhoneNumber";
 NSString * const keyFaxNumber = @"FaxNumber";
 NSString * const keyCustomerNo = @"CustomerNo";
 NSString * const keyContactName = @"ContactName";
 NSString * const keySynced = @"Synced";
 NSString * const keyCode = @"Code";
 NSString * const keyToSync = @"ToSync";

@end

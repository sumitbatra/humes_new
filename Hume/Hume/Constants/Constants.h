//
//  Constants.h
//  Hume
//
//  Created by User on 12/30/14.
//  Copyright (c) 2014 Damco. All rights reserved.
//

#import <Foundation/Foundation.h>


//************* Netwoking- API URLs Start *************************************
extern NSString *const kHumeAPIKey;
extern NSString *const kHumeBaseURLString;
extern NSString *const kHumeBaseURLStringStage;
extern NSString *const kLoginUser;
extern NSString *const kLogOutUser;
extern NSString *const kCustomerDetailSMyAccount;
extern NSString *const kOrders;

extern NSString *const kRegister;

extern NSString *const kUpdateToBillAddress;

extern NSString *const kUpdateShipToAddress;
extern NSString *const kAddShipToAddress;
extern NSString *const kPrimaryShpiptoAddress;
extern NSString *const kShippingAddress;
extern NSString *const kSearchOrderNo;

extern NSString *const CUSTOMERNOKEY;
extern NSString *const ACCESSTOKEN;
extern NSString * const kUpdateCustomer;
extern NSString * const kUpdateContact;

extern NSString * const kProductListing ;
extern NSString * const kProductPrice;
extern NSString * const kSearchProduct ;
extern NSString * const kProductDetail;

extern NSString * const kOrderSalesDeatils;
extern NSString * const kOrderDeleteProduct;
extern NSString * const kOrderUpdateProduct;
extern NSString * const kOrderAddNewProduct;

extern NSString * const kOrderDeleteSaleOrder;
extern NSString * const kOrderUpdateSaleOrder;
extern NSString * const kOrderAddNewSaleOrder;
extern NSString * const kOrderUpdateSaleOrderStatus;

extern NSString * const kDiscountsList;
extern NSString * const kUpSellsList;
extern NSString * const kMultiBuyList;
extern NSString * const kRewardsList;

extern NSString * const kGetAllPromotions;

extern NSString * const kapplicationBackground;
extern NSString * const kcatalogueButtonBackground;
extern NSString * const kgeneralButtonsBackground;
extern NSString * const kheaderViewBackground;
extern NSString * const khomeScreenButtons;
extern NSString * const klogoImageBinary;
extern NSString * const knavigationBarBackground;
extern NSString * const kpickerBackground;
extern NSString * const ksearchBarBackground;
extern NSString * const ktableCellHeaderBackground;
extern NSString * const kmyaccount_button;
extern NSString * const korders_button;
extern NSString * const kproducts_button;
extern NSString * const kpromotions_button;
extern NSString * const ktemplate_button;


//************* Netwoking- API URLs End *************************************


extern NSString * const kStandardErrorMessage;


#define TITLE_LABEL_TAG 101


#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_HEIGHT < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_HEIGHT == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_HEIGHT == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_HEIGHT == 736.0)

//******** Alert buttons *********

#define ALERT_BTN_CHANGE @"Change"
#define ALERT_BTN_OK @"Ok"
#define ALERT_BTN_PROCEED @"Proceed"
#define ALERT_BTN_CANCEL @"Cancel"
#define ALERT_BTN_YES @"Yes"

//********************************** MyAccountTableViewController Constants Start ************//

#define TITLE_LABEL_TAG 101
#define CELL_TEXT_LABEL_TAG 2001
#define CELL_BACKGROUND_COLOR [UIColor colorWithRed:75.0/255.0 green:135.0/255.0 blue:202.0/255.0 alpha:1.0]

#define SKYBLUECOLOR 3c72be

#define kExpandedCellHeight 150;
#define kNormalCellHeigh 44;
#define kCollapsedFrame CGRectMake(0, 146, 320, 4)
#define kExpandedFrame CGRectMake(0, 146, 320, 4)
#define kLabelFrame CGRectMake(10, 5, 260, 34)


#define kItemCrossRefDetail @"product/ItemCrossRefDetail?itemNo=%@&custNO=%@&itemUOM=%@"


//Error Messages
#define NO_RESULT_FOUND @"No results found"
#define SEARCH_TEXT_REQUIRED @"Please enter text for search"
#define ITEMS_REQUIRED_FOR_TEMPLETE @"Please add items to template"
#define ORDER_CREATED @"Order has been created from template"
#define NETWORK_ERROR @"Network Failure!"
#define NO_OPEN_ORDER @"There is no open order, please open a new one!"
#define ITEMS_REQUIRED @"Please add items"
#define R_U_SURE @"Are you sure?"
#define NO_PROFILE @"Profile not available"
#define CUSTOMER_INFO_DISBL @"Customer Info is disabled"
#define CUSTOMER_INFO_EDIT_DISBL @"Customer Info Editing is disabled"
#define ADDRESS_EDIT_DISBL @"Address Editing is disabled"
#define USER_EMAIL_REQUIRED @"Please enter a  username / email ."
#define TRY_LATER @"Please try again"
#define DELIVERY_DATE_REQUIRED @"Please select delivery date."
#define NO_ITEMS_IN_ORDERS @"No items Present in order, Please add item on click of catalougue"
#define DELETE_ORDER @"Are you sure you want to delete Order?"
#define ALL_FIELDS_ARE_REQUIRED @"Please enter required fields."
#define PASS_CHANGED @"Password changed successfully"
#define OLD_AND_NEW_PASS_DIFF @"New password and confirm password do not match."
#define FILL_ALL @"Please fill the values"
#define PASS_VALIDATE @"Password should be more than three characters"
#define ACCT_CREATED @"Thank you for registering an account. Access will be granted once your account has been verified by an Administrator"
#define FORM_BLANK @"Please complete the form"
#define URL_UPDATED @"Url updated successfully."
#define URL_BLANK @"Please enter the url."
#define TITLE_CHANGE_URL @"Change url"
#define PRICE_NOT_AVL @"Price not available."
#define CANT_ADD_ITEM @"Cannot add item!"
#define UOM_REQUIRED @"Please select Unit of measure"


//********************************** MyAccountTableViewController Constants End *************//

@interface Constants : NSObject

extern NSString * const FONT_BOLD;

extern NSString * const FONT_COND;

extern NSString * const FONT_REGULAR;

extern NSString * const MY_ACCOUNT_TITLE;

extern int const MY_ACCOUNT_TITLE_FONT_SIZE;

extern NSString * const MY_ACCOUNT_TABLE_VIEW_CELL;

extern NSString * const ORDERS_TITLE;
extern NSString * const SHIPPING_ADD_TITLE;
extern NSString * const EDIT_ADD_TITLE;
extern NSString * const NEW_SHIPPING_ADD_TITLE;
extern NSString * const BILLING_ADD_TITLE;
extern NSString * const PANTRYLIST;
extern NSString * const SUCCESSMESSAGE;
extern NSString * const NEWADDRESSSUCCESSMSG;
extern NSString * const UPDATEPROFILE;
extern NSString * const EDIT_CUSTOMER_TITLE;
extern NSString * const kForgotPassWord;
extern NSString * const kSearchOrderNo;
extern NSString * const EDITORDERS;
extern NSString * const kServerError;
extern NSString * const PRODUCT_DETAIL;
extern NSString * const kProductListingAll;
extern NSString * const kGetBasePrice;
extern NSString * const kGetItemUOMs;
extern NSString * const kkItemCrossRefDetail;

extern NSString * const CUSTOMEREDITABLE;
extern NSString * const kPantryList;
extern NSString * const kPantryItemSearch ;
extern NSString * const kPantryInsertItemIntoPantry;
extern NSString * const kPantryItemRemove ;
extern NSString * const kPantryItemUpdate;
extern NSString * const kInsertSaleOrderLinesfromPantry;
extern NSString * const ksegmantbarBackground;
extern NSString * const kdatePickerBackground;

//Key value constants.
extern NSString * const keyStatusCode;
extern NSString * const keyUsername;
extern NSString * const keyPassword;
extern NSString * const keyTitle;
extern NSString * const keyFirstName;
extern NSString * const keyLastName;
extern NSString * const keyCompanyName;
extern NSString * const keyAddressLine1;
extern NSString * const keyAddressLine2;
extern NSString * const keyCity;
extern NSString * const keyState;
extern NSString * const keyPostCode;
extern NSString * const keyEmail;
extern NSString * const keyPhoneNumber;
extern NSString * const keyFaxNumber;
extern NSString * const keyCustomerNo;
extern NSString * const keyContactName;
extern NSString * const keySynced;
extern NSString * const keyCode;
extern NSString * const keyToSync;


@end

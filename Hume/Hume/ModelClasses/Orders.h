//
//  Data.h
//
//  Created by user  on 15/01/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class: Orders Model object class
 @brief: A Orders data class with property
 
 - NSString *invoiceDiscountPercentage;
 
 - id shipToCity;
 
 - NSString *orderNo;
 
 - NSString *customer;
 
 - NSString *signatureImage;
 
 - id shipToState;
 
 - BOOL synced;
 
 - id comments;
 
 - id returnsToSalesReferenceNo;
 
 - NSString *type;
 
 - id shipToAddressLine2;
 
 - NSString *totalExclGST;
 
 - NSString *orderDate;
 
 - id salesperson;
 
 - NSString * shipmentDate;
 
 - NSString *orderTime;
 
 - id shipToAddressLine1;
 
 - NSString *syncDateTime;
 
 - id shipToPostcode;
 
 - NSString *invoiceDiscountAmount;
 
 - id shipToAddressCode;
 
 - NSString *shippingAdvice;
 
 - id shipToContactName;
 
 - id shipToPhoneNo;
 
 - NSString *status;
 
 - NSString *gSTAmount;
 
 - NSString *amount;
 
 - NSString *amountExclGST;
 
 - BOOL toSync;
 
 - NSString *name;
 
 @discussion: Can perform given below operation using function

 @method:
 
 modelObjectWithDictionary
 - Return a model object with all available property
 
 initWithDictionary
 - Setting property from dictionary
 
 dictionaryRepresentation
 - Distionary reprensentaion from model object.
 */

@interface Orders : NSObject <NSCoding>

//Properties
@property (nonatomic, strong) NSString *invoiceDiscountPercentage;
@property (nonatomic, assign) id shipToCity;
@property (nonatomic, strong) NSString *orderNo;
@property (nonatomic, strong) NSString *customer;
@property (nonatomic, strong) NSString *signatureImage;
@property (nonatomic, assign) id shipToState;
@property (nonatomic, assign) BOOL synced;
@property (nonatomic, assign) id comments;
@property (nonatomic, assign) id returnsToSalesReferenceNo;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, assign) id shipToAddressLine2;
@property (nonatomic, strong) NSString *totalExclGST;
@property (nonatomic, strong) NSString *orderDate;
@property (nonatomic, assign) id salesperson;
@property (nonatomic, assign) NSString * shipmentDate;
@property (nonatomic, strong) NSString *orderTime;
@property (nonatomic, assign) id shipToAddressLine1;
@property (nonatomic, strong) NSString *syncDateTime;
@property (nonatomic, assign) id shipToPostcode;
@property (nonatomic, strong) NSString *invoiceDiscountAmount;
@property (nonatomic, assign) id shipToAddressCode;
@property (nonatomic, strong) NSString *shippingAdvice;
@property (nonatomic, assign) id shipToContactName;
@property (nonatomic, assign) id shipToPhoneNo;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *gSTAmount;
@property (nonatomic, strong) NSString *amount;
@property (nonatomic, strong) NSString *amountExclGST;
@property (nonatomic, assign) BOOL toSync;
@property (nonatomic, strong) NSString *name;

//Methods
+ (Orders *)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

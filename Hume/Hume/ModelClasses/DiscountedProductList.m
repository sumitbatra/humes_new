//
//  DiscountedProductList.m
//
//  Created by user  on 04/02/15
//  Copyright (c) 2015 __Damco Solution__. All rights reserved.
//

#import "DiscountedProductList.h"


NSString *const kDiscountedProductListDiscountPercentage = @"DiscountPercentage";
NSString *const kDiscountedProductListMinBuyQty = @"MinBuyQty";
NSString *const kDiscountedProductListMinPurchaseAmount = @"MinPurchaseAmount";
NSString *const kDiscountedProductListItemDescription = @"ItemDescription";
NSString *const kDiscountedProductListRewardPoints = @"RewardPoints";
NSString *const kDiscountedProductListPicture = @"Picture";
NSString *const kDiscountedProductListItemCode = @"ItemCode";
NSString *const kDiscountedProductListOfferQty = @"OfferQty";
NSString *const kDiscountedProductListUpsellDescription = @"UpsellDescription";
NSString *const kDiscountedProductListPerQuantity = @"PerQuantity";
NSString *const kDiscountedProductListOfferItemDescription = @"OfferItemDescription";
NSString *const kDiscountedProductListOfferItemCode = @"OfferItemCode";


@interface DiscountedProductList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation DiscountedProductList

@synthesize discountPercentage = _discountPercentage;
@synthesize minBuyQty = _minBuyQty;
@synthesize minPurchaseAmount = _minPurchaseAmount;
@synthesize itemDescription = _itemDescription;
@synthesize rewardPoints = _rewardPoints;
@synthesize picture = _picture;
@synthesize itemCode = _itemCode;
@synthesize offerQty = _offerQty;
@synthesize upsellDescription = _upsellDescription;
@synthesize perQuantity = _perQuantity;
@synthesize offerItemDescription = _offerItemDescription;
@synthesize offerItemCode = _offerItemCode;

///Creating and returning model object from dictionary.
+ (DiscountedProductList *)modelObjectWithDictionary:(NSDictionary *)dict
{
    DiscountedProductList *instance = [[DiscountedProductList alloc] initWithDictionary:dict];
    return instance;
}


///Setting model class property from dictionary

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.discountPercentage = [self objectOrNilForKey:kDiscountedProductListDiscountPercentage fromDictionary:dict];
            self.minBuyQty = [self objectOrNilForKey:kDiscountedProductListMinBuyQty fromDictionary:dict];
            self.minPurchaseAmount = [self objectOrNilForKey:kDiscountedProductListMinPurchaseAmount fromDictionary:dict];
            self.itemDescription = [self objectOrNilForKey:kDiscountedProductListItemDescription fromDictionary:dict];
            self.rewardPoints = [self objectOrNilForKey:kDiscountedProductListRewardPoints fromDictionary:dict];
            self.picture = [self objectOrNilForKey:kDiscountedProductListPicture fromDictionary:dict];
            self.itemCode = [self objectOrNilForKey:kDiscountedProductListItemCode fromDictionary:dict];
            self.offerQty = [self objectOrNilForKey:kDiscountedProductListOfferQty fromDictionary:dict];
            self.upsellDescription = [self objectOrNilForKey:kDiscountedProductListUpsellDescription fromDictionary:dict];
            self.perQuantity = [self objectOrNilForKey:kDiscountedProductListPerQuantity fromDictionary:dict];
            self.offerItemDescription = [self objectOrNilForKey:kDiscountedProductListOfferItemDescription fromDictionary:dict];
            self.offerItemCode = [self objectOrNilForKey:kDiscountedProductListOfferItemCode fromDictionary:dict];

    }
    
    return self;
    
}

///Create a dictionary from model class object property

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.discountPercentage forKey:kDiscountedProductListDiscountPercentage];
    [mutableDict setValue:self.minBuyQty forKey:kDiscountedProductListMinBuyQty];
    [mutableDict setValue:self.minPurchaseAmount forKey:kDiscountedProductListMinPurchaseAmount];
    [mutableDict setValue:self.itemDescription forKey:kDiscountedProductListItemDescription];
    [mutableDict setValue:self.rewardPoints forKey:kDiscountedProductListRewardPoints];
    [mutableDict setValue:self.picture forKey:kDiscountedProductListPicture];
    [mutableDict setValue:self.itemCode forKey:kDiscountedProductListItemCode];
    [mutableDict setValue:self.offerQty forKey:kDiscountedProductListOfferQty];
    [mutableDict setValue:self.upsellDescription forKey:kDiscountedProductListUpsellDescription];
    [mutableDict setValue:self.perQuantity forKey:kDiscountedProductListPerQuantity];
    [mutableDict setValue:self.offerItemDescription forKey:kDiscountedProductListOfferItemDescription];
    [mutableDict setValue:self.offerItemCode forKey:kDiscountedProductListOfferItemCode];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.discountPercentage = [aDecoder decodeObjectForKey:kDiscountedProductListDiscountPercentage];
    self.minBuyQty = [aDecoder decodeObjectForKey:kDiscountedProductListMinBuyQty];
    self.minPurchaseAmount = [aDecoder decodeObjectForKey:kDiscountedProductListMinPurchaseAmount];
    self.itemDescription = [aDecoder decodeObjectForKey:kDiscountedProductListItemDescription];
    self.rewardPoints = [aDecoder decodeObjectForKey:kDiscountedProductListRewardPoints];
    self.picture = [aDecoder decodeObjectForKey:kDiscountedProductListPicture];
    self.itemCode = [aDecoder decodeObjectForKey:kDiscountedProductListItemCode];
    self.offerQty = [aDecoder decodeObjectForKey:kDiscountedProductListOfferQty];
    self.upsellDescription = [aDecoder decodeObjectForKey:kDiscountedProductListUpsellDescription];
    self.perQuantity = [aDecoder decodeObjectForKey:kDiscountedProductListPerQuantity];
    self.offerItemDescription = [aDecoder decodeObjectForKey:kDiscountedProductListOfferItemDescription];
    self.offerItemCode = [aDecoder decodeObjectForKey:kDiscountedProductListOfferItemCode];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_discountPercentage forKey:kDiscountedProductListDiscountPercentage];
    [aCoder encodeObject:_minBuyQty forKey:kDiscountedProductListMinBuyQty];
    [aCoder encodeObject:_minPurchaseAmount forKey:kDiscountedProductListMinPurchaseAmount];
    [aCoder encodeObject:_itemDescription forKey:kDiscountedProductListItemDescription];
    [aCoder encodeObject:_rewardPoints forKey:kDiscountedProductListRewardPoints];
    [aCoder encodeObject:_picture forKey:kDiscountedProductListPicture];
    [aCoder encodeObject:_itemCode forKey:kDiscountedProductListItemCode];
    [aCoder encodeObject:_offerQty forKey:kDiscountedProductListOfferQty];
    [aCoder encodeObject:_upsellDescription forKey:kDiscountedProductListUpsellDescription];
    [aCoder encodeObject:_perQuantity forKey:kDiscountedProductListPerQuantity];
    [aCoder encodeObject:_offerItemDescription forKey:kDiscountedProductListOfferItemDescription];
    [aCoder encodeObject:_offerItemCode forKey:kDiscountedProductListOfferItemCode];
}


@end

//
//  DiscountedProductList.h
//
//  Created by user  on 04/02/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 @class: DiscountedProductList Model object class
 @brief: A DiscountedProductList data class with property
 - NSString *discountPercentage;
 
 - id minBuyQty;
 
 - NSString *minPurchaseAmount;
 
 - id itemDescription;
 
 - id rewardPoints;
 
 - id picture;
 
 - id itemCode;
 
 - id offerQty;
 
 - id upsellDescription;
 
 - id perQuantity;
 
 - id offerItemDescription;
 
 - id offerItemCode;
 
 @discussion: Can perform givenbelow operation using function
 
 @function:
 modelObjectWithDictionary
 - Return a model object with all available property
 
 initWithDictionary
 - Setting property from dictionary
 
 dictionaryRepresentation
 - Distionary reprensentaion from model object.
 */



@interface DiscountedProductList : NSObject <NSCoding>

//Properties
@property (nonatomic, strong) NSString *discountPercentage;
@property (nonatomic, assign) id minBuyQty;
@property (nonatomic, strong) NSString *minPurchaseAmount;
@property (nonatomic, assign) id itemDescription;
@property (nonatomic, assign) id rewardPoints;
@property (nonatomic, assign) id picture;
@property (nonatomic, assign) id itemCode;
@property (nonatomic, assign) id offerQty;
@property (nonatomic, assign) id upsellDescription;
@property (nonatomic, assign) id perQuantity;
@property (nonatomic, assign) id offerItemDescription;
@property (nonatomic, assign) id offerItemCode;

//Methods
+ (DiscountedProductList *)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

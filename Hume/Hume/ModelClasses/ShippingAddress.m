//
//  ShippingAddress.m
//
//  Created by user  on 14/01/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "ShippingAddress.h"
#import "Utility.h"

NSString *const kShippingAddressCustomerNo = @"CustomerNo";
NSString *const kShippingAddressAddressLine1 = @"AddressLine1";
NSString *const kShippingAddressPostcode = @"Postcode";
NSString *const kShippingAddressAddressLine2 = @"AddressLine2";
NSString *const kShippingAddressState = @"State";
NSString *const kShippingAddressSynced = @"Synced";
NSString *const kShippingAddressContactName = @"ContactName";
NSString *const kShippingAddressPrimary = @"Primary";
NSString *const kShippingAddressCode = @"Code";
NSString *const kShippingAddressPhoneNo = @"PhoneNo";
NSString *const kShippingAddressToSync = @"ToSync";
NSString *const kShippingAddressCity = @"City";


@interface ShippingAddress ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ShippingAddress

@synthesize customerNo = _customerNo;
@synthesize addressLine1 = _addressLine1;
@synthesize postcode = _postcode;
@synthesize addressLine2 = _addressLine2;
@synthesize state = _state;
@synthesize synced = _synced;
@synthesize contactName = _contactName;
@synthesize primary = _primary;
@synthesize code = _code;
@synthesize phoneNo = _phoneNo;
@synthesize toSync = _toSync;
@synthesize city = _city;

///Creating and returning model object from dictionary.
+ (ShippingAddress *)modelObjectWithDictionary:(NSDictionary *)dict
{
    
    ShippingAddress *instance = [[ShippingAddress alloc] initWithDictionary:[Utility parseNullValuesInDictionary:dict]];
    return instance;
}

///Setting model class property from dictionary

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.customerNo = [self objectOrNilForKey:kShippingAddressCustomerNo fromDictionary:dict];
            self.addressLine1 = [self objectOrNilForKey:kShippingAddressAddressLine1 fromDictionary:dict];
            self.postcode = [self objectOrNilForKey:kShippingAddressPostcode fromDictionary:dict];
            self.addressLine2 = [self objectOrNilForKey:kShippingAddressAddressLine2 fromDictionary:dict];
            self.state = [self objectOrNilForKey:kShippingAddressState fromDictionary:dict];
            self.synced = [self objectOrNilForKey:kShippingAddressSynced fromDictionary:dict];
            self.contactName = [self objectOrNilForKey:kShippingAddressContactName fromDictionary:dict];
            self.primary = [self objectOrNilForKey:kShippingAddressPrimary fromDictionary:dict];
            self.code = [self objectOrNilForKey:kShippingAddressCode fromDictionary:dict];
            self.phoneNo = [self objectOrNilForKey:kShippingAddressPhoneNo fromDictionary:dict];
            self.toSync = [self objectOrNilForKey:kShippingAddressToSync fromDictionary:dict];
            self.city = [self objectOrNilForKey:kShippingAddressCity fromDictionary:dict];

    }
    
    return self;
    
}

///Create a dictionary from model class object property

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.customerNo forKey:kShippingAddressCustomerNo];
    [mutableDict setValue:self.addressLine1 forKey:kShippingAddressAddressLine1];
    [mutableDict setValue:self.postcode forKey:kShippingAddressPostcode];
    [mutableDict setValue:self.addressLine2 forKey:kShippingAddressAddressLine2];
    [mutableDict setValue:self.state forKey:kShippingAddressState];
    [mutableDict setValue:self.synced forKey:kShippingAddressSynced];
    [mutableDict setValue:self.contactName forKey:kShippingAddressContactName];
    [mutableDict setValue:self.primary forKey:kShippingAddressPrimary];
    [mutableDict setValue:self.code forKey:kShippingAddressCode];
    [mutableDict setValue:self.phoneNo forKey:kShippingAddressPhoneNo];
    [mutableDict setValue:self.toSync forKey:kShippingAddressToSync];
    [mutableDict setValue:self.city forKey:kShippingAddressCity];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.customerNo = [aDecoder decodeObjectForKey:kShippingAddressCustomerNo];
    self.addressLine1 = [aDecoder decodeObjectForKey:kShippingAddressAddressLine1];
    self.postcode = [aDecoder decodeObjectForKey:kShippingAddressPostcode];
    self.addressLine2 = [aDecoder decodeObjectForKey:kShippingAddressAddressLine2];
    self.state = [aDecoder decodeObjectForKey:kShippingAddressState];
    self.synced = [aDecoder decodeObjectForKey:kShippingAddressSynced];
    self.contactName = [aDecoder decodeObjectForKey:kShippingAddressContactName];
    self.primary = [aDecoder decodeObjectForKey:kShippingAddressPrimary];
    self.code = [aDecoder decodeObjectForKey:kShippingAddressCode];
    self.phoneNo = [aDecoder decodeObjectForKey:kShippingAddressPhoneNo];
    self.toSync = [aDecoder decodeObjectForKey:kShippingAddressToSync];
    self.city = [aDecoder decodeObjectForKey:kShippingAddressCity];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_customerNo forKey:kShippingAddressCustomerNo];
    [aCoder encodeObject:_addressLine1 forKey:kShippingAddressAddressLine1];
    [aCoder encodeObject:_postcode forKey:kShippingAddressPostcode];
    [aCoder encodeObject:_addressLine2 forKey:kShippingAddressAddressLine2];
    [aCoder encodeObject:_state forKey:kShippingAddressState];
    [aCoder encodeObject:_synced forKey:kShippingAddressSynced];
    [aCoder encodeObject:_contactName forKey:kShippingAddressContactName];
    [aCoder encodeObject:_primary forKey:kShippingAddressPrimary];
    [aCoder encodeObject:_code forKey:kShippingAddressCode];
    [aCoder encodeObject:_phoneNo forKey:kShippingAddressPhoneNo];
    [aCoder encodeObject:_toSync forKey:kShippingAddressToSync];
    [aCoder encodeObject:_city forKey:kShippingAddressCity];
}


@end

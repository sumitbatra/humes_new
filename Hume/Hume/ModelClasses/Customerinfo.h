//
//  Customerinfo.h
//
//  Created by user  on 14/01/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 @class: Customerinfo Model object class
 @brief: A Customerinfo data class with property
 
 - NSString *address2;
 
 - NSString *telex;
 
 - NSString *postCode;
 
 - id synced;
 
 - NSString *city;
 
 - id recordUpdated;
 
 - NSString *priceGroup;
 
 - NSString *paymentTerms;
 
 - NSString *customerDiscountGroup;
 
 - NSString *name;
 
 - NSString *gSTBusinessPostingGroup;
 
 - NSString *fax;
 
 - NSString *state;
 
 - NSString *address;
 
 - id amountDueGT30days;
 
 - id amountDueGT60days;
 
 - id creditLimit;
 
 - id country;
 
 - NSString *email;
 
 - id syncDateTime;
 
 - NSString *noProperty;
 
 - NSString *phoneNo;
 
 - NSString *shippingAdvice;
 
 - NSString *salesPersonCode;
 
 - id rewards;
 
 - NSString *contact;
 
 - id balance;
 
 - id toSync;
 
 - id amountDueGT90days;
 
 - id amountDue;
 
 @discussion: Can perform given below operation using function

 @function:
 modelObjectWithDictionary
 - Return a model object with all available property
 
 initWithDictionary
 - Setting property from dictionary
 
 dictionaryRepresentation
 - Distionary reprensentaion from model object.
 */



@interface Customerinfo : NSObject <NSCoding>

//Properties
@property (nonatomic, strong) NSString *address2;
@property (nonatomic, strong) NSString *telex;
@property (nonatomic, strong) NSString *postCode;
@property (nonatomic, assign) id synced;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, assign) id recordUpdated;
@property (nonatomic, strong) NSString *priceGroup;
@property (nonatomic, strong) NSString *paymentTerms;
@property (nonatomic, strong) NSString *customerDiscountGroup;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *gSTBusinessPostingGroup;
@property (nonatomic, strong) NSString *fax;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, assign) id amountDueGT30days;
@property (nonatomic, assign) id amountDueGT60days;
@property (nonatomic, assign) id creditLimit;
@property (nonatomic, assign) id country;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, assign) id syncDateTime;
@property (nonatomic, strong) NSString *noProperty;
@property (nonatomic, strong) NSString *phoneNo;
@property (nonatomic, strong) NSString *shippingAdvice;
@property (nonatomic, strong) NSString *salesPersonCode;
@property (nonatomic, assign) id rewards;
@property (nonatomic, strong) NSString *contact;
@property (nonatomic, assign) id balance;
@property (nonatomic, assign) id toSync;
@property (nonatomic, assign) id amountDueGT90days;
@property (nonatomic, assign) id amountDue;

//Methods
+ (Customerinfo *)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

//
//  IUOMGetItemUOM.m
//
//  Created by User  on 6/26/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "IUOMGetItemUOM.h"
#import "IUOMData.h"


NSString *const kIUOMGetItemUOMStatusCode = @"statusCode";
NSString *const kIUOMGetItemUOMData = @"data";


@interface IUOMGetItemUOM ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation IUOMGetItemUOM

@synthesize statusCode = _statusCode;
@synthesize data = _data;

///Creating and returning model object from dictionary.
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

///Setting model class property from dictionary
- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.statusCode = [[self objectOrNilForKey:kIUOMGetItemUOMStatusCode fromDictionary:dict] doubleValue];
    NSObject *receivedIUOMData = [dict objectForKey:kIUOMGetItemUOMData];
    NSMutableArray *parsedIUOMData = [NSMutableArray array];
    if ([receivedIUOMData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedIUOMData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedIUOMData addObject:[IUOMData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedIUOMData isKindOfClass:[NSDictionary class]]) {
       [parsedIUOMData addObject:[IUOMData modelObjectWithDictionary:(NSDictionary *)receivedIUOMData]];
    }

    self.data = [NSArray arrayWithArray:parsedIUOMData];

    }
    
    return self;
    
}

///Create a dictionary from model class object property

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.statusCode] forKey:kIUOMGetItemUOMStatusCode];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kIUOMGetItemUOMData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.statusCode = [aDecoder decodeDoubleForKey:kIUOMGetItemUOMStatusCode];
    self.data = [aDecoder decodeObjectForKey:kIUOMGetItemUOMData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_statusCode forKey:kIUOMGetItemUOMStatusCode];
    [aCoder encodeObject:_data forKey:kIUOMGetItemUOMData];
}

- (id)copyWithZone:(NSZone *)zone
{
    IUOMGetItemUOM *copy = [[IUOMGetItemUOM alloc] init];
    
    if (copy) {

        copy.statusCode = self.statusCode;
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end

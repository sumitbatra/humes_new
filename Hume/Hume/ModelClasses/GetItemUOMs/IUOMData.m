//
//  IUOMData.m
//
//  Created by User  on 6/26/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "IUOMData.h"


NSString *const kIUOMDataItem = @"Item";
NSString *const kIUOMDataUOM = @"UOM";
NSString *const kIUOMDataQtyPerUOM = @"QtyPerUOM";


@interface IUOMData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation IUOMData

@synthesize item = _item;
@synthesize uOM = _uOM;
@synthesize qtyPerUOM = _qtyPerUOM;

///Creating and returning model object from dictionary.
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}


///Setting model class property from dictionary
- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.item = [self objectOrNilForKey:kIUOMDataItem fromDictionary:dict];
            self.uOM = [self objectOrNilForKey:kIUOMDataUOM fromDictionary:dict];
            self.qtyPerUOM = [self objectOrNilForKey:kIUOMDataQtyPerUOM fromDictionary:dict];

    }
    
    return self;
    
}

///Create a dictionary from model class object property
- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.item forKey:kIUOMDataItem];
    [mutableDict setValue:self.uOM forKey:kIUOMDataUOM];
    [mutableDict setValue:self.qtyPerUOM forKey:kIUOMDataQtyPerUOM];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.item = [aDecoder decodeObjectForKey:kIUOMDataItem];
    self.uOM = [aDecoder decodeObjectForKey:kIUOMDataUOM];
    self.qtyPerUOM = [aDecoder decodeObjectForKey:kIUOMDataQtyPerUOM];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_item forKey:kIUOMDataItem];
    [aCoder encodeObject:_uOM forKey:kIUOMDataUOM];
    [aCoder encodeObject:_qtyPerUOM forKey:kIUOMDataQtyPerUOM];
}

- (id)copyWithZone:(NSZone *)zone
{
    IUOMData *copy = [[IUOMData alloc] init];
    
    if (copy) {

        copy.item = [self.item copyWithZone:zone];
        copy.uOM = [self.uOM copyWithZone:zone];
        copy.qtyPerUOM = [self.qtyPerUOM copyWithZone:zone];
    }
    
    return copy;
}


@end

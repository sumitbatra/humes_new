//
//  IUOMData.h
//
//  Created by User  on 6/26/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class: IUOMData Model object class
 @brief: A IUOM data class with property 
 
 - NSString *item;
 
 - NSString *uOM;
 
 - NSString *qtyPerUOM;
 
 @discussion: Can perform givenbelow operation using function
 
 @function:
 
 modelObjectWithDictionary
 - Return a model object with all available property
 
 initWithDictionary
 - Setting property from dictionary

 dictionaryRepresentation
 - Distionary reprensentaion from model object.
 */

@interface IUOMData : NSObject <NSCoding, NSCopying>

//Properties
@property (nonatomic, strong) NSString *item;
@property (nonatomic, strong) NSString *uOM;
@property (nonatomic, strong) NSString *qtyPerUOM;

//Methods
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

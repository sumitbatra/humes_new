//
//  PLData.m
//
//  Created by User  on 5/25/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "PLData.h"


NSString *const kPLDataItem1 = @"Item1";
NSString *const kPLDataText5 = @"Text5";
NSString *const kPLDataText3 = @"Text3";
NSString *const kPLDataMessage2 = @"Message2";
NSString *const kPLDataStockOnHand = @"StockOnHand";
NSString *const kPLDataToSync = @"ToSync";
NSString *const kPLDataText1 = @"Text1";
NSString *const kPLDataIsBonus = @"IsBonus";
NSString *const kPLDataPicture = @"Picture";
NSString *const kPLDataUnitPrice = @"UnitPrice";
NSString *const kPLDataSalesUnitOfMeasure = @"SalesUnitOfMeasure";
NSString *const kPLDataMessage4 = @"Message4";
NSString *const kPLDataIsBatch = @"IsBatch";
NSString *const kPLDataProductSubGroupCode = @"ProductSubGroupCode";
NSString *const kPLDataSyncDateTime = @"SyncDateTime";
NSString *const kPLDataMessage1 = @"Message1";
NSString *const kPLDataRecordUpdated = @"RecordUpdated";
NSString *const kPLDataStatus = @"Status";
NSString *const kPLDataText4 = @"Text4";
NSString *const kPLDataProductGroup = @"ProductGroup";
NSString *const kPLDataSynced = @"Synced";
NSString *const kPLDataItemCategory = @"ItemCategory";
NSString *const kPLDataText2 = @"Text2";
NSString *const kPLDataDescription = @"Description";
NSString *const kPLDataDescription2 = @"Description2";
NSString *const kPLDataMessage3 = @"Message3";
NSString *const kPLDataGSTProductPostingGroup = @"GSTProductPostingGroup";
NSString *const kPLDataUserTimeStamp = @"UserTimeStamp";
NSString *const kPLDataDiscountGroup = @"DiscountGroup";
NSString *const kPLDataDiscountPercentage = @"DiscountPercentage";
NSString *const kPLDataBaseUnitOfMeasure = @"BaseUnitOfMeasure";
NSString *const kPLDataPromoStatus = @"PromoStatus";
NSString *const kPLDataMessage5 = @"Message5";
NSString *const kPLDataCrossReferenceNumber = @"CrossRef";


@interface PLData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PLData


@synthesize crossReferenceNumber = _crossReferenceNumber;
@synthesize item1 = _item1;
@synthesize text5 = _text5;
@synthesize text3 = _text3;
@synthesize message2 = _message2;
@synthesize stockOnHand = _stockOnHand;
@synthesize toSync = _toSync;
@synthesize text1 = _text1;
@synthesize isBonus = _isBonus;
@synthesize picture = _picture;
@synthesize unitPrice = _unitPrice;
@synthesize salesUnitOfMeasure = _salesUnitOfMeasure;
@synthesize message4 = _message4;
@synthesize isBatch = _isBatch;
@synthesize productSubGroupCode = _productSubGroupCode;
@synthesize syncDateTime = _syncDateTime;
@synthesize message1 = _message1;
@synthesize recordUpdated = _recordUpdated;
@synthesize status = _status;
@synthesize text4 = _text4;
@synthesize productGroup = _productGroup;
@synthesize synced = _synced;
@synthesize itemCategory = _itemCategory;
@synthesize text2 = _text2;
@synthesize dataDescription = _dataDescription;
@synthesize description2 = _description2;
@synthesize message3 = _message3;
@synthesize gSTProductPostingGroup = _gSTProductPostingGroup;
@synthesize userTimeStamp = _userTimeStamp;
@synthesize discountGroup = _discountGroup;
@synthesize discountPercentage = _discountPercentage;
@synthesize baseUnitOfMeasure = _baseUnitOfMeasure;
@synthesize promoStatus = _promoStatus;
@synthesize message5 = _message5;

///Creating and returning model object from dictionary.
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}


///Setting model class property from dictionary

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.item1 = [self objectOrNilForKey:kPLDataItem1 fromDictionary:dict];
        self.text5 = [self objectOrNilForKey:kPLDataText5 fromDictionary:dict];
        self.text3 = [self objectOrNilForKey:kPLDataText3 fromDictionary:dict];
        self.message2 = [self objectOrNilForKey:kPLDataMessage2 fromDictionary:dict];
        self.stockOnHand = [self objectOrNilForKey:kPLDataStockOnHand fromDictionary:dict];
        self.toSync = [[self objectOrNilForKey:kPLDataToSync fromDictionary:dict] boolValue];
        self.text1 = [self objectOrNilForKey:kPLDataText1 fromDictionary:dict];
        self.isBonus = [self objectOrNilForKey:kPLDataIsBonus fromDictionary:dict];
        self.picture = [self objectOrNilForKey:kPLDataPicture fromDictionary:dict];
        self.unitPrice = [self objectOrNilForKey:kPLDataUnitPrice fromDictionary:dict];
        self.salesUnitOfMeasure = [self objectOrNilForKey:kPLDataSalesUnitOfMeasure fromDictionary:dict];
        self.message4 = [self objectOrNilForKey:kPLDataMessage4 fromDictionary:dict];
        self.isBatch = [self objectOrNilForKey:kPLDataIsBatch fromDictionary:dict];
        self.productSubGroupCode = [self objectOrNilForKey:kPLDataProductSubGroupCode fromDictionary:dict];
        self.syncDateTime = [self objectOrNilForKey:kPLDataSyncDateTime fromDictionary:dict];
        self.message1 = [self objectOrNilForKey:kPLDataMessage1 fromDictionary:dict];
        self.recordUpdated = [self objectOrNilForKey:kPLDataRecordUpdated fromDictionary:dict];
        self.status = [self objectOrNilForKey:kPLDataStatus fromDictionary:dict];
        self.text4 = [self objectOrNilForKey:kPLDataText4 fromDictionary:dict];
        self.productGroup = [self objectOrNilForKey:kPLDataProductGroup fromDictionary:dict];
        self.synced = [self objectOrNilForKey:kPLDataSynced fromDictionary:dict];
        self.itemCategory = [self objectOrNilForKey:kPLDataItemCategory fromDictionary:dict];
        self.text2 = [self objectOrNilForKey:kPLDataText2 fromDictionary:dict];
        self.dataDescription = [self objectOrNilForKey:kPLDataDescription fromDictionary:dict];
        self.description2 = [self objectOrNilForKey:kPLDataDescription2 fromDictionary:dict];
        self.message3 = [self objectOrNilForKey:kPLDataMessage3 fromDictionary:dict];
        self.gSTProductPostingGroup = [self objectOrNilForKey:kPLDataGSTProductPostingGroup fromDictionary:dict];
        self.userTimeStamp = [self objectOrNilForKey:kPLDataUserTimeStamp fromDictionary:dict];
        self.discountGroup = [self objectOrNilForKey:kPLDataDiscountGroup fromDictionary:dict];
        self.discountPercentage = [self objectOrNilForKey:kPLDataDiscountPercentage fromDictionary:dict];
        self.baseUnitOfMeasure = [self objectOrNilForKey:kPLDataBaseUnitOfMeasure fromDictionary:dict];
        self.promoStatus = [[self objectOrNilForKey:kPLDataPromoStatus fromDictionary:dict] boolValue];
        self.message5 = [self objectOrNilForKey:kPLDataMessage5 fromDictionary:dict];
        self.crossReferenceNumber = [self objectOrNilForKey:kPLDataCrossReferenceNumber fromDictionary:dict];
        
        
    }
    
    return self;
    
}

///Create a dictionary from model class object property

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.item1 forKey:kPLDataItem1];
    [mutableDict setValue:self.text5 forKey:kPLDataText5];
    [mutableDict setValue:self.text3 forKey:kPLDataText3];
    [mutableDict setValue:self.message2 forKey:kPLDataMessage2];
    [mutableDict setValue:self.stockOnHand forKey:kPLDataStockOnHand];
    [mutableDict setValue:[NSNumber numberWithBool:self.toSync] forKey:kPLDataToSync];
    [mutableDict setValue:self.text1 forKey:kPLDataText1];
    [mutableDict setValue:self.isBonus forKey:kPLDataIsBonus];
    [mutableDict setValue:self.picture forKey:kPLDataPicture];
    [mutableDict setValue:self.unitPrice forKey:kPLDataUnitPrice];
    [mutableDict setValue:self.salesUnitOfMeasure forKey:kPLDataSalesUnitOfMeasure];
    [mutableDict setValue:self.message4 forKey:kPLDataMessage4];
    [mutableDict setValue:self.isBatch forKey:kPLDataIsBatch];
    [mutableDict setValue:self.productSubGroupCode forKey:kPLDataProductSubGroupCode];
    [mutableDict setValue:self.syncDateTime forKey:kPLDataSyncDateTime];
    [mutableDict setValue:self.message1 forKey:kPLDataMessage1];
    [mutableDict setValue:self.recordUpdated forKey:kPLDataRecordUpdated];
    [mutableDict setValue:self.status forKey:kPLDataStatus];
    [mutableDict setValue:self.text4 forKey:kPLDataText4];
    [mutableDict setValue:self.productGroup forKey:kPLDataProductGroup];
    [mutableDict setValue:self.synced forKey:kPLDataSynced];
    [mutableDict setValue:self.itemCategory forKey:kPLDataItemCategory];
    [mutableDict setValue:self.text2 forKey:kPLDataText2];
    [mutableDict setValue:self.dataDescription forKey:kPLDataDescription];
    [mutableDict setValue:self.description2 forKey:kPLDataDescription2];
    [mutableDict setValue:self.message3 forKey:kPLDataMessage3];
    [mutableDict setValue:self.gSTProductPostingGroup forKey:kPLDataGSTProductPostingGroup];
    [mutableDict setValue:self.userTimeStamp forKey:kPLDataUserTimeStamp];
    [mutableDict setValue:self.discountGroup forKey:kPLDataDiscountGroup];
    [mutableDict setValue:self.discountPercentage forKey:kPLDataDiscountPercentage];
    [mutableDict setValue:self.baseUnitOfMeasure forKey:kPLDataBaseUnitOfMeasure];
    [mutableDict setValue:[NSNumber numberWithBool:self.promoStatus] forKey:kPLDataPromoStatus];
    [mutableDict setValue:self.message5 forKey:kPLDataMessage5];
    [mutableDict setValue:self.crossReferenceNumber forKey:kPLDataCrossReferenceNumber];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? @"" : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.item1 = [aDecoder decodeObjectForKey:kPLDataItem1];
    self.text5 = [aDecoder decodeObjectForKey:kPLDataText5];
    self.text3 = [aDecoder decodeObjectForKey:kPLDataText3];
    self.message2 = [aDecoder decodeObjectForKey:kPLDataMessage2];
    self.stockOnHand = [aDecoder decodeObjectForKey:kPLDataStockOnHand];
    self.toSync = [aDecoder decodeBoolForKey:kPLDataToSync];
    self.text1 = [aDecoder decodeObjectForKey:kPLDataText1];
    self.isBonus = [aDecoder decodeObjectForKey:kPLDataIsBonus];
    self.picture = [aDecoder decodeObjectForKey:kPLDataPicture];
    self.unitPrice = [aDecoder decodeObjectForKey:kPLDataUnitPrice];
    self.salesUnitOfMeasure = [aDecoder decodeObjectForKey:kPLDataSalesUnitOfMeasure];
    self.message4 = [aDecoder decodeObjectForKey:kPLDataMessage4];
    self.isBatch = [aDecoder decodeObjectForKey:kPLDataIsBatch];
    self.productSubGroupCode = [aDecoder decodeObjectForKey:kPLDataProductSubGroupCode];
    self.syncDateTime = [aDecoder decodeObjectForKey:kPLDataSyncDateTime];
    self.message1 = [aDecoder decodeObjectForKey:kPLDataMessage1];
    self.recordUpdated = [aDecoder decodeObjectForKey:kPLDataRecordUpdated];
    self.status = [aDecoder decodeObjectForKey:kPLDataStatus];
    self.text4 = [aDecoder decodeObjectForKey:kPLDataText4];
    self.productGroup = [aDecoder decodeObjectForKey:kPLDataProductGroup];
    self.synced = [aDecoder decodeObjectForKey:kPLDataSynced];
    self.itemCategory = [aDecoder decodeObjectForKey:kPLDataItemCategory];
    self.text2 = [aDecoder decodeObjectForKey:kPLDataText2];
    self.dataDescription = [aDecoder decodeObjectForKey:kPLDataDescription];
    self.description2 = [aDecoder decodeObjectForKey:kPLDataDescription2];
    self.message3 = [aDecoder decodeObjectForKey:kPLDataMessage3];
    self.gSTProductPostingGroup = [aDecoder decodeObjectForKey:kPLDataGSTProductPostingGroup];
    self.userTimeStamp = [aDecoder decodeObjectForKey:kPLDataUserTimeStamp];
    self.discountGroup = [aDecoder decodeObjectForKey:kPLDataDiscountGroup];
    self.discountPercentage = [aDecoder decodeObjectForKey:kPLDataDiscountPercentage];
    self.baseUnitOfMeasure = [aDecoder decodeObjectForKey:kPLDataBaseUnitOfMeasure];
    self.promoStatus = [aDecoder decodeBoolForKey:kPLDataPromoStatus];
    self.message5 = [aDecoder decodeObjectForKey:kPLDataMessage5];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_item1 forKey:kPLDataItem1];
    [aCoder encodeObject:_text5 forKey:kPLDataText5];
    [aCoder encodeObject:_text3 forKey:kPLDataText3];
    [aCoder encodeObject:_message2 forKey:kPLDataMessage2];
    [aCoder encodeObject:_stockOnHand forKey:kPLDataStockOnHand];
    [aCoder encodeBool:_toSync forKey:kPLDataToSync];
    [aCoder encodeObject:_text1 forKey:kPLDataText1];
    [aCoder encodeObject:_isBonus forKey:kPLDataIsBonus];
    [aCoder encodeObject:_picture forKey:kPLDataPicture];
    [aCoder encodeObject:_unitPrice forKey:kPLDataUnitPrice];
    [aCoder encodeObject:_salesUnitOfMeasure forKey:kPLDataSalesUnitOfMeasure];
    [aCoder encodeObject:_message4 forKey:kPLDataMessage4];
    [aCoder encodeObject:_isBatch forKey:kPLDataIsBatch];
    [aCoder encodeObject:_productSubGroupCode forKey:kPLDataProductSubGroupCode];
    [aCoder encodeObject:_syncDateTime forKey:kPLDataSyncDateTime];
    [aCoder encodeObject:_message1 forKey:kPLDataMessage1];
    [aCoder encodeObject:_recordUpdated forKey:kPLDataRecordUpdated];
    [aCoder encodeObject:_status forKey:kPLDataStatus];
    [aCoder encodeObject:_text4 forKey:kPLDataText4];
    [aCoder encodeObject:_productGroup forKey:kPLDataProductGroup];
    [aCoder encodeObject:_synced forKey:kPLDataSynced];
    [aCoder encodeObject:_itemCategory forKey:kPLDataItemCategory];
    [aCoder encodeObject:_text2 forKey:kPLDataText2];
    [aCoder encodeObject:_dataDescription forKey:kPLDataDescription];
    [aCoder encodeObject:_description2 forKey:kPLDataDescription2];
    [aCoder encodeObject:_message3 forKey:kPLDataMessage3];
    [aCoder encodeObject:_gSTProductPostingGroup forKey:kPLDataGSTProductPostingGroup];
    [aCoder encodeObject:_userTimeStamp forKey:kPLDataUserTimeStamp];
    [aCoder encodeObject:_discountGroup forKey:kPLDataDiscountGroup];
    [aCoder encodeObject:_discountPercentage forKey:kPLDataDiscountPercentage];
    [aCoder encodeObject:_baseUnitOfMeasure forKey:kPLDataBaseUnitOfMeasure];
    [aCoder encodeBool:_promoStatus forKey:kPLDataPromoStatus];
    [aCoder encodeObject:_message5 forKey:kPLDataMessage5];
}

- (id)copyWithZone:(NSZone *)zone
{
    PLData *copy = [[PLData alloc] init];
    
    if (copy) {
        
        copy.item1 = [self.item1 copyWithZone:zone];
        copy.text5 = [self.text5 copyWithZone:zone];
        copy.text3 = [self.text3 copyWithZone:zone];
        copy.message2 = [self.message2 copyWithZone:zone];
        copy.stockOnHand = [self.stockOnHand copyWithZone:zone];
        copy.toSync = self.toSync;
        copy.text1 = [self.text1 copyWithZone:zone];
        copy.isBonus = [self.isBonus copyWithZone:zone];
        copy.picture = [self.picture copyWithZone:zone];
        copy.unitPrice = [self.unitPrice copyWithZone:zone];
        copy.salesUnitOfMeasure = [self.salesUnitOfMeasure copyWithZone:zone];
        copy.message4 = [self.message4 copyWithZone:zone];
        copy.isBatch = [self.isBatch copyWithZone:zone];
        copy.productSubGroupCode = [self.productSubGroupCode copyWithZone:zone];
        copy.syncDateTime = [self.syncDateTime copyWithZone:zone];
        copy.message1 = [self.message1 copyWithZone:zone];
        copy.recordUpdated = [self.recordUpdated copyWithZone:zone];
        copy.status = [self.status copyWithZone:zone];
        copy.text4 = [self.text4 copyWithZone:zone];
        copy.productGroup = [self.productGroup copyWithZone:zone];
        copy.synced = [self.synced copyWithZone:zone];
        copy.itemCategory = [self.itemCategory copyWithZone:zone];
        copy.text2 = [self.text2 copyWithZone:zone];
        copy.dataDescription = [self.dataDescription copyWithZone:zone];
        copy.description2 = [self.description2 copyWithZone:zone];
        copy.message3 = [self.message3 copyWithZone:zone];
        copy.gSTProductPostingGroup = [self.gSTProductPostingGroup copyWithZone:zone];
        copy.userTimeStamp = [self.userTimeStamp copyWithZone:zone];
        copy.discountGroup = [self.discountGroup copyWithZone:zone];
        copy.discountPercentage = [self.discountPercentage copyWithZone:zone];
        copy.baseUnitOfMeasure = [self.baseUnitOfMeasure copyWithZone:zone];
        copy.promoStatus = self.promoStatus;
        copy.message5 = [self.message5 copyWithZone:zone];
    }
    
    return copy;
}


@end

//
//  OrderDetails.m
//
//  Created by user  on 02/02/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "OrderDetails.h"
#import "Utility.h"

NSString *const kOrderDetailsReturnQuantity = @"ReturnQuantity";
NSString *const kOrderDetailsType = @"Type";
NSString *const kOrderDetailsDiscountPercentage = @"DiscountPercentage";
NSString *const kOrderDetailsDescription = @"Description";
NSString *const kOrderDetailsUnitPrice = @"UnitPrice";
NSString *const kOrderDetailsUnitOfMeasure = @"UnitOfMeasure";
NSString *const kOrderDetailsLineAmount = @"LineAmount";
NSString *const kOrderDetailsReturnsReason = @"ReturnsReason";
NSString *const kOrderDetailsItemNo = @"ItemNo";
NSString *const kOrderDetailsSynced = @"Synced";
NSString *const kOrderDetailsOrderNo = @"OrderNo";
NSString *const kOrderDetailsBonusStock = @"BonusStock";
NSString *const kOrderDetailsStatus = @"Status";
NSString *const kOrderDetailsGrossAmount = @"GrossAmount";
NSString *const kOrderDetailsSyncDateTime = @"SyncDateTime";
NSString *const kOrderDetailsQuantity = @"Quantity";
NSString *const kOrderDetailsToSync = @"ToSync";
NSString *const kOrderDetailsLineNo = @"LineNo";
NSString *const kOrderDetailsGSTPercentage = @"GSTPercentage";


@interface OrderDetails ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation OrderDetails

@synthesize returnQuantity = _returnQuantity;
@synthesize type = _type;
@synthesize discountPercentage = _discountPercentage;
@synthesize OrderDetailsDescription = _OrderDetailsDescription;
@synthesize unitPrice = _unitPrice;
@synthesize unitOfMeasure = _unitOfMeasure;
@synthesize lineAmount = _lineAmount;
@synthesize returnsReason = _returnsReason;
@synthesize itemNo = _itemNo;
@synthesize synced = _synced;
@synthesize orderNo = _orderNo;
@synthesize bonusStock = _bonusStock;
@synthesize status = _status;
@synthesize grossAmount = _grossAmount;
@synthesize syncDateTime = _syncDateTime;
@synthesize quantity = _quantity;
@synthesize toSync = _toSync;
@synthesize lineNo = _lineNo;
@synthesize gSTPercentage = _gSTPercentage;

///Creating and returning model object from dictionary.
+ (OrderDetails *)modelObjectWithDictionary:(NSDictionary *)dict
{
    OrderDetails *instance = [[OrderDetails alloc] initWithDictionary:[Utility parseNullValuesInDictionary:dict]];
    return instance;
}


///Setting model class property from dictionary

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.returnQuantity = [self objectOrNilForKey:kOrderDetailsReturnQuantity fromDictionary:dict];
            self.type = [self objectOrNilForKey:kOrderDetailsType fromDictionary:dict];
            self.discountPercentage = [self objectOrNilForKey:kOrderDetailsDiscountPercentage fromDictionary:dict];
            self.OrderDetailsDescription = [self objectOrNilForKey:kOrderDetailsDescription fromDictionary:dict];
            self.unitPrice = [self objectOrNilForKey:kOrderDetailsUnitPrice fromDictionary:dict];
            self.unitOfMeasure = [self objectOrNilForKey:kOrderDetailsUnitOfMeasure fromDictionary:dict];
            self.lineAmount = [self objectOrNilForKey:kOrderDetailsLineAmount fromDictionary:dict];
            self.returnsReason = [self objectOrNilForKey:kOrderDetailsReturnsReason fromDictionary:dict];
            self.itemNo = [self objectOrNilForKey:kOrderDetailsItemNo fromDictionary:dict];
            self.synced = [[self objectOrNilForKey:kOrderDetailsSynced fromDictionary:dict] boolValue];
            self.orderNo = [self objectOrNilForKey:kOrderDetailsOrderNo fromDictionary:dict];
            self.bonusStock = [self objectOrNilForKey:kOrderDetailsBonusStock fromDictionary:dict];
            self.status = [self objectOrNilForKey:kOrderDetailsStatus fromDictionary:dict];
            self.grossAmount = [self objectOrNilForKey:kOrderDetailsGrossAmount fromDictionary:dict];
            self.syncDateTime = [self objectOrNilForKey:kOrderDetailsSyncDateTime fromDictionary:dict];
            self.quantity = [self objectOrNilForKey:kOrderDetailsQuantity fromDictionary:dict];
            self.toSync = [[self objectOrNilForKey:kOrderDetailsToSync fromDictionary:dict] boolValue];
            self.lineNo = [self objectOrNilForKey:kOrderDetailsLineNo fromDictionary:dict];
            self.gSTPercentage = [self objectOrNilForKey:kOrderDetailsGSTPercentage fromDictionary:dict];

    }
    
    return self;
    
}

///Create a dictionary from model class object property

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.returnQuantity forKey:kOrderDetailsReturnQuantity];
    [mutableDict setValue:self.type forKey:kOrderDetailsType];
    [mutableDict setValue:self.discountPercentage forKey:kOrderDetailsDiscountPercentage];
    [mutableDict setValue:self.OrderDetailsDescription forKey:kOrderDetailsDescription];
    [mutableDict setValue:self.unitPrice forKey:kOrderDetailsUnitPrice];
    [mutableDict setValue:self.unitOfMeasure forKey:kOrderDetailsUnitOfMeasure];
    [mutableDict setValue:self.lineAmount forKey:kOrderDetailsLineAmount];
    [mutableDict setValue:self.returnsReason forKey:kOrderDetailsReturnsReason];
    [mutableDict setValue:self.itemNo forKey:kOrderDetailsItemNo];
    [mutableDict setValue:[NSNumber numberWithBool:self.synced] forKey:kOrderDetailsSynced];
    [mutableDict setValue:self.orderNo forKey:kOrderDetailsOrderNo];
    [mutableDict setValue:self.bonusStock forKey:kOrderDetailsBonusStock];
    [mutableDict setValue:self.status forKey:kOrderDetailsStatus];
    [mutableDict setValue:self.grossAmount forKey:kOrderDetailsGrossAmount];
    [mutableDict setValue:self.syncDateTime forKey:kOrderDetailsSyncDateTime];
    [mutableDict setValue:self.quantity forKey:kOrderDetailsQuantity];
    [mutableDict setValue:[NSNumber numberWithBool:self.toSync] forKey:kOrderDetailsToSync];
    [mutableDict setValue:self.lineNo forKey:kOrderDetailsLineNo];
    [mutableDict setValue:self.gSTPercentage forKey:kOrderDetailsGSTPercentage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.returnQuantity = [aDecoder decodeObjectForKey:kOrderDetailsReturnQuantity];
    self.type = [aDecoder decodeObjectForKey:kOrderDetailsType];
    self.discountPercentage = [aDecoder decodeObjectForKey:kOrderDetailsDiscountPercentage];
    self.OrderDetailsDescription = [aDecoder decodeObjectForKey:kOrderDetailsDescription];
    self.unitPrice = [aDecoder decodeObjectForKey:kOrderDetailsUnitPrice];
    self.unitOfMeasure = [aDecoder decodeObjectForKey:kOrderDetailsUnitOfMeasure];
    self.lineAmount = [aDecoder decodeObjectForKey:kOrderDetailsLineAmount];
    self.returnsReason = [aDecoder decodeObjectForKey:kOrderDetailsReturnsReason];
    self.itemNo = [aDecoder decodeObjectForKey:kOrderDetailsItemNo];
    self.synced = [aDecoder decodeBoolForKey:kOrderDetailsSynced];
    self.orderNo = [aDecoder decodeObjectForKey:kOrderDetailsOrderNo];
    self.bonusStock = [aDecoder decodeObjectForKey:kOrderDetailsBonusStock];
    self.status = [aDecoder decodeObjectForKey:kOrderDetailsStatus];
    self.grossAmount = [aDecoder decodeObjectForKey:kOrderDetailsGrossAmount];
    self.syncDateTime = [aDecoder decodeObjectForKey:kOrderDetailsSyncDateTime];
    self.quantity = [aDecoder decodeObjectForKey:kOrderDetailsQuantity];
    self.toSync = [aDecoder decodeBoolForKey:kOrderDetailsToSync];
    self.lineNo = [aDecoder decodeObjectForKey:kOrderDetailsLineNo];
    self.gSTPercentage = [aDecoder decodeObjectForKey:kOrderDetailsGSTPercentage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_returnQuantity forKey:kOrderDetailsReturnQuantity];
    [aCoder encodeObject:_type forKey:kOrderDetailsType];
    [aCoder encodeObject:_discountPercentage forKey:kOrderDetailsDiscountPercentage];
    [aCoder encodeObject:_OrderDetailsDescription forKey:kOrderDetailsDescription];
    [aCoder encodeObject:_unitPrice forKey:kOrderDetailsUnitPrice];
    [aCoder encodeObject:_unitOfMeasure forKey:kOrderDetailsUnitOfMeasure];
    [aCoder encodeObject:_lineAmount forKey:kOrderDetailsLineAmount];
    [aCoder encodeObject:_returnsReason forKey:kOrderDetailsReturnsReason];
    [aCoder encodeObject:_itemNo forKey:kOrderDetailsItemNo];
    [aCoder encodeBool:_synced forKey:kOrderDetailsSynced];
    [aCoder encodeObject:_orderNo forKey:kOrderDetailsOrderNo];
    [aCoder encodeObject:_bonusStock forKey:kOrderDetailsBonusStock];
    [aCoder encodeObject:_status forKey:kOrderDetailsStatus];
    [aCoder encodeObject:_grossAmount forKey:kOrderDetailsGrossAmount];
    [aCoder encodeObject:_syncDateTime forKey:kOrderDetailsSyncDateTime];
    [aCoder encodeObject:_quantity forKey:kOrderDetailsQuantity];
    [aCoder encodeBool:_toSync forKey:kOrderDetailsToSync];
    [aCoder encodeObject:_lineNo forKey:kOrderDetailsLineNo];
    [aCoder encodeObject:_gSTPercentage forKey:kOrderDetailsGSTPercentage];
}


@end

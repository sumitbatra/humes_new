//
//  PLData.h
//
//  Created by User  on 5/25/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class: PLData Model object class
 @brief: A PLData data class with property
 - NSString *item1;
 
 -  NSString *text5;
 
 - id text3;
 
 -  id message2;
 
 -  id stockOnHand;
 
 -  BOOL toSync;
 
 -  id text1;
 
 -  id isBonus;
 
 -  NSString *picture;
 
 -  NSString *unitPrice;
 
 -  NSString *salesUnitOfMeasure;
 
 -  id message4;
 
 -  id isBatch;
 
 -  id productSubGroupCode;
 
 -  id syncDateTime;
 
 -  id message1;
 
 -  id recordUpdated;
 
 -  NSString *status;
 
 -  id text4;
 
 -  NSString *productGroup;
 
 -  id synced;
 
 -  NSString *itemCategory;
 
 -  id text2;
 
 -  NSString *dataDescription;
 
 -  NSString *description2;
 
 - id message3;
 
 -  NSString *gSTProductPostingGroup;
 
 -  id userTimeStamp;
 
 -  id discountGroup;
 
 -  id discountPercentage;
 
 -  NSString *baseUnitOfMeasure;
 
 -  BOOL promoStatus;
 
 -  id message5;
 
 -  NSString *crossReferenceNumber; 
 
 @discussion: Can perform given below operation using function

 @function:
 modelObjectWithDictionary
 - Return a model object with all available property
 
 initWithDictionary
 - Setting property from dictionary
 
 dictionaryRepresentation
 - Distionary reprensentaion from model object.
 */



@interface PLData : NSObject <NSCoding, NSCopying>

//Properties
@property (nonatomic, strong) NSString *item1;
@property (nonatomic, strong) NSString *text5;
@property (nonatomic, strong) id text3;
@property (nonatomic, strong) id message2;
@property (nonatomic, strong) id stockOnHand;
@property (nonatomic, assign) BOOL toSync;
@property (nonatomic, strong) id text1;
@property (nonatomic, strong) id isBonus;
@property (nonatomic, strong) NSString *picture;
@property (nonatomic, strong) NSString *unitPrice;
@property (nonatomic, strong) NSString *salesUnitOfMeasure;
@property (nonatomic, strong) id message4;
@property (nonatomic, strong) id isBatch;
@property (nonatomic, strong) id productSubGroupCode;
@property (nonatomic, strong) id syncDateTime;
@property (nonatomic, strong) id message1;
@property (nonatomic, strong) id recordUpdated;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) id text4;
@property (nonatomic, strong) NSString *productGroup;
@property (nonatomic, strong) id synced;
@property (nonatomic, strong) NSString *itemCategory;
@property (nonatomic, strong) id text2;
@property (nonatomic, strong) NSString *dataDescription;
@property (nonatomic, strong) NSString *description2;
@property (nonatomic, strong) id message3;
@property (nonatomic, strong) NSString *gSTProductPostingGroup;
@property (nonatomic, strong) id userTimeStamp;
@property (nonatomic, strong) id discountGroup;
@property (nonatomic, strong) id discountPercentage;
@property (nonatomic, strong) NSString *baseUnitOfMeasure;
@property (nonatomic, assign) BOOL promoStatus;
@property (nonatomic, strong) id message5;
@property(nonatomic, strong) NSString *crossReferenceNumber;

//Methods
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

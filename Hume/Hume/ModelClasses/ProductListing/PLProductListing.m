//
//  PLProductList.m
//
//  Created by User  on 5/25/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "PLProductListing.h"
#import "PLData.h"


NSString *const kPLProductListStatusCode = @"statusCode";
NSString *const kPLProductListData = @"data";


@interface PLProductListing ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PLProductListing

@synthesize statusCode = _statusCode;
@synthesize data = _data;

///Creating and returning model object from dictionary.
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}


///Setting model class property from dictionary

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.statusCode = [[self objectOrNilForKey:kPLProductListStatusCode fromDictionary:dict] doubleValue];
        NSObject *receivedPLData = [dict objectForKey:kPLProductListData];
        NSMutableArray *parsedPLData = [NSMutableArray array];
        if ([receivedPLData isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedPLData) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedPLData addObject:[PLData modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedPLData isKindOfClass:[NSDictionary class]]) {
            [parsedPLData addObject:[PLData modelObjectWithDictionary:(NSDictionary *)receivedPLData]];
        }
        
        self.data = [NSArray arrayWithArray:parsedPLData];
        
    }
    
    return self;
    
}

///Create a dictionary from model class object property

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.statusCode] forKey:kPLProductListStatusCode];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kPLProductListData];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.statusCode = [aDecoder decodeDoubleForKey:kPLProductListStatusCode];
    self.data = [aDecoder decodeObjectForKey:kPLProductListData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeDouble:_statusCode forKey:kPLProductListStatusCode];
    [aCoder encodeObject:_data forKey:kPLProductListData];
}

- (id)copyWithZone:(NSZone *)zone
{
    PLProductListing *copy = [[PLProductListing alloc] init];
    
    if (copy) {
        
        copy.statusCode = self.statusCode;
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end

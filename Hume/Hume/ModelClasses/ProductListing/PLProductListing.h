//
//  PLProductList.h
//
//  Created by User  on 5/25/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 @class: PLProductListing Model object class
 @brief: A PLProductListing data class with property
 - double statusCode;
 - NSArray *data;

 @discussion: Can perform given below operation using function

 @function:
 modelObjectWithDictionary
 - Return a model object with all available property
 
 initWithDictionary
 - Setting property from dictionary
 
 dictionaryRepresentation
 - Distionary reprensentaion from model object.
 */



@interface PLProductListing : NSObject <NSCoding, NSCopying>

//Properties
@property (nonatomic, assign) double statusCode;
@property (nonatomic, strong) NSArray *data;

//Methods
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

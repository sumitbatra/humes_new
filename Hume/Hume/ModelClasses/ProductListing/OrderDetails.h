//
//  Data.h
//
//  Created by user  on 02/02/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 @class: OrderDetails Model object class
 @brief: A OrderDetails data class with property
 - id returnQuantity;
 
 -  NSString *type;
 
 -  NSString *discountPercentage;
 
 -  NSString *OrderDetailsDescription;
 
 -  NSString *unitPrice;
 
 -  NSString *unitOfMeasure;
 
 -  NSString *lineAmount;
 
 -  id returnsReason;
 
 -  NSString *itemNo;
 
 -  BOOL synced;
 
 -  NSString *orderNo;
 
 -  id bonusStock;
 
 -  NSString *status;
 
 -  id grossAmount;
 
 -  NSString *syncDateTime;
 
 -  NSString *quantity;
 
 -  BOOL toSync;
 
 -  NSString *lineNo;
 
 -  NSString *gSTPercentage;
 
 @discussion: Can perform given below operation using function

 @function:
 modelObjectWithDictionary
 - Return a model object with all available property
 
 initWithDictionary
 - Setting property from dictionary
 
 dictionaryRepresentation
 - Distionary reprensentaion from model object.
 */


@interface OrderDetails : NSObject <NSCoding>

//Properties
@property (nonatomic, assign) id returnQuantity;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *discountPercentage;
@property (nonatomic, strong) NSString *OrderDetailsDescription;
@property (nonatomic, strong) NSString *unitPrice;
@property (nonatomic, strong) NSString *unitOfMeasure;
@property (nonatomic, strong) NSString *lineAmount;
@property (nonatomic, assign) id returnsReason;
@property (nonatomic, strong) NSString *itemNo;
@property (nonatomic, assign) BOOL synced;
@property (nonatomic, strong) NSString *orderNo;
@property (nonatomic, assign) id bonusStock;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, assign) id grossAmount;
@property (nonatomic, strong) NSString *syncDateTime;
@property (nonatomic, strong) NSString *quantity;
@property (nonatomic, assign) BOOL toSync;
@property (nonatomic, strong) NSString *lineNo;
@property (nonatomic, strong) NSString *gSTPercentage;

//Methods
+ (OrderDetails *)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

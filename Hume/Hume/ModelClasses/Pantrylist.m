//
//  Pantrylist.m
//
//  Created by user  on 12/02/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "Pantrylist.h"


NSString *const kPantrylistItemNo = @"ItemNo";
NSString *const kPantrylistPicture = @"Picture";
NSString *const kPantrylistItemCategory = @"ItemCategory";
NSString *const kPantrylistUnitPrice = @"UnitPrice";
NSString *const kPantrylistQuantity = @"Quantity";
NSString *const kPantrylistUOM = @"UOM";
NSString *const kPantrylistDescription = @"Description";
NSString *const kPantrylistProductGroup = @"ProductGroup";


@interface Pantrylist ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Pantrylist

@synthesize itemNo = _itemNo;
@synthesize picture = _picture;
@synthesize itemCategory = _itemCategory;
@synthesize unitPrice = _unitPrice;
@synthesize quantity = _quantity;
@synthesize uOM = _uOM;
@synthesize pantrylistDescription = _pantrylistDescription;
@synthesize productGroup = _productGroup;

///Creating and returning model object from dictionary.
+ (Pantrylist *)modelObjectWithDictionary:(NSDictionary *)dict
{
    Pantrylist *instance = [[Pantrylist alloc] initWithDictionary:dict];
    return instance;
}

///Setting model class property from dictionary
- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.itemNo = [self objectOrNilForKey:kPantrylistItemNo fromDictionary:dict];
            self.picture = [self objectOrNilForKey:kPantrylistPicture fromDictionary:dict];
            self.itemCategory = [self objectOrNilForKey:kPantrylistItemCategory fromDictionary:dict];
            self.unitPrice = [self objectOrNilForKey:kPantrylistUnitPrice fromDictionary:dict];
            self.quantity = [self objectOrNilForKey:kPantrylistQuantity fromDictionary:dict];
            self.uOM = [self objectOrNilForKey:kPantrylistUOM fromDictionary:dict];
            self.pantrylistDescription = [self objectOrNilForKey:kPantrylistDescription fromDictionary:dict];
            self.productGroup = [self objectOrNilForKey:kPantrylistProductGroup fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.itemNo forKey:kPantrylistItemNo];
    [mutableDict setValue:self.picture forKey:kPantrylistPicture];
    [mutableDict setValue:self.itemCategory forKey:kPantrylistItemCategory];
    [mutableDict setValue:self.unitPrice forKey:kPantrylistUnitPrice];
    [mutableDict setValue:self.quantity forKey:kPantrylistQuantity];
    [mutableDict setValue:self.uOM forKey:kPantrylistUOM];
    [mutableDict setValue:self.pantrylistDescription forKey:kPantrylistDescription];
    [mutableDict setValue:self.productGroup forKey:kPantrylistProductGroup];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return ([object isEqual:[NSNull null]] || [object isKindOfClass:[NSNull class]]) ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.itemNo = [aDecoder decodeObjectForKey:kPantrylistItemNo];
    self.picture = [aDecoder decodeObjectForKey:kPantrylistPicture];
    self.itemCategory = [aDecoder decodeObjectForKey:kPantrylistItemCategory];
    self.unitPrice = [aDecoder decodeObjectForKey:kPantrylistUnitPrice];
    self.quantity = [aDecoder decodeObjectForKey:kPantrylistQuantity];
    self.uOM = [aDecoder decodeObjectForKey:kPantrylistUOM];
    self.pantrylistDescription = [aDecoder decodeObjectForKey:kPantrylistDescription];
    self.productGroup = [aDecoder decodeObjectForKey:kPantrylistProductGroup];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_itemNo forKey:kPantrylistItemNo];
    [aCoder encodeObject:_picture forKey:kPantrylistPicture];
    [aCoder encodeObject:_itemCategory forKey:kPantrylistItemCategory];
    [aCoder encodeObject:_unitPrice forKey:kPantrylistUnitPrice];
    [aCoder encodeObject:_quantity forKey:kPantrylistQuantity];
    [aCoder encodeObject:_uOM forKey:kPantrylistUOM];
    [aCoder encodeObject:_pantrylistDescription forKey:kPantrylistDescription];
    [aCoder encodeObject:_productGroup forKey:kPantrylistProductGroup];
}


@end

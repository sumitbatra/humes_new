//
//  Data.m
//
//  Created by user  on 14/01/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "Data.h"
#import "ShippingAddress.h"
#import "Customerinfo.h"


NSString *const kDataShippingAddress = @"shippingAddress";
NSString *const kDataCustomerinfo = @"customerinfo";


@interface Data ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Data

@synthesize shippingAddress = _shippingAddress;
@synthesize customerinfo = _customerinfo;

///Creating and returning model object from dictionary.
+ (Data *)modelObjectWithDictionary:(NSDictionary *)dict
{
    Data *instance = [[Data alloc] initWithDictionary:dict];
    return instance;
}

///Setting model class property from dictionary

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedShippingAddress = [dict objectForKey:kDataShippingAddress];
    NSMutableArray *parsedShippingAddress = [NSMutableArray array];
    if ([receivedShippingAddress isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedShippingAddress) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedShippingAddress addObject:[ShippingAddress modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedShippingAddress isKindOfClass:[NSDictionary class]]) {
       [parsedShippingAddress addObject:[ShippingAddress modelObjectWithDictionary:(NSDictionary *)receivedShippingAddress]];
    }

    self.shippingAddress = [NSArray arrayWithArray:parsedShippingAddress];
            self.customerinfo = [Customerinfo modelObjectWithDictionary:[dict objectForKey:kDataCustomerinfo]];

    }
    
    return self;
    
}

///Create a dictionary from model class object property

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
NSMutableArray *tempArrayForShippingAddress = [NSMutableArray array];
    for (NSObject *subArrayObject in self.shippingAddress) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForShippingAddress addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForShippingAddress addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForShippingAddress] forKey:@"kDataShippingAddress"];
    [mutableDict setValue:[self.customerinfo dictionaryRepresentation] forKey:kDataCustomerinfo];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.shippingAddress = [aDecoder decodeObjectForKey:kDataShippingAddress];
    self.customerinfo = [aDecoder decodeObjectForKey:kDataCustomerinfo];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_shippingAddress forKey:kDataShippingAddress];
    [aCoder encodeObject:_customerinfo forKey:kDataCustomerinfo];
}


@end

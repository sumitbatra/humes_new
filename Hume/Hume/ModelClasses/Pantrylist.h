//
//  Pantrylist.h
//
//  Created by user  on 12/02/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 @class: Pantrylist Model object class
 @brief: A Pantrylist data class with property
 - NSString *itemNo;
 
 - id picture;
 
 - NSString *itemCategory;
 
 - NSString *unitPrice;
 
 - NSString *quantity;
 
 - NSString *uOM;
 
 - NSString *pantrylistDescription;
 
 - NSString *productGroup;

 @discussion: Can perform givenbelow operation using function
 
 @function:
 modelObjectWithDictionary
 - Return a model object with all available property
 
 initWithDictionary
 - Setting property from dictionary
 
 dictionaryRepresentation
 - Distionary reprensentaion from model object.
 */


@interface Pantrylist : NSObject <NSCoding>

//Properties
@property (nonatomic, strong) NSString *itemNo;
@property (nonatomic, assign) id picture;
@property (nonatomic, strong) NSString *itemCategory;
@property (nonatomic, strong) NSString *unitPrice;
@property (nonatomic, strong) NSString *quantity;
@property (nonatomic, strong) NSString *uOM;
@property (nonatomic, strong) NSString *pantrylistDescription;
@property (nonatomic, strong) NSString *productGroup;

//Methods
+ (Pantrylist *)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

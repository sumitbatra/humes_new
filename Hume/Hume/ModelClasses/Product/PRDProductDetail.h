//
//  PRDProductDetail.h
//
//  Created by User  on 1/27/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 @class: PRDProductDetail Model object class
 @brief: A PRDProductDetail data class with property
 
 - double statusCode;
 
 - PRDData *data;

 @discussion: Can perform given below operation using function

  @function:
 modelObjectWithDictionary
 - Return a model object with all available property
 
 initWithDictionary
 - Setting property from dictionary
 
 dictionaryRepresentation
 - Distionary reprensentaion from model object.
 */

@class PRDData;

@interface PRDProductDetail : NSObject <NSCoding, NSCopying>

//Properties
@property (nonatomic, assign) double statusCode;
@property (nonatomic, strong) PRDData *data;

//Methods
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

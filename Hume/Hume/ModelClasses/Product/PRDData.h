//
//  PRDData.h
//
//  Created by User  on 1/27/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PRDItem;

/**
 @class: PRDData Model object class
 @brief: A PRDData data class with property
 - PRDItem *item;
 
 - NSArray *promoDiscount;
 
 - NSArray *promoMultiBuy;
 
 - NSArray *promoUpsell;
 
 - NSArray *promoReward;
 
 @discussion: Can perform given below operation using function
 
  @function:
 modelObjectWithDictionary
 - Return a model object with all available property
 
 initWithDictionary
 - Setting property from dictionary
 
 dictionaryRepresentation
 - Distionary reprensentaion from model object.
 */
@interface PRDData : NSObject <NSCoding, NSCopying>

//Properties
@property (nonatomic, strong) PRDItem *item;
@property (nonatomic, strong) NSArray *promoDiscount;
@property (nonatomic, strong) NSArray *promoMultiBuy;
@property (nonatomic, strong) NSArray *promoUpsell;
@property (nonatomic, strong) NSArray *promoReward;

//Methods
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

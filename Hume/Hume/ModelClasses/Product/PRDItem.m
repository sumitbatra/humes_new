//
//  PRDItem.m
//
//  Created by User  on 1/27/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "PRDItem.h"


NSString *const kPRDItemMessage1 = @"Message1";
NSString *const kPRDItemItem1 = @"Item1";
NSString *const kPRDItemText3 = @"Text3";
NSString *const kPRDItemMessage5 = @"Message5";
NSString *const kPRDItemIsBonus = @"IsBonus";
NSString *const kPRDItemMessage2 = @"Message2";
NSString *const kPRDItemStockOnHand = @"StockOnHand";
NSString *const kPRDItemText2 = @"Text2";
NSString *const kPRDItemSynced = @"Synced";
NSString *const kPRDItemSalesUnitOfMeasure = @"SalesUnitOfMeasure";
NSString *const kPRDItemRecordUpdated = @"RecordUpdated";
NSString *const kPRDItemGSTProductPostingGroup = @"GSTProductPostingGroup";
NSString *const kPRDItemProductGroup = @"ProductGroup";
NSString *const kPRDItemDiscountGroup = @"DiscountGroup";
NSString *const kPRDItemMessage3 = @"Message3";
NSString *const kPRDItemText1 = @"Text1";
NSString *const kPRDItemUnitPrice = @"UnitPrice";
NSString *const kPRDItemBaseUnitOfMeasure = @"BaseUnitOfMeasure";
NSString *const kPRDItemUserTimeStamp = @"UserTimeStamp";
NSString *const kPRDItemSyncDateTime = @"SyncDateTime";
NSString *const kPRDItemDescription = @"Description";
NSString *const kPRDItemMessage4 = @"Message4";
NSString *const kPRDItemIsBatch = @"IsBatch";
NSString *const kPRDItemItemCategory = @"ItemCategory";
NSString *const kPRDItemPicture = @"Picture";
NSString *const kPRDItemText5 = @"Text5";
NSString *const kPRDItemDiscountPercentage = @"DiscountPercentage";
NSString *const kPRDItemStatus = @"Status";
NSString *const kPRDItemText4 = @"Text4";
NSString *const kPRDItemToSync = @"ToSync";
NSString *const kPRDItemProductSubGroupCode = @"ProductSubGroupCode";
NSString *const kPRDItemDescription2 = @"Description2";


@interface PRDItem ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PRDItem

@synthesize message1 = _message1;
@synthesize item1 = _item1;
@synthesize text3 = _text3;
@synthesize message5 = _message5;
@synthesize isBonus = _isBonus;
@synthesize message2 = _message2;
@synthesize stockOnHand = _stockOnHand;
@synthesize text2 = _text2;
@synthesize synced = _synced;
@synthesize salesUnitOfMeasure = _salesUnitOfMeasure;
@synthesize recordUpdated = _recordUpdated;
@synthesize gSTProductPostingGroup = _gSTProductPostingGroup;
@synthesize productGroup = _productGroup;
@synthesize discountGroup = _discountGroup;
@synthesize message3 = _message3;
@synthesize text1 = _text1;
@synthesize unitPrice = _unitPrice;
@synthesize baseUnitOfMeasure = _baseUnitOfMeasure;
@synthesize userTimeStamp = _userTimeStamp;
@synthesize syncDateTime = _syncDateTime;
@synthesize itemDescription = _itemDescription;
@synthesize message4 = _message4;
@synthesize isBatch = _isBatch;
@synthesize itemCategory = _itemCategory;
@synthesize picture = _picture;
@synthesize text5 = _text5;
@synthesize discountPercentage = _discountPercentage;
@synthesize status = _status;
@synthesize text4 = _text4;
@synthesize toSync = _toSync;
@synthesize productSubGroupCode = _productSubGroupCode;
@synthesize description2 = _description2;

///Creating and returning model object from dictionary.
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}


///Setting model class property from dictionary

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.message1 = [self objectOrNilForKey:kPRDItemMessage1 fromDictionary:dict];
            self.item1 = [self objectOrNilForKey:kPRDItemItem1 fromDictionary:dict];
            self.text3 = [self objectOrNilForKey:kPRDItemText3 fromDictionary:dict];
            self.message5 = [self objectOrNilForKey:kPRDItemMessage5 fromDictionary:dict];
            self.isBonus = [self objectOrNilForKey:kPRDItemIsBonus fromDictionary:dict];
            self.message2 = [self objectOrNilForKey:kPRDItemMessage2 fromDictionary:dict];
            self.stockOnHand = [self objectOrNilForKey:kPRDItemStockOnHand fromDictionary:dict];
            self.text2 = [self objectOrNilForKey:kPRDItemText2 fromDictionary:dict];
            self.synced = [self objectOrNilForKey:kPRDItemSynced fromDictionary:dict];
            self.salesUnitOfMeasure = [self objectOrNilForKey:kPRDItemSalesUnitOfMeasure fromDictionary:dict];
            self.recordUpdated = [self objectOrNilForKey:kPRDItemRecordUpdated fromDictionary:dict];
            self.gSTProductPostingGroup = [self objectOrNilForKey:kPRDItemGSTProductPostingGroup fromDictionary:dict];
            self.productGroup = [self objectOrNilForKey:kPRDItemProductGroup fromDictionary:dict];
            self.discountGroup = [self objectOrNilForKey:kPRDItemDiscountGroup fromDictionary:dict];
            self.message3 = [self objectOrNilForKey:kPRDItemMessage3 fromDictionary:dict];
            self.text1 = [self objectOrNilForKey:kPRDItemText1 fromDictionary:dict];
            self.unitPrice = [self objectOrNilForKey:kPRDItemUnitPrice fromDictionary:dict];
            self.baseUnitOfMeasure = [self objectOrNilForKey:kPRDItemBaseUnitOfMeasure fromDictionary:dict];
            self.userTimeStamp = [self objectOrNilForKey:kPRDItemUserTimeStamp fromDictionary:dict];
            self.syncDateTime = [self objectOrNilForKey:kPRDItemSyncDateTime fromDictionary:dict];
            self.itemDescription = [self objectOrNilForKey:kPRDItemDescription fromDictionary:dict];
            self.message4 = [self objectOrNilForKey:kPRDItemMessage4 fromDictionary:dict];
            self.isBatch = [self objectOrNilForKey:kPRDItemIsBatch fromDictionary:dict];
            self.itemCategory = [self objectOrNilForKey:kPRDItemItemCategory fromDictionary:dict];
            self.picture = [self objectOrNilForKey:kPRDItemPicture fromDictionary:dict];
            self.text5 = [self objectOrNilForKey:kPRDItemText5 fromDictionary:dict];
            self.discountPercentage = [self objectOrNilForKey:kPRDItemDiscountPercentage fromDictionary:dict];
            self.status = [self objectOrNilForKey:kPRDItemStatus fromDictionary:dict];
            self.text4 = [self objectOrNilForKey:kPRDItemText4 fromDictionary:dict];
            self.toSync = [self objectOrNilForKey:kPRDItemToSync fromDictionary:dict];
            self.productSubGroupCode = [self objectOrNilForKey:kPRDItemProductSubGroupCode fromDictionary:dict];
            self.description2 = [self objectOrNilForKey:kPRDItemDescription2 fromDictionary:dict];

    }
    
    return self;
    
}

///Create a dictionary from model class object property

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.message1 forKey:kPRDItemMessage1];
    [mutableDict setValue:self.item1 forKey:kPRDItemItem1];
    [mutableDict setValue:self.text3 forKey:kPRDItemText3];
    [mutableDict setValue:self.message5 forKey:kPRDItemMessage5];
    [mutableDict setValue:self.isBonus forKey:kPRDItemIsBonus];
    [mutableDict setValue:self.message2 forKey:kPRDItemMessage2];
    [mutableDict setValue:self.stockOnHand forKey:kPRDItemStockOnHand];
    [mutableDict setValue:self.text2 forKey:kPRDItemText2];
    [mutableDict setValue:self.synced forKey:kPRDItemSynced];
    [mutableDict setValue:self.salesUnitOfMeasure forKey:kPRDItemSalesUnitOfMeasure];
    [mutableDict setValue:self.recordUpdated forKey:kPRDItemRecordUpdated];
    [mutableDict setValue:self.gSTProductPostingGroup forKey:kPRDItemGSTProductPostingGroup];
    [mutableDict setValue:self.productGroup forKey:kPRDItemProductGroup];
    [mutableDict setValue:self.discountGroup forKey:kPRDItemDiscountGroup];
    [mutableDict setValue:self.message3 forKey:kPRDItemMessage3];
    [mutableDict setValue:self.text1 forKey:kPRDItemText1];
    [mutableDict setValue:self.unitPrice forKey:kPRDItemUnitPrice];
    [mutableDict setValue:self.baseUnitOfMeasure forKey:kPRDItemBaseUnitOfMeasure];
    [mutableDict setValue:self.userTimeStamp forKey:kPRDItemUserTimeStamp];
    [mutableDict setValue:self.syncDateTime forKey:kPRDItemSyncDateTime];
    [mutableDict setValue:self.itemDescription forKey:kPRDItemDescription];
    [mutableDict setValue:self.message4 forKey:kPRDItemMessage4];
    [mutableDict setValue:self.isBatch forKey:kPRDItemIsBatch];
    [mutableDict setValue:self.itemCategory forKey:kPRDItemItemCategory];
    [mutableDict setValue:self.picture forKey:kPRDItemPicture];
    [mutableDict setValue:self.text5 forKey:kPRDItemText5];
    [mutableDict setValue:self.discountPercentage forKey:kPRDItemDiscountPercentage];
    [mutableDict setValue:self.status forKey:kPRDItemStatus];
    [mutableDict setValue:self.text4 forKey:kPRDItemText4];
    [mutableDict setValue:self.toSync forKey:kPRDItemToSync];
    [mutableDict setValue:self.productSubGroupCode forKey:kPRDItemProductSubGroupCode];
    [mutableDict setValue:self.description2 forKey:kPRDItemDescription2];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.message1 = [aDecoder decodeObjectForKey:kPRDItemMessage1];
    self.item1 = [aDecoder decodeObjectForKey:kPRDItemItem1];
    self.text3 = [aDecoder decodeObjectForKey:kPRDItemText3];
    self.message5 = [aDecoder decodeObjectForKey:kPRDItemMessage5];
    self.isBonus = [aDecoder decodeObjectForKey:kPRDItemIsBonus];
    self.message2 = [aDecoder decodeObjectForKey:kPRDItemMessage2];
    self.stockOnHand = [aDecoder decodeObjectForKey:kPRDItemStockOnHand];
    self.text2 = [aDecoder decodeObjectForKey:kPRDItemText2];
    self.synced = [aDecoder decodeObjectForKey:kPRDItemSynced];
    self.salesUnitOfMeasure = [aDecoder decodeObjectForKey:kPRDItemSalesUnitOfMeasure];
    self.recordUpdated = [aDecoder decodeObjectForKey:kPRDItemRecordUpdated];
    self.gSTProductPostingGroup = [aDecoder decodeObjectForKey:kPRDItemGSTProductPostingGroup];
    self.productGroup = [aDecoder decodeObjectForKey:kPRDItemProductGroup];
    self.discountGroup = [aDecoder decodeObjectForKey:kPRDItemDiscountGroup];
    self.message3 = [aDecoder decodeObjectForKey:kPRDItemMessage3];
    self.text1 = [aDecoder decodeObjectForKey:kPRDItemText1];
    self.unitPrice = [aDecoder decodeObjectForKey:kPRDItemUnitPrice];
    self.baseUnitOfMeasure = [aDecoder decodeObjectForKey:kPRDItemBaseUnitOfMeasure];
    self.userTimeStamp = [aDecoder decodeObjectForKey:kPRDItemUserTimeStamp];
    self.syncDateTime = [aDecoder decodeObjectForKey:kPRDItemSyncDateTime];
    self.itemDescription = [aDecoder decodeObjectForKey:kPRDItemDescription];
    self.message4 = [aDecoder decodeObjectForKey:kPRDItemMessage4];
    self.isBatch = [aDecoder decodeObjectForKey:kPRDItemIsBatch];
    self.itemCategory = [aDecoder decodeObjectForKey:kPRDItemItemCategory];
    self.picture = [aDecoder decodeObjectForKey:kPRDItemPicture];
    self.text5 = [aDecoder decodeObjectForKey:kPRDItemText5];
    self.discountPercentage = [aDecoder decodeObjectForKey:kPRDItemDiscountPercentage];
    self.status = [aDecoder decodeObjectForKey:kPRDItemStatus];
    self.text4 = [aDecoder decodeObjectForKey:kPRDItemText4];
    self.toSync = [aDecoder decodeObjectForKey:kPRDItemToSync];
    self.productSubGroupCode = [aDecoder decodeObjectForKey:kPRDItemProductSubGroupCode];
    self.description2 = [aDecoder decodeObjectForKey:kPRDItemDescription2];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_message1 forKey:kPRDItemMessage1];
    [aCoder encodeObject:_item1 forKey:kPRDItemItem1];
    [aCoder encodeObject:_text3 forKey:kPRDItemText3];
    [aCoder encodeObject:_message5 forKey:kPRDItemMessage5];
    [aCoder encodeObject:_isBonus forKey:kPRDItemIsBonus];
    [aCoder encodeObject:_message2 forKey:kPRDItemMessage2];
    [aCoder encodeObject:_stockOnHand forKey:kPRDItemStockOnHand];
    [aCoder encodeObject:_text2 forKey:kPRDItemText2];
    [aCoder encodeObject:_synced forKey:kPRDItemSynced];
    [aCoder encodeObject:_salesUnitOfMeasure forKey:kPRDItemSalesUnitOfMeasure];
    [aCoder encodeObject:_recordUpdated forKey:kPRDItemRecordUpdated];
    [aCoder encodeObject:_gSTProductPostingGroup forKey:kPRDItemGSTProductPostingGroup];
    [aCoder encodeObject:_productGroup forKey:kPRDItemProductGroup];
    [aCoder encodeObject:_discountGroup forKey:kPRDItemDiscountGroup];
    [aCoder encodeObject:_message3 forKey:kPRDItemMessage3];
    [aCoder encodeObject:_text1 forKey:kPRDItemText1];
    [aCoder encodeObject:_unitPrice forKey:kPRDItemUnitPrice];
    [aCoder encodeObject:_baseUnitOfMeasure forKey:kPRDItemBaseUnitOfMeasure];
    [aCoder encodeObject:_userTimeStamp forKey:kPRDItemUserTimeStamp];
    [aCoder encodeObject:_syncDateTime forKey:kPRDItemSyncDateTime];
    [aCoder encodeObject:_itemDescription forKey:kPRDItemDescription];
    [aCoder encodeObject:_message4 forKey:kPRDItemMessage4];
    [aCoder encodeObject:_isBatch forKey:kPRDItemIsBatch];
    [aCoder encodeObject:_itemCategory forKey:kPRDItemItemCategory];
    [aCoder encodeObject:_picture forKey:kPRDItemPicture];
    [aCoder encodeObject:_text5 forKey:kPRDItemText5];
    [aCoder encodeObject:_discountPercentage forKey:kPRDItemDiscountPercentage];
    [aCoder encodeObject:_status forKey:kPRDItemStatus];
    [aCoder encodeObject:_text4 forKey:kPRDItemText4];
    [aCoder encodeObject:_toSync forKey:kPRDItemToSync];
    [aCoder encodeObject:_productSubGroupCode forKey:kPRDItemProductSubGroupCode];
    [aCoder encodeObject:_description2 forKey:kPRDItemDescription2];
}

- (id)copyWithZone:(NSZone *)zone
{
    PRDItem *copy = [[PRDItem alloc] init];
    
    if (copy) {

        copy.message1 = [self.message1 copyWithZone:zone];
        copy.item1 = [self.item1 copyWithZone:zone];
        copy.text3 = [self.text3 copyWithZone:zone];
        copy.message5 = [self.message5 copyWithZone:zone];
        copy.isBonus = [self.isBonus copyWithZone:zone];
        copy.message2 = [self.message2 copyWithZone:zone];
        copy.stockOnHand = [self.stockOnHand copyWithZone:zone];
        copy.text2 = [self.text2 copyWithZone:zone];
        copy.synced = [self.synced copyWithZone:zone];
        copy.salesUnitOfMeasure = [self.salesUnitOfMeasure copyWithZone:zone];
        copy.recordUpdated = [self.recordUpdated copyWithZone:zone];
        copy.gSTProductPostingGroup = [self.gSTProductPostingGroup copyWithZone:zone];
        copy.productGroup = [self.productGroup copyWithZone:zone];
        copy.discountGroup = [self.discountGroup copyWithZone:zone];
        copy.message3 = [self.message3 copyWithZone:zone];
        copy.text1 = [self.text1 copyWithZone:zone];
        copy.unitPrice = [self.unitPrice copyWithZone:zone];
        copy.baseUnitOfMeasure = [self.baseUnitOfMeasure copyWithZone:zone];
        copy.userTimeStamp = [self.userTimeStamp copyWithZone:zone];
        copy.syncDateTime = [self.syncDateTime copyWithZone:zone];
        copy.itemDescription = [self.itemDescription copyWithZone:zone];
        copy.message4 = [self.message4 copyWithZone:zone];
        copy.isBatch = [self.isBatch copyWithZone:zone];
        copy.itemCategory = [self.itemCategory copyWithZone:zone];
        copy.picture = [self.picture copyWithZone:zone];
        copy.text5 = [self.text5 copyWithZone:zone];
        copy.discountPercentage = [self.discountPercentage copyWithZone:zone];
        copy.status = [self.status copyWithZone:zone];
        copy.text4 = [self.text4 copyWithZone:zone];
        copy.toSync = [self.toSync copyWithZone:zone];
        copy.productSubGroupCode = [self.productSubGroupCode copyWithZone:zone];
        copy.description2 = [self.description2 copyWithZone:zone];
    }
    
    return copy;
}


@end

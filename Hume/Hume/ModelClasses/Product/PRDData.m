//
//  PRDData.m
//
//  Created by User  on 1/27/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "PRDData.h"
#import "PRDItem.h"
#import "PRDPromoDiscount.h"


NSString *const kPRDDataItem = @"Item";
NSString *const kPRDDataPromoDiscount = @"PromoDiscount";
NSString *const kPRDDataPromoMultiBuy = @"PromoMultiBuy";
NSString *const kPRDDataPromoUpsell = @"PromoUpsell";
NSString *const kPRDDataPromoReward = @"PromoReward";


@interface PRDData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PRDData

@synthesize item = _item;
@synthesize promoDiscount = _promoDiscount;
@synthesize promoMultiBuy = _promoMultiBuy;
@synthesize promoUpsell = _promoUpsell;
@synthesize promoReward = _promoReward;

///Creating and returning model object from dictionary.
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}


///Setting model class property from dictionary

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.item = [PRDItem modelObjectWithDictionary:[dict objectForKey:kPRDDataItem]];
    NSObject *receivedPRDPromoDiscount = [dict objectForKey:kPRDDataPromoDiscount];
    NSMutableArray *parsedPRDPromoDiscount = [NSMutableArray array];
    if ([receivedPRDPromoDiscount isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedPRDPromoDiscount) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedPRDPromoDiscount addObject:[PRDPromoDiscount modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedPRDPromoDiscount isKindOfClass:[NSDictionary class]]) {
       [parsedPRDPromoDiscount addObject:[PRDPromoDiscount modelObjectWithDictionary:(NSDictionary *)receivedPRDPromoDiscount]];
    }

    self.promoDiscount = [NSArray arrayWithArray:parsedPRDPromoDiscount];
            self.promoMultiBuy = [self objectOrNilForKey:kPRDDataPromoMultiBuy fromDictionary:dict];
            self.promoUpsell = [self objectOrNilForKey:kPRDDataPromoUpsell fromDictionary:dict];
            self.promoReward = [self objectOrNilForKey:kPRDDataPromoReward fromDictionary:dict];

    }
    
    return self;
    
}

///Create a dictionary from model class object property

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.item dictionaryRepresentation] forKey:kPRDDataItem];
    NSMutableArray *tempArrayForPromoDiscount = [NSMutableArray array];
    for (NSObject *subArrayObject in self.promoDiscount) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForPromoDiscount addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForPromoDiscount addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForPromoDiscount] forKey:kPRDDataPromoDiscount];
    NSMutableArray *tempArrayForPromoMultiBuy = [NSMutableArray array];
    for (NSObject *subArrayObject in self.promoMultiBuy) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForPromoMultiBuy addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForPromoMultiBuy addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForPromoMultiBuy] forKey:kPRDDataPromoMultiBuy];
    NSMutableArray *tempArrayForPromoUpsell = [NSMutableArray array];
    for (NSObject *subArrayObject in self.promoUpsell) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForPromoUpsell addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForPromoUpsell addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForPromoUpsell] forKey:kPRDDataPromoUpsell];
    NSMutableArray *tempArrayForPromoReward = [NSMutableArray array];
    for (NSObject *subArrayObject in self.promoReward) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForPromoReward addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForPromoReward addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForPromoReward] forKey:kPRDDataPromoReward];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.item = [aDecoder decodeObjectForKey:kPRDDataItem];
    self.promoDiscount = [aDecoder decodeObjectForKey:kPRDDataPromoDiscount];
    self.promoMultiBuy = [aDecoder decodeObjectForKey:kPRDDataPromoMultiBuy];
    self.promoUpsell = [aDecoder decodeObjectForKey:kPRDDataPromoUpsell];
    self.promoReward = [aDecoder decodeObjectForKey:kPRDDataPromoReward];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_item forKey:kPRDDataItem];
    [aCoder encodeObject:_promoDiscount forKey:kPRDDataPromoDiscount];
    [aCoder encodeObject:_promoMultiBuy forKey:kPRDDataPromoMultiBuy];
    [aCoder encodeObject:_promoUpsell forKey:kPRDDataPromoUpsell];
    [aCoder encodeObject:_promoReward forKey:kPRDDataPromoReward];
}

- (id)copyWithZone:(NSZone *)zone
{
    PRDData *copy = [[PRDData alloc] init];
    
    if (copy) {

        copy.item = [self.item copyWithZone:zone];
        copy.promoDiscount = [self.promoDiscount copyWithZone:zone];
        copy.promoMultiBuy = [self.promoMultiBuy copyWithZone:zone];
        copy.promoUpsell = [self.promoUpsell copyWithZone:zone];
        copy.promoReward = [self.promoReward copyWithZone:zone];
    }
    
    return copy;
}


@end

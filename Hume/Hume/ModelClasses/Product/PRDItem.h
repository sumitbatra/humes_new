//
//  PRDItem.h
//
//  Created by User  on 1/27/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class: PRDItem Model object class
 @brief: A PRDItem data class with property
 - id message1;
 
 - NSString *item1;
 
 - id text3;
 
 - id message5;
 
 - id isBonus;
 
 - id message2;
 
 - id stockOnHand;
 
 - id text2;
 
 - id synced;
 
 - NSString *salesUnitOfMeasure;
 
 - id recordUpdated;
 
 - NSString *gSTProductPostingGroup;
 
 - NSString *productGroup;
 
 - NSString *discountGroup;
 
 - id message3;
 
 - id text1;
 
 - NSString *unitPrice;
 
 - NSString *baseUnitOfMeasure;
 
 - id userTimeStamp;
 
 - id syncDateTime;
 
 - NSString *itemDescription;
 
 - id message4;
 
 - id isBatch;
 
 - NSString *itemCategory;
 
 - id picture;
 
 - id text5;
 
 - id discountPercentage;
 
 - id status;
 
 - id text4;
 
 - id toSync;
 
 - id productSubGroupCode;
 
 - NSString *description2; 
 
 @discussion: Can perform given below operation using function

 @function:
 modelObjectWithDictionary
 - Return a model object with all available property
 
 initWithDictionary
 - Setting property from dictionary
 
 dictionaryRepresentation
 - Distionary reprensentaion from model object.
 */


@interface PRDItem : NSObject <NSCoding, NSCopying>

//Properties
@property (nonatomic, strong) id message1;
@property (nonatomic, strong) NSString *item1;
@property (nonatomic, strong) id text3;
@property (nonatomic, strong) id message5;
@property (nonatomic, strong) id isBonus;
@property (nonatomic, strong) id message2;
@property (nonatomic, strong) id stockOnHand;
@property (nonatomic, strong) id text2;
@property (nonatomic, strong) id synced;
@property (nonatomic, strong) NSString *salesUnitOfMeasure;
@property (nonatomic, strong) id recordUpdated;
@property (nonatomic, strong) NSString *gSTProductPostingGroup;
@property (nonatomic, strong) NSString *productGroup;
@property (nonatomic, strong) NSString *discountGroup;
@property (nonatomic, strong) id message3;
@property (nonatomic, strong) id text1;
@property (nonatomic, strong) NSString *unitPrice;
@property (nonatomic, strong) NSString *baseUnitOfMeasure;
@property (nonatomic, strong) id userTimeStamp;
@property (nonatomic, strong) id syncDateTime;
@property (nonatomic, strong) NSString *itemDescription;
@property (nonatomic, strong) id message4;
@property (nonatomic, strong) id isBatch;
@property (nonatomic, strong) NSString *itemCategory;
@property (nonatomic, strong) id picture;
@property (nonatomic, strong) id text5;
@property (nonatomic, strong) id discountPercentage;
@property (nonatomic, strong) id status;
@property (nonatomic, strong) id text4;
@property (nonatomic, strong) id toSync;
@property (nonatomic, strong) id productSubGroupCode;
@property (nonatomic, strong) NSString *description2;

//Methods
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

//
//  PRDPromoDiscount.m
//
//  Created by User  on 1/27/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "PRDPromoDiscount.h"


NSString *const kPRDPromoDiscountDiscountPercentage = @"DiscountPercentage";
NSString *const kPRDPromoDiscountMinBuyQty = @"MinBuyQty";
NSString *const kPRDPromoDiscountMinPurchaseAmount = @"MinPurchaseAmount";
NSString *const kPRDPromoDiscountItemDescription = @"ItemDescription";
NSString *const kPRDPromoDiscountRewardPoints = @"RewardPoints";
NSString *const kPRDPromoDiscountPicture = @"Picture";
NSString *const kPRDPromoDiscountItemCode = @"ItemCode";
NSString *const kPRDPromoDiscountOfferQty = @"OfferQty";
NSString *const kPRDPromoDiscountUpsellDescription = @"UpsellDescription";
NSString *const kPRDPromoDiscountPerQuantity = @"PerQuantity";
NSString *const kPRDPromoDiscountOfferItemDescription = @"OfferItemDescription";
NSString *const kPRDPromoDiscountOfferItemCode = @"OfferItemCode";


@interface PRDPromoDiscount ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PRDPromoDiscount

@synthesize discountPercentage = _discountPercentage;
@synthesize minBuyQty = _minBuyQty;
@synthesize minPurchaseAmount = _minPurchaseAmount;
@synthesize itemDescription = _itemDescription;
@synthesize rewardPoints = _rewardPoints;
@synthesize picture = _picture;
@synthesize itemCode = _itemCode;
@synthesize offerQty = _offerQty;
@synthesize upsellDescription = _upsellDescription;
@synthesize perQuantity = _perQuantity;
@synthesize offerItemDescription = _offerItemDescription;
@synthesize offerItemCode = _offerItemCode;

///Creating and returning model object from dictionary.
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}


///Setting model class property from dictionary

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.discountPercentage = [self objectOrNilForKey:kPRDPromoDiscountDiscountPercentage fromDictionary:dict];
            self.minBuyQty = [self objectOrNilForKey:kPRDPromoDiscountMinBuyQty fromDictionary:dict];
            self.minPurchaseAmount = [self objectOrNilForKey:kPRDPromoDiscountMinPurchaseAmount fromDictionary:dict];
            self.itemDescription = [self objectOrNilForKey:kPRDPromoDiscountItemDescription fromDictionary:dict];
            self.rewardPoints = [self objectOrNilForKey:kPRDPromoDiscountRewardPoints fromDictionary:dict];
            self.picture = [self objectOrNilForKey:kPRDPromoDiscountPicture fromDictionary:dict];
            self.itemCode = [self objectOrNilForKey:kPRDPromoDiscountItemCode fromDictionary:dict];
            self.offerQty = [self objectOrNilForKey:kPRDPromoDiscountOfferQty fromDictionary:dict];
            self.upsellDescription = [self objectOrNilForKey:kPRDPromoDiscountUpsellDescription fromDictionary:dict];
            self.perQuantity = [self objectOrNilForKey:kPRDPromoDiscountPerQuantity fromDictionary:dict];
            self.offerItemDescription = [self objectOrNilForKey:kPRDPromoDiscountOfferItemDescription fromDictionary:dict];
            self.offerItemCode = [self objectOrNilForKey:kPRDPromoDiscountOfferItemCode fromDictionary:dict];

    }
    
    return self;
    
}

///Create a dictionary from model class object property

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.discountPercentage forKey:kPRDPromoDiscountDiscountPercentage];
    [mutableDict setValue:self.minBuyQty forKey:kPRDPromoDiscountMinBuyQty];
    [mutableDict setValue:self.minPurchaseAmount forKey:kPRDPromoDiscountMinPurchaseAmount];
    [mutableDict setValue:self.itemDescription forKey:kPRDPromoDiscountItemDescription];
    [mutableDict setValue:self.rewardPoints forKey:kPRDPromoDiscountRewardPoints];
    [mutableDict setValue:self.picture forKey:kPRDPromoDiscountPicture];
    [mutableDict setValue:self.itemCode forKey:kPRDPromoDiscountItemCode];
    [mutableDict setValue:self.offerQty forKey:kPRDPromoDiscountOfferQty];
    [mutableDict setValue:self.upsellDescription forKey:kPRDPromoDiscountUpsellDescription];
    [mutableDict setValue:self.perQuantity forKey:kPRDPromoDiscountPerQuantity];
    [mutableDict setValue:self.offerItemDescription forKey:kPRDPromoDiscountOfferItemDescription];
    [mutableDict setValue:self.offerItemCode forKey:kPRDPromoDiscountOfferItemCode];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.discountPercentage = [aDecoder decodeObjectForKey:kPRDPromoDiscountDiscountPercentage];
    self.minBuyQty = [aDecoder decodeObjectForKey:kPRDPromoDiscountMinBuyQty];
    self.minPurchaseAmount = [aDecoder decodeObjectForKey:kPRDPromoDiscountMinPurchaseAmount];
    self.itemDescription = [aDecoder decodeObjectForKey:kPRDPromoDiscountItemDescription];
    self.rewardPoints = [aDecoder decodeObjectForKey:kPRDPromoDiscountRewardPoints];
    self.picture = [aDecoder decodeObjectForKey:kPRDPromoDiscountPicture];
    self.itemCode = [aDecoder decodeObjectForKey:kPRDPromoDiscountItemCode];
    self.offerQty = [aDecoder decodeObjectForKey:kPRDPromoDiscountOfferQty];
    self.upsellDescription = [aDecoder decodeObjectForKey:kPRDPromoDiscountUpsellDescription];
    self.perQuantity = [aDecoder decodeObjectForKey:kPRDPromoDiscountPerQuantity];
    self.offerItemDescription = [aDecoder decodeObjectForKey:kPRDPromoDiscountOfferItemDescription];
    self.offerItemCode = [aDecoder decodeObjectForKey:kPRDPromoDiscountOfferItemCode];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_discountPercentage forKey:kPRDPromoDiscountDiscountPercentage];
    [aCoder encodeObject:_minBuyQty forKey:kPRDPromoDiscountMinBuyQty];
    [aCoder encodeObject:_minPurchaseAmount forKey:kPRDPromoDiscountMinPurchaseAmount];
    [aCoder encodeObject:_itemDescription forKey:kPRDPromoDiscountItemDescription];
    [aCoder encodeObject:_rewardPoints forKey:kPRDPromoDiscountRewardPoints];
    [aCoder encodeObject:_picture forKey:kPRDPromoDiscountPicture];
    [aCoder encodeObject:_itemCode forKey:kPRDPromoDiscountItemCode];
    [aCoder encodeObject:_offerQty forKey:kPRDPromoDiscountOfferQty];
    [aCoder encodeObject:_upsellDescription forKey:kPRDPromoDiscountUpsellDescription];
    [aCoder encodeObject:_perQuantity forKey:kPRDPromoDiscountPerQuantity];
    [aCoder encodeObject:_offerItemDescription forKey:kPRDPromoDiscountOfferItemDescription];
    [aCoder encodeObject:_offerItemCode forKey:kPRDPromoDiscountOfferItemCode];
}

- (id)copyWithZone:(NSZone *)zone
{
    PRDPromoDiscount *copy = [[PRDPromoDiscount alloc] init];
    
    if (copy) {

        copy.discountPercentage = [self.discountPercentage copyWithZone:zone];
        copy.minBuyQty = [self.minBuyQty copyWithZone:zone];
        copy.minPurchaseAmount = [self.minPurchaseAmount copyWithZone:zone];
        copy.itemDescription = [self.itemDescription copyWithZone:zone];
        copy.rewardPoints = [self.rewardPoints copyWithZone:zone];
        copy.picture = [self.picture copyWithZone:zone];
        copy.itemCode = [self.itemCode copyWithZone:zone];
        copy.offerQty = [self.offerQty copyWithZone:zone];
        copy.upsellDescription = [self.upsellDescription copyWithZone:zone];
        copy.perQuantity = [self.perQuantity copyWithZone:zone];
        copy.offerItemDescription = [self.offerItemDescription copyWithZone:zone];
        copy.offerItemCode = [self.offerItemCode copyWithZone:zone];
    }
    
    return copy;
}


@end

//
//  PRDProductDetail.m
//
//  Created by User  on 1/27/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "PRDProductDetail.h"
#import "PRDData.h"


NSString *const kPRDProductDetailStatusCode = @"statusCode";
NSString *const kPRDProductDetailData = @"data";


@interface PRDProductDetail ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PRDProductDetail

@synthesize statusCode = _statusCode;
@synthesize data = _data;

///Creating and returning model object from dictionary.
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}


///Setting model class property from dictionary

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.statusCode = [[self objectOrNilForKey:kPRDProductDetailStatusCode fromDictionary:dict] doubleValue];
            self.data = [PRDData modelObjectWithDictionary:[dict objectForKey:kPRDProductDetailData]];

    }
    
    return self;
    
}

///Create a dictionary from model class object property

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.statusCode] forKey:kPRDProductDetailStatusCode];
    [mutableDict setValue:[self.data dictionaryRepresentation] forKey:kPRDProductDetailData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.statusCode = [aDecoder decodeDoubleForKey:kPRDProductDetailStatusCode];
    self.data = [aDecoder decodeObjectForKey:kPRDProductDetailData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_statusCode forKey:kPRDProductDetailStatusCode];
    [aCoder encodeObject:_data forKey:kPRDProductDetailData];
}

- (id)copyWithZone:(NSZone *)zone
{
    PRDProductDetail *copy = [[PRDProductDetail alloc] init];
    
    if (copy) {

        copy.statusCode = self.statusCode;
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end

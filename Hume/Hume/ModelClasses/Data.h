//
//  Data.h
//
//  Created by user  on 14/01/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Customerinfo;
/**
 @class: Data Model object class
 @brief: A Data data class with property
 
 - NSArray *shippingAddress;
 
 - Customerinfo *customerinfo;
 
 @discussion: Can perform given below operation using function

 @function:
 modelObjectWithDictionary
 - Return a model object with all available property
 
 initWithDictionary
 - Setting property from dictionary
 
 dictionaryRepresentation
 - Distionary reprensentaion from model object.
 */

@interface Data : NSObject <NSCoding>

//Properties
@property (nonatomic, strong) NSArray *shippingAddress;
@property (nonatomic, strong) Customerinfo *customerinfo;

//Methods
+ (Data *)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

//
//  Customerinfo.m
//
//  Created by user  on 14/01/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "Customerinfo.h"
#import "Utility.h"

NSString *const kCustomerinfoAddress2 = @"Address2";
NSString *const kCustomerinfoTelex = @"Telex";
NSString *const kCustomerinfoPostCode = @"PostCode";
NSString *const kCustomerinfoSynced = @"Synced";
NSString *const kCustomerinfoCity = @"City";
NSString *const kCustomerinfoRecordUpdated = @"RecordUpdated";
NSString *const kCustomerinfoPriceGroup = @"PriceGroup";
NSString *const kCustomerinfoPaymentTerms = @"PaymentTerms";
NSString *const kCustomerinfoCustomerDiscountGroup = @"CustomerDiscountGroup";
NSString *const kCustomerinfoName = @"Name";
NSString *const kCustomerinfoGSTBusinessPostingGroup = @"GSTBusinessPostingGroup";
NSString *const kCustomerinfoFax = @"Fax";
NSString *const kCustomerinfoState = @"State";
NSString *const kCustomerinfoAddress = @"Address";
NSString *const kCustomerinfoAmountDueGT30days = @"AmountDueGT30days";
NSString *const kCustomerinfoAmountDueGT60days = @"AmountDueGT60days";
NSString *const kCustomerinfoCreditLimit = @"CreditLimit";
NSString *const kCustomerinfoCountry = @"Country";
NSString *const kCustomerinfoEmail = @"Email";
NSString *const kCustomerinfoSyncDateTime = @"SyncDateTime";
NSString *const kCustomerinfoNo = @"No";
NSString *const kCustomerinfoPhoneNo = @"PhoneNo";
NSString *const kCustomerinfoShippingAdvice = @"ShippingAdvice";
NSString *const kCustomerinfoSalesPersonCode = @"SalesPersonCode";
NSString *const kCustomerinfoRewards = @"Rewards";
NSString *const kCustomerinfoContact = @"Contact";
NSString *const kCustomerinfoBalance = @"Balance";
NSString *const kCustomerinfoToSync = @"ToSync";
NSString *const kCustomerinfoAmountDueGT90days = @"AmountDueGT90days";
NSString *const kCustomerinfoAmountDue = @"AmountDue";


@interface Customerinfo ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Customerinfo

@synthesize address2 = _address2;
@synthesize telex = _telex;
@synthesize postCode = _postCode;
@synthesize synced = _synced;
@synthesize city = _city;
@synthesize recordUpdated = _recordUpdated;
@synthesize priceGroup = _priceGroup;
@synthesize paymentTerms = _paymentTerms;
@synthesize customerDiscountGroup = _customerDiscountGroup;
@synthesize name = _name;
@synthesize gSTBusinessPostingGroup = _gSTBusinessPostingGroup;
@synthesize fax = _fax;
@synthesize state = _state;
@synthesize address = _address;
@synthesize amountDueGT30days = _amountDueGT30days;
@synthesize amountDueGT60days = _amountDueGT60days;
@synthesize creditLimit = _creditLimit;
@synthesize country = _country;
@synthesize email = _email;
@synthesize syncDateTime = _syncDateTime;
@synthesize noProperty = _noProperty;
@synthesize phoneNo = _phoneNo;
@synthesize shippingAdvice = _shippingAdvice;
@synthesize salesPersonCode = _salesPersonCode;
@synthesize rewards = _rewards;
@synthesize contact = _contact;
@synthesize balance = _balance;
@synthesize toSync = _toSync;
@synthesize amountDueGT90days = _amountDueGT90days;
@synthesize amountDue = _amountDue;

///Creating and returning model object from dictionary.
+ (Customerinfo *)modelObjectWithDictionary:(NSDictionary *)dict
{
    Customerinfo *instance = [[Customerinfo alloc] initWithDictionary:[Utility parseNullValuesInDictionary:dict]];
    return instance;
}

///Setting model class property from dictionary

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.address2 = [self objectOrNilForKey:kCustomerinfoAddress2 fromDictionary:dict];
            self.telex = [self objectOrNilForKey:kCustomerinfoTelex fromDictionary:dict];
            self.postCode = [self objectOrNilForKey:kCustomerinfoPostCode fromDictionary:dict];
            self.synced = [self objectOrNilForKey:kCustomerinfoSynced fromDictionary:dict];
            self.city = [self objectOrNilForKey:kCustomerinfoCity fromDictionary:dict];
            self.recordUpdated = [self objectOrNilForKey:kCustomerinfoRecordUpdated fromDictionary:dict];
            self.priceGroup = [self objectOrNilForKey:kCustomerinfoPriceGroup fromDictionary:dict];
            self.paymentTerms = [self objectOrNilForKey:kCustomerinfoPaymentTerms fromDictionary:dict];
            self.customerDiscountGroup = [self objectOrNilForKey:kCustomerinfoCustomerDiscountGroup fromDictionary:dict];
            self.name = [self objectOrNilForKey:kCustomerinfoName fromDictionary:dict];
            self.gSTBusinessPostingGroup = [self objectOrNilForKey:kCustomerinfoGSTBusinessPostingGroup fromDictionary:dict];
            self.fax = [self objectOrNilForKey:kCustomerinfoFax fromDictionary:dict];
            self.state = [self objectOrNilForKey:kCustomerinfoState fromDictionary:dict];
            self.address = [self objectOrNilForKey:kCustomerinfoAddress fromDictionary:dict];
            self.amountDueGT30days = [self objectOrNilForKey:kCustomerinfoAmountDueGT30days fromDictionary:dict];
            self.amountDueGT60days = [self objectOrNilForKey:kCustomerinfoAmountDueGT60days fromDictionary:dict];
            self.creditLimit = [self objectOrNilForKey:kCustomerinfoCreditLimit fromDictionary:dict];
            self.country = [self objectOrNilForKey:kCustomerinfoCountry fromDictionary:dict];
            self.email = [self objectOrNilForKey:kCustomerinfoEmail fromDictionary:dict];
            self.syncDateTime = [self objectOrNilForKey:kCustomerinfoSyncDateTime fromDictionary:dict];
            self.noProperty = [self objectOrNilForKey:kCustomerinfoNo fromDictionary:dict];
            self.phoneNo = [self objectOrNilForKey:kCustomerinfoPhoneNo fromDictionary:dict];
            self.shippingAdvice = [self objectOrNilForKey:kCustomerinfoShippingAdvice fromDictionary:dict];
            self.salesPersonCode = [self objectOrNilForKey:kCustomerinfoSalesPersonCode fromDictionary:dict];
            self.rewards = [self objectOrNilForKey:kCustomerinfoRewards fromDictionary:dict];
            self.contact = [self objectOrNilForKey:kCustomerinfoContact fromDictionary:dict];
            self.balance = [self objectOrNilForKey:kCustomerinfoBalance fromDictionary:dict];
            self.toSync = [self objectOrNilForKey:kCustomerinfoToSync fromDictionary:dict];
            self.amountDueGT90days = [self objectOrNilForKey:kCustomerinfoAmountDueGT90days fromDictionary:dict];
            self.amountDue = [self objectOrNilForKey:kCustomerinfoAmountDue fromDictionary:dict];

    }
    
    return self;
    
}


///Create a dictionary from model class object property

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.address2 forKey:kCustomerinfoAddress2];
    [mutableDict setValue:self.telex forKey:kCustomerinfoTelex];
    [mutableDict setValue:self.postCode forKey:kCustomerinfoPostCode];
    [mutableDict setValue:self.synced forKey:kCustomerinfoSynced];
    [mutableDict setValue:self.city forKey:kCustomerinfoCity];
    [mutableDict setValue:self.recordUpdated forKey:kCustomerinfoRecordUpdated];
    [mutableDict setValue:self.priceGroup forKey:kCustomerinfoPriceGroup];
    [mutableDict setValue:self.paymentTerms forKey:kCustomerinfoPaymentTerms];
    [mutableDict setValue:self.customerDiscountGroup forKey:kCustomerinfoCustomerDiscountGroup];
    [mutableDict setValue:self.name forKey:kCustomerinfoName];
    [mutableDict setValue:self.gSTBusinessPostingGroup forKey:kCustomerinfoGSTBusinessPostingGroup];
    [mutableDict setValue:self.fax forKey:kCustomerinfoFax];
    [mutableDict setValue:self.state forKey:kCustomerinfoState];
    [mutableDict setValue:self.address forKey:kCustomerinfoAddress];
    [mutableDict setValue:self.amountDueGT30days forKey:kCustomerinfoAmountDueGT30days];
    [mutableDict setValue:self.amountDueGT60days forKey:kCustomerinfoAmountDueGT60days];
    [mutableDict setValue:self.creditLimit forKey:kCustomerinfoCreditLimit];
    [mutableDict setValue:self.country forKey:kCustomerinfoCountry];
    [mutableDict setValue:self.email forKey:kCustomerinfoEmail];
    [mutableDict setValue:self.syncDateTime forKey:kCustomerinfoSyncDateTime];
    [mutableDict setValue:self.noProperty forKey:kCustomerinfoNo];
    [mutableDict setValue:self.phoneNo forKey:kCustomerinfoPhoneNo];
    [mutableDict setValue:self.shippingAdvice forKey:kCustomerinfoShippingAdvice];
    [mutableDict setValue:self.salesPersonCode forKey:kCustomerinfoSalesPersonCode];
    [mutableDict setValue:self.rewards forKey:kCustomerinfoRewards];
    [mutableDict setValue:self.contact forKey:kCustomerinfoContact];
    [mutableDict setValue:self.balance forKey:kCustomerinfoBalance];
    [mutableDict setValue:self.toSync forKey:kCustomerinfoToSync];
    [mutableDict setValue:self.amountDueGT90days forKey:kCustomerinfoAmountDueGT90days];
    [mutableDict setValue:self.amountDue forKey:kCustomerinfoAmountDue];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.address2 = [aDecoder decodeObjectForKey:kCustomerinfoAddress2];
    self.telex = [aDecoder decodeObjectForKey:kCustomerinfoTelex];
    self.postCode = [aDecoder decodeObjectForKey:kCustomerinfoPostCode];
    self.synced = [aDecoder decodeObjectForKey:kCustomerinfoSynced];
    self.city = [aDecoder decodeObjectForKey:kCustomerinfoCity];
    self.recordUpdated = [aDecoder decodeObjectForKey:kCustomerinfoRecordUpdated];
    self.priceGroup = [aDecoder decodeObjectForKey:kCustomerinfoPriceGroup];
    self.paymentTerms = [aDecoder decodeObjectForKey:kCustomerinfoPaymentTerms];
    self.customerDiscountGroup = [aDecoder decodeObjectForKey:kCustomerinfoCustomerDiscountGroup];
    self.name = [aDecoder decodeObjectForKey:kCustomerinfoName];
    self.gSTBusinessPostingGroup = [aDecoder decodeObjectForKey:kCustomerinfoGSTBusinessPostingGroup];
    self.fax = [aDecoder decodeObjectForKey:kCustomerinfoFax];
    self.state = [aDecoder decodeObjectForKey:kCustomerinfoState];
    self.address = [aDecoder decodeObjectForKey:kCustomerinfoAddress];
    self.amountDueGT30days = [aDecoder decodeObjectForKey:kCustomerinfoAmountDueGT30days];
    self.amountDueGT60days = [aDecoder decodeObjectForKey:kCustomerinfoAmountDueGT60days];
    self.creditLimit = [aDecoder decodeObjectForKey:kCustomerinfoCreditLimit];
    self.country = [aDecoder decodeObjectForKey:kCustomerinfoCountry];
    self.email = [aDecoder decodeObjectForKey:kCustomerinfoEmail];
    self.syncDateTime = [aDecoder decodeObjectForKey:kCustomerinfoSyncDateTime];
    self.noProperty = [aDecoder decodeObjectForKey:kCustomerinfoNo];
    self.phoneNo = [aDecoder decodeObjectForKey:kCustomerinfoPhoneNo];
    self.shippingAdvice = [aDecoder decodeObjectForKey:kCustomerinfoShippingAdvice];
    self.salesPersonCode = [aDecoder decodeObjectForKey:kCustomerinfoSalesPersonCode];
    self.rewards = [aDecoder decodeObjectForKey:kCustomerinfoRewards];
    self.contact = [aDecoder decodeObjectForKey:kCustomerinfoContact];
    self.balance = [aDecoder decodeObjectForKey:kCustomerinfoBalance];
    self.toSync = [aDecoder decodeObjectForKey:kCustomerinfoToSync];
    self.amountDueGT90days = [aDecoder decodeObjectForKey:kCustomerinfoAmountDueGT90days];
    self.amountDue = [aDecoder decodeObjectForKey:kCustomerinfoAmountDue];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_address2 forKey:kCustomerinfoAddress2];
    [aCoder encodeObject:_telex forKey:kCustomerinfoTelex];
    [aCoder encodeObject:_postCode forKey:kCustomerinfoPostCode];
    [aCoder encodeObject:_synced forKey:kCustomerinfoSynced];
    [aCoder encodeObject:_city forKey:kCustomerinfoCity];
    [aCoder encodeObject:_recordUpdated forKey:kCustomerinfoRecordUpdated];
    [aCoder encodeObject:_priceGroup forKey:kCustomerinfoPriceGroup];
    [aCoder encodeObject:_paymentTerms forKey:kCustomerinfoPaymentTerms];
    [aCoder encodeObject:_customerDiscountGroup forKey:kCustomerinfoCustomerDiscountGroup];
    [aCoder encodeObject:_name forKey:kCustomerinfoName];
    [aCoder encodeObject:_gSTBusinessPostingGroup forKey:kCustomerinfoGSTBusinessPostingGroup];
    [aCoder encodeObject:_fax forKey:kCustomerinfoFax];
    [aCoder encodeObject:_state forKey:kCustomerinfoState];
    [aCoder encodeObject:_address forKey:kCustomerinfoAddress];
    [aCoder encodeObject:_amountDueGT30days forKey:kCustomerinfoAmountDueGT30days];
    [aCoder encodeObject:_amountDueGT60days forKey:kCustomerinfoAmountDueGT60days];
    [aCoder encodeObject:_creditLimit forKey:kCustomerinfoCreditLimit];
    [aCoder encodeObject:_country forKey:kCustomerinfoCountry];
    [aCoder encodeObject:_email forKey:kCustomerinfoEmail];
    [aCoder encodeObject:_syncDateTime forKey:kCustomerinfoSyncDateTime];
    [aCoder encodeObject:_noProperty forKey:kCustomerinfoNo];
    [aCoder encodeObject:_phoneNo forKey:kCustomerinfoPhoneNo];
    [aCoder encodeObject:_shippingAdvice forKey:kCustomerinfoShippingAdvice];
    [aCoder encodeObject:_salesPersonCode forKey:kCustomerinfoSalesPersonCode];
    [aCoder encodeObject:_rewards forKey:kCustomerinfoRewards];
    [aCoder encodeObject:_contact forKey:kCustomerinfoContact];
    [aCoder encodeObject:_balance forKey:kCustomerinfoBalance];
    [aCoder encodeObject:_toSync forKey:kCustomerinfoToSync];
    [aCoder encodeObject:_amountDueGT90days forKey:kCustomerinfoAmountDueGT90days];
    [aCoder encodeObject:_amountDue forKey:kCustomerinfoAmountDue];
}


@end

//
//  ShippingAddress.h
//
//  Created by user  on 14/01/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class: ShippingAddress Model object class
 @brief: A ShippingAddress data class with property
 
 - NSString *customerNo;
 
 - NSString *addressLine1;
 
 - NSString *postcode;
 
 - id addressLine2;
 
 - NSString *state;
 
 - id synced;
 
 - NSString *contactName;
 
 - id primary;
 
 - NSString *code;
 
 - NSString *phoneNo;
 
 - id toSync;
 
 - NSString *city;
 
 @discussion: Can perform given below operation using function

 @function:
 modelObjectWithDictionary
 - Return a model object with all available property
 
 initWithDictionary
 - Setting property from dictionary
 
 dictionaryRepresentation
 - Distionary reprensentaion from model object.
 */


@interface ShippingAddress : NSObject <NSCoding>

//Properties
@property (nonatomic, strong) NSString *customerNo;
@property (nonatomic, strong) NSString *addressLine1;
@property (nonatomic, strong) NSString *postcode;
@property (nonatomic, assign) id addressLine2;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, assign) id synced;
@property (nonatomic, strong) NSString *contactName;
@property (nonatomic, assign) id primary;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *phoneNo;
@property (nonatomic, assign) id toSync;
@property (nonatomic, strong) NSString *city;

//Methods
+ (ShippingAddress *)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

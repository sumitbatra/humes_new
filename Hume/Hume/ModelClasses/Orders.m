//
//  Data.m
//
//  Created by user  on 15/01/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "Orders.h"
#import "Utility.h"

NSString *const kDataInvoiceDiscountPercentage = @"InvoiceDiscountPercentage";
NSString *const kDataShipToCity = @"ShipToCity";
NSString *const kDataOrderNo = @"OrderNo";
NSString *const kDataCustomer = @"Customer";
NSString *const kDataSignatureImage = @"SignatureImage";
NSString *const kDataShipToState = @"ShipToState";
NSString *const kDataSynced = @"Synced";
NSString *const kDataComments = @"Comments";
NSString *const kDataReturnsToSalesReferenceNo = @"ReturnsToSalesReferenceNo";
NSString *const kDataType = @"Type";
NSString *const kDataShipToAddressLine2 = @"ShipToAddressLine2";
NSString *const kDataTotalExclGST = @"TotalExclGST";
NSString *const kDataOrderDate = @"OrderDate";
NSString *const kDataSalesperson = @"Salesperson";
NSString *const kDataShipmentDate = @"ShipmentDate";
NSString *const kDataOrderTime = @"OrderTime";
NSString *const kDataShipToAddressLine1 = @"ShipToAddressLine1";
NSString *const kDataSyncDateTime = @"SyncDateTime";
NSString *const kDataShipToPostcode = @"ShipToPostcode";
NSString *const kDataInvoiceDiscountAmount = @"InvoiceDiscountAmount";
NSString *const kDataShipToAddressCode = @"ShipToAddressCode";
NSString *const kDataShippingAdvice = @"ShippingAdvice";
NSString *const kDataShipToContactName = @"ShipToContactName";
NSString *const kDataShipToPhoneNo = @"ShipToPhoneNo";
NSString *const kDataStatus = @"Status";
NSString *const kDataGSTAmount = @"GSTAmount";
NSString *const kDataAmount = @"Amount";
NSString *const kDataAmountExclGST = @"AmountExclGST";
NSString *const kDataToSync = @"ToSync";
NSString *const kDataName = @"Name";

@interface Orders ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Orders

@synthesize invoiceDiscountPercentage = _invoiceDiscountPercentage;
@synthesize shipToCity = _shipToCity;
@synthesize orderNo = _orderNo;
@synthesize customer = _customer;
@synthesize signatureImage = _signatureImage;
@synthesize shipToState = _shipToState;
@synthesize synced = _synced;
@synthesize comments = _comments;
@synthesize returnsToSalesReferenceNo = _returnsToSalesReferenceNo;
@synthesize type = _type;
@synthesize shipToAddressLine2 = _shipToAddressLine2;
@synthesize totalExclGST = _totalExclGST;
@synthesize orderDate = _orderDate;
@synthesize salesperson = _salesperson;
@synthesize shipmentDate = _shipmentDate;
@synthesize orderTime = _orderTime;
@synthesize shipToAddressLine1 = _shipToAddressLine1;
@synthesize syncDateTime = _syncDateTime;
@synthesize shipToPostcode = _shipToPostcode;
@synthesize invoiceDiscountAmount = _invoiceDiscountAmount;
@synthesize shipToAddressCode = _shipToAddressCode;
@synthesize shippingAdvice = _shippingAdvice;
@synthesize shipToContactName = _shipToContactName;
@synthesize shipToPhoneNo = _shipToPhoneNo;
@synthesize status = _status;
@synthesize gSTAmount = _gSTAmount;
@synthesize amount = _amount;
@synthesize amountExclGST = _amountExclGST;
@synthesize toSync = _toSync;
@synthesize name = _name;

///Creating and returning model object from dictionary.
+ (Orders *)modelObjectWithDictionary:(NSDictionary *)dict
{
    Orders *instance = [[Orders alloc] initWithDictionary:[Utility parseNullValuesInDictionary:dict]];
    return instance;
}


///Setting model class property from dictionary

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.invoiceDiscountPercentage = [self objectOrNilForKey:kDataInvoiceDiscountPercentage fromDictionary:dict];
            self.shipToCity = [self objectOrNilForKey:kDataShipToCity fromDictionary:dict];
            self.orderNo = [self objectOrNilForKey:kDataOrderNo fromDictionary:dict];
            self.customer = [self objectOrNilForKey:kDataCustomer fromDictionary:dict];
            self.signatureImage = [self objectOrNilForKey:kDataSignatureImage fromDictionary:dict];
            self.shipToState = [self objectOrNilForKey:kDataShipToState fromDictionary:dict];
            self.synced = [[self objectOrNilForKey:kDataSynced fromDictionary:dict] boolValue];
            self.comments = [self objectOrNilForKey:kDataComments fromDictionary:dict];
            self.returnsToSalesReferenceNo = [self objectOrNilForKey:kDataReturnsToSalesReferenceNo fromDictionary:dict];
            self.type = [self objectOrNilForKey:kDataType fromDictionary:dict];
            self.shipToAddressLine2 = [self objectOrNilForKey:kDataShipToAddressLine2 fromDictionary:dict];
            self.totalExclGST = [self objectOrNilForKey:kDataTotalExclGST fromDictionary:dict];
            self.orderDate = [self objectOrNilForKey:kDataOrderDate fromDictionary:dict];
            self.salesperson = [self objectOrNilForKey:kDataSalesperson fromDictionary:dict];
            self.shipmentDate = [self objectOrNilForKey:kDataShipmentDate fromDictionary:dict];
            self.orderTime = [self objectOrNilForKey:kDataOrderTime fromDictionary:dict];
            self.shipToAddressLine1 = [self objectOrNilForKey:kDataShipToAddressLine1 fromDictionary:dict];
            self.syncDateTime = [self objectOrNilForKey:kDataSyncDateTime fromDictionary:dict];
            self.shipToPostcode = [self objectOrNilForKey:kDataShipToPostcode fromDictionary:dict];
            self.invoiceDiscountAmount = [self objectOrNilForKey:kDataInvoiceDiscountAmount fromDictionary:dict];
            self.shipToAddressCode = [self objectOrNilForKey:kDataShipToAddressCode fromDictionary:dict];
            self.shippingAdvice = [self objectOrNilForKey:kDataShippingAdvice fromDictionary:dict];
            self.shipToContactName = [self objectOrNilForKey:kDataShipToContactName fromDictionary:dict];
            self.shipToPhoneNo = [self objectOrNilForKey:kDataShipToPhoneNo fromDictionary:dict];
            self.status = [self objectOrNilForKey:kDataStatus fromDictionary:dict];
            self.gSTAmount = [self objectOrNilForKey:kDataGSTAmount fromDictionary:dict];
            self.amount = [self objectOrNilForKey:kDataAmount fromDictionary:dict];
            self.amountExclGST = [self objectOrNilForKey:kDataAmountExclGST fromDictionary:dict];
            self.toSync = [[self objectOrNilForKey:kDataToSync fromDictionary:dict] boolValue];
            self.name = [self objectOrNilForKey:kDataName fromDictionary:dict];
    }
    
    return self;
    
}

///Create a dictionary from model class object property

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.invoiceDiscountPercentage forKey:kDataInvoiceDiscountPercentage];
    [mutableDict setValue:self.shipToCity forKey:kDataShipToCity];
    [mutableDict setValue:self.orderNo forKey:kDataOrderNo];
    [mutableDict setValue:self.customer forKey:kDataCustomer];
    [mutableDict setValue:self.signatureImage forKey:kDataSignatureImage];
    [mutableDict setValue:self.shipToState forKey:kDataShipToState];
    [mutableDict setValue:[NSNumber numberWithBool:self.synced] forKey:kDataSynced];
    [mutableDict setValue:self.comments forKey:kDataComments];
    [mutableDict setValue:self.returnsToSalesReferenceNo forKey:kDataReturnsToSalesReferenceNo];
    [mutableDict setValue:self.type forKey:kDataType];
    [mutableDict setValue:self.shipToAddressLine2 forKey:kDataShipToAddressLine2];
    [mutableDict setValue:self.totalExclGST forKey:kDataTotalExclGST];
    [mutableDict setValue:self.orderDate forKey:kDataOrderDate];
    [mutableDict setValue:self.salesperson forKey:kDataSalesperson];
    [mutableDict setValue:self.shipmentDate forKey:kDataShipmentDate];
    [mutableDict setValue:self.orderTime forKey:kDataOrderTime];
    [mutableDict setValue:self.shipToAddressLine1 forKey:kDataShipToAddressLine1];
    [mutableDict setValue:self.syncDateTime forKey:kDataSyncDateTime];
    [mutableDict setValue:self.shipToPostcode forKey:kDataShipToPostcode];
    [mutableDict setValue:self.invoiceDiscountAmount forKey:kDataInvoiceDiscountAmount];
    [mutableDict setValue:self.shipToAddressCode forKey:kDataShipToAddressCode];
    [mutableDict setValue:self.shippingAdvice forKey:kDataShippingAdvice];
    [mutableDict setValue:self.shipToContactName forKey:kDataShipToContactName];
    [mutableDict setValue:self.shipToPhoneNo forKey:kDataShipToPhoneNo];
    [mutableDict setValue:self.status forKey:kDataStatus];
    [mutableDict setValue:self.gSTAmount forKey:kDataGSTAmount];
    [mutableDict setValue:self.amount forKey:kDataAmount];
    [mutableDict setValue:self.amountExclGST forKey:kDataAmountExclGST];
    [mutableDict setValue:[NSNumber numberWithBool:self.toSync] forKey:kDataToSync];
    [mutableDict setValue:self.name  forKey:kDataName];
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.invoiceDiscountPercentage = [aDecoder decodeObjectForKey:kDataInvoiceDiscountPercentage];
    self.shipToCity = [aDecoder decodeObjectForKey:kDataShipToCity];
    self.orderNo = [aDecoder decodeObjectForKey:kDataOrderNo];
    self.customer = [aDecoder decodeObjectForKey:kDataCustomer];
    self.signatureImage = [aDecoder decodeObjectForKey:kDataSignatureImage];
    self.shipToState = [aDecoder decodeObjectForKey:kDataShipToState];
    self.synced = [aDecoder decodeBoolForKey:kDataSynced];
    self.comments = [aDecoder decodeObjectForKey:kDataComments];
    self.returnsToSalesReferenceNo = [aDecoder decodeObjectForKey:kDataReturnsToSalesReferenceNo];
    self.type = [aDecoder decodeObjectForKey:kDataType];
    self.shipToAddressLine2 = [aDecoder decodeObjectForKey:kDataShipToAddressLine2];
    self.totalExclGST = [aDecoder decodeObjectForKey:kDataTotalExclGST];
    self.orderDate = [aDecoder decodeObjectForKey:kDataOrderDate];
    self.salesperson = [aDecoder decodeObjectForKey:kDataSalesperson];
    self.shipmentDate = [aDecoder decodeObjectForKey:kDataShipmentDate];
    self.orderTime = [aDecoder decodeObjectForKey:kDataOrderTime];
    self.shipToAddressLine1 = [aDecoder decodeObjectForKey:kDataShipToAddressLine1];
    self.syncDateTime = [aDecoder decodeObjectForKey:kDataSyncDateTime];
    self.shipToPostcode = [aDecoder decodeObjectForKey:kDataShipToPostcode];
    self.invoiceDiscountAmount = [aDecoder decodeObjectForKey:kDataInvoiceDiscountAmount];
    self.shipToAddressCode = [aDecoder decodeObjectForKey:kDataShipToAddressCode];
    self.shippingAdvice = [aDecoder decodeObjectForKey:kDataShippingAdvice];
    self.shipToContactName = [aDecoder decodeObjectForKey:kDataShipToContactName];
    self.shipToPhoneNo = [aDecoder decodeObjectForKey:kDataShipToPhoneNo];
    self.status = [aDecoder decodeObjectForKey:kDataStatus];
    self.gSTAmount = [aDecoder decodeObjectForKey:kDataGSTAmount];
    self.amount = [aDecoder decodeObjectForKey:kDataAmount];
    self.amountExclGST = [aDecoder decodeObjectForKey:kDataAmountExclGST];
    self.toSync = [aDecoder decodeBoolForKey:kDataToSync];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_invoiceDiscountPercentage forKey:kDataInvoiceDiscountPercentage];
    [aCoder encodeObject:_shipToCity forKey:kDataShipToCity];
    [aCoder encodeObject:_orderNo forKey:kDataOrderNo];
    [aCoder encodeObject:_customer forKey:kDataCustomer];
    [aCoder encodeObject:_signatureImage forKey:kDataSignatureImage];
    [aCoder encodeObject:_shipToState forKey:kDataShipToState];
    [aCoder encodeBool:_synced forKey:kDataSynced];
    [aCoder encodeObject:_comments forKey:kDataComments];
    [aCoder encodeObject:_returnsToSalesReferenceNo forKey:kDataReturnsToSalesReferenceNo];
    [aCoder encodeObject:_type forKey:kDataType];
    [aCoder encodeObject:_shipToAddressLine2 forKey:kDataShipToAddressLine2];
    [aCoder encodeObject:_totalExclGST forKey:kDataTotalExclGST];
    [aCoder encodeObject:_orderDate forKey:kDataOrderDate];
    [aCoder encodeObject:_salesperson forKey:kDataSalesperson];
    [aCoder encodeObject:_shipmentDate forKey:kDataShipmentDate];
    [aCoder encodeObject:_orderTime forKey:kDataOrderTime];
    [aCoder encodeObject:_shipToAddressLine1 forKey:kDataShipToAddressLine1];
    [aCoder encodeObject:_syncDateTime forKey:kDataSyncDateTime];
    [aCoder encodeObject:_shipToPostcode forKey:kDataShipToPostcode];
    [aCoder encodeObject:_invoiceDiscountAmount forKey:kDataInvoiceDiscountAmount];
    [aCoder encodeObject:_shipToAddressCode forKey:kDataShipToAddressCode];
    [aCoder encodeObject:_shippingAdvice forKey:kDataShippingAdvice];
    [aCoder encodeObject:_shipToContactName forKey:kDataShipToContactName];
    [aCoder encodeObject:_shipToPhoneNo forKey:kDataShipToPhoneNo];
    [aCoder encodeObject:_status forKey:kDataStatus];
    [aCoder encodeObject:_gSTAmount forKey:kDataGSTAmount];
    [aCoder encodeObject:_amount forKey:kDataAmount];
    [aCoder encodeObject:_amountExclGST forKey:kDataAmountExclGST];
    [aCoder encodeBool:_toSync forKey:kDataToSync];
}


@end

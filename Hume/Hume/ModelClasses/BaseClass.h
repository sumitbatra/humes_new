//
//  BaseClass.h
//
//  Created by user  on 14/01/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 @class: BaseClass Model object class
 @brief: A BaseClass data class with property
 
 - double statusCode;
 
 - Data *data;

 @discussion: Can perform given below operation using function

 @function:
 modelObjectWithDictionary
 - Return a model object with all available property
 
 initWithDictionary
 - Setting property from dictionary
 
 dictionaryRepresentation
 - Distionary reprensentaion from model object.
 */
@class Data;

@interface BaseClass : NSObject <NSCoding>

//Properties
@property (nonatomic, assign) double statusCode;
@property (nonatomic, strong) Data *data;

//Methods
+ (BaseClass *)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

//
//  HMBgView.m
//  Hume
//
//  Created by Juli on 26/11/15.
//  Copyright © 2015 Damco. All rights reserved.
//

#import "HMBgView.h"
#import "AppDelegate.h"

@implementation HMBgView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(id)initWithFrame:(CGRect)frame{
	
	self = [super initWithFrame:frame];
	if (self) {
		
	}
	return self;
	
}
/**
 @brief:Overdding default method with custem params like View background color.
 */
-(void)awakeFromNib{
	if ([APP_DELEGATE applicationTheameSetting] != nil){
		self.backgroundColor = [Utility colorFromHexString:[[APP_DELEGATE applicationTheameSetting] applicationBackground]];
	}
	else{
		self.backgroundColor = [Utility colorFromHexString: kapplicationBackground];
	}
	
}
@end

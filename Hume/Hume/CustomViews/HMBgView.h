//
//  HMBgView.h
//  Hume
//
//  Created by Juli on 26/11/15.
//  Copyright © 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 @brief: Custom BgView class created for setting up  custom background color for theming.
 Simple get update by passing backgound color Hex value
 @discussion: We can get background colors from server & update the default.
 */
@interface HMBgView : UIView

@end

//
//  HMTextField.h
//  Hume
//
//  Created by Juli on 26/11/15.
//  Copyright © 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
/**
 @brief: Custom TextField class created for setting up  custom background color for theming.
 Simple get update by passing backgound color Hex value
 @discussion: We can get background colors from server & update the default.
 */

@interface HMTextField : UITextField


@end

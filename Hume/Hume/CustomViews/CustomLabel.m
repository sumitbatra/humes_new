//
//  CustomLabel.m
//  Hume
//
//  Created by User on 12/29/14.
//  Copyright (c) 2014 Damco. All rights reserved.
//

#import "CustomLabel.h"

@implementation CustomLabel


-(void) awakeFromNib
{
    if (self.tag == TITLE_LABEL_TAG) {
        UIFont *customFont = [UIFont fontWithName:FONT_COND size:MY_ACCOUNT_TITLE_FONT_SIZE];
        self.font = customFont;
//        [self setFont:[UIFont fontWithName:FONT_REGULAR size:10]];
    }
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) {
        
    }
    return self;
}

/**
 @brief:Overdding default method with custem params like font name & font size.
 */
-(void) updateLabelsBasedOnTags
{
    if (self.tag == CELL_TEXT_LABEL_TAG) {
        UIFont *customFont = [UIFont fontWithName:FONT_COND size:MY_ACCOUNT_TITLE_FONT_SIZE];
        self.font = customFont;
        //        [self setFont:[UIFont fontWithName:FONT_REGULAR size:10]];
    }
}
@end

//
//  HMView.h
//  Hume
//
//  Created by Juli on 26/11/15.
//  Copyright © 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 @brief: Custom View class created for setting up  custom background color for theming.
 @discussion: We can get background colors from server & update the default.
 */
@interface HMView : UIView

@end

//
//  CustomLabel.h
//  Hume
//
//  Created by User on 12/29/14.
//  Copyright (c) 2014 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @brief: Custom label class created for setting up  custom font name & size
 @param: font name & font size using function updateLabelsBasedOnTags
 */

@interface CustomLabel : UILabel

-(void) updateLabelsBasedOnTags;

@end

//
//  HMButton.m
//  Hume
//
//  Created by Juli on 25/11/15.
//  Copyright © 2015 Damco. All rights reserved.
//

#import "HMButton.h"
#import "AppDelegate.h"

@implementation HMButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(id)initWithFrame:(CGRect)frame{
	
	self = [super initWithFrame:frame];
	if (self) {
		
	}
	return self;
	
}
/**
 @brief:Overdding default method with custem params like View background color.
 */
-(void)awakeFromNib{
	if ([APP_DELEGATE applicationTheameSetting] != nil){
		self.backgroundColor = [Utility colorFromHexString:[[APP_DELEGATE applicationTheameSetting] generalButtonsBackground]];
	}
	else{
		self.backgroundColor = [Utility colorFromHexString: kgeneralButtonsBackground];
	}

}

@end

//
//  HMButton.h
//  Hume
//
//  Created by Juli on 25/11/15.
//  Copyright © 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 @brief: Custom Button class created for setting up  custom background color.
 @discussion: We can get background colors from server & update the default.
 */

@interface HMButton : UIButton

@end

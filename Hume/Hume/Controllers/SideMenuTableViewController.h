//
//  SideMenuTableViewController.h
//  Hume
//
//  Created by User on 4/1/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @brief: SideMenuTableViewController will display side menu list with couple of options like Home, Change Password, Logout.
 Can perform selected operation like Change password, Logout, Back to home Page.
 RelativeViewcontrollers:ViewController, ChangePasswordViewController
 */

@interface SideMenuTableViewController : UITableViewController

@end

//
//  ViewController.h
//  Hume
//
//  Created by User on 12/29/14.
//  Copyright (c) 2014 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"

/**
 @brief: Viewcontroller can perform user authentication operation like(Login, forgotpassword, update url, new user registration).
 sucessfully login will move user to HomeViewcontroller.
 */
@interface ViewController : UIViewController{

}

//Properties
@property(nonatomic,weak)IBOutlet UITextField *txtFieldLogin;
@property(nonatomic,weak)IBOutlet UITextField *txtFieldPassword;
@property(nonatomic,weak)IBOutlet UIImageView *imgViewCheckBox;
@property(nonatomic,weak)IBOutlet UIButton *btnSignIn;
@property(nonatomic,weak)IBOutlet UIButton *btnForgotPassword;
@property(nonatomic,weak)IBOutlet UIImageView *imgViewApplogo;
@property(nonatomic,weak)IBOutlet UILabel *lblSignIn;

// Functions
-(IBAction)LoginBtnCall:(id)sender;
-(IBAction)ForgotBtnCall:(id)sender;
-(IBAction)RegisterBtnCall:(id)sender;
-(IBAction)openChangeUrlScreen:(id)sender;

@end


//
//  DiscountedProductTableViewController.h
//  Hume
//
//  Created by user on 05/02/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @class:DiscountedProductTableViewController will diaplay list of different type of products like discounted,Rewards,Multibuy products. etc.
 @property: a refernce table view, Caller class String type.
 */
@interface DiscountedProductTableViewController : UITableViewController
{
    
}
//Properties
@property(nonatomic,retain)IBOutlet UITableView *tableView;
@property(nonatomic,retain) NSString *strCallerName;
@end

//
//  DiscountedProductTableViewController.m
//  Hume
//
//  Created by user on 05/02/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "DiscountedProductTableViewController.h"
#import "HumeApiClient.h"
#import "APIResponse.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import "Constants.h"



@interface DiscountedProductTableViewController ()
{
    NSMutableArray *aryDicsountedOProductedList;
}
@end

@implementation DiscountedProductTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Private Methods

/**
 @brief: FetchDataForDiscountedProducts 
 @param: str(NSString)
 @discussion Get service call for getting all list of discounted product with callbackblock
 
 Success: handleSuccessResponseForDiscountedProducts
 
 Failure: handleFailureResponseDiscountedProducts
 */


//FIXME: WebService class should have wrappers on direct REST API methods
//FIXME: Request & Response should be handled by a Webservice class, only data should be provided to the calling class.

-(void)FetchDataForDiscountedProducts:(NSString *)str
{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [[HumeApiClient SharedClient]AddAccessTokenInHeader];
    //Fetching all discounted product list from server.
    [[HumeApiClient SharedClient] GET:kDiscountsList parameters:nil success:^(NSURLSessionDataTask *task, id ResponseObject)
     {
         //Success block
         NSLog(@"Discounted order: %@",ResponseObject);
         [self handleSuccessResponseForDiscountedProducts:ResponseObject];
         
     }failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         //Failure block
         NSLog(@"Discounted order: Failed");
         [self handleFailureResponseDiscountedProducts];
     }
     ];
    
    
}

/**
 @brief: handleSuccessResponseForDiscountedProducts
 @param object(id) 
 @discussion
 Is a success call back for FetchDataForDiscountedProducts
 Response1:update aryDicsountedOProductedList with latest data
 
 Response:0: show failure message
 */
- (void)handleSuccessResponseForDiscountedProducts:(id)object {
    
    // [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    APIResponse *response = [APIResponse modelObjecttWithDictionary:object];
    
    //OrderDetails *orderDetailObj = [OrderDetails modelObjectWithDictionary:response.data];
    if(response.isSuccess==1)
    {
        //After getting success adding all discounted product to array & refesh the tableview
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        aryDicsountedOProductedList = [NSMutableArray arrayWithArray:(NSMutableArray *)response.data];
        [self.tableView reloadData];
    }else{
        
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:response.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

/**
 @brief:Is a failure calback for FetchDataForDiscountedProducts
 */
- (void)handleFailureResponseDiscountedProducts
{
    
    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    [[[UIAlertView alloc] initWithTitle:@"" message:kStandardErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    return;
}



#pragma FETCHING DATA FROM SERVER FOR MultiBuy PRODUCTS
/**
 @brief: Will call server for getting multi buy list with complition
 
 Success: handleSuccessResponseForDiscountedProducts
 
 Failure: handleFailureResponseDiscountedProducts
 */
-(void)FetchDataForMultiBuyProducts
{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [[HumeApiClient SharedClient]AddAccessTokenInHeader];
    
    //Fetching all product list available in multibuy category.
    [[HumeApiClient SharedClient] GET:kMultiBuyList  parameters:nil success:^(NSURLSessionDataTask *task, id ResponseObject)
     {
         //Success block
         NSLog(@"Discounted order: %@",ResponseObject);
         [self handleSuccessResponseForDiscountedProducts:ResponseObject];
         
     }failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         //Failure block

         NSLog(@"Discounted order: Failed");
         [self handleFailureResponseDiscountedProducts];
     }
     ];
   
}

#pragma FETCHING DATA FROM SERVER FOR Rewards PRODUCTS
/**
 @brief: Will call server for getting rewards list with complition
 
 Success: handleSuccessResponseForDiscountedProducts
 
 Failure: handleFailureResponseDiscountedProducts
 */

//FIXME: WebService class should have wrappers on direct REST API methods
-(void)FetchDataForRewardsProducts
{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [[HumeApiClient SharedClient]AddAccessTokenInHeader];
    
    //Fetching all list of product related to rewads.
    [[HumeApiClient SharedClient] GET:kRewardsList parameters:nil success:^(NSURLSessionDataTask *task, id ResponseObject)
     {         //Success block

         NSLog(@"Discounted order: %@",ResponseObject);
         [self handleSuccessResponseForDiscountedProducts:ResponseObject];
         
     }failure:^(NSURLSessionDataTask *task, NSError *error)
     {         //Failure block

         NSLog(@"Discounted order: Failed");
         [self handleFailureResponseDiscountedProducts];
     }
     ];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
     return [aryDicsountedOProductedList count];
}


@end

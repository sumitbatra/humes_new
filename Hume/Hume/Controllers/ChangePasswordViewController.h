//
//  ChangePasswordViewController.h
//  Hume
//
//  Created by User on 28/09/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 @brief: ChangePasswordViewController will perform password update with passing
 Old & New password along with user Name  to server.
 */
@interface ChangePasswordViewController : UIViewController<UITextFieldDelegate>
{

IBOutlet UITextField *field_oldpwd,*field_cnfirmpwd,*field_newpwd;

}
@end

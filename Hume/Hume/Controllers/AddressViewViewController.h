//
//  AddressViewViewController.h
//  Hume
//
//  Created by user on 19/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import "APIResponse.h"
#import "Constants.h"
#import "HumeApiClient.h"

/**
 @brief: AddressViewViewController will perform different type of updation in address(Shipping address,Billing Address).
 Can Edit biulling & shipping address.
 Can Add new billing & shippiong address.
 Default Fields for Address:City, ContactName, State, PostCode, PhoneNumber etc.
 @property:
 - NSDictionary *dicData;
 
 - UILabel *lblTitle;
 
 - UIButton *btnMakePrimary;
 
 -UILabel *lblPrimary;
 
 - UITextField *txtAddressLine1;
 
 - UITextField *txtAddressLine2;
 
 - UITextField *txtCity;
 
 - UITextField *txtState;
 
 - UITextField *txtContactName;
 
 - UITextField *txtPhoneNo;
 
 - UITextField *txtPostCode;
 
 - UIButton *btnEdit;
 
 - UIButton *btnDelete;
 
 - UIButton *btnSave;
 
 - UIImageView *imgViewCheckMark;
 
 - NSString *callerViewName;
 
 - IBOutlet UIScrollView *scrollViewEdit;

 */
@interface AddressViewViewController : UIViewController<UITextFieldDelegate>
{
    
}

//Properties
@property(nonatomic,retain) NSDictionary *dicData;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnMakePrimary;
@property (weak, nonatomic) IBOutlet UILabel *lblPrimary;
@property (weak, nonatomic) IBOutlet UITextField *txtAddressLine1;
@property (weak, nonatomic) IBOutlet UITextField *txtAddressLine2;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtState;
@property (weak, nonatomic) IBOutlet UITextField *txtContactName;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNo;
@property (weak, nonatomic) IBOutlet UITextField *txtPostCode;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCheckMark;
@property (nonatomic,retain) NSString *callerViewName;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewEdit;

@end
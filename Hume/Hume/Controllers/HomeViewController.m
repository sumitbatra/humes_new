//
//  HomeViewController.m
//  Hume
//
//  Created by user on 06/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "HomeViewController.h"
#import "MyAccountTableViewController.h"
#import "OrdersViewControllerTableViewController.h"
#import "ProductListingViewController.h"
#import "UIButton+Position.h"
#import "PromotionTableViewController.h"
#import "PantryListTableViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "AppDelegate.h"


@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    if (IS_IPHONE_6P ) [self adjustOffsets:10];
    if (IS_IPHONE_5) [self adjustOffsets:30];
    if (IS_IPHONE_6) [self adjustOffsets:20];
    if (IS_IPAD) [self adjustOffsets:-30];
    
    [self setTheme];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = true;
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = false;
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.title = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Private Methods

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

//Update offsets for different sizes.
- (void)adjustOffsets:(NSInteger)space
{
    
    [btnMyAccount centerButtonAndImageWithSpacing:space];
    if (IS_IPAD) {
        [btnOrders centerButtonAndImageWithSpacing:0];
    }
    else {
        [btnOrders centerButtonAndImageWithSpacing:space];
    }
    if (IS_IPAD) {
        [btnProducts centerButtonAndImageWithSpacing:-10];
    }
    else {
        [btnProducts centerButtonAndImageWithSpacing:space];
    }
    [btnPromotions centerButtonAndImageWithSpacing:space];
    [btnPantryList centerButtonAndImageWithSpacing:space];
    
    
}

///Dynamic theme update
-(void)setTheme
{
	if ([APP_DELEGATE applicationTheameSetting] != nil){
	btnMyAccount.backgroundColor = [Utility colorFromHexString:[[APP_DELEGATE applicationTheameSetting] myaccount_button]];
	btnOrders.backgroundColor = [Utility colorFromHexString:[[APP_DELEGATE applicationTheameSetting] orders_button]];
	btnPantryList.backgroundColor = [Utility colorFromHexString:[[APP_DELEGATE applicationTheameSetting] template_button]];
	btnProducts.backgroundColor = [Utility colorFromHexString:[[APP_DELEGATE applicationTheameSetting] products_button]];
	btnPromotions.backgroundColor = [Utility colorFromHexString:[[APP_DELEGATE applicationTheameSetting] promotions_button]];
		
		NSData* imageData =   [[NSData alloc] initWithBase64EncodedString:[[APP_DELEGATE applicationTheameSetting] logoImageBinary] options:0];   //[[[APP_DELEGATE applicationTheameSetting] logoImageBinary] dataUsingEncoding:NSUTF8StringEncoding];
			//[btnLogo setBackgroundImage:[UIImage imageWithData:imageData] forState:UIControlStateNormal];
		imageLogo.image = [UIImage imageWithData:imageData];
		
			//UIImage(data: NSData(base64EncodedString: imageString!, options: NSDataBase64DecodingOptions())!)
		
	}
	else{
		btnMyAccount.backgroundColor = [Utility colorFromHexString:kmyaccount_button];
		btnOrders.backgroundColor = [Utility colorFromHexString:korders_button];
		btnPantryList.backgroundColor = [Utility colorFromHexString:ktemplate_button];
		btnProducts.backgroundColor = [Utility colorFromHexString:kproducts_button];
		btnPromotions.backgroundColor = [Utility colorFromHexString:kpromotions_button];
	
	}
}


/**
 @brief: MyAccountBtnCall with pram sender(id)
 Changing the root view controller from cheking setting action 
 with contoller MyAccountTableViewController
  */
-(IBAction)MyAccountBtnCall:(id)sender
{
    //Incorporate sliding view here
    
    MyAccountTableViewController *myAc;
    myAc = [[MyAccountTableViewController alloc] initWithNibName:@"MyAccountTableViewController" bundle:nil];
    
    UINavigationController *nvcontrol = [[UINavigationController alloc]initWithRootViewController:myAc];
    [nvcontrol.navigationBar setTranslucent:NO];
	
	if ([APP_DELEGATE applicationTheameSetting] != nil){
		nvcontrol.navigationBar.barTintColor = [Utility colorFromHexString:[[APP_DELEGATE applicationTheameSetting] navigationBarBackground]];
	}
	else{
		nvcontrol.navigationBar.barTintColor = [Utility colorFromHexString:knavigationBarBackground];
	}
		//nvcontrol.navigationBar.barTintColor = [UIColor colorWithRed:2.0/255.0 green:133.0/255.0 blue:62.0/255.0 alpha:1.0];
    nvcontrol.navigationBar.translucent = NO;
    
    SideMenuTableViewController *leftMenuViewController= [[SideMenuTableViewController alloc]init];
    [APP_DELEGATE setLeftMenuViewController:leftMenuViewController];
    
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:nvcontrol
                                                    leftMenuViewController:leftMenuViewController
                                                    rightMenuViewController:nil];
    
    
    [[APP_DELEGATE window] setRootViewController:container];
    
}

/**
 @brief:OrdersBtnCall with 
 push to a new OrdersViewControllerTableViewController
 @param: sender(id)

 */
-(IBAction)OrdersBtnCall:(id)sender
{
    OrdersViewControllerTableViewController *OrdersViewObj = [[OrdersViewControllerTableViewController alloc]initWithNibName:@"OrdersViewControllerTableViewController" bundle:nil];
    OrdersViewObj.orderSelectionMode = OrderSelectionAllOrders;
    [self.navigationController  pushViewController:OrdersViewObj animated:YES];
    
}

-(IBAction)ProductBtnCall:(id)sender
{
    ProductListingViewController *productsObj = [[ProductListingViewController alloc] initWithNibName:@"ProductListingViewController" bundle:nil];
    [self.navigationController pushViewController:productsObj animated:YES];
}

-(IBAction)PromotionsBtnCall:(id)sender
{
    
    PromotionTableViewController *promoObj = [[PromotionTableViewController alloc] init];
    [self.navigationController pushViewController:promoObj animated:YES];
    
}

-(IBAction)PantryListBtnCall:(id)sender
{
    
    PantryListTableViewController *pantryObj = [[PantryListTableViewController alloc] init];
    [self.navigationController pushViewController:pantryObj animated:YES];
}

@end

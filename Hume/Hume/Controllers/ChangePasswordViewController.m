//
//  ChangePasswordViewController.m
//  Hume
//
//  Created by User on 28/09/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "HumeApiClient.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import "APIResponse.h"
#import "Constants.h"



@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Change Password";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Private Methods


/*!
 @brief Update the old password with New one
 
 @discussion service call for update with (usrname, new , old, confirm password) complition Block
 Success will call handleSuccessResponseForChangePassword
 Failure Will call handleFailureResponseForChangePassword

 @param  (id)sender
 */

//FIXME: Request & Response should be handled by a Webservice class, only data should be provided to the calling class.
-(IBAction)Submit:(id)sender
{
    //Checking all fileds are filled
    if ([field_cnfirmpwd.text length] && [field_newpwd.text length] && [field_oldpwd.text length])
    {
        if([field_newpwd.text isEqualToString:field_cnfirmpwd.text])//New & confirm should be same
        {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            //Service call with default paremeter for updating a password
        [[HumeApiClient SharedClient] POST:@"user/ChangeUserPassword" parameters:@{@"UserName":[[NSUserDefaults standardUserDefaults]valueForKey:@"username"],@"CustomerNo":[[NSUserDefaults standardUserDefaults]valueForKey:CUSTOMERNOKEY],@"ExistingPassword":field_oldpwd.text,@"NewPassword":field_newpwd.text} success:^(NSURLSessionDataTask *task, id ResponseObject)
         {
             
             [self handleSuccessResponseForChangePassword:ResponseObject];
             
         }failure:^(NSURLSessionDataTask *task, NSError *error)//Failure block
         {
             NSLog(@"Failed");
             [self handleFailureResponseForChangePassword:error];
         }
         ];
        }
        
        else
        {
        [[[UIAlertView alloc] initWithTitle:@"" message:OLD_AND_NEW_PASS_DIFF delegate:nil  cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        }
      
    }else{
        [[[UIAlertView alloc] initWithTitle:@"" message:ALL_FIELDS_ARE_REQUIRED delegate:nil  cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
    
}

/**
 @brief: handleSuccessResponseForChangePassword call back success block with 
 
 @discussion Response Code:1 show password updated succ
 
 ResponseCode:0 Failed to update.
 
 @param: object(id)
 
 */


- (void)handleSuccessResponseForChangePassword:(id)object {
    
    APIResponse *response = [APIResponse modelObjecttWithDictionary:object];
    
    if(response.isSuccess==1){
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        [self.navigationController popViewControllerAnimated:TRUE];
        [[[UIAlertView alloc] initWithTitle:@"" message:PASS_CHANGED delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        
    }else{
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:response.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

/**
 @brief: handleFailureResponseForChangePassword call back failure block with param error(NSError)
Show Server Error to user
 
 @param: (NSError*)error
 */
- (void)handleFailureResponseForChangePassword:(NSError*)error {
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [[[UIAlertView alloc] initWithTitle:@"" message:kStandardErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    return;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


@end

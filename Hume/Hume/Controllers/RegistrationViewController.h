//
//  RegistrationViewController.h
//  Hume
//
//  Created by User on 5/18/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 @brief: A new user can register in application through RegistrationViewController. Just entering
 @property:
 - UIButton *btnMr;
 
 - UIButton *btnMrs;
 
 - UIButton *btnMiss;
 
 - UITextField *txtUserName;
 
 - UITextField *txtPassword;
 
 - UITextField *txtFirstName;
 
 - UITextField *txtLastName;
 
 - UITextField *txtCompanyName;
 
 - UITextField *txtADL1;
 
 - UITextField *txtADL2;
 
 - UITextField *txtCity;
 
 - UITextField *txtState;
 
 - UITextField *txtPostCode;
 
 - UITextField *txtEmail;
 
 - UITextField *txtPhoneNo;
 
 - UITextField *txtFaxNo;

 Can perform different validations over user entered fields.
 */
@interface RegistrationViewController : UIViewController<UIAlertViewDelegate>{
    
    IBOutlet UIButton *btnMr;
    IBOutlet UIButton *btnMrs;
    IBOutlet UIButton *btnMiss;

    IBOutlet UITextField *txtUserName;
    IBOutlet UITextField *txtPassword;
    IBOutlet UITextField *txtFirstName;
    IBOutlet UITextField *txtLastName;
    IBOutlet UITextField *txtCompanyName;
    IBOutlet UITextField *txtADL1;
    IBOutlet UITextField *txtADL2;
    IBOutlet UITextField *txtCity;
    IBOutlet UITextField *txtState;
    IBOutlet UITextField *txtPostCode;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtPhoneNo;
    IBOutlet UITextField *txtFaxNo;
    

}

//Properties
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewCust;

//Methods
-(IBAction)selectTitle:(id)sender;
-(IBAction)doneButton:(id)sender;
@end

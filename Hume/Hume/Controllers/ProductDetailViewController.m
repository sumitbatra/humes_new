//
//  ProductDetailViewController.m
//  Hume
//
//  Created by User on 1/27/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "Constants.h"
#import "HumeApiClient.h"
#import "ProductListingViewController.h"

#import "PRDData.h"
#import "PRDItem.h"
#import "OrdersViewControllerTableViewController.h"
#import "MBProgressHud.h"
#import "APIResponse.h"
#import "DiscountedProductList.h"

#import "IUOMData.h"
#import "IUOMGetItemUOM.h"


#define PRICE_NOT_AVL @"Price not available."
#define CANT_ADD_ITEM @"Cannot add item!"
#define UOM_REQUIRED @"Please select Unit of measure"



@interface ProductDetailViewController ()
{
}
@end

@implementation ProductDetailViewController
@synthesize detail;

-(id)init{
    
    return  self;
}


#pragma mark - View LifeCycle Methods
#pragma mark -
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _baseSelected = NO;
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
        [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        [[HumeApiClient SharedClient] AddAccessTokenInHeader];
        [[HumeApiClient SharedClient] GET:[NSString stringWithFormat:kProductDetail,self.itemCode] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            
            [self handleSuccessDetail:responseObject];
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            
            [self handleFailureDetail:error];
            
        }];
    
    [self.pkrUom setDelegate:self];
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    self.title = @"Product Description";
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                   [UIColor whiteColor],
                                                                   NSForegroundColorAttributeName,
                                                                   [UIFont fontWithName:FONT_COND size:23],
                                                                   NSFontAttributeName,
                                                                   nil];
    [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:[UIColor whiteColor]];
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    //self.title = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Private Methods
#pragma mark - 

/** 
 @brief: Product details service success call back method
 
 - Cheking response object alss type
 
 - Converting into dictionary
 
 - Parsing of response object to model class
 
 - Based on status code setting up default values.
 
 - Fetching UOM product list.
 
 @param: (id)responseObject
 */
-(void)handleSuccessDetail:(id)responseObject{
    
    if (self.productSelectMode == ProductEditMode) {
        [self.addToCart setHidden:YES];
        [self.addToPantry setHidden:YES];
        [self.updatePantry setHidden:NO];
    }
    
    NSLog(@"%@",responseObject);
    NSDictionary* json = nil;
    if ([responseObject isKindOfClass:[NSDictionary class]]){
        json = (NSDictionary*)responseObject;
    }
    detail = nil;
    detail = [PRDProductDetail modelObjectWithDictionary:json];
    
    if (detail.statusCode==1) {
        _itemPrice = [detail.data.item.unitPrice floatValue];
        [self.productDescription setText:[NSString stringWithFormat:@"%@ %@",detail.data.item.itemDescription, detail.data.item.description2]];
        
        [self.productGroup setText:detail.data.item.productGroup];
        
        NSString *b64String = [NSString stringWithFormat:@"%@",detail.data.item.picture];
        NSData *imgData = [[NSData alloc] initWithBase64EncodedString:b64String options:0];

        if (imgData) {
            
            if([[[NSUserDefaults standardUserDefaults] valueForKey:@"DISPLAYIMAGE"] integerValue]==1){
                [self.productImage setImage:[UIImage imageWithData:imgData]];
            }
        }
        
        if (self.orderDetailsObj) {
            [self.txtUom setText:self.orderDetailsObj.unitOfMeasure];
            [self.lblQuantity setText:self.orderDetailsObj.quantity];
            [self.lblPrice setText:[@"$" stringByAppendingString:self.orderDetailsObj.lineAmount]];
            [self.stpQuantity setValue:[self.orderDetailsObj.quantity doubleValue]];
        }
        else if (self.pList) {
            [self.txtUom setText:self.pList.uOM];
            [self.lblQuantity setText:self.pList.quantity];
            [self.stpQuantity setValue:[self.pList.quantity doubleValue]];
            
            if([self.pList.unitPrice doubleValue]==0)
            {
                [self.updatePantry setEnabled:FALSE];
                [self.updatePantry setAlpha:0.5];
            
            }
            
        }
        else {
            
        }
        
        _arrUom
        = [[NSMutableArray alloc] init];
        _uomAndPrice = [[NSMutableDictionary alloc] init];
        
        [[HumeApiClient SharedClient] AddAccessTokenInHeader];
        [[HumeApiClient SharedClient] GET:[NSString stringWithFormat:kGetItemUOMs,self.itemCode,[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMERNOKEY]] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            
            IUOMGetItemUOM *itemUom = [IUOMGetItemUOM modelObjectWithDictionary:responseObject];
            if (itemUom.statusCode==1) {
                for (IUOMData *uoms in itemUom.data) {
                    [_arrUom addObject:uoms.uOM];
                    if ([self.orderDetailsObj.unitOfMeasure isEqualToString:uoms.uOM]) {
                        [_uomAndPrice setValue:self.orderDetailsObj.lineAmount forKey:uoms.uOM];
                    }
                    else {
                        [_uomAndPrice setValue:@"" forKey:uoms.uOM];
                    }
                }
                if ([_arrUom count]>0) {
                    if (!self.orderDetailsObj && !self.pList) {
                        [self.txtUom setText:[_arrUom objectAtIndex:0]];
                        //call service to get price
                        [self getPriceOfUom:[_arrUom objectAtIndex:0] withQuantity:@"1"];
                    }
                    
                       //Code Added By Sumit Batra
                        
                        [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                        
                        [[HumeApiClient SharedClient] AddAccessTokenInHeader];
                        
                        NSString *str = self.itemCode;
                        NSString *hitURL = [NSString stringWithFormat:kItemCrossRefDetail,str,[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMERNOKEY],self.txtUom.text];
                        [[HumeApiClient SharedClient] GET:hitURL parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
                            
                            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                            NSDictionary *responseDict = (NSDictionary*)responseObject[@"data"];
                            
                            if ([responseDict count]) {
                                
                                if ([responseDict valueForKey:@"Description"] != NULL) {
                                    _productDescription.text = [responseDict objectForKey:@"Description"];
                                }
                                 NSString *strItemRef = [responseDict objectForKey:@"CrossRefNo"];
                                if (([strItemRef isEqual:[NSNull null]] || [strItemRef isKindOfClass:[NSNull class]])) {
                                    [self.itemCodeAndCategory setText:[NSString stringWithFormat:@"#%@     %@", detail.data.item.item1, detail.data.item.itemCategory]];
                                }
                                else {
                                    [self.itemCodeAndCategory setText:[NSString stringWithFormat:@"#%@     %@", strItemRef.length? strItemRef : detail.data.item.item1, detail.data.item.itemCategory]];
                                }
                                
                                
                            }
                        } failure:^(NSURLSessionDataTask *task, NSError *error) {
                            
                            [self handleFailureDetail:error];
                            [self.itemCodeAndCategory setText:[NSString stringWithFormat:@"#%@     %@",  detail.data.item.item1, detail.data.item.itemCategory]];
                            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                            
                        }];
                    
                }
            }
            else {
                //disable buttons
                
                [self.itemCodeAndCategory setText:[NSString stringWithFormat:@"#%@     %@",  detail.data.item.item1, detail.data.item.itemCategory]];
                
                [self.addToCart setEnabled:NO];
                [self.addToPantry setEnabled:NO];
                [self.addToCart setAlpha:0.5];
                [self.addToPantry setAlpha:0.5];
                [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
            }
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            //disable buttons
            [self.addToCart setEnabled:NO];
            [self.addToPantry setEnabled:NO];
            [self.addToCart setAlpha:0.5];
            [self.addToPantry setAlpha:0.5];
            [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        }];
        
    }
    
    if ([detail.data.item.salesUnitOfMeasure length]>0 && !self.orderDetailsObj && !self.pList) {
        //[self getPriceOfUom:detail.data.item.salesUnitOfMeasure];
    }
    else if (self.orderDetailsObj){
        [self getPriceOfUom:self.orderDetailsObj.unitOfMeasure withQuantity:self.orderDetailsObj.quantity];
    }
    else if (self.pList){
        [self getPriceOfUom:self.pList.uOM withQuantity:self.pList.quantity];
    }
    
}

/**
 @brief: Service call for getting Price details of UOM
 
 - Sepating all price list 
 
 @param:
 
  (NSString *)unitOfMeasure ,(NSString*)minQ
 
 */
-(void)getPriceOfUom:(NSString *)unitOfMeasure withQuantity:(NSString*)minQ
{
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [[HumeApiClient SharedClient] AddAccessTokenInHeader];
    [[HumeApiClient SharedClient] POST:kProductPrice parameters:@{@"Customer":[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMERNOKEY],@"Item":self.itemCode,@"MinQty":minQ,@"UOM":unitOfMeasure,@"Contract":@"true"} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([[responseObject valueForKey:@"statusCode"] integerValue]==1) {
            if ([[[responseObject valueForKey:@"data"] valueForKey:@"UnitPrice"] integerValue]!=0) {
                
                _itemPrice = [[[responseObject valueForKey:@"data"] valueForKey:@"UnitPrice"] floatValue];
                
                [self.lblPrice setText:[@"$" stringByAppendingString:[NSString stringWithFormat:@"%.02f",_itemPrice*[self.lblQuantity.text integerValue]]]];
                
                for (int i=0; i<[_arrUom count]; i++) {
                    if ([[_arrUom objectAtIndex:i] isEqualToString:unitOfMeasure]) {
                        [_uomAndPrice setValue:[[responseObject valueForKey:@"data"] valueForKey:@"UnitPrice"] forKey:[_arrUom objectAtIndex:i]];
                    }
                    
                }
                [self.addToCart setEnabled:YES];
                [self.addToPantry setEnabled:YES];
                [self.updatePantry setEnabled:YES];
                [self.updatePantry setAlpha:1.0];
                [self.addToCart setAlpha:1.0];
                [self.addToPantry setAlpha:1.0];
            }
            else {
                [self.lblPrice setText:@"$0.00"];
                _itemPrice = 0.00;
                for (int i=0; i<[_arrUom count]; i++) {
                    if ([[_arrUom objectAtIndex:i] isEqualToString:detail.data.item.salesUnitOfMeasure]) {
                        [_uomAndPrice setValue:@"0.00" forKey:[_arrUom objectAtIndex:i]];
                    }
                    
                }
                [self.addToCart setEnabled:NO];
                [self.addToPantry setEnabled:NO];
                [self.updatePantry setEnabled:NO];
                [self.addToCart setAlpha:0.5];
                [self.updatePantry setAlpha:0.5];
                [self.addToPantry setAlpha:0.5];
            }
            [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    }];
    

}

-(void)handleFailureDetail:(NSError*)error{
     [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
}

/**
 @brief: Stepper value controll method
  - After varyfying with reference updating to text.
 */
- (IBAction)valueChanged:(UIStepper *)sender {
    double value = [sender value];
    
    NSLog(@"step...%@",[NSString stringWithFormat:@"%d", (int)value]);
    
    if ((int)value<[self.lblQuantity.text integerValue]) {
        CGFloat price;
        if (_baseSelected==YES) {
            price = [[self.lblPrice.text substringFromIndex:1] floatValue]-_basePrice;
        }
        else {
        price = [[self.lblPrice.text substringFromIndex:1] floatValue]-_itemPrice;
        }
        [self.lblPrice setText:[@"$" stringByAppendingString:[NSString stringWithFormat:@"%.02f",price]]];
    }
    else {
        CGFloat price;
        if (_baseSelected==YES) {
            price = _basePrice*(int)value;
        }
        else {
            price = _itemPrice*(int)value;
        }
        [self.lblPrice setText:[@"$" stringByAppendingString:[NSString stringWithFormat:@"%.02f",price]]];
    }
    
    [self.lblQuantity setText:[NSString stringWithFormat:@"%d", (int)value]];
    
    
}

/**
 @brief: Service call for getting cart product price
 
 - Getting reponse from server manging a separate list of product.
 
 @param:
 - (NSString *)unitOfO
 
 - (NSString *)minQ
 
 */
-(void)getPriceForCartWithUOM:(NSString *)unitOfO andQuantity:(NSString *)minQ
{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [[HumeApiClient SharedClient] AddAccessTokenInHeader];
    [[HumeApiClient SharedClient] POST:kProductPrice parameters:@{@"Customer":[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMERNOKEY],@"Item":self.itemCode,@"MinQty":minQ,@"UOM":unitOfO,@"Contract":@"true"} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([[responseObject valueForKey:@"statusCode"] integerValue]==1) {
            if ([[[responseObject valueForKey:@"data"] valueForKey:@"UnitPrice"] integerValue]!=0) {
                
                _itemPrice = [[[responseObject valueForKey:@"data"] valueForKey:@"UnitPrice"] floatValue];
                
                [self.lblPrice setText:[@"$" stringByAppendingString:[NSString stringWithFormat:@"%.02f",_itemPrice*[self.lblQuantity.text integerValue]]]];
                
                if (self.txtUom.text.length>0) {
                    if (self.strOrderNo.length >0)
                    {
                        
                        [self AddProductIntoAnOrder];
                        
                        
                    }else{
                        
                        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
                        
                        if (self.orderObj.orderNo.length>0) {
                            [self AddProductIntoAnOrder];
                            
                        } else if([[self.lblPrice.text substringFromIndex:1] integerValue]!=0){
                            
                            // Add prodcuct into an open order .
                            OrdersViewControllerTableViewController *objOrders = [[OrdersViewControllerTableViewController alloc] initWithNibName:@"OrdersViewControllerTableViewController" bundle:nil];
                            [objOrders setStrCallerView:PRODUCT_DETAIL];
                            
                            //Pass the prodcut detail object
                            [objOrders setProductDetailObj:self];
                            [objOrders setPriceOfItem:[self.lblPrice.text substringFromIndex:1]];
                            objOrders.orderSelectionMode = OrderSelectionOpenOrders;
                            
                            
                            [self.navigationController pushViewController:objOrders animated:YES];
                            
                        }
                        else {
                            [[[UIAlertView alloc] initWithTitle:PRICE_NOT_AVL message:CANT_ADD_ITEM delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil]show];
                            
                        }
                        
                    }
                }
                else {
                    
                    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
                    [[[UIAlertView alloc] initWithTitle:@"" message:UOM_REQUIRED delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil]show];
                    
                }
            }
            else {
                [self.lblPrice setText:@"$0.00"];
                _itemPrice = 0.00;
                [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
            }
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    }];

    
}

/**
 @brief: Addto cat button action
 
 - Calling getPriceForCartWithUOM service with
 
 @param:
 - UOM 
 
 - Quantity
 
 */
- (IBAction)addToCart:(id)sender
{
    [self getPriceForCartWithUOM:self.txtUom.text andQuantity:self.lblQuantity.text];
    
}


#pragma mark UIAlertViewDelegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

     NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqual:ALERT_BTN_YES]) {
        //self.strOrderNo = self.prvOrderNo;
        [self AddProductIntoAnOrder];
    } else if([title isEqualToString:ALERT_BTN_OK]){
        //do nothing
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        
        // Add prodcuct into an open order .
        OrdersViewControllerTableViewController *objOrders = [[OrdersViewControllerTableViewController alloc] initWithNibName:@"OrdersViewControllerTableViewController" bundle:nil];
        [objOrders setStrCallerView:PRODUCT_DETAIL];
        
        //Pass the prodcut detail object
        [objOrders setProductDetailObj:self];
        objOrders.orderSelectionMode = OrderSelectionOpenOrders;
        
        
        [self.navigationController pushViewController:objOrders animated:YES];
    }
    

}

/**
 @brief: Getting pantry product price details
 
 - Calling a server with UOM & MinQty
 
 @param:
 
 - (NSString *)unitOfO
 
 - (NSString *)minQ
 
 */

-(void)getPriceForPantryWithUOM:(NSString *)unitOfO withQuantity:(NSString *)minQ{
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [[HumeApiClient SharedClient] AddAccessTokenInHeader];
    [[HumeApiClient SharedClient] POST:kProductPrice parameters:@{@"Customer":[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMERNOKEY],@"Item":self.itemCode,@"MinQty":minQ,@"UOM":unitOfO,@"Contract":@"true"} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([[responseObject valueForKey:@"statusCode"] integerValue]==1) {
            if ([[[responseObject valueForKey:@"data"] valueForKey:@"UnitPrice"] integerValue]!=0) {
                
                _itemPrice = [[[responseObject valueForKey:@"data"] valueForKey:@"UnitPrice"] floatValue];
                
                [self.lblPrice setText:[@"$" stringByAppendingString:[NSString stringWithFormat:@"%.02f",_itemPrice*[self.lblQuantity.text integerValue]]]];
                
                if ([self.txtUom.text length]<=0) {
                    [[[UIAlertView alloc] initWithTitle:@"" message:UOM_REQUIRED delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil]show];
                } else if(([[self.lblPrice.text substringFromIndex:1] integerValue]!=0)){
                    //NSString *unitOfMeasure = [NSString stringWithFormat:@"%@",detail.data.item.salesUnitOfMeasure?detail.data.item.salesUnitOfMeasure:@""];
                    NSString *unitOfMeasure = self.txtUom.text;
                    //NSString *unitPrice = [NSString stringWithFormat:@"%@",[self.lblPrice.text substringFromIndex:1]];
                    NSString *quantity = [NSString stringWithFormat:@"%@",self.lblQuantity.text?self.lblQuantity.text:@""];
                    NSString *itemNo = [NSString stringWithFormat:@"%@",detail.data.item.item1?detail.data.item.item1:@""];
                    NSString *description = [NSString stringWithFormat:@"%@",detail.data.item.description2?detail.data.item.itemDescription:@""];
                    NSString *customerNo = (NSString *)[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMERNOKEY];
                    
                    [[HumeApiClient SharedClient]AddAccessTokenInHeader];
                    [[HumeApiClient SharedClient] POST:kPantryInsertItemIntoPantry parameters:@{@"CustomerNo":customerNo,@"Quantity":quantity,@"UOM":unitOfMeasure,@"ItemNo":itemNo,@"description":description,@"UnitPrice":[NSString stringWithFormat:@"%.02f",(_baseSelected==YES)?_basePrice:_itemPrice], @"synced":@"0",@"ToSync":@"1"} success:^(NSURLSessionDataTask *task, id ResponseObject)
                     {
                         NSLog(@"Pantry: %@",ResponseObject);
                         [self handleSuccessResponseForPantryCall:ResponseObject];
                         
                     }failure:^(NSURLSessionDataTask *task, NSError *error)
                     {
                         NSLog(@"Pantry: Failed");
                         [self handleFailureResponseAddProduct];
                     }
                     ];
                }
                else {
                    
                    [[[UIAlertView alloc] initWithTitle:PRICE_NOT_AVL message:CANT_ADD_ITEM delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil]show];
                    
                }
                
            }
            else {
                [self.lblPrice setText:@"$0.00"];
                _itemPrice = 0.00;
                [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
            }
            
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    }];
    

    
}

/**
 @brief: Addto pantry button action
 
 - Calling addToPantryCall service with
 
 @param:
 sender (id)
 
 */
-(IBAction)addToPantryCall:(id)sender
{
    
    [self getPriceForPantryWithUOM:self.txtUom.text withQuantity:self.lblQuantity.text];
   
}


/**
 @brief: Success call back method for PentryProduct price
 
 @param:
 
 (id)ResponseObject
 */

-(void)handleSuccessResponseForPantryCall:(id)ResponseObject
{

    APIResponse *response = [APIResponse modelObjecttWithDictionary:ResponseObject];
    
    //OrderDetails *orderDetailObj = [OrderDetails modelObjectWithDictionary:response.data];
    if(response.isSuccess==1)
    {
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        
    }else{
        
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Technical glich" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }

}

/**
 @brief: Adding a selected product into a order list.
 
 - Service call to updadating in server with order number.
 */
-(void)AddProductIntoAnOrder
{
    NSString *orderNo = [NSString stringWithFormat:@"%@",self.orderObj.orderNo?self.orderObj.orderNo:@""];
    if (orderNo.length>0)
    {
        
        NSString *type = [NSString stringWithFormat:@"%@",self.orderObj.type?self.orderObj.type:@""];
        // Discussed with webservice guy - send line no 0
        NSString *lineno = [NSString stringWithFormat:@"0"];

        NSString *quantity = [NSString stringWithFormat:@"%@",self.lblQuantity.text?self.lblQuantity.text:@""];
        NSString *unitOfMeasure = [NSString stringWithFormat:@"%@",self.txtUom.text];
        NSString *description = [NSString stringWithFormat:@"%@ %@",detail.data.item.itemDescription?detail.data.item.itemDescription:@"",detail.data.item.description2?detail.data.item.description2:@""];
        
        NSString *itemNo = [NSString stringWithFormat:@"%@",detail.data.item.item1?detail.data.item.item1:@""];
        
        [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        [[HumeApiClient SharedClient]AddAccessTokenInHeader];
        [[HumeApiClient SharedClient] POST:kOrderAddNewProduct parameters:@{@"Type":type,@"OrderNo":self.orderObj.orderNo ,@"Quantity":quantity,@"LineNo":lineno, @"UnitOfmeasure":unitOfMeasure,@"ItemNo":itemNo,@"description":description,@"UnitPrice":[NSString stringWithFormat:@"%.02f",(_baseSelected==YES)?_basePrice:_itemPrice], @"synced":@"0",@"ToSync":@"1"}  success:^(NSURLSessionDataTask *task, id ResponseObject)
         {
             NSLog(@"sales order: %@",ResponseObject);
             [self handleSuccessResponseForAddProduct:ResponseObject];
             
         }failure:^(NSURLSessionDataTask *task, NSError *error)
         {
             NSLog(@"sales order: Failed");
             [self handleFailureResponseAddProduct];
         }
         ];
 
        
    }else{
        
        
    }
    
    id<ProductDetailViewControllerDelegate> strongDelegate = self.delegate;
    
    // Our delegate method is optional, so we should
    // check that the delegate implements it
    if ([strongDelegate respondsToSelector:@selector(saveOrderNumber:)]) {
        [self.delegate saveOrderNumber:self.orderObj];
    }
}


/**
 @brief: Success call back method for crating aproduct list.
 
 @param: (id)object
 */
- (void)handleSuccessResponseForAddProduct:(id)object {
    
    APIResponse *response = [APIResponse modelObjecttWithDictionary:object];
    
    if(response.isSuccess==1)
    {
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        
        for (id controller in [self.navigationController viewControllers]) {
            if ([controller isKindOfClass:[ProductListingViewController class]]) {
                [self.navigationController popToViewController:controller animated:YES];
                break;
            }
        }
        if (self.strCallerViewName!=nil) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FETCH_ORDERS" object:nil];
        }
        
    }else{
        
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Technical glich" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

- (void)handleFailureResponseAddProduct{
    
    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    [[[UIAlertView alloc] initWithTitle:@"" message:kStandardErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    return;
}

#pragma mark - UIPickerView methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
    
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return [_arrUom count];
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [self.txtUom setText:[_arrUom objectAtIndex:row]];
    [self getPriceOfUom:[_arrUom objectAtIndex:row] withQuantity:self.lblQuantity.text];
        
          [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        [[HumeApiClient SharedClient] AddAccessTokenInHeader];
        NSString *str = self.itemCode; //@"CW1000";//self.str_CrossReference;
        NSString *hitURL = [NSString stringWithFormat:kItemCrossRefDetail, str,[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMERNOKEY], _txtUom.text];
        [[HumeApiClient SharedClient] GET:hitURL parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            
                [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
            NSDictionary *responseDict = (NSDictionary*)responseObject[@"data"];
            
            if ([responseDict count]) {

                if ([responseDict valueForKey:@"Description"] != NULL) {
                    _productDescription.text = [responseDict objectForKey:@"Description"];
                    NSString *strItemRef = [responseDict objectForKey:@"CrossRefNo"];
                    if (([strItemRef isEqual:[NSNull null]] || [strItemRef isKindOfClass:[NSNull class]])) {
                        [self.itemCodeAndCategory setText:[NSString stringWithFormat:@"#%@     %@", detail.data.item.item1, detail.data.item.itemCategory]];
                        
                    }
                    else {
                    [self.itemCodeAndCategory setText:[NSString stringWithFormat:@"#%@     %@", strItemRef.length? strItemRef : detail.data.item.item1, detail.data.item.itemCategory]];
                    }
                    
                }
                
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            
            [self handleFailureDetail:error];
            [self.itemCodeAndCategory setText:[NSString stringWithFormat:@"#%@     %@", detail.data.item.item1, detail.data.item.itemCategory]];
            
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];

        }];
    
    [UIView animateWithDuration:0.3 animations:^{
        //self.pkrUom.center = CGPointMake(self.pkrUom.center.x, self.pkrUom.center.y - self.pkrUom.frame.size.height);
        self.pkrUom.frame = CGRectMake(0, self.view.frame.size.height, self.pkrUom.frame.size.width, self.pkrUom.frame.size.height);
    } completion:^(BOOL finished){
        self.pkrUom.hidden = NO;
    }];
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if ([_arrUom count]>0) {
        return [_arrUom objectAtIndex:row];
    }
    else
    return @"";

}

- (IBAction)pickUom:(id)sender{
    
    //Set width for picker according to width of device
    
    if ([_arrUom count]>0) {
        [self.pkrUom setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.pkrUom.frame.size.height)];
        [self.view addSubview:self.pkrUom];
        [UIView animateWithDuration:0.3 animations:^{
            //self.pkrUom.center = CGPointMake(self.pkrUom.center.x, self.pkrUom.center.y - self.pkrUom.frame.size.height);
            self.pkrUom.frame = CGRectMake(0, self.view.frame.size.height-self.pkrUom.frame.size.height, self.pkrUom.frame.size.width, self.pkrUom.frame.size.height);
        } completion:^(BOOL finished){
            self.pkrUom.hidden = NO;
        }];
    }
    else {
        if ([UIAlertController class]) {
            UIAlertController *exe = [UIAlertController alertControllerWithTitle:@"" message:UOM_REQUIRED preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //
            }];
            [exe addAction:dismiss];
            [self presentViewController:exe animated:YES completion:^{
                //
            }];
            
        }
        
    }
    

}


/**
 @brief: Gettinga a price of a product with UOM & Minqunatity
 
 - server call for getting data.
 
 @param:
 
 - (NSString *)unitOfO
 
 - (NSString *)minQ
 
 
 */
-(void)getPriceForUpdateWithUOM:(NSString *)unitOfO withQuantity:(NSString *)minQ{
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [[HumeApiClient SharedClient] AddAccessTokenInHeader];
    [[HumeApiClient SharedClient] POST:kProductPrice parameters:@{@"Customer":[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMERNOKEY],@"Item":self.itemCode,@"MinQty":minQ,@"UOM":unitOfO,@"Contract":@"true"} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([[responseObject valueForKey:@"statusCode"] integerValue]==1) {
            if ([[[responseObject valueForKey:@"data"] valueForKey:@"UnitPrice"] integerValue]!=0) {
                
                _itemPrice = [[[responseObject valueForKey:@"data"] valueForKey:@"UnitPrice"] floatValue];
                
                [self.lblPrice setText:[@"$" stringByAppendingString:[NSString stringWithFormat:@"%.02f",_itemPrice*[self.lblQuantity.text integerValue]]]];
                
                if (self.orderDetailsObj) {
                    [self.orderDetailsObj setUnitOfMeasure:self.txtUom.text];
                    [self.orderDetailsObj setQuantity:self.lblQuantity.text];
                    [self.orderDetailsObj setUnitPrice:[NSString stringWithFormat:@"%.02f",(_baseSelected==YES)?_basePrice:_itemPrice]];
                    [[HumeApiClient SharedClient]AddAccessTokenInHeader];
                    [[HumeApiClient SharedClient]POST:kOrderUpdateProduct parameters:[self.orderDetailsObj dictionaryRepresentation] success:^(NSURLSessionDataTask *task, id responseObject) {
                        //
                        if ([[responseObject valueForKey:@"statusCode"] integerValue]==1) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"FETCH_ORDER_DETAILS" object:nil];
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"FETCH_ORDERS" object:nil];
                            [self.navigationController popViewControllerAnimated:YES];
                            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                        }
                        else {
                            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                        }
                        
                    } failure:^(NSURLSessionDataTask *task, NSError *error) {
                        //
                        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                    }];
                }
                if (self.pList) {
                    [self.pList setUOM:self.txtUom.text];
                    [self.pList setQuantity:self.lblQuantity.text];
                    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                    [[HumeApiClient SharedClient]AddAccessTokenInHeader];
                    [[HumeApiClient SharedClient]POST:kPantryItemUpdate parameters:@{@"CustomerNo":[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMERNOKEY],@"ItemNo":self.pList.itemNo,@"Description":self.pList.pantrylistDescription,@"Quantity":self.pList.quantity,@"UOM":self.pList.uOM,@"UnitPrice":[NSString stringWithFormat:@"%.02f",_itemPrice],@"ToSync":@"true",@"Synced":@"false",@"SyncDateTime":@""} success:^(NSURLSessionDataTask *task, id responseObject) {
                        //
                        if ([[responseObject valueForKey:@"statusCode"] integerValue]==1) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"FETCH_PANTRY" object:nil];
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                    } failure:^(NSURLSessionDataTask *task, NSError *error) {
                        //
                        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                    }];
                }
            }
            else {
                [self.lblPrice setText:@"$0.00"];
                _itemPrice = 0.00;
                [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
                
            }
           
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    }];
}

/**
 @brief: Updating Product price list with UOM.
 
 @param:
 - (id)sender
 */
-(IBAction)updateOrder:(id)sender{
    
    [self getPriceForUpdateWithUOM:self.txtUom.text withQuantity:self.lblQuantity.text];
    
    
}




@end

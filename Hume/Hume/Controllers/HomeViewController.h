//
//  HomeViewController.h
//  Hume
//
//  Created by user on 06/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @brief:  HomeViewController provide privilege to select multiple options like my account,protuct, templete pramotions
 RelativeControllers: MyAccountTableViewController,SideMenuTableViewController,OrdersViewControllerTableViewController,ProductListingViewController,PromotionTableViewController,PantryListTableViewController
 */
@interface HomeViewController : UIViewController{

    IBOutlet UIButton *btnMyAccount;
    IBOutlet UIButton *btnOrders;
    IBOutlet UIButton *btnProducts;
    IBOutlet UIButton *btnPantryList;
    IBOutlet UIButton *btnPromotions;
    IBOutlet UIButton *btnLogo;
    
	__weak IBOutlet UIImageView *imageLogo;
}

//Methods
- (void)adjustOffsets:(NSInteger)space ;
-(IBAction)MyAccountBtnCall:(id)sender;
-(IBAction)OrdersBtnCall:(id)sender;
-(IBAction)ProductBtnCall:(id)sender;
-(IBAction)PromotionsBtnCall:(id)sender;
-(IBAction)PantryListBtnCall:(id)sender;
@end

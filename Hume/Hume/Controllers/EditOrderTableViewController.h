//
//  EditOrderTableViewController.h
//  Hume
//
//  Created by user on 02/02/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Orders.h"

@protocol EditOrderTableViewControllerDelegate;

/**
 @brief: EditOrderTableViewController can perform operation like
 Fetch all available order list & edit them.
 Can delete a selected one
 Can modify selected
 */
@interface EditOrderTableViewController : UIViewController <UITableViewDataSource,UITableViewDelegate, UIAlertViewDelegate>
{
    
}

//Properties
@property(nonatomic,strong)NSMutableArray *itemstodelete_array;
@property(nonatomic,retain) IBOutlet UIView *infoView;
@property(nonatomic,retain) IBOutlet UILabel *lblItemCount;
@property(nonatomic,retain) IBOutlet UILabel *lblBalance;
@property(nonatomic,retain) IBOutlet UILabel *lblRewardsPoints;
@property(nonatomic,retain) IBOutlet UILabel *lblTotalDue;
@property(nonatomic,retain) IBOutlet UILabel *lblOrderNo;
@property(nonatomic,retain) IBOutlet UILabel *lblDate;
@property(nonatomic,retain) IBOutlet UILabel *lblTime;
@property(nonatomic,retain) IBOutlet UILabel *lblAmount;
@property(nonatomic,retain) IBOutlet UILabel *lblAmountExcluding;
@property(nonatomic,retain) IBOutlet UILabel *lblGSTAmount;
@property(nonatomic,retain) IBOutlet UILabel *lblTotalExcuding;
@property(nonatomic,retain) IBOutlet UILabel *lblAddress;
@property(nonatomic,retain) IBOutlet UILabel *lblInvoiceDiscAmount;
@property(nonatomic,retain) IBOutlet UILabel *lblInvoiceDiscPrecentage;
@property(nonatomic,retain) IBOutlet UIButton *btn_process;
@property(nonatomic,retain) IBOutlet UIButton *btn_catalogue;
@property(nonatomic,retain) IBOutlet UITableView *tableview;
@property(nonatomic,retain) NSString *strOrderNo;
@property(nonatomic,retain) Orders *ordersModelObj;
@property(nonatomic,weak) id<EditOrderTableViewControllerDelegate> delegate;

//Methods
-(IBAction)processOrder:(id)sender;
@end

//Protocol
@protocol EditOrderTableViewControllerDelegate<NSObject>
-(void)fetchOrdersAndPop:(EditOrderTableViewController*)editView;
@end

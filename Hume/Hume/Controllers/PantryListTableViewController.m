//
//  PantryListTableViewController.m
//  Hume
//
//  Created by user on 12/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "PantryListTableViewController.h"
#import "HumeApiClient.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import "APIResponse.h"
#import "Pantrylist.h"
#import "PantryListTableViewCell.h"
#import "OrdersViewControllerTableViewController.h"
#import "OrderDetails.h"
#import "Orders.h"
#import "Utility.h"
#import "EditOrderTableViewController.h"
#import "EditOrderTableViewCell.h"




@interface PantryListTableViewController ()
{
    NSMutableArray *arrayPantryList;
    NSMutableArray *arrayPantryListCopy;
    NSMutableArray *cartItems;
    NSMutableArray *checkSelectedStatus;
    NSMutableArray *arrayItemNo;
    void(^runBlock)(void);
    void(^runBlockRemove)(void);
}
@end

@implementation PantryListTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"";
    [Utility setTintColorOfNavigationBar:[UIColor whiteColor] OnNavigationController:self.navigationController];
    //Anish Changes
    static NSString *CellIdentifier1 = @"EditOrderTableViewCell";
    UINib *nib = [UINib nibWithNibName:@"EditOrderTableViewCell" bundle:nil];
    [self.tableview registerNib:nib forCellReuseIdentifier:CellIdentifier1];
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 105, 30)];
    [button setTitle:@"Create Order" forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(placeOrder) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barBtnItem = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barBtnItem;
    cartItems = [[NSMutableArray alloc] init];
    checkSelectedStatus = [[NSMutableArray alloc] init];
    arrayItemNo = [[NSMutableArray alloc]init];
    
    [self fetchDataFromServerPantryList];
    [self.lblCutomerID setText:[[NSUserDefaults standardUserDefaults]valueForKey:CUSTOMERNOKEY]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchDataFromServerPantryList) name:@"FETCH_PANTRY" object:nil];
    
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
#if !__has_feature(objc_arc)
    [super dealloc];
#endif
}

#pragma mark Private Methods

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/**
 @brief: Service call for placing a new order
 
 - Calling success & failure callback methods.
 */
-(void)placeOrder
{
    //Conver all the pantry list items into an order.
    //Create an order and add item into them.
    if ([arrayPantryList count]>0) {
        NSDateFormatter *dateForMatter  = [[NSDateFormatter alloc]init];
        [dateForMatter setDateFormat:@"dd/MM/yyyy"];
        NSString *strDate = [dateForMatter stringFromDate:[NSDate date]];
        NSDateFormatter *time  = [[NSDateFormatter alloc]init];
        [time setDateFormat:@"HH:mm"];
        NSString *strTime = [time stringFromDate:[NSDate date]];
        [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        [[HumeApiClient SharedClient] AddAccessTokenInHeader];
        [[HumeApiClient SharedClient] POST:kOrderAddNewSaleOrder parameters:@{@"Type":@"SALES",@"OrderTime":strTime,@"OrderDate":strDate,@"synced":@"0",@"ToSync":@"1",@"Status":@"",@"username":[[NSUserDefaults standardUserDefaults] valueForKey:@"username"],@"Customer":[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMERNOKEY]} success:^(NSURLSessionDataTask *task, id responseObject) {
            //
            [self handleSuccessNewSalesOrder:responseObject];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            //
            [self handleFailureNewSalesOrder:error];
            
        }];
    }
    else {
        if ([UIAlertController class]) {
            UIAlertController *toast = [UIAlertController alertControllerWithTitle:@"" message:ITEMS_REQUIRED_FOR_TEMPLETE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //do nothing
            }];
            [toast addAction:okAction];
            [self presentViewController:toast animated:YES completion:^{
                //
            }];
        }
    }
 }
/**
 @brief: Success call back for New sales order creation.
 - After getting server response filling up the  order details model.
 */

-(void)handleSuccessNewSalesOrder:(id)responseObject{
    APIResponse *response = [APIResponse modelObjecttWithDictionary:responseObject];
    if (response.isSuccess==1) {
        Orders *order = [Orders modelObjectWithDictionary:response.data];
        NSMutableArray *pantryItems = [[NSMutableArray alloc] init] ;
        if ([cartItems count]<=0) {
        for (int i = 0;i<[arrayPantryList count];i++) {
            OrderDetails *detail = [OrderDetails modelObjectWithDictionary:nil];
            Pantrylist *listItem = [Pantrylist modelObjectWithDictionary:[arrayPantryList objectAtIndex:i]];
            double price =  [listItem.unitPrice doubleValue];
            if(price!=0)
            {
                detail.type = order.type;
                detail.orderNo = order.orderNo;
                detail.itemNo = listItem.itemNo;
                detail.unitPrice = listItem.unitPrice;
                detail.unitOfMeasure = listItem.uOM;
                detail.quantity = listItem.quantity;
                detail.OrderDetailsDescription = listItem.pantrylistDescription;
                [pantryItems addObject:[detail dictionaryRepresentation]];
            }
            }
        }
        else {
            for (int i = 0;i<[cartItems count];i++) {
                OrderDetails *detail = [OrderDetails modelObjectWithDictionary:nil];
                Pantrylist *listItem = [Pantrylist modelObjectWithDictionary:[(Pantrylist*)[cartItems objectAtIndex:i] dictionaryRepresentation] ];
                 double price =  [listItem.unitPrice doubleValue];
                if(price!=0)
                {
                detail.type = order.type;
                detail.orderNo = order.orderNo;
                detail.itemNo = listItem.itemNo;
                detail.unitPrice = listItem.unitPrice;
                detail.unitOfMeasure = listItem.uOM;
                detail.quantity = listItem.quantity;
                detail.OrderDetailsDescription = listItem.pantrylistDescription;
                [pantryItems addObject:[detail dictionaryRepresentation]];
             }
            }
        }
        [[HumeApiClient SharedClient] POST:kInsertSaleOrderLinesfromPantry parameters:@{@"items":pantryItems} success:^(NSURLSessionDataTask *task, id responseObject) {
            [[[UIAlertView alloc] initWithTitle:@"" message:ORDER_CREATED delegate:self cancelButtonTitle:nil otherButtonTitles:ALERT_BTN_PROCEED, nil] show];
            PantryListTableViewController * __weak weakSelf = self;
            runBlock = ^{
                EditOrderTableViewController *editOrder = [[EditOrderTableViewController alloc] init];
                editOrder.ordersModelObj = order;
                editOrder.strOrderNo = order.orderNo;
                [weakSelf.navigationController pushViewController:editOrder animated:YES];
            };
            
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            //
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        }];
    }
}

-(void)handleFailureNewSalesOrder:(NSError *)error{
    
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
}

#pragma mark UIAlertViewDelegate methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSString *alertTitle = [alertView buttonTitleAtIndex:buttonIndex];
    if ([alertTitle isEqualToString:ALERT_BTN_PROCEED]) {
        //move to edit order screen
        runBlock();
    }
    else if([alertTitle isEqualToString:ALERT_BTN_YES]){
        
        runBlockRemove();
    
    }
    
}

#pragma FETCHING DATA FROM SERVER
/**
 @brief: Fetching the pantry list data from server.
 - Calling success & failure callback methods.
 */
-(void)fetchDataFromServerPantryList
{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [[HumeApiClient SharedClient]AddAccessTokenInHeader];
    [[HumeApiClient SharedClient] GET:kPantryList parameters:nil success:^(NSURLSessionDataTask *task, id ResponseObject)
     {
         NSLog(@"%@",ResponseObject);
         [self handleSuccessResponseForShippingAddress:ResponseObject];
         
     }failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"Failed");
         [self handleFailureResponseShippingAddress];
     }
     ];
}

/**
 @brief: Success call back method for shippin address 
 - Updating the Arrays based on the server response status.
 */
- (void)handleSuccessResponseForShippingAddress:(id)object {
    
    // [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    APIResponse *response = [APIResponse modelObjecttWithDictionary:object];
    if([[object valueForKey:@"statusCode"] integerValue]==1 )
    {
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        NSSortDescriptor *des = [NSSortDescriptor sortDescriptorWithKey:@"Description" ascending:YES];
        NSArray *sortDesc = [NSArray arrayWithObject:des];
        NSArray *sortedList = [(NSArray*)response.data sortedArrayUsingDescriptors:sortDesc];
        arrayPantryList =  [[NSMutableArray  alloc]initWithArray:sortedList];
        arrayPantryListCopy = [[NSMutableArray alloc]initWithArray:arrayPantryList];
        for (int i =0; i<[arrayPantryList count]; i++) {
            [checkSelectedStatus addObject:@"0"];
        }
        [cartItems removeAllObjects];
        [arrayItemNo removeAllObjects];
        [self.tableview reloadData];
    }
    else if([[object valueForKey:@"statusCode"] integerValue]==0 ){
            //
        [arrayPantryList removeAllObjects];
        [arrayPantryListCopy removeAllObjects];
        [self.tableview reloadData];
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    }
    else{
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:[object valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

- (void)handleFailureResponseShippingAddress{
    
    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    [[[UIAlertView alloc] initWithTitle:@"" message:kStandardErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    return;
}

/**
 @brief:
 Adding a selected product to the cart.
 */
-(void)addToCart:(id)sender{

    CGPoint buttonPosition ;
    NSIndexPath *indexPath ;

    buttonPosition = [sender convertPoint:CGPointZero toView:self.tableview];
    indexPath = [self.tableview indexPathForRowAtPoint:buttonPosition];
    Pantrylist *listItem = [Pantrylist modelObjectWithDictionary:[arrayPantryList objectAtIndex:indexPath.row]];
    NSString *itemNo = listItem.itemNo;
    if ([listItem.itemNo length]>0) {
       
        if ([arrayItemNo containsObject :listItem.itemNo]) {
            for (int i =0; i<[cartItems count]; i++) {
                NSString *itemN = [(Pantrylist *)[cartItems objectAtIndex:i] itemNo];
                if ([itemN isEqualToString:itemNo]) {
                    [cartItems removeObjectAtIndex:i];
                    [arrayItemNo removeObjectAtIndex:i];
                    break;
                }
            }
        }
        else {
            [cartItems addObject:listItem];
            [arrayItemNo addObject:listItem.itemNo];
        }
    }
    [self.tableview reloadData];
    

}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return [arrayPantryList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"PantryListTableViewCell";
    PantryListTableViewCell *cell = (PantryListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"PantryListTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.tag = indexPath.row;
    }
    Pantrylist *pantryObj = [Pantrylist modelObjectWithDictionary:[arrayPantryList objectAtIndex:indexPath.row]];
    
    [cell.lblqty  setText:[@"x" stringByAppendingString:pantryObj.quantity]];
    [cell.lblItemNo setText:pantryObj.itemNo];
    [cell.lblDescription setText:pantryObj.pantrylistDescription];
    [cell.lblUOM setText:pantryObj.uOM];
    [cell.lblItemGroup setText:[@"$" stringByAppendingString:[NSString stringWithFormat:@"%@/%@",pantryObj.unitPrice,pantryObj.uOM]]];
    [cell.lblUnitPrice setText:pantryObj.productGroup];
    [cell.btnCheckBox addTarget:self action:@selector(addToCart:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnCheckBox setTag:indexPath.row];
    
    if ([arrayItemNo containsObject:pantryObj.itemNo]) {
        [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"checkbox-active@2x.png"] forState:UIControlStateNormal];
    }
    else {
        [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"checkbox-inactive@2x.png"] forState:UIControlStateNormal];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    [cell.btnEdit setTag:indexPath.row];
    [cell.btnDelete setTag:indexPath.row];
    [cell.btnEdit addTarget:self action:@selector(EditButtonCall:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDelete  addTarget:self action:@selector(DeleteButtonCall:) forControlEvents:UIControlEventTouchUpInside];
    
    return  cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 65;
    
}

/**
 @brief:
 Calling ProductDetailViewController while edit mode press
 
 */

-(void)EditButtonCall:(id)sender
{
    Pantrylist *pantryObj = [Pantrylist modelObjectWithDictionary:[arrayPantryList objectAtIndex:((UIButton *)sender).tag]];
    ProductDetailViewController *proDetail = [[ProductDetailViewController alloc] init];
    proDetail.itemCode = pantryObj.itemNo;
    proDetail.pList = pantryObj;
    proDetail.productSelectMode = ProductEditMode;
    [self.navigationController pushViewController:proDetail animated:YES];
}

/**
 @brief:
 Delete item from pantry and fetch pantry list again when get success in response
 */
-(void)DeleteButtonCall:(id)sender
{
    //Delete item from pantry and fetch pantry list again when get success in response
   
    Pantrylist *pantryObj = [Pantrylist modelObjectWithDictionary:[arrayPantryList objectAtIndex:((UIButton *)sender).tag]];
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];

    if ([UIAlertController class]) {
    UIAlertController *confirmRemove = [UIAlertController alertControllerWithTitle:@"" message:R_U_SURE preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actConfirm = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [[HumeApiClient SharedClient]AddAccessTokenInHeader];
        NSArray *item = [NSArray arrayWithObjects:@{@"CustomerNo":[[NSUserDefaults standardUserDefaults] objectForKey:CUSTOMERNOKEY],@"Description":pantryObj.pantrylistDescription,@"ItemNo":pantryObj.itemNo,@"Synced":@"false",@"ToSync":@"true",@"SyncDateTime":@""}, nil];
        NSDictionary *params = [NSDictionary dictionaryWithObject:item forKey:@"items"];
        [[HumeApiClient SharedClient] POST:kPantryItemRemove parameters:params success:^(NSURLSessionDataTask *task, id ResponseObject)
         {
             NSLog(@"%@",ResponseObject);
             [self handleSuccessResponseForDeleteItemFromPantry:ResponseObject];
             
         }failure:^(NSURLSessionDataTask *task, NSError *error)
         {
             NSLog(@"Failed");
             [self handleFailureResponseDeleteItemFromPantry];
         }
         ];

    }];
    [confirmRemove addAction:actConfirm];
    UIAlertAction *actCancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        //do nothing
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    }];
    [confirmRemove addAction:actCancel];
    [self presentViewController:confirmRemove animated:YES completion:^{
        //
    }];
    }
    else {
        
        [[[UIAlertView alloc] initWithTitle:@"" message:R_U_SURE delegate:self cancelButtonTitle:@"No" otherButtonTitles:ALERT_BTN_YES, nil] show];
        PantryListTableViewController * __weak weakSelf= self;
        runBlockRemove = ^{
            [[HumeApiClient SharedClient]AddAccessTokenInHeader];
            NSArray *item = [NSArray arrayWithObjects:@{@"CustomerNo":[[NSUserDefaults standardUserDefaults] objectForKey:CUSTOMERNOKEY],@"Description":pantryObj.pantrylistDescription,@"ItemNo":pantryObj.itemNo,@"Synced":@"false",@"ToSync":@"true",@"SyncDateTime":@""}, nil];
            NSDictionary *params = [NSDictionary dictionaryWithObject:item forKey:@"items"];
            [[HumeApiClient SharedClient] POST:kPantryItemRemove parameters:params success:^(NSURLSessionDataTask *task, id ResponseObject)
             {
                 NSLog(@"%@",ResponseObject);
                 [weakSelf handleSuccessResponseForDeleteItemFromPantry:ResponseObject];
                 
             }failure:^(NSURLSessionDataTask *task, NSError *error)
             {
                 NSLog(@"Failed");
                 [weakSelf handleFailureResponseDeleteItemFromPantry];
             }
             ];
        };
    }

}

/**
 @brief: Success call back for deleting items from the pantry list
  - based on server status fetching the latest updated list.
 */
-(void)handleSuccessResponseForDeleteItemFromPantry:(id)ResponseObject
{
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    APIResponse *response = [APIResponse modelObjecttWithDictionary:ResponseObject];
    
    if(response.isSuccess==1)
    {
        [filterSegment setSelectedSegmentIndex:0];
        [self fetchDataFromServerPantryList];
    
    }else{
      
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:response.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }

}

-(void)handleFailureResponseDeleteItemFromPantry
{
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
}

#pragma mark UISEARCHBAR DELEGATE

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    //SEND  REQUEST ON SERVER FOR SEARCH AN ODER WITH ORDER NO
    //kSearchOrderNo
    
    [searchBar resignFirstResponder];
    
    if ([searchBar.text length]>0)
    {
        [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        [[HumeApiClient SharedClient]AddAccessTokenInHeader];
        NSString *customSearchURL = [NSString stringWithFormat:@"%@%@",kPantryItemSearch,[searchBar.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [[HumeApiClient SharedClient] GET:customSearchURL parameters:nil success:^(NSURLSessionDataTask *task, id ResponseObject)
         {
             NSLog(@"Pantry order: %@",ResponseObject);
             [self handleSuccessResponseForPantrySearch:ResponseObject];
             
         }failure:^(NSURLSessionDataTask *task, NSError *error)
         {
             NSLog(@"Pantry order: Failed");
             [self handleFailureResponsePantrySearch];
         }
         ];
    }else{
        [[[UIAlertView alloc]initWithTitle:@"" message:SEARCH_TEXT_REQUIRED delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        [searchBar resignFirstResponder];
        arrayPantryList = arrayPantryListCopy;
        [self.tableview reloadData];
    }
    
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    searchBar.showsCancelButton = YES;
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    searchBar.showsCancelButton=NO;
    arrayPantryList = arrayPantryListCopy;
    [filterSegment setSelectedSegmentIndex:0];
    [self.tableview reloadData];
}
/**
 @brief: Success call back for pantry search
  - based on server response status filtering the data
  - Reloading the tableview
 */
- (void)handleSuccessResponseForPantrySearch:(id)object {
    [filterSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
    APIResponse *response = [APIResponse modelObjecttWithDictionary:object];
    if(response.isSuccess==1)
    {
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        arrayPantryList = [[NSMutableArray alloc]initWithArray:response.data];
        [self.tableview reloadData];
    }else{
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:NO_RESULT_FOUND delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

- (void)handleFailureResponsePantrySearch{
    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    [[[UIAlertView alloc] initWithTitle:@"" message:NETWORK_ERROR delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    return;
}

/**
 @brief:
 - storage button Action.
 - Pickihng up selected segment index 
 - filtering storage array.
 */
-(IBAction)selectStorage:(id)sender{
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    if ([filterSegment selectedSegmentIndex]==0) {
        [self filterArrayByStorage:@"All" completion:^{
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        }];
    }
    if ([filterSegment selectedSegmentIndex]==1) {
        [self filterArrayByStorage:@"CHILLED" completion:^{
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        }];
    }
    if ([filterSegment selectedSegmentIndex]==2) {
        [self filterArrayByStorage:@"FROZEN" completion:^{
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        }];
    }
    if ([filterSegment selectedSegmentIndex]==3) {
        [self filterArrayByStorage:@"DRY" completion:^{
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        }];
    }
    
}

/**
 @brief:
  - Filtering array result based on filtring status.
 */
-(void)filterArrayByStorage:(NSString*)storage completion:(void(^)(void))callback{
    
    if ([storage isEqualToString:@"All"]) {
        arrayPantryList = arrayPantryListCopy;
        [self.tableview reloadData];
        callback();
    }
    else {
        NSPredicate *predStat = [NSPredicate predicateWithFormat:@"(ProductGroup==%@)",storage];
        NSArray *filteredArray = [arrayPantryListCopy filteredArrayUsingPredicate:predStat];
        arrayPantryList = [NSMutableArray arrayWithArray:filteredArray];
        [self.tableview reloadData];
        callback();
    }
    
}



@end

//
//  ViewController.m
//  Hume
//
//  Created by User on 12/29/14.
//  Copyright (c) 2014 Damco. All rights reserved.
//

#import "ViewController.h"
#import "MyAccountTableViewController.h"
#import "HumeApiClient.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import "APIResponse.h"
#import "Constants.h"
#import "ForgotPasswordViewController.h"
#import "RegistrationViewController.h"
#import "ChangeURLViewController.h"
#import "Settings.h"
#import "AppDelegate.h"

#define   NO_USER_PASSWORD @"Please enter a  username / password."
#define   REGISTRATION_SUCCESSFULL @"Thank you for registering an account. Access will be granted once your account has been verified by an Administrator"


@interface ViewController ()<UpdateUrlDelegate>

@property (nonatomic, strong) NSString *currentUrl;

@end

@implementation ViewController
@synthesize txtFieldLogin,txtFieldPassword,imgViewCheckBox,btnForgotPassword,lblSignIn;

#pragma mark View LifeCycle Methods
#pragma mark -

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    
    //Hiding Navigation Bar.
    self.navigationController.navigationBarHidden = YES;
    
    //Setting Navigation Bar Style
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;    //change status bar style if using navigation controller
    
    //Setting Navigation Bar Color based on Settings' class "navigationBarBackground" property value
	if ([APP_DELEGATE applicationTheameSetting] != nil){
		self.navigationController.navigationBar.barTintColor = [Utility colorFromHexString:[[APP_DELEGATE applicationTheameSetting] navigationBarBackground]];
	}
	else{
		self.navigationController.navigationBar.barTintColor = [Utility colorFromHexString:knavigationBarBackground];//[UIColor colorWithRed:2.0/255.0 green:133.0/255.0 blue:62.0/255.0 alpha:1.0];
	}
}


-(void)viewWillAppear:(BOOL)animated
{
    //Hiding Navigation Bar.
    self.navigationController.navigationBarHidden = true;

    [self configureServerUrl];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark Private Methods
#pragma mark -
/**
 @brief:
 IBAction of button "Sign In"
 This function gets called as an action to tap event on "Sign In" button in xib.
 @param: - sender, sender here representing the button reference.
 **/
- (IBAction)LoginBtnCall:(id)sender{
    
    [self.txtFieldLogin resignFirstResponder];
    [self.txtFieldPassword  resignFirstResponder];
    
    if ([self.txtFieldLogin.text length] && [self.txtFieldPassword.text length])
    {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[HumeApiClient SharedClient] POST:kLoginUser parameters:@{@"username":self.txtFieldLogin.text,@"pwd":self.txtFieldPassword.text} success:^(NSURLSessionDataTask *task, id ResponseObject)
        {
            NSLog(@"LOGIN::ResponseObject%@",ResponseObject);
            [self handleSuccessResponseForLogin:ResponseObject];
            
        }failure:^(NSURLSessionDataTask *task, NSError *error)
        {
             NSLog(@"Failed");
            self.currentUrl = @"";
            [self handleFailureResponseForLogin:error];
        }
        ];
        
    
     }else{
            [[[UIAlertView alloc] initWithTitle:@"" message:NO_USER_PASSWORD delegate:nil  cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
    
}

/**
 @brief:
 Function CallBack "handleSuccessResponseForLogin:"
 This function is a callback gets called when server request gets succeeded.
 This function checks if registration went successful.
 If yes then extracts the values from the response. 
 If not then showing the reason of failure.
 @param: - object, object containing server response.
 **/
- (void)handleSuccessResponseForLogin:(id)object {
    
    // [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    APIResponse *response = [APIResponse modelObjecttWithDictionary:object];
    
    if(response.isSuccess==1){
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        if ([[object valueForKey:@"CustomerNo"] isEqualToString:@"14136"]) {
             [[[UIAlertView alloc] initWithTitle:@"" message:REGISTRATION_SUCCESSFULL delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
            
            [[NSUserDefaults standardUserDefaults] setValue:[response.data valueForKey:@"token"]
                                                     forKey:ACCESSTOKEN];
           [[NSUserDefaults standardUserDefaults] setValue:[HumeApiClient SharedClient].baseURL.absoluteString forKey:@"defaultUrl"];

            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:[[object valueForKey:@"GetDisplayImage"] integerValue]] forKey:@"DISPLAYIMAGE"];
            [[NSUserDefaults standardUserDefaults] setValue:[object valueForKey:@"CustomerNo"] forKey:CUSTOMERNOKEY];
            [[NSUserDefaults standardUserDefaults] setValue:txtFieldLogin.text forKey:@"username"];
            [[NSUserDefaults standardUserDefaults] setValue:[[object valueForKey:@"CustomerEditEnabled"] stringValue] forKey:CUSTOMEREDITABLE];
		 
			 NSDictionary* dict = [object valueForKey:@"GetTheme"];
		// [APP_DELEGATE setApplicationTheameSetting:[Settings insertWithJSONData:dict]];
		 if (dict != nil){
			 [APP_DELEGATE setApplicationTheameSetting:[Settings insertWithJSONData:dict]];
		 }
            HomeViewController *homeViewObj = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
		 if ([APP_DELEGATE applicationTheameSetting] != nil){
			 self.navigationController.navigationBar.barTintColor = [Utility colorFromHexString:[[APP_DELEGATE applicationTheameSetting] navigationBarBackground]];
		 }
		 else{
           self.navigationController.navigationBar.barTintColor =  [Utility colorFromHexString:knavigationBarBackground];
		 }
        
            //[self presentViewController:homeViewObj animated:YES completion:nil];
            [self.navigationController pushViewController:homeViewObj animated:YES];
            
    
    }else{
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:response.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

/**
 @brief:
 Function CallBack "handleFailureResponseForLogin:"
 This function is a callback gets called when server request gets failed.
 @param: - error, object of NSError class containing information about request failure.
 **/
-(void)handleFailureResponseForLogin:(NSError*)error {
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [[[UIAlertView alloc] initWithTitle:@"" message:kStandardErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    return;
}

/**
 @brief:
 IBAction of button "Forgot password?"
 This function gets called as an action to tap event on "Forgot password?" button in xib.
 It will direct user to  Reset Password Screen.
 @param: - sender, sender here representing the button reference.
 **/
-(IBAction)ForgotBtnCall:(id)sender
{
    ForgotPasswordViewController *forgotObj = [[ForgotPasswordViewController alloc]init];
    [self.navigationController pushViewController:forgotObj animated:YES];
}

/**
 @brief:
 IBAction of button "Register Here"
 This function gets called as an action to tap event on "Register Here" button in xib.
 It will direct user to  Registration Screen.
 @param: - sender, sender here representing the button reference.
 **/
-(IBAction)RegisterBtnCall:(id)sender{
    RegistrationViewController *reg = [[RegistrationViewController alloc] init];
    [self.navigationController pushViewController:reg animated:YES];
}

#pragma mark UITextFieldDelegate methods
#pragma mark -
- (BOOL)textFieldShouldReturn:(UITextField *)textField{

    [textField resignFirstResponder];
    return  YES;
}

#pragma mark Change Url Stuff
#pragma mark -

/**
 @brief:
 IBAction of button "Change Url"
 This function gets called as an action to tap event on "Change Url" button in xib.
 It will direct user to Change Url Screen.
 @param: - sender, sender here representing the button reference.
 */
- (IBAction)openChangeUrlScreen:(id)sender {
    ChangeURLViewController *cUvc = [[ChangeURLViewController alloc ] init];
    cUvc.delegate = self;
    cUvc.currentUrl = self.currentUrl;
    [self.navigationController pushViewController:cUvc animated:true];
}

/**
 @brief:
 Function CallBack "didUrlUpdated:"
 This function is a callback gets called everytime the url is updated
 @param: - url, url which is updated
 */
- (void)didUrlUpdated:(NSString*)url;
{
  id share = [[HumeApiClient SharedClient]initWithBaseURL:[NSURL URLWithString:url] ];
    NSLog(@"share:%@",share);
}


/**
 @brief:
 Fuction: configureServerUrl
 This function checks, is there any url set as a default url and if not then sets it.
 **/
- (void)configureServerUrl
{
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"defaultUrl"] length]) {
        [[NSUserDefaults standardUserDefaults]setValue:kHumeBaseURLString forKey:@"defaultUrl" ];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"defaultUrl"] isEqualToString:kHumeBaseURLStringStage]){
        [[NSUserDefaults standardUserDefaults]setValue:kHumeBaseURLString forKey:@"defaultUrl" ];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
}

#pragma mark Memory Warning Method
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

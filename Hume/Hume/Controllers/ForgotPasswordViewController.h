//
//  ForgotPasswordViewController.h
//  Hume
//
//  Created by user on 27/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @brief: ForgotPasswordViewController Can perform formget password operation.
 User can update new password if forgot old by updating forgot link in server
 with passing Username & Email to server.
 Can perform email validation, entered by user.
 */
@interface ForgotPasswordViewController : UIViewController<UITextFieldDelegate>
//Properties
@property(nonatomic,retain)IBOutlet  UITextField *txtuserName;
@property(nonatomic,retain)IBOutlet  UITextField *txtemail;
@end

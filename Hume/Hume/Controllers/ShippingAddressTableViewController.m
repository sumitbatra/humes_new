//
//  ShippingAddressTableViewController.m
//  Hume
//
//  Created by user on 19/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "ShippingAddressTableViewController.h"
#import "MyAccountTableViewCell1.h"
#import "AddressViewViewController.h"
#import "Customerinfo.h"
#import "ShippingAddress.h"

#define ADDING_ADDRESS_DISABLED @"Adding address is disabled"
#define SHIPPING_ADDRESS_UPDATED  @"Primary shipping address changed successfully"

@interface ShippingAddressTableViewController ()

@end

@implementation ShippingAddressTableViewController
@synthesize arrayAddress,tableView;

-(id)init
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

        self = [[ShippingAddressTableViewController alloc]initWithNibName:@"ShippingAddressTableViewController" bundle:nil];
    
    return  self;
}

#pragma mark View LifeCycle Methods
#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Add new address button on top right corner on Navigation Bar
    UIButton * btnNewOrder = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 12, 15)];
    [btnNewOrder addTarget:self action:@selector(addNewAddress) forControlEvents:UIControlEventTouchUpInside];
    [btnNewOrder setBackgroundImage:[UIImage imageNamed:@"plusicon.png"] forState:UIControlStateNormal];
    [btnNewOrder.titleLabel setTextColor:[UIColor whiteColor ]];
    UIBarButtonItem *barButtonNewOrder = [[UIBarButtonItem alloc]initWithCustomView:btnNewOrder];
    [self.navigationItem setRightBarButtonItem:barButtonNewOrder];
}

-(void)viewWillAppear:(BOOL)animated
{
    //Setting Navigation Bar's Title Text Attributes
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                [UIColor whiteColor],
                                                                NSForegroundColorAttributeName,
                                                                [UIFont fontWithName:FONT_COND size:23],
                                                                NSFontAttributeName,
                                                                nil];
    
    [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:[UIColor whiteColor]];
    self.title = SHIPPING_ADD_TITLE;

    //Fetching saved addresses from server
    [self fetchDataFromServerShippingAddress];
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.title = @"";
}



#pragma mark Private Methods
#pragma mark -

/**
 @brief:
 Function  "fetchDataFromServerShippingAddres:"
 This function make server request to fetch shipping addresses
 */
-(void)fetchDataFromServerShippingAddress
{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    [[HumeApiClient SharedClient]AddAccessTokenInHeader];
    [[HumeApiClient SharedClient] GET:kShippingAddress parameters:nil success:^(NSURLSessionDataTask *task, id ResponseObject)
     {
         NSLog(@"%@",ResponseObject);
         [self handleSuccessResponseForShippingAddress:ResponseObject];
         
     }failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"Failed");
         [self handleFailureResponseShippingAddress];
     }
     ];
}

/**
 @brief:
 Function CallBack "handleSuccessResponseForShippingAddress:"
 This function is a callback gets called when server request for fetching shipping addresses gets succeeded.
 This function extracts shipping addresses from the response.
 @param: - object, object containing server response.
 */

- (void)handleSuccessResponseForShippingAddress:(id)object {
    
    // [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    APIResponse *response = [APIResponse modelObjecttWithDictionary:object];
    
    if(response.isSuccess==1)
    {
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        
        self.arrayAddress =  [[NSMutableArray  alloc]initWithArray:(NSArray*)response.data];
       
        [self.tableView reloadData];
        
    }else{
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:response.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

/**
 @brief:
 Function CallBack "handleFailureResponseShippingAddress:"
 This function is a callback gets called when server request for fetching shipping addresses gets failed.
 **/
- (void)handleFailureResponseShippingAddress{
    
    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    [[[UIAlertView alloc] initWithTitle:@"" message:kStandardErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    return;
}



/**
 @brief:
 Function: addNewAddress
 This function lets user add new address.
 */
-(void)addNewAddress
{
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMEREDITABLE] isEqualToString:@"1"]){    //CALL A NEW SHIP ADDRESS VIEW HERE FOR ADDING NEW ADDRESS
        AddressViewViewController *addressEditObj = [[AddressViewViewController alloc]initWithNibName:@"AddressViewViewController" bundle:nil];
        //[addressEditObj setDicData:[arrayAddress objectAtIndex:0]];
        [addressEditObj setCallerViewName:NEW_SHIPPING_ADD_TITLE];
        [self.navigationController pushViewController:addressEditObj animated:YES];
     }
    else {
        
        if ([UIAlertController class]) {
            UIAlertController *editAlert = [UIAlertController alertControllerWithTitle:@"" message:ADDING_ADDRESS_DISABLED preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //
            }];
            [editAlert addAction:okBtn];
            [self presentViewController:editAlert animated:YES completion:^{
                //
            }];
        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"" message:ADDING_ADDRESS_DISABLED delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }
    }
}


#pragma mark - Table view data source
#pragma mark -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [self.arrayAddress count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   return 100;
   
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ShippingAddress *shippingInfo = [[ShippingAddress alloc]initWithDictionary:[arrayAddress objectAtIndex:indexPath.row]];
    
    
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell; //= [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    // Configure the cell...
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier ];
    }
    
    [cell.textLabel setText:[NSString stringWithFormat:@"%@ %@ %@ %@ %@ \nContact No %@",shippingInfo.addressLine1 ,shippingInfo.addressLine2,shippingInfo.city,shippingInfo.state,shippingInfo.postcode,shippingInfo.phoneNo]];
    
    [cell.textLabel setFont:[UIFont fontWithName:FONT_REGULAR size:15]];
    [cell.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [cell.textLabel setNumberOfLines:6];
    //[cell.textLabel setTextColor:[UIColor whiteColor]];
    UIButton *editBtn = [[UIButton alloc ]initWithFrame:CGRectMake(SCREEN_WIDTH-20, 40, 15, 15)];
    [editBtn setBackgroundImage:[UIImage imageNamed:@"icon_edit.png"] forState:UIControlStateNormal] ;
    [cell.contentView addSubview:editBtn];
    [editBtn setTag:indexPath.row];
    
    [editBtn addTarget:self action:@selector(editBtnCall:) forControlEvents:UIControlEventTouchUpInside];
    //[cell setBackgroundColor:[Utility colorFromHexString:@"3c72BE"]];
    //[cell.contentView setBackgroundColor:[Utility colorFromHexString:@"3c72BE"]];
     [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return  cell;
}


#pragma mark - Table View Delegate Methods
#pragma mark -
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.shippingMode==ShippingAddressChange) {
        // change primary
        [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        ShippingAddress *shipToAddress = [ShippingAddress modelObjectWithDictionary:[arrayAddress objectAtIndex:indexPath.row]];
        [[HumeApiClient SharedClient] POST:kPrimaryShpiptoAddress parameters:[shipToAddress dictionaryRepresentation] success:^(NSURLSessionDataTask *task, id responseObject) {
            if ([[responseObject valueForKey:@"statusCode"] isEqualToString:@"1"]) {
                [[[UIAlertView alloc] initWithTitle:@"" message:SHIPPING_ADDRESS_UPDATED delegate:self cancelButtonTitle:ALERT_BTN_PROCEED otherButtonTitles:nil, nil] show];
                [self.navigationController popViewControllerAnimated:YES];
            }
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
             [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        }];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    NSString *buttonText = [alertView buttonTitleAtIndex:buttonIndex];
    if ([buttonText isEqualToString:ALERT_BTN_PROCEED]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FETCH_ORDERS" object:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }

}


/**
 IBAction of button "Edit"
 This function can update saved shipping address after moving to Address View controller.
 Parameter - sender, sender here representing the button reference.
 **/

-(IBAction)editBtnCall:(id)sender
{
    UIButton *btnTemp = (UIButton *)sender;
    
     if ([[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMEREDITABLE] isEqualToString:@"1"]) {
         //Open address edit view here
         AddressViewViewController *addressEditObj = [[AddressViewViewController alloc]initWithNibName:@"AddressViewViewController" bundle:nil];
         [addressEditObj setDicData:[arrayAddress objectAtIndex:btnTemp.tag]];
         [addressEditObj setCallerViewName:SHIPPING_ADD_TITLE];
         [self.navigationController pushViewController:addressEditObj animated:YES];
     }
     else {
         if ([UIAlertController class]) {
             UIAlertController *editAlert = [UIAlertController alertControllerWithTitle:@"" message:ADDING_ADDRESS_DISABLED preferredStyle:UIAlertControllerStyleAlert];
             UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                 //
             }];
             [editAlert addAction:okBtn];
             [self presentViewController:editAlert animated:YES completion:^{
                 //
             }];
         }
         else {
             [[[UIAlertView alloc] initWithTitle:@"" message:ADDING_ADDRESS_DISABLED delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
         }
     }
}
#pragma mark - Memory Management Methods
#pragma mark -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  DatePickerViewController.h
//  Hume
//
//  Created by User on 15/10/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>

//Protocol
@protocol MyDatePickerDelegate <NSObject>
- (void)dateDidSelect:(NSString*)dateStr;
@end

/**
 @brief: DatePickerViewController Can perform date selection operation for different controllers.
 Pressessor view controller can Add MyDatePickerDelegate for getting selected Date.
 */
@interface DatePickerViewController : UIViewController

//Properties
@property (strong, nonatomic) NSDate *date;
@property (weak, nonatomic) id<MyDatePickerDelegate>delegate;

@end

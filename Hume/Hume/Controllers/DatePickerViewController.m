//
//  DatePickerViewController.m
//  Hume
//
//  Created by User on 15/10/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "DatePickerViewController.h"


@interface DatePickerViewController ()
@property (weak, nonatomic) IBOutlet UIDatePicker *datePIcker;
@property (strong,nonatomic) NSString *selectedDate;
@end

@implementation DatePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.title = @"Shipment Date";
    self.selectedDate = [self dateStringWithFormat:[NSDate date]];
    self.datePIcker.minimumDate = [NSDate date];
   // [self.datePIcker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
    
		// self.datePIcker.backgroundColor = [UIColor lightGrayColor];
    [self leftBarbutton];
}


- (void)viewDidDisappear:(BOOL)animated
{
    if ([self.delegate conformsToProtocol:@protocol(MyDatePickerDelegate)]) {
        [self.delegate dateDidSelect:self.selectedDate];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Private Methods
/**
 @brief: Will get selected date from Picker
 by calling simply [self dateStringWithFormat:date];
 
 @param: (id)sender
 */
- (IBAction)getDate:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)sender;
    NSDate *date = [picker date];
    self.selectedDate = [self dateStringWithFormat:date];
    
}

/**
 @brief: Will set specific date formate.
 @param: (NSDate*)date
 */
- (NSString *) dateStringWithFormat:(NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    return [formatter stringFromDate:date];
}

/**
 @brief: Left bar button creation. with done title
 */
- (void)leftBarbutton
{
    UIButton *leftBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
    [leftBtn setTitle:@"Done" forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(done) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barBtn = [[UIBarButtonItem alloc]
                                   initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = barBtn;
}

- (void)done
{
    [self.navigationController popViewControllerAnimated:true];
}
@end

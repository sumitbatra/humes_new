//
//  ChangeURLViewController.h
//  Hume
//
//  Created by User on 10/14/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>

//Protocol
@protocol UpdateUrlDelegate <NSObject>
- (void)didUrlUpdated:(NSString*)url;
@end

/**
 @brief:  Can perform operation related to server url .
 User can manually enter a server url in place of default.
 And logged in successfully after updating recently updated Url will be considered as default url.
 */
@interface ChangeURLViewController : UIViewController

//Properties
@property (weak, nonatomic) id<UpdateUrlDelegate>delegate;
@property (assign, nonatomic) NSString *currentUrl;

@end

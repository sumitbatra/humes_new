//
//  MyAccountTableViewController.h
//  Hume
//
//  Created by User on 12/29/14.
//  Copyright (c) 2014 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @brief: MyAccountTableViewController performing operation attached with user info like customer name, billing address, shipping address, Amount out standing 
 */
@interface MyAccountTableViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    BOOL cellExpanded;
}
//Properties
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerCode;
@property(nonatomic,retain) IBOutlet  UILabel *lblCustomerName;
@property(nonatomic,retain) IBOutlet  UILabel *lblCustomerRewardsPnt;
@property(nonatomic,retain) IBOutlet  UILabel *lblCustomer;
@property(nonatomic,retain) IBOutlet  UILabel *lblBalance;
@end

//
//  PromotionTableViewController.h
//  Hume
//
//  Created by user on 03/02/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @brief: PromotionTableViewController will show all available promotions like Discounts, MultiBuy, Upsell, Rewards etc.
 */
@interface PromotionTableViewController : UITableViewController

@end

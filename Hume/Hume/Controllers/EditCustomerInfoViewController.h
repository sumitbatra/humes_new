//
//  EditCustomerInfoViewController.h
//  Hume
//
//  Created by user on 27/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 @brief: EditCustomerInfoViewController will perform operation related to customer info updations like
 fetching all availble shipping address & update the selected one.
 */
@interface EditCustomerInfoViewController : UIViewController<UITextFieldDelegate>
{
    
}

//Properties
@property(nonatomic,retain) NSMutableDictionary *dicInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtContactName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtContactNo;
@property (weak, nonatomic) IBOutlet UITextField *txtTelex;
@property (weak, nonatomic) IBOutlet UITextField *txtFax;
@property (weak, nonatomic) IBOutlet UIButton *btnClear;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (nonatomic,retain) NSString *callerViewName;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewCust;
@end

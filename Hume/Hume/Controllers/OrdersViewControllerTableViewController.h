//
//  OrdersViewControllerTableViewController.h
//  Hume
//
//  Created by user on 15/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailViewController.h"
#import "EditOrderTableViewController.h"

//Enum
typedef enum  OrderSelectionMode {
    OrderSelectionModeTypes = 0,
    OrderSelectionAllOrders,
    OrderSelectionOpenOrders,
}OrderSelectionFetchMode;

/**
 @brief: OrdersViewControllerTableViewController will display all order list with differnt status like Open, Processing, Completed, Closed etc.
 Can edit selected order, Create new order, Filter based on search criteria, also perform the order details.
 RelativeViewCOntroller: ProductListingViewController,EditOrderTableViewController
 */
@interface OrdersViewControllerTableViewController : UIViewController<EditOrderTableViewControllerDelegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate, UIAlertViewDelegate>
{
    void(^someBlock)(void);
    void(^editCall)(void);
    IBOutlet UISegmentedControl *filterSegment;
}
//Properties
@property (nonatomic, retain) NSString *priceOfItem;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UILabel *lblSerachOrderResult;
@property (weak, nonatomic) IBOutlet UISearchBar *uiSearchBarOrder;
@property (nonatomic, assign) OrderSelectionFetchMode orderSelectionMode;
@property (nonatomic,retain) NSString *strCallerView;
@property (nonatomic,retain) ProductDetailViewController *productDetailObj;
//Methods
-(IBAction)addNeworder;
-(IBAction)selectStatus:(id)sender;
@end

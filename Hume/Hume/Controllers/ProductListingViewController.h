//
//  ProductListingViewController.h
//  Hume
//
//  Created by User on 1/20/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Orders.h"
#import "ProductDetailViewController.h"

/**
 @brief:  ProductListingViewController Will show list of Products with different criteria list like Chelled, Frozen Dry
 Can filter product list with specific criteria.
 Can perform Add to templete and Add to Cart.
 RelativeViewControllers: ProductDetailViewController
 */
@interface ProductListingViewController : UIViewController<UISearchBarDelegate, ProductDetailViewControllerDelegate>{

    IBOutlet UITableView *tblProductList;
    NSMutableArray *listOfProducts;
    NSMutableArray *listOfProductsCopy;
    NSMutableArray *listOfSearchResults;
    NSInteger pageCount;
    IBOutlet UISearchBar *searchResults;
    IBOutlet UISegmentedControl *filterSegment;
    
}
//Properties
@property(nonatomic,retain) Orders *orderObj;
@property(nonatomic,retain)NSString *strCallerViewName;
@property(nonatomic,retain)NSString *strOrderNo;
@property(nonatomic,retain)NSString *strLineNo;

//Methods
-(IBAction)selectStorage:(id)sender;

@end

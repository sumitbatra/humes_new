//
//  ForgotPasswordViewController.m
//  Hume
//
//  Created by user on 27/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "APIResponse.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import "Constants.h"
#import "HumeApiClient.h"
#import "Utility.h"



@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController


-(id)init
{
    return  self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Utility setTintColorOfNavigationBar:[UIColor whiteColor] OnNavigationController:self.navigationController];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Private Methods

/** 
 @brief: Will check valid user name & email
 
 Success:Start service call with post type using default pareameter like user name & email. If request get success get success then call
 handleSuccessResponseForForgot with ResponseObject
 
 Failure: Will show alert user email required.
 */

//FIXME: WebService class should have wrappers on direct REST API methods
//FIXME: Request & Response should be handled by a Webservice class, only data should be provided to the calling class.
- (IBAction)ForgotBtnCall:(id)sender{
    
    [self.txtuserName resignFirstResponder];
    [self.txtemail  resignFirstResponder];
    
    if ([self NSStringIsValidEmail:self.txtemail.text] && [self.txtuserName.text length])
    {
       [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        [[HumeApiClient SharedClient] POST:kForgotPassWord parameters:@{@"username":self.txtuserName.text,@"Email":self.txtemail.text} success:^(NSURLSessionDataTask *task, id ResponseObject)
         {
             NSLog(@"Forgot::ResponseObject%@",ResponseObject);
             [self handleSuccessResponseForForgot:ResponseObject];
             
         }failure:^(NSURLSessionDataTask *task, NSError *error)
         {
             NSLog(@"Failed");
             [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
             [[[UIAlertView alloc] initWithTitle:@"" message:kStandardErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
             return;

         }
         ];
        
        
    }else{
        [[[UIAlertView alloc] initWithTitle:@"" message:USER_EMAIL_REQUIRED delegate:nil  cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
    
}

/**
 @brief: Will accept checking string & based on
 Nspredicate will check for valid email charectors.
 */
//Validate Email address
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [self.view.window endEditing:true];
    [textField resignFirstResponder];
    return YES;
}

/**
 @brief: A callback success block with accepting id type object
 Will check status code for failure & Success, And show user interaction message
 
 @param: (id)object
 */

- (void)handleSuccessResponseForForgot:(id)object
{
    APIResponse *response = [APIResponse modelObjecttWithDictionary:object];
    if(response.isSuccess==1){
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:response.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        [self.navigationController popViewControllerAnimated:YES];
      
    }else{
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:response.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}


@end

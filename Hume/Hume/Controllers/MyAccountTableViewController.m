//
//  MyAccountTableViewController.m
//  Hume
//
//  Created by User on 12/29/14.
//  Copyright (c) 2014 Damco. All rights reserved.
//

#import "MyAccountTableViewController.h"
#import "CustomLabel.h"
#import "MyAccountTableViewCell1.h"
#import "UIViewController+MFSideMenuAdditions.h"
#import "MFSideMenuContainerViewController.h"

#import "HumeApiClient.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import "APIResponse.h"
#import "Constants.h"
#import "Customerinfo.h"
#import "ShippingAddress.h"
#import "AmountOutStandingTableViewCell.h"
#import "ShippingAddressTableViewController.h"
#import "AddressViewViewController.h"
#import "EditCustomerInfoViewController.h"

#import "HomeViewController.h"
#import "AppDelegate.h"



@interface MyAccountTableViewController ()
{
    NSArray *dataArray;
    NSMutableArray *expandedCells;
    UIView *customWhiteView;
    ShippingAddress *shippingAddress;
    Customerinfo  *customerInfo;
    NSMutableArray *shippingAry;
    NSIndexPath *CurrentIndexPath;
    NSDictionary *customerDic;
    NSMutableArray *indexAry;
    BOOL menuOpen;
}
@end



@implementation MyAccountTableViewController
NSInteger selectedRowNumber = -1;
@synthesize lblCustomerCode,lblCustomerName,lblBalance,lblCustomerRewardsPnt,lblCustomer;

-(id)init
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self = [[MyAccountTableViewController alloc]initWithNibName:@"MyAccountTableViewController" bundle:nil];
        
    }else{
        self = [[MyAccountTableViewController alloc]initWithNibName:@"MyAccountTableViewController" bundle:nil];
    }
    return  self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goHome:) name:@"GoToHome" object:nil];
    //[Utility setTintColorOfNavigationBar:[UIColor whiteColor] OnNavigationController:self.navigationController];
    [Utility setTintColorOfNavigationBar:[UIColor whiteColor] OnNavigationController:self.navigationController];
    // [Utility setTitleOnNavigationControllerObj:self.navigationController WithString:MY_ACCOUNT_TITLE];
    // [self setTitle:MY_ACCOUNT_TITLE];
    
    self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
    menuOpen = NO;
    
    indexAry = [[NSMutableArray alloc]init];
    dataArray = [[NSMutableArray alloc]initWithObjects:@"  CUSTOMER NAME", @"  BILLING ADDRESS", @"  SHIPPING ADDRESS", @"  AMOUNT OUTSTANDING", nil];
    NSString *idfMA = @"MyAccount";
    NSString *idfAM = @"Amount";
    
    [self.tableview registerNib:[UINib nibWithNibName:@"MyAccountTableViewCell1" bundle:nil] forCellReuseIdentifier:idfMA];
    [self.tableview registerNib:[UINib nibWithNibName:@"AmountOutStandingTableViewCell" bundle:nil] forCellReuseIdentifier:idfAM];
    expandedCells = [[NSMutableArray alloc]init];
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
   [indexAry removeAllObjects];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                   [UIColor whiteColor],
                                                                   NSForegroundColorAttributeName,
                                                                   [UIFont fontWithName:FONT_COND size:23],
                                                                   NSFontAttributeName,
                                                                   nil];
    
    [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:[UIColor whiteColor]];
     self.title = @"My Account";
    [self fetchDataFromServerMyAccount];

}
-(void)viewWillDisappear:(BOOL)animated
{
    self.title = @"";
    
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
#if !__has_feature(objc_arc)
    [super dealloc];
#endif
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Private Methods

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



/**
 @brief: Can perform go back to home operation from side menu bar.
 @param: (id)selector
 */
- (void)goHome:(id)selector{
    
    HomeViewController *homeObj = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    
    UINavigationController *nvcontrol = [[UINavigationController alloc]initWithRootViewController:homeObj];
	
	if ([APP_DELEGATE applicationTheameSetting] != nil){
		nvcontrol.navigationBar.barTintColor = [Utility colorFromHexString:[[APP_DELEGATE applicationTheameSetting] navigationBarBackground]];
	}
	else{
		nvcontrol.navigationBar.barTintColor = [Utility colorFromHexString:knavigationBarBackground];//[UIColor colorWithRed:2.0/255.0 green:133.0/255.0 blue:62.0/255.0 alpha:1.0];
	}
    nvcontrol.navigationBar.translucent = NO;
    [homeObj adjustOffsets:40];
    
    [[APP_DELEGATE window] setRootViewController:nvcontrol];
    
}


-(UIBarButtonItem*)leftMenuBarButtonItem{
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonPressed)];
    return self.navigationItem.leftBarButtonItem;
    
}


/**
 @brief: Will show side menu options.
 */
-(void)menuButtonPressed
{
    
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
        if (self.menuContainerViewController.menuState == MFSideMenuStateClosed) {
            menuOpen = NO;
        }
        else{
            menuOpen = YES;
        }
    }];
    
}

#pragma FETCHING DATA FROM SERVER FORMY ACCOUNT
/** 
 @brief: Getting customer details from server with callback
 
 Success: handleSuccessResponseForMyAccount
 
 Failure: handleFailureResponseMyAccount
 
 */

//FIXME: WebService class should have wrappers on direct REST API methods
//FIXME: Request & Response should be handled by a Webservice class, only data should be provided to the calling class.
-(void)fetchDataFromServerMyAccount
{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    [[HumeApiClient SharedClient]AddAccessTokenInHeader];
    [[HumeApiClient SharedClient] POST:kCustomerDetailSMyAccount parameters:nil success:^(NSURLSessionDataTask *task, id ResponseObject)
     {
         NSLog(@"%@",ResponseObject);
         [self handleSuccessResponseForMyAccount:ResponseObject];
         
     }failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"Failed");
         [self handleFailureResponseMyAccount];
     }
     ];

    
}

/** 
 @brief: Success call back for fetchDataFromServerMyAccount
 @param: (id)object
 */
- (void)handleSuccessResponseForMyAccount:(id)object {
    
    // [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    APIResponse *response = [APIResponse modelObjecttWithDictionary:object];
    
    if(response.isSuccess==1)
    {
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        
        shippingAry = [[NSMutableArray alloc]initWithArray:[response.data valueForKey:@"shippingAddress" ]];
        shippingAddress = [ShippingAddress modelObjectWithDictionary:[shippingAry objectAtIndex:0]];
        int i=0;
        for (i=0; i<[shippingAry count];i++)
        {
          ShippingAddress *shipObj    = [ShippingAddress modelObjectWithDictionary:[shippingAry objectAtIndex:i]];
            if ([shipObj.primary boolValue]==1)
            {
               shippingAddress = [ShippingAddress modelObjectWithDictionary:[shippingAry objectAtIndex:i]];
            }
        }
        
        customerInfo = [Customerinfo modelObjectWithDictionary:(NSDictionary *)[response.data valueForKey:@"customerinfo"]];
        customerDic = [NSDictionary dictionaryWithDictionary:(NSDictionary *)[response.data valueForKey:@"customerinfo"]];
        //[[NSUserDefaults standardUserDefaults] setValue:customerInfo.noProperty forKey:CUSTOMERNOKEY];
        [self.lblCustomerCode setText:[NSString stringWithFormat:@"Code : %@",customerInfo.noProperty]];
        [self.lblCustomerName setText:[NSString stringWithFormat:@"Name : %@",customerInfo.name]];
        [self.lblBalance setText:[NSString stringWithFormat:@"Balance : %@",customerInfo.balance]];
        [self.lblCustomerRewardsPnt setText:[NSString stringWithFormat:@"Rewards : %@",customerInfo.rewards]];
        
        
        [self.tableview reloadData];
        
    }else{
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        if (response.data) {
             [[[UIAlertView alloc] initWithTitle:@"" message:response.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else {
             [[[UIAlertView alloc] initWithTitle:@"" message:NO_PROFILE delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
       
    }
}


/**
 @brief: failure call back for fetchDataFromServerMyAccount
 */
- (void)handleFailureResponseMyAccount{
    
    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    [[[UIAlertView alloc] initWithTitle:@"" message:kStandardErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    return;
}


#pragma mark - Table view data source




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    
    
    return 4;
    //return [dataArray count];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat ExpandedCellHeight = 131;
    CGFloat NormalCellHeight = 35;
    
    
    if ([expandedCells containsObject:indexPath])
    {
        //[customWhiteView setFrame:kExpandedFrame];
        NSLog(@"ExpandedCell");
        return ExpandedCellHeight; //It's not necessary a constant, though
        
    }
    else
    {
        //[customWhiteView setFrame:kCollapsedFrame];
         NSLog(@"NormalCell");
        return NormalCellHeight; //Again not necessary a constant
    }
}

// FIXME: So many lines of code in a single methods. Better approach should be followed. Create common methods if possible.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Identifing here which cell will be load at runtime.
    //AmountOutStandingTableViewCell
    
    static NSString *CellIdentifier1 = @"MyAccount";
    static NSString *CellIdentifier2 = @"Amount";
    
     if (indexPath.row<3)
    {
        //CellIdentifier = @"MyAccountTableViewCell1";
        MyAccountTableViewCell1 *cell = (MyAccountTableViewCell1 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        
        [cell.uiViewEnxpandableView setHidden:YES];
        
        
        if (IS_IPHONE)
        {
            [cell.txtViewAddress setFrame:CGRectMake(0, 13, SCREEN_WIDTH, 90)];
            
        }
        cell.lblCellHeading.text = [dataArray objectAtIndex:indexPath.row];
        //Set tag value for edit button
        [cell.btnEdit setTag:indexPath.row];
        [cell.btnEdit addTarget:self action:@selector(editBtnCall:) forControlEvents:UIControlEventTouchUpInside];
        
        switch (indexPath.row)
        {
            case 0:
               
                [cell.txtViewAddress setHidden:false];
                [cell.lblFax setHidden:FALSE];
                [cell.lblTelex setHidden:FALSE];
                [cell.lblEmail setHidden:FALSE];
                [cell.lblContactNo setHidden:FALSE];
                [cell.lblCustomerName setHidden:FALSE];
                [cell.txtViewAddress setText:@""];
                
                if ([expandedCells containsObject:indexPath]) {
                    [cell.uiViewEnxpandableView setHidden:NO];
                }
               
                [cell.lblCustomerName setText:[NSString stringWithFormat:@"Name : %@",customerInfo.contact]];
                [cell.lblTelex setText:[NSString stringWithFormat:@"Phone : %@",customerInfo.phoneNo]];
                [cell.lblFax setText:[NSString stringWithFormat:@"Fax : %@",customerInfo.fax]];
                [cell.lblEmail setText:[NSString stringWithFormat:@"Email : %@",customerInfo.email]];
                [cell.lblContactNo setText:[NSString stringWithFormat:@"Contact No : %@",customerInfo.phoneNo]];

                
                break;
                
            case 1:
                //Show primary address only.
                
                cell.txtViewAddress.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ \n %@",customerInfo.address,customerInfo.address2,customerInfo.city,customerInfo.state, customerInfo.postCode,customerInfo.phoneNo];
                
                [cell.txtViewAddress setHidden:false];
                
                [cell.lblFax setHidden:TRUE];
                [cell.lblTelex setHidden:TRUE];
                [cell.lblEmail setHidden:TRUE];
                [cell.lblContactNo setHidden:TRUE];
                [cell.lblCustomerName setHidden:TRUE];
                if ([expandedCells containsObject:indexPath]) {
                    [cell.uiViewEnxpandableView setHidden:NO];
                }
                
                break;
                
            case 2:
                cell.txtViewAddress.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ \n %@",shippingAddress.addressLine1,shippingAddress.addressLine2,shippingAddress.city,shippingAddress.state,shippingAddress.postcode,shippingAddress.phoneNo];
                
                [cell.txtViewAddress setHidden:FALSE];
                [cell.lblFax setHidden:TRUE];
                [cell.lblTelex setHidden:TRUE];
                [cell.lblEmail setHidden:TRUE];
                [cell.lblContactNo setHidden:TRUE];
                [cell.lblCustomerName setHidden:TRUE];
                if ([expandedCells containsObject:indexPath]) {
                    [cell.uiViewEnxpandableView setHidden:NO];
                }
                
                break;
                
            case 3:
                
                break;
            default:
                break;
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
        
    }else{
        // CellIdentifier = @"AmountOutStandingTableViewCell";
        AmountOutStandingTableViewCell *cell = (AmountOutStandingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        
        [cell.uiViewEnxpandableView setHidden:YES];
        cell.lblAmountOustanding.text = [dataArray objectAtIndex:indexPath.row];
        [cell.lblBalance1Value setText:[NSString stringWithFormat:@"$%@",customerInfo.amountDueGT30days]];
        [cell.lblBalance2Value setText:[NSString stringWithFormat:@"$%@",customerInfo.amountDueGT60days]];
        [cell.lblBalance3Value setText:[NSString stringWithFormat:@"$%@",customerInfo.amountDueGT90days]];
        
        //[cell.lblAmountOustanding setFrame:CGRectMake(0, 0, 320, 30)];

        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        if ([expandedCells containsObject:indexPath]) {
            [cell.uiViewEnxpandableView setHidden:NO];
        }
        return cell;
    }
    
    
    
    
    
  
}

#pragma mark - Table view delegate

// FIXME: So many lines of code in a single methods. Better approach should be followed. Create common methods if possible.

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedRowNumber = indexPath.row;
   
    CurrentIndexPath = indexPath;
     [indexAry addObject:indexPath];
    
    if (indexPath.row<3)
    {
        MyAccountTableViewCell1 *myaccountTableViewCell = (MyAccountTableViewCell1 *)[tableView cellForRowAtIndexPath:indexPath];
        if ([expandedCells containsObject:indexPath])
        {
            [expandedCells removeObject:indexPath];
            [myaccountTableViewCell.uiViewEnxpandableView setHidden:TRUE];
            [myaccountTableViewCell.imgaeViewArrow setImage:[UIImage imageNamed:@"arrow_bottom_small.png"]];
        }
        else
        {
            [myaccountTableViewCell.uiViewEnxpandableView setHidden:FALSE];
            [myaccountTableViewCell.imgaeViewArrow setImage:[UIImage imageNamed:@"arrow_top_small.png"]];
            [expandedCells addObject:indexPath];
        }
        //for Iphone specific
        if (IS_IPHONE)
        {
            [myaccountTableViewCell.txtViewAddress setFrame:CGRectMake(0, 13, SCREEN_WIDTH, 90)];
    
        }
        
        [tableView beginUpdates];
        [tableView endUpdates];
        
    }else{
        
        
        AmountOutStandingTableViewCell *amountCell = (AmountOutStandingTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        if ([expandedCells containsObject:indexPath])
        {
            [expandedCells removeObject:indexPath];
            [amountCell.uiViewEnxpandableView setHidden:TRUE];
            [amountCell.imgaeViewArrow setImage:[UIImage imageNamed:@"arrow_bottom_small.png"]];
        }
        else
        {
            [amountCell.uiViewEnxpandableView setHidden:FALSE];
            [amountCell.imgaeViewArrow setImage:[UIImage imageNamed:@"arrow_top_small.png"]];
            [expandedCells addObject:indexPath];
        }
       // [amountCell.lblAmountOustanding setFrame:CGRectMake(0, 0, 320, 30)];

        [tableView beginUpdates];
        [tableView endUpdates];
    }
    
}

/**
 @brief: Edit button call while pressing edit order.
 @param: (id)sender
 */

// FIXME: So many lines of code in a single methods. Better approach should be followed. Create common methods if possible.
-(IBAction)editBtnCall:(id)sender
{
    UIButton *btnTemp = (UIButton *)sender;
   
    
    
    
    for (NSIndexPath *indexpath in indexAry)
    {
        MyAccountTableViewCell1 *myaccountTableViewCell = (MyAccountTableViewCell1 *)[self.tableview cellForRowAtIndexPath:indexpath];
        if ([expandedCells containsObject:indexpath])
        {
            [expandedCells removeObject:indexpath];
            [myaccountTableViewCell.uiViewEnxpandableView setHidden:TRUE];
            [myaccountTableViewCell.imgaeViewArrow setImage:[UIImage imageNamed:@"arrow_bottom_small.png"]];
        }
        else
        {
            
            [myaccountTableViewCell.uiViewEnxpandableView setHidden:FALSE];
            [myaccountTableViewCell.imgaeViewArrow setImage:[UIImage imageNamed:@"arrow_top_small.png"]];
            [expandedCells addObject:indexpath];
        }
        
        //[myaccountTableViewCell.lblCellHeading setFrame:CGRectMake(0, 0, 320, 30)];
        
        
        [self.tableview beginUpdates];
        [self.tableview endUpdates];
        
    }
    //[self.tableview deselectRowAtIndexPath:CurrentIndexPath animated:YES];
     switch (btnTemp.tag)
    {
        case 0:
            //For customer Info edit
            {
                if ([[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMEREDITABLE] isEqualToString:@"1"]){
                    // Pass a dic for data
                    EditCustomerInfoViewController *editInfoViewObj = [[EditCustomerInfoViewController alloc]initWithNibName:@"EditCustomerInfoViewController" bundle:nil];
                    [editInfoViewObj setDicInfo:[customerDic mutableCopy]];
                    
                    [self.navigationController pushViewController:editInfoViewObj animated:YES];
                }
                else {
                    
                    if ([UIAlertController class]) {
                        UIAlertController *editAlert = [UIAlertController alertControllerWithTitle:@"" message:CUSTOMER_INFO_EDIT_DISBL preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                            //
                        }];
                        [editAlert addAction:okBtn];
                        [self presentViewController:editAlert animated:YES completion:^{
                            //
                        }];
                    }
                    else {
                        [[[UIAlertView alloc] initWithTitle:@"" message:CUSTOMER_INFO_DISBL delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                    }
                }
                
            }
            break;
        case 1:
        { //For Billing addrees
            
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMEREDITABLE] isEqualToString:@"1"]) {
                AddressViewViewController *addressEditObj = [[AddressViewViewController alloc]initWithNibName:@"AddressViewViewController" bundle:nil];
                //[addressEditObj setDicData:[arrayAddress objectAtIndex:0]];
                [addressEditObj setCallerViewName:BILLING_ADD_TITLE];
                [addressEditObj setDicData:customerDic];
                [self.navigationController pushViewController:addressEditObj animated:YES];
            }
            else {
                
                if ([UIAlertController class]) {
                    UIAlertController *editAlert = [UIAlertController alertControllerWithTitle:@"" message:ADDRESS_EDIT_DISBL preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                        //
                    }];
                    [editAlert addAction:okBtn];
                    [self presentViewController:editAlert animated:YES completion:^{
                        //
                    }];
                }
                else {
                    [[[UIAlertView alloc] initWithTitle:@"" message:ADDRESS_EDIT_DISBL delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                }
                
                
            }
            
            
        }
            break;
        case 2:
        { //For Shipping address
            
            ShippingAddressTableViewController *shipAddressObj = [[ShippingAddressTableViewController  alloc]init];
           [self.navigationController pushViewController:shipAddressObj animated:YES];
        }
            break;
        default:
            break;
    }
    
}



@end

//
//  ShippingAddressTableViewController.h
//  Hume
//
//  Created by user on 19/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import "APIResponse.h"
#import "Constants.h"
#import "HumeApiClient.h"

//Enum
typedef enum  ShippingAddressMode {
    ShippingAddressModeTypes = 0,
    ShippingAddressChange
}ShippingAddressFetchMode;


@protocol ShippingAddressTableViewControllerDelegate;

/**
 @brief: ShippingAddressTableViewController Will display all avalaible shipping address
 Can Edit shipping address.
 Can Add multiuple shipping addresses
 RelativeViewControllers: AddressViewViewController
 */
@interface ShippingAddressTableViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    
    
}

//Properties
@property(nonatomic,retain)IBOutlet UITableView *tableView;
@property(nonatomic,retain)NSMutableArray *arrayAddress;
@property (nonatomic,assign)ShippingAddressFetchMode shippingMode;
@property (nonatomic,assign)id<ShippingAddressTableViewControllerDelegate> delegate;
@end


//Protocol
@protocol ShippingAddressTableViewControllerDelegate <NSObject>
-(void)fetchAllOrders;
@end
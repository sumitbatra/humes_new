//
//  EditOrderTableViewController.m
//  Hume
//
//  Created by user on 02/02/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "EditOrderTableViewController.h"
#import "HumeApiClient.h"
#import "Constants.h"
#import "APIResponse.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import "EditOrderTableViewCell.h"
#import "OrderDetails.h"
#import "ProductListingViewController.h"
#import "ProductDetailViewController.h"
#import "DatePickerViewController.h"
#import "AppDelegate.h"


static NSString *CellIdentifier1 = @"EditOrderTableViewCell";
static NSString *CellIdentifier2 = @"saleOrderinfoCell";



@interface EditOrderTableViewController ()<MyDatePickerDelegate>
{
    NSMutableArray *aryOrderDetails;
    BOOL editable;
}
@property (weak, nonatomic) IBOutlet UILabel *deliveryDate;
@property (strong, nonatomic) NSString *shipmentDate;

@end

@implementation EditOrderTableViewController

// FIXME: Don't put random code directly. Add documented code in a method serving the purpose & call it here.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    aryOrderDetails =  [[NSMutableArray alloc]init];
    self.itemstodelete_array = [[NSMutableArray alloc]init];
    
    if ([self.ordersModelObj.status isEqualToString:@"A"]) {
        
        UIBarButtonItem *deleteButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteOrder:)];
        self.navigationItem.rightBarButtonItem = deleteButton;
        editable = YES;
    } else {
        //do nothing
        editable = NO;
        [self.btn_catalogue setHidden:TRUE];
        [self.btn_process setHidden:TRUE];
        
    }
    
    UINib *nib = [UINib nibWithNibName:@"EditOrderTableViewCell" bundle:nil];
    [self.tableview registerNib:nib forCellReuseIdentifier:CellIdentifier1];
    
    nib = [UINib nibWithNibName:@"saleOrderinfoCell" bundle:nil];
    [self.tableview registerNib:nib forCellReuseIdentifier:CellIdentifier2];
    [self.lblOrderNo setText:[NSString stringWithFormat:@"OrderNo : %@",self.ordersModelObj.orderNo]];
    [self.lblDate setText:[NSString stringWithFormat:@"Date : %@",self.ordersModelObj.orderDate]];
    [self.lblTime setText:[NSString stringWithFormat:@"Time : %@",self.ordersModelObj.orderTime]];
    [self.lblAmount setText:[NSString stringWithFormat:@"Amount : %@",self.ordersModelObj.amount]];
    
    if (self.ordersModelObj.shipmentDate.length>0 && ![self.ordersModelObj.shipmentDate rangeOfString:@"null"].length) {
        [self.deliveryDate setText:[NSString stringWithFormat:@"Delivery Date: %@",self.ordersModelObj.shipmentDate]];
    }
    
    [self.lblItemCount setText:[NSString stringWithFormat:@"%d Items",(int)[aryOrderDetails count]]];
    
    
    if([self.strOrderNo length]>0)
    {
        [self FetchDataFromServerForOrderNo:self.strOrderNo];
        
    }else{
        
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callFetchDataFromServerForOrderNo) name:@"FETCH_ORDER_DETAILS" object:nil];
    
}

// FIXME: Don't put random code directly. Add documented code in a method serving the purpose & call it here.
-(void)viewWillAppear:(BOOL)animated
{
    if (![self.ordersModelObj.status isEqualToString:@"A"]) {
        self.title = @"PROCESSED ORDER";
    }
    else {
        self.title = EDITORDERS;
    }
    
    [self refreshOrderSummary];
  
}


-(void)viewWillDisappear:(BOOL)animated
{
    self.title = @"";

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
#if !__has_feature(objc_arc)
    [super dealloc];
#endif
}


#pragma mark Private Methods
/**
 @brief: Will fetch latest order summary from server with 2 blocks like .
 
 Success: checking with respoinse code & creating model order object.
 
 Failure: Show failure to user.
 
 */

//FIXME: WebService class should have wrappers on direct REST API methods
//FIXME: Request & Response should be handled by a Webservice class, only data should be provided to the calling class.
- (void)refreshOrderSummary
{
    [[HumeApiClient SharedClient]AddAccessTokenInHeader];
    NSString *customSearchURL = [NSString stringWithFormat:@"%@%@",kSearchOrderNo,self.ordersModelObj.orderNo];
    [[HumeApiClient SharedClient] GET:customSearchURL parameters:nil success:^(NSURLSessionDataTask *task, id ResponseObject)
     {
         NSLog(@"sales order: %@",ResponseObject);
         APIResponse *response = [APIResponse modelObjecttWithDictionary:ResponseObject];
         if (response.isSuccess==1) {
             NSArray *result = [NSArray arrayWithArray:response.data];
             self.ordersModelObj = [Orders modelObjectWithDictionary:[result objectAtIndex:0]];
             [self.tableview reloadData];
         }
     }failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"sales order: Failed");
         
     }
     ];
}

//Function: Will fetch data from server for a specific order number.
-(void)callFetchDataFromServerForOrderNo
{

    [self FetchDataFromServerForOrderNo:self.strOrderNo];
}

/**
 @brief: Will remove order from order list with passing ordernumber to server.
 Different alert view for different iOS versions.
 Having callBack like Success: comparing with true status code & setting a FETCH_ORDERS Post notification.
 
 Failure: will show server failure to server.
 
 @param: (id)sender
 */


//FIXME: WebService class should have wrappers on direct REST API methods
//FIXME: Request & Response should be handled by a Webservice class, only data should be provided to the calling class.
- (void)deleteOrder:(id)sender{
    
    if ([UIAlertController class]) {
        UIAlertController *confirmControl = [UIAlertController alertControllerWithTitle:@"" message:DELETE_ORDER preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *deleteBtn = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            [[HumeApiClient SharedClient]AddAccessTokenInHeader];
            [[HumeApiClient SharedClient] POST:kOrderDeleteSaleOrder parameters:@{@"Type":@"SALES",@"OrderNo":self.strOrderNo} success:^(NSURLSessionDataTask *task, id responseObject) {
                //
                if ([[responseObject valueForKey:@"statusCode"] integerValue]==1) {
                    //
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"FETCH_ORDERS" object:nil];
                    [self.navigationController popViewControllerAnimated:YES];
                   
                }
                [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                //
                [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
            }];
        }];
        [confirmControl addAction:deleteBtn];
        UIAlertAction *cancelBtn = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //
        }];
        [confirmControl addAction:cancelBtn];
        [self presentViewController:confirmControl animated:YES completion:^{
            //
        }];
    }
    else {
        [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        [[HumeApiClient SharedClient]AddAccessTokenInHeader];
        [[HumeApiClient SharedClient] POST:kOrderDeleteSaleOrder parameters:@{@"Type":@"SALES",@"OrderNo":self.strOrderNo} success:^(NSURLSessionDataTask *task, id responseObject) {
            //
            if ([[responseObject valueForKey:@"statusCode"] integerValue]==1) {
                //
                [[NSNotificationCenter defaultCenter] postNotificationName:@"FETCH_ORDERS" object:nil];
                [self.navigationController popViewControllerAnimated:YES];
               
            }
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            //
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        }];
    }
}


#pragma FETCHING DATA FROM SERVER FOR ORDER DETAIL
/**
 @brief :Accepting order number sting.
 Server call with failure:handleFailureResponseOrders & success:handleSuccessResponseForOrders for all orders.
 */


//FIXME: WebService class should have wrappers on direct REST API methods
//FIXME: Request & Response should be handled by a Webservice class, only data should be provided to the calling class.
-(void)FetchDataFromServerForOrderNo:(NSString *)str
{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    [[HumeApiClient SharedClient]AddAccessTokenInHeader];
    
    NSString  *customURL =  [[NSString alloc]initWithFormat:@"%@%@",kOrderSalesDeatils,str];
    
    [[HumeApiClient SharedClient] GET:customURL parameters:nil success:^(NSURLSessionDataTask *task, id ResponseObject)
     {
         NSLog(@"sales order: %@",ResponseObject);
         [self handleSuccessResponseForOrders:ResponseObject];
         
     }failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"sales order: Failed");
         [self handleFailureResponseOrders];
     }
     ];
}

/**
 @brief: Success call back function
 status code varification
 
 Success:Stop loading. store order details in array & interct with user if valid orders found.
 
 Failure:Show failure message to user & removed all previous objects from object array.
 
 @param: (id)object
 */
- (void)handleSuccessResponseForOrders:(id)object {
    
    // [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    APIResponse *response = [APIResponse modelObjecttWithDictionary:object];
    
    //OrderDetails *orderDetailObj = [OrderDetails modelObjectWithDictionary:response.data];
    if(response.isSuccess==1)
    {
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        aryOrderDetails = [NSMutableArray arrayWithArray:(NSMutableArray *)response.data];
        if ([aryOrderDetails count]==0)
        {
            [[[UIAlertView alloc] initWithTitle:@"" message:NO_ITEMS_IN_ORDERS delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        
        
        [self.tableview reloadData];
    }
    else if(response.isSuccess==0){
    
        [aryOrderDetails removeAllObjects];
        [self.tableview reloadData];
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    }
    else{
        
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:kServerError delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

- (void)handleFailureResponseOrders
{
    
    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    [[[UIAlertView alloc] initWithTitle:@"" message:kStandardErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    return;
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    return [aryOrderDetails count]+1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0){
        return 85;
    }else{
        return 64;
    }
}

// FIXME: Setting properties of custom cell should not exceed more than 5 lines if it does so then all the code should be moved to a separate method intended for the same.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0){
        static NSString *CellIdentifier = @"saleOrderinfoCell";
        EditOrderTableViewCell  *cell = (EditOrderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"saleOrderinfoCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.tag = indexPath.row;
        }
        
        //Setting all table labels values from model object.
        CGFloat amountIncluding = [self.ordersModelObj.gSTAmount floatValue] + [self.ordersModelObj.totalExclGST floatValue];
        [cell.lblAmountExcluding setText:[NSString stringWithFormat:@"Amount Including GST : $%.02f",amountIncluding]];
        [cell.lblGSTAmount setText:[NSString stringWithFormat:@"GST Amount : $%@",self.ordersModelObj.gSTAmount]];
        [cell.lblTotalExcuding setText:[NSString stringWithFormat:@"Total Excluding GST : $%@",self.ordersModelObj.totalExclGST]];
        
        [cell.lblInvoiceDiscAmount setText: [NSString stringWithFormat:@"Invoice Discount amount : $%@",self.ordersModelObj.invoiceDiscountAmount]];
        [cell.lblInvoiceDiscPrecentage setText: [NSString stringWithFormat:@"Invoice Disc : %@%@",@"%",self.ordersModelObj.invoiceDiscountPercentage]];
        [cell.lblContactName setText: [NSString stringWithFormat:@"Contact Name: %@",self.ordersModelObj.name]];
        
        [cell.lblAddress setText:[NSString stringWithFormat:@"Address : %@ %@ %@ %@ %@ %@",self.ordersModelObj.shipToAddressCode,self.ordersModelObj.shipToAddressLine1,self.ordersModelObj.shipToAddressLine2,self.ordersModelObj.shipToCity,self.ordersModelObj.shipToState,self.ordersModelObj.shipToPostcode]];
		 	if ([APP_DELEGATE applicationTheameSetting] != nil){
		 		cell.contentView.backgroundColor = [Utility colorFromHexString:[[APP_DELEGATE applicationTheameSetting] tableCellHeaderBackground]];
		 	}
		 	else{
		 		cell.contentView.backgroundColor = [Utility colorFromHexString: ktableCellHeaderBackground];
		 	}

        return  cell;

        
    }else{
       
        static NSString *CellIdentifier = @"EditOrderTableViewCell";
        EditOrderTableViewCell  *cell = (EditOrderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"EditOrderTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.tag = indexPath.row;
            
        }
        
        OrderDetails *ordersObj = [OrderDetails modelObjectWithDictionary:[aryOrderDetails objectAtIndex:indexPath.row-1]];
        
        [cell.lblSkuNo setText:ordersObj.itemNo];
        
        [cell.lblProductsName setText:ordersObj.OrderDetailsDescription];
        [cell.lblPrice setText:[NSString stringWithFormat:@"$%@", ordersObj.lineAmount]];
        [cell.lblQuantity setText: [NSString stringWithFormat:@"x%@",ordersObj.quantity]];
        [cell.lblRatePerCartoon setText:[NSString stringWithFormat:@"$%@/%@",ordersObj.unitPrice,ordersObj.unitOfMeasure]];
        [cell.btnEdit setTag:indexPath.row-1];
        [cell.btnDelete setTag:indexPath.row-1];
        if (editable==NO) {
            [cell.btnEdit setHidden:YES];
            [cell.btnDelete setHidden:YES];
        }
        
        [cell.btnDelete addTarget:self action:@selector(DeleteBtnCall:) forControlEvents:UIControlEventTouchUpInside] ;
        [cell.btnEdit addTarget:self action:@selector(EditBtnCall:) forControlEvents:UIControlEventTouchUpInside] ;
        //[cell setSelectionStyle:UITableViewCellEditingStyleNone];
         return  cell;
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}


/**
 @brief: DeleteBtnCall with id type sender
 Having Alert for iOS versions
 Will show alert with Yes or No
 Yes:Perform delete action for selected order(Call service with slected order number
 
 Success: Will call handleSuccessResponseForDeleteOrderLines
 
 Failure: Will handleFailureResponseDeleteOrderLines.
 
 ).
 No:Cancel the delete operation.
 
 @param: (id)sender
 */

//FIXME: WebService class should have wrappers on direct REST API methods
//FIXME: Request & Response should be handled by a Webservice class, only data should be provided to the calling class.
-(void)DeleteBtnCall:(id)sender
{
    // Delete an item here and reload table view
    if ([UIAlertController class]) {
        UIAlertController *alertConfirm = [UIAlertController alertControllerWithTitle:@"Remove Line item" message:R_U_SURE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *confirmBtn = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            UIButton *button = (UIButton *)sender;
            [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            [[HumeApiClient SharedClient]AddAccessTokenInHeader];
            
            OrderDetails *ordersObj = [OrderDetails modelObjectWithDictionary:[aryOrderDetails objectAtIndex:button.tag]];
            
            [[HumeApiClient SharedClient] POST:kOrderDeleteProduct parameters:@{@"Type":ordersObj.type,@"OrderNo":self.ordersModelObj.orderNo,@"LineNo":ordersObj.lineNo} success:^(NSURLSessionDataTask *task, id ResponseObject)
             {
                 NSLog(@"One Item deleted : %@",ResponseObject);
                 [self handleSuccessResponseForDeleteOrderLines:ResponseObject];
                 
             }failure:^(NSURLSessionDataTask *task, NSError *error)
             {
                 NSLog(@"sales order: Failed");
                 [self handleFailureResponseDeleteOrderLines];
             }
             ];
        }];
        [alertConfirm addAction:confirmBtn];
        UIAlertAction *cancelBtn = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            //
        }];
        [alertConfirm addAction:cancelBtn];
        [self presentViewController:alertConfirm animated:YES completion:^{
            //
        }];
    }
    else {

    
    UIButton *button = (UIButton *)sender;
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    [[HumeApiClient SharedClient]AddAccessTokenInHeader];
    
   
    OrderDetails *ordersObj = [OrderDetails modelObjectWithDictionary:[aryOrderDetails objectAtIndex:button.tag]];

    [[HumeApiClient SharedClient] POST:kOrderDeleteProduct parameters:@{@"Type":ordersObj.type,@"OrderNo":self.ordersModelObj.orderNo,@"LineNo":ordersObj.lineNo} success:^(NSURLSessionDataTask *task, id ResponseObject)
     {
         NSLog(@"One Item deleted : %@",ResponseObject);
         [self handleSuccessResponseForDeleteOrderLines:ResponseObject];
         
     }failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"sales order: Failed");
         [self handleFailureResponseDeleteOrderLines];
     }
     ];
        
    }
    
}

/**
 @brief: Call back success block for DeleteBtnCall
 
 Response 0: Cancel call with proper error alert to user
 
 Response 1: Will remove loading, remove order from current list, 
 
 fetch the latest from server, refresh the table list
 
 @param: object(id)
 */

- (void)handleSuccessResponseForDeleteOrderLines:(id)object {
    APIResponse *response = [APIResponse modelObjecttWithDictionary:object];
    if(response.isSuccess==1)
    {
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        [self FetchDataFromServerForOrderNo:self.strOrderNo];
        [self refreshOrderSummary];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FETCH_ORDERS" object:nil];
    }else{
        
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:response.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

/**
 @brief: Call back failure block for DeleteBtnCall.
 Remove Loading with proper error alert to user.
 */
- (void)handleFailureResponseDeleteOrderLines{
    
    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    [[[UIAlertView alloc] initWithTitle:@"" message:kStandardErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    return;
}


#pragma mark Delete Button Call
/**
 @brief: EditBtnCall with paremeter sender id type
 filter seleted order object from array list.
 Open Product detail view controller with passing order object & product mode.
 */
-(void)EditBtnCall:(id)sender
{
    UIButton *button = (UIButton*)sender;
    OrderDetails *ordersObj = [OrderDetails modelObjectWithDictionary:[aryOrderDetails objectAtIndex:button.tag]];
    
    ProductDetailViewController *prodDetail  = [[ProductDetailViewController alloc]init];
    prodDetail.itemCode = ordersObj.itemNo;
    prodDetail.orderDetailsObj = ordersObj;
    prodDetail.productSelectMode = ProductEditMode;
    [self.navigationController pushViewController:prodDetail animated:YES];
    
    
}

#pragma mark catalougue Button Call
/**
 @brief: catalougue with paremeter sender id type
 filter seleted order object from array list.
 Open ProductListingViewController  with passing order object & caller view name.
 */
-(IBAction)catalougue:(id)sender
{
    //Move to Product listing View
    if (editable==YES) {
    ProductListingViewController *productListObj = [[ProductListingViewController alloc]init];
    [productListObj setOrderObj:self.ordersModelObj];
    [productListObj setStrOrderNo:self.ordersModelObj.orderNo];
    [productListObj setStrCallerViewName:EDITORDERS];
    [self.navigationController pushViewController:productListObj animated:YES];
    
    }
    
}

/**
 @brief: RefreshPrice with paremeter data(NSMutableArray) and count(int)
 Server call with param(customer,UOM,Contract,MinQty) with call back function like
 
 Success:
    StatusCode 1:Add an order to itemstodelete_array , if order list count more then call function CallItemsTodelete
 
 Failure: Show server Error.
 
 @param: (NSMutableArray*)data & count:(int)index
 */

//FIXME: WebService class should have wrappers on direct REST API methods
//FIXME: Request & Response should be handled by a Webservice class, only data should be provided to the calling class.
-(void)RefreshPrice:(NSMutableArray*)data count:(int)index
{
    __block int increment_index =index;
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [[HumeApiClient SharedClient] AddAccessTokenInHeader];
    [[HumeApiClient SharedClient] POST:kProductPrice parameters:@{@"Customer":[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMERNOKEY],@"Item":[[data objectAtIndex:index] objectForKey:@"ItemNo"],@"MinQty":[[data objectAtIndex:index] objectForKey:@"Quantity"],@"UOM":[[data objectAtIndex:index] objectForKey:@"UnitOfMeasure"],@"Contract":@"true"} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([[responseObject valueForKey:@"statusCode"] integerValue]==1) {
            if ([[[responseObject valueForKey:@"data"] valueForKey:@"UnitPrice"] integerValue]==0) {
                [self.itemstodelete_array addObject:[aryOrderDetails objectAtIndex:increment_index]];
            }
                
                if(increment_index==[aryOrderDetails count]-1)
                {
                    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
                    
                    if([self.itemstodelete_array count]>0)
                    {
                    
                        [self CallItemsTodelete:self.itemstodelete_array index:0];
                    
                    }
                    else {
                        
                       // [self updateSalesOrder];
                        [self showPicker];
                        
                    }
                    
                }
                else
                {
                    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
                    
                    int sendIndex = increment_index + 1;
                [self RefreshPrice:data count:sendIndex];
                
                }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:TRY_LATER delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        return;
    }];

  }


/**
 @brief: updateSalesOrder
 Set date fromate @"dd/MM/yyyy"
 update current selected order details like date & time with current date and time.
 Server call for latest sales order list with call back
 
 Success:With status code 1 perform post notification raised inlast controller for FETCH_ORDERS
 
 Failure: Show error to user.
 */
- (void)updateSalesOrder
{
    NSDateFormatter *dateForMatter  = [[NSDateFormatter alloc]init];
    [dateForMatter setDateFormat:@"dd/MM/yyyy"];
    NSString *strDate = [dateForMatter stringFromDate:[NSDate date]];
    NSDateFormatter *time  = [[NSDateFormatter alloc]init];
    [time setDateFormat:@"HH:mm"];
    NSString *strTime = [time stringFromDate:[NSDate date]];
    Orders *ord = self.ordersModelObj ;
    ord.orderDate = strDate;
    ord.orderTime = strTime;
    ord.shipmentDate = self.shipmentDate;
    
    if ([aryOrderDetails count]>0) {
        [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        [[HumeApiClient SharedClient] POST:kOrderUpdateSaleOrderStatus parameters:[ord dictionaryRepresentation] success:^(NSURLSessionDataTask *task, id responseObject) {
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
            if ([[responseObject valueForKey:@"statusCode"]integerValue]==1) {
                [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
                [self.navigationController popViewControllerAnimated:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"FETCH_ORDERS" object:nil];
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        }];
    }
}

/**
 @brief: CallItemsTodelete with
 Server post call with post param (type, orderNo, LineNo) callback
 
 Success: Updating date formate and order time with current time deleting from list, agin server call for updating order status on server.
 
 Failure: Show a alert with server Error.
 
 @param: param array(NSMutableArray) index(int)
 */

//FIXME: WebService class should have wrappers on direct REST API methods
//FIXME: Request & Response should be handled by a Webservice class, only data should be provided to the calling class.
-(void)CallItemsTodelete:(NSMutableArray*)array index:(int)count
{
    __block int increment_index =count;
        [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        [[HumeApiClient SharedClient]AddAccessTokenInHeader];
        OrderDetails *ordersObj = [OrderDetails modelObjectWithDictionary:[array objectAtIndex:increment_index]];
        [[HumeApiClient SharedClient] POST:kOrderDeleteProduct parameters:@{@"Type":ordersObj.type,@"OrderNo":self.ordersModelObj.orderNo,@"LineNo":ordersObj.lineNo} success:^(NSURLSessionDataTask *task, id ResponseObject)
         {
             NSLog(@"One Item deleted : %@",ResponseObject);
             if(increment_index==[array count]-1)
             {
                   NSDateFormatter *dateForMatter  = [[NSDateFormatter alloc]init];
                  [dateForMatter setDateFormat:@"dd/MM/yyyy"];
                  NSString *strDate = [dateForMatter stringFromDate:[NSDate date]];
                  NSDateFormatter *time  = [[NSDateFormatter alloc]init];
                  [time setDateFormat:@"HH:mm"];
                  NSString *strTime = [time stringFromDate:[NSDate date]];
                  Orders *ord = self.ordersModelObj ;
                  ord.orderDate = strDate;
                  ord.orderTime = strTime;
                  if ([aryOrderDetails count]>0) {
                  [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                  [[HumeApiClient SharedClient] POST:kOrderUpdateSaleOrderStatus parameters:[ord dictionaryRepresentation] success:^(NSURLSessionDataTask *task, id responseObject) {
                  [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                  if ([[responseObject valueForKey:@"statusCode"]integerValue]==1) {
                  [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
                  [self.navigationController popViewControllerAnimated:YES];
                  [[NSNotificationCenter defaultCenter] postNotificationName:@"FETCH_ORDERS" object:nil];
                  }
                  } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                  
                  }];
                  }
             }
             else
             {
                 int sendIndex = increment_index + 1;
                 [self CallItemsTodelete:array index:sendIndex];
             }
          
         }failure:^(NSURLSessionDataTask *task, NSError *error)
         {
             [[[UIAlertView alloc] initWithTitle:@"" message:TRY_LATER delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
         }
         ];
}

/**
 @brief: processOrder with 
 iOS version check for Alert view & alert controller
 Push view to ProductListingViewController with updated model order object
 
 @param: param sender(id)
 */

// FIXME: Code is not properly organized.
// FIXME: Add common method having UIAlertViewController code with input parameters acting as arguments to UIAlertViewController
- (IBAction)processOrder:(id)sender
{
    
    if (editable==YES) {
        if([aryOrderDetails count]==0)
        {
            if ([UIAlertController class]) {
                UIAlertController *addProducts = [UIAlertController alertControllerWithTitle:@"" message:ITEMS_REQUIRED preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    //open catalog page
                    ProductListingViewController *productListObj = [[ProductListingViewController alloc]init];
                    [productListObj setOrderObj:self.ordersModelObj];
                    [productListObj setStrOrderNo:self.ordersModelObj.orderNo];
                    [productListObj setStrCallerViewName:EDITORDERS];
                    [self.navigationController pushViewController:productListObj animated:YES];
                }];
                UIAlertAction *cancelBtn = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    //do nothing
                }];
                [addProducts addAction:okBtn];
                [addProducts addAction:cancelBtn];
                [self presentViewController:addProducts animated:YES
                                 completion:^{
                                 }];
            }
            else {
                [[[UIAlertView alloc] initWithTitle:@"" message:ITEMS_REQUIRED delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
        }
        else {
        
        if([UIAlertController class]){
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@""
                                      message:DELIVERY_DATE_REQUIRED
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 if([aryOrderDetails count]>0)
                                 //As per CR updated by on 15 oct 2015
                                 [self RefreshPrice:aryOrderDetails count:0];
                                [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
        [alert addAction:ok];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
        }
            else {
                // As per CR updated by  on 15 oct 2015
                if([aryOrderDetails count]>0){
                    UIAlertView *shipmentAlert = [[UIAlertView alloc]initWithTitle:DELIVERY_DATE_REQUIRED message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil ];
                shipmentAlert.tag = 1001;
                [shipmentAlert show];
                }
            }
       }
    }
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:ALERT_BTN_PROCEED]) {
        ProductListingViewController *productListObj = [[ProductListingViewController alloc]init];
        
        [productListObj setOrderObj:self.ordersModelObj];
        [productListObj setStrOrderNo:self.ordersModelObj.orderNo];
        
        [productListObj setStrCallerViewName:EDITORDERS];
        [self.navigationController pushViewController:productListObj animated:YES];
    }
    // As per CR updated by  on 15 oct 2015
    if (alertView.tag == 1001) {
        [self RefreshPrice:aryOrderDetails count:0];
    }

}

#pragma mark DatePicker Stuff

///Will show a picker view option for date selection.
- (void)showPicker
{
    DatePickerViewController *datePicker = [[DatePickerViewController alloc]initWithNibName:@"DatePickerViewController" bundle:nil ];
    datePicker.delegate = self;
    [self.navigationController pushViewController:datePicker animated:true];
}

- (void)dateDidSelect:(NSString*)dateStr;
{
    self.shipmentDate = dateStr;
    [self updateSalesOrder];
}


@end

//
//  ProductListingViewController.m
//  Hume
//
//  Created by User on 1/20/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "ProductListingViewController.h"
#import "ProductListingTableViewCell.h"
#import "HumeApiClient.h"
#import "Constants.h"
#import "PLProductListing.h"
#import "PLData.h"
#import "SVPullToRefresh.h"
#import "MBProgressHud.h"
#import "ProductDetailViewController.h"
#import "APIResponse.h"



@interface ProductListingViewController ()
{
    NSString *orderNumber;
}

@end

@implementation ProductListingViewController

-(id)init
{
    self = [[ProductListingViewController alloc]initWithNibName:@"ProductListingViewController" bundle:nil];

    return  self;
}

#pragma mark - View LifeCycle Methods
#pragma mark -
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"PRODUCTS";
    pageCount = 0;
    orderNumber = @"";
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    [Utility setTintColorOfNavigationBar:[UIColor whiteColor] OnNavigationController:self.navigationController];
    [tblProductList registerClass:[ProductListingTableViewCell class] forCellReuseIdentifier:@"ProductListingTableViewCell"];
    [tblProductList registerNib:[UINib nibWithNibName:@"ProductListingTableViewCell" bundle:nil] forCellReuseIdentifier:@"ProductListingTableViewCell"];

    
    //API call to get product list
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HumeApiClient SharedClient] AddAccessTokenInHeader];
    [[HumeApiClient SharedClient] GET:kProductListingAll parameters:nil
                              success:^(NSURLSessionDataTask *task, id responseObject) {
                                  
                                  NSLog(@"%@", responseObject);
                                  [self handleSuccess:responseObject];
                                  
                              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                  [self handleFailure:error];
                              }];
    
    if ([self.strCallerViewName isEqualToString:ORDERS_TITLE] || [self.strCallerViewName isEqualToString:EDITORDERS]) {
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction:)];
        self.navigationItem.rightBarButtonItem = doneButton;
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(goBack:)];
        self.navigationItem.leftBarButtonItem = cancelButton;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    self.title = @"PRODUCTS";
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                   [UIColor whiteColor],
                                                                   NSForegroundColorAttributeName,
                                                                   [UIFont fontWithName:FONT_COND size:23],
                                                                   NSFontAttributeName,
                                                                   nil];
    [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:[UIColor whiteColor]];
}

-(void)viewWillDisappear:(BOOL)animated
{
    //self.title = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Private Methods
#pragma mark -

-(void)goBack:(id)sender{
    
    if ([self.strCallerViewName isEqualToString:ORDERS_TITLE]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FETCH_ORDERS" object:nil];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else if([self.strCallerViewName isEqualToString:EDITORDERS]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FETCH_ORDER_DETAILS" object:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)doneAction:(id)sender{

    if ([self.strCallerViewName isEqualToString:ORDERS_TITLE]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FETCH_ORDERS" object:nil];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else if([self.strCallerViewName isEqualToString:EDITORDERS]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FETCH_ORDER_DETAILS" object:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}


- (void)enableCancelButton:(UISearchBar *)searchBar
{
    for (UIView *view in searchBar.subviews)
    {
        for (id subview in view.subviews)
        {
            if ( [subview isKindOfClass:[UIButton class]] )
            {
                [subview setEnabled:YES];
                NSLog(@"enableCancelButton");
                return;
            }
        }
    }
}

/**
 Function CallBack "handleSuccess:"
 This function is a callback gets called when server request gets succeeded.
 parameter - responseObject, object containing server response.
 **/

-(void)handleSuccess:(id)responseObject{

    APIResponse *response = [APIResponse modelObjecttWithDictionary:responseObject];
    
    if (response.isSuccess==1) {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Description" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    listOfProducts = [[NSMutableArray alloc] initWithArray:response.data];
    listOfProducts = (NSMutableArray*)[listOfProducts sortedArrayUsingDescriptors:sortDescriptors];
    listOfProductsCopy = [[NSMutableArray alloc] initWithArray:response.data];
    listOfProductsCopy = (NSMutableArray*)[listOfProductsCopy sortedArrayUsingDescriptors:sortDescriptors];
    [tblProductList reloadData];
    }
    else {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:NO_RESULT_FOUND delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

/**
 Function CallBack "handleFailure:"
 This function is a callback gets called when server request gets failed.
 parameter - responseObject, object containing server response.
 **/
-(void)handleFailure:(NSError *)error{
    //deal with it
     [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}


/**
 @brief: This function makes server request to fetch products list.
 with page counter 10. It will make load more result while scrolling.
 **/

-(void)loadMoreItems{
    
    if ([listOfSearchResults count]<=0) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        pageCount++;
        //API call to get more items
        int64_t delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        NSString *pageCounter = [NSString stringWithFormat:@"%li",(long)pageCount];
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [[HumeApiClient SharedClient] AddAccessTokenInHeader];
            [[HumeApiClient SharedClient] GET:[NSString stringWithFormat:kProductListing,pageCounter,@"10"] parameters:nil
                                      success:^(NSURLSessionDataTask *task, id responseObject) {
                                          
                                          NSLog(@"%@", responseObject);
                                          NSDictionary* json = nil;
                                          if ([responseObject isKindOfClass:[NSDictionary class]]){
                                              json = (NSDictionary*)responseObject;
                                          }
                                          PLProductListing *products = [PLProductListing modelObjectWithDictionary:json];
                                          if (products.statusCode==1) {
                                              [searchResults setUserInteractionEnabled:NO];
                                              [tblProductList beginUpdates];
                                              for (NSDictionary *item in products.data) {
                                                  [listOfProducts addObject:item];
                                                  [tblProductList insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:listOfProducts.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
                                              }
                                              [tblProductList endUpdates];
                                              [searchResults setUserInteractionEnabled:YES];
                                          }
                                          
                                      } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                          [self handleFailure:error];
                                      }];
            [tblProductList.infiniteScrollingView stopAnimating];
            
        });
    }
    [tblProductList.infiniteScrollingView stopAnimating];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}


#pragma mark - UITableViewDataSource methods
#pragma mark -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [listOfProducts count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier;
    
    ProductListingTableViewCell *cell ;
    
    simpleTableIdentifier = @"ProductListingTableViewCell";
    cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    PLData *product = [PLData modelObjectWithDictionary:[listOfProducts objectAtIndex:indexPath.row]];
    [cell.lblProductDescription setText:[NSString stringWithFormat:@"%@ %@",product.dataDescription,product.description2]];
    
    if([product.crossReferenceNumber length]!=0)
    [cell.lblItemNo setText:product.crossReferenceNumber];
    else
    [cell.lblItemNo setText:product.item1];
    [cell.lblUom setText:product.salesUnitOfMeasure];
    [cell.lblListPrice setText:[@"$" stringByAppendingString:product.unitPrice]];
    [cell.lblCategory setText:product.productGroup];


    NSString *b64String = product.picture.length>0?product.picture:@"";
    NSData *imgData = [[NSData alloc] initWithBase64EncodedString:b64String options:0];
    
    if (![b64String isEqualToString:@""]) {
        
        [cell.imgPicture setImage:[UIImage imageWithData:imgData]];
    
    }
    else {
        
        [cell.imgPicture setImage:[UIImage imageNamed:@"no_image"]];
    }

    
    [cell.btnAddToCart setTag:100];
    [cell.btnAddToCart addTarget:self action:@selector(addToCart:) forControlEvents:UIControlEventTouchDown];
    
    [cell.btnAddToPantry setTag:101];
    [cell.btnAddToPantry addTarget:self action:@selector(addToPantry:) forControlEvents:UIControlEventTouchDown];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

/**
 @brief:This function takes you to the product detail screen & creates products to be added into cart.
 **/
-(void)addToCart:(id)sender{
    
    NSLog(@"cart.....");
    
    self.title = @"";
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:tblProductList];
    NSIndexPath *indexPath = [tblProductList indexPathForRowAtPoint:buttonPosition];
    
    ProductDetailViewController *objDetail = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil];
    //Passing order no and calller view name
    [objDetail setDelegate:(id<ProductDetailViewControllerDelegate>)self];
    [objDetail setStrOrderNo:self.strOrderNo];
    [objDetail setOrderObj:self.orderObj];
    [objDetail setStrCallerViewName:self.strCallerViewName];
    [objDetail setPrvOrderNo:orderNumber];
    
    PLData *data = [PLData modelObjectWithDictionary:[listOfProducts objectAtIndex:indexPath.row]];
    objDetail.itemCode = data.item1;
    objDetail.productobj = data;
    objDetail.str_CrossReference = data.crossReferenceNumber;
    [self.navigationController pushViewController:objDetail animated:YES];
    
}

#pragma mark ProductDetailViewControllerDelegate method
#pragma mark -
- (void)saveOrderNumber:(Orders *)orderDetails{
    //orderNumber = orderNo;
    self.orderObj = orderDetails;
}
/**
 @brief: This function will add selected product to the pantry list.
 @param: (id)sender
 */
-(void)addToPantry:(id)sender{
    
    NSLog(@"pantry.....");
    
    self.title = @"";
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:tblProductList];
    NSIndexPath *indexPath = [tblProductList indexPathForRowAtPoint:buttonPosition];
    
    ProductDetailViewController *objDetail = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil];
    //Passing order no and calller view name
    [objDetail setStrOrderNo:self.strOrderNo];
    [objDetail setOrderObj:self.orderObj];
    [objDetail setStrCallerViewName:self.strCallerViewName];
    
    
    PLData *data = [PLData modelObjectWithDictionary:[listOfProducts objectAtIndex:indexPath.row]];
    objDetail.itemCode = data.item1;
    objDetail.str_CrossReference = data.crossReferenceNumber;
    [self.navigationController pushViewController:objDetail animated:YES];
}

#pragma mark - UITableViewDelegateSource methods
#pragma mark -

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    

}

#pragma mark - UISearchBarDelegate methods
#pragma mark -

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    //API call for product search
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HumeApiClient SharedClient] GET:[NSString stringWithFormat:kSearchProduct,[searchBar.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //
        [self handleSearchSuccess:responseObject];
        [searchBar resignFirstResponder];
        [self enableCancelButton:searchBar];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //
        [self handleSearchFailure:error];
        [searchBar resignFirstResponder];
        searchBar.text=nil;
        [self enableCancelButton:searchBar];
    }];
    
} // called when keyboard search button pressed

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    searchBar.showsCancelButton = YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    searchBar.showsCancelButton=NO;
    listOfProducts = listOfProductsCopy;
    [filterSegment setSelectedSegmentIndex:0];
    [tblProductList reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{

}  // called when text changes (including clear)


/**
 @brief:This function is a callback gets called when search products server request gets succeeded.
 parameter - responseObject, object containing server response.
 @param: (id)responseObject
  **/
-(void)handleSearchSuccess:(id)responseObject{
    
    [filterSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
    
    APIResponse *response = [APIResponse modelObjecttWithDictionary:responseObject];
    
    if(response.isSuccess==1)
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        listOfProducts = [[NSMutableArray alloc] initWithArray:response.data];
        [tblProductList reloadData];
    }
    else{
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:NO_RESULT_FOUND delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

/**
 @brief: CallBack "handleSearchFailure:"
 This function is a callback gets called when search products server request gets failed.
 parameter - handleSearchFailure, object containing error information.
 @param: (NSError*)error
 **/
-(void)handleSearchFailure:(NSError*)error{
    
    //deal with it
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [searchResults resignFirstResponder];
    [self enableCancelButton:searchResults];
}

/**
 @brief: IBAction of button "filterSegment"
 This function gets called as an action to tap event on "filter segment" button in xib.
 This function lets user filter products by ALL, CHILLED, FROZEN, DRY state.
 Parameter - sender, sender here representing the segment control reference.
 */
-(IBAction)selectStorage:(id)sender{

    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    if ([filterSegment selectedSegmentIndex]==0) {
        [self filterArrayByStorage:@"All" completion:^{
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        }];
    }
    if ([filterSegment selectedSegmentIndex]==1) {
        [self filterArrayByStorage:@"CHILLED" completion:^{
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        }];
    }
    if ([filterSegment selectedSegmentIndex]==2) {
        [self filterArrayByStorage:@"FROZEN" completion:^{
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        }];
    }
    if ([filterSegment selectedSegmentIndex]==3) {
        [self filterArrayByStorage:@"DRY" completion:^{
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        }];
    }
    
}

-(void)filterArrayByStorage:(NSString*)storage completion:(void(^)(void))callback{

    if ([storage isEqualToString:@"All"]) {
        listOfProducts = listOfProductsCopy;
        [tblProductList reloadData];
        callback();
    }
    else {
    NSPredicate *predStat = [NSPredicate predicateWithFormat:@"(ProductGroup==%@)",storage];
    listOfProducts = (NSMutableArray *)[listOfProductsCopy filteredArrayUsingPredicate:predStat];
    [tblProductList reloadData];
    callback();
    }

}


@end

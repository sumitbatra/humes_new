//
//  OrdersViewControllerTableViewController.m
//  Hume
//
//  Created by user on 15/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "OrdersViewControllerTableViewController.h"
#import "HumeApiClient.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import "APIResponse.h"
#import "Orders.h"
#import "OrdersTableViewCell.h"
#import "Constants.h"
#import "EditOrderTableViewController.h"
#import "ProductListingViewController.h"
#import "Utility.h"
#import "ShippingAddress.h"
#import "ShippingAddressTableViewController.h"




@interface OrdersViewControllerTableViewController ()
{
    NSMutableArray *ordersAry;
    NSMutableArray *ordersAryCopy;
    NSMutableArray *orderSearchAry;
}
@end

@implementation OrdersViewControllerTableViewController
@synthesize tableview,lblSerachOrderResult, priceOfItem;

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton * btnNewOrder = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 12, 15)];
    [btnNewOrder setBackgroundImage:[UIImage imageNamed:@"plusicon.png"] forState:UIControlStateNormal];
    
    [btnNewOrder addTarget:self action:@selector(addNeworder) forControlEvents:UIControlEventTouchUpInside];
    [btnNewOrder.titleLabel setTextColor:[UIColor whiteColor ]];
    UIBarButtonItem *barButtonNewOrder = [[UIBarButtonItem alloc] initWithTitle:@"New Order" style:UIBarButtonItemStylePlain target:self action:@selector(addNeworder)];
    [self.navigationItem setRightBarButtonItem:barButtonNewOrder];
    
    [Utility setTintColorOfNavigationBar:[UIColor whiteColor] OnNavigationController:self.navigationController];
    self.title = ORDERS_TITLE;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchDataFromServerOrders) name:@"FETCH_ORDERS" object:nil];
    
    [self fetchDataFromServerOrders];
}


-(void)viewWillAppear:(BOOL)animated
{
    if (self.orderSelectionMode==OrderSelectionOpenOrders) {
        self.title = @"Select Order";
    }
    else{
    self.title = @"ORDERS";
    }
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                   [UIColor whiteColor],
                                                                   NSForegroundColorAttributeName,
                                                                   [UIFont fontWithName:FONT_COND size:23],
                                                                   NSFontAttributeName,
                                                                   nil];
    [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:[UIColor whiteColor]];
     
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.title = @"";
    
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
#if !__has_feature(objc_arc)
    [super dealloc];
#endif
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark EditOrder Delegate method
-(void)fetchOrdersAndPop:(EditOrderTableViewController *)editView
{
    [self fetchDataFromServerOrders];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma FETCHING DATA FROM SERVER FOR ORDERS

/**
 @brief: Server call for fetching orders data
 
 - Success & failure call back methods.
 */
-(void)fetchDataFromServerOrders
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[HumeApiClient SharedClient]AddAccessTokenInHeader];
    
    [[HumeApiClient SharedClient] GET:kOrders parameters:nil success:^(NSURLSessionDataTask *task, id ResponseObject)
     {
         NSLog(@"sales order: %@",ResponseObject);
         [self handleSuccessResponseForOrders:ResponseObject];
         
     }failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"sales order: Failed");
         [self handleFailureResponseOrders];
     }
     ];
}

/**
 @brief: Success call back methods for orders.
 
 - based on server status updating order arrays.
 */
- (void)handleSuccessResponseForOrders:(id)object {
    
    // [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    APIResponse *response = [APIResponse modelObjecttWithDictionary:object];
    
    if(response.isSuccess==1)
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if (self.orderSelectionMode==OrderSelectionAllOrders) {
            [filterSegment setSelectedSegmentIndex:0];
            ordersAry = [[NSMutableArray alloc]initWithArray:response.data];
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"OrderNo" ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            ordersAryCopy = [[NSMutableArray alloc]initWithArray:response.data];
            ordersAry = (NSMutableArray*)[ordersAry sortedArrayUsingDescriptors:sortDescriptors];
            ordersAryCopy = (NSMutableArray *)[ordersAryCopy sortedArrayUsingDescriptors:sortDescriptors];
            [self.tableview reloadData];
        }
        else if (self.orderSelectionMode==OrderSelectionOpenOrders){
            
            [filterSegment setUserInteractionEnabled:NO];
            [filterSegment setSelectedSegmentIndex:1];
            
            NSMutableArray *ordersOpen = [[NSMutableArray alloc] init];
           
            for (NSDictionary *dic in  [Utility parseNullValuesInArray:
                                         response.data]) {
                if ([[dic valueForKey:@"Status"] isEqualToString:@"A"]) {
                    [ordersOpen addObject:dic];
                }
                ordersAry = [[NSMutableArray alloc]initWithArray:ordersOpen];
                ordersAryCopy = [[NSMutableArray alloc]initWithArray:ordersOpen];
                [self.tableview reloadData];
                }
            
            if([ordersOpen count]==0)
            {
                [self addNeworderforOpen];
            }
        
        }
    }
    else if(response.isSuccess==0){
        //do nothing
        ordersAry = nil;
        ordersAryCopy = nil;
        [self.tableview reloadData];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if (self.orderSelectionMode==OrderSelectionOpenOrders) {
            [[[UIAlertView alloc] initWithTitle:@"" message:NO_OPEN_ORDER delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
    }
    else{
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:response.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

- (void)handleFailureResponseOrders{
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [[[UIAlertView alloc] initWithTitle:@"" message:NO_RESULT_FOUND delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    return;
}

/**
 @breif: Add new order button action.
 
 Creating a new order in the list.
 
 - Success & failure call back methods.
 */
-(IBAction)addNeworder
{
    NSLog(@"Add New Order Screen: function called");
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HumeApiClient SharedClient]AddAccessTokenInHeader];

    NSDateFormatter *dateForMatter  = [[NSDateFormatter alloc]init];
    [dateForMatter setDateFormat:@"dd/MM/yyyy"];
    NSString *strDate = [dateForMatter stringFromDate:[NSDate date]];
    NSDateFormatter *time  = [[NSDateFormatter alloc]init];
    [time setDateFormat:@"HH:mm"];
    NSString *strTime = [time stringFromDate:[NSDate date]];
    [[HumeApiClient SharedClient] POST :kOrderAddNewSaleOrder parameters:@{@"Type":@"SALES",@"OrderTime":strTime,@"OrderDate":strDate,@"synced":@"0",@"ToSync":@"1",@"Status":@"",@"username":[[NSUserDefaults standardUserDefaults] valueForKey:@"username"],@"Customer":[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMERNOKEY]} success:^(NSURLSessionDataTask *task, id ResponseObject)
         {
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             APIResponse *response = [APIResponse modelObjecttWithDictionary:ResponseObject];
             if(response.isSuccess==1)
             {
                 Orders *ordersObj = [Orders modelObjectWithDictionary:response.data];
                 NSLog(@"sales order: %@",ResponseObject);
                 //[self handleSuccessResponseForOrders:ResponseObject]
                 //Product already selected(Product>Order>CreateNewOrder)
                 if (self.orderSelectionMode==OrderSelectionOpenOrders) {
                     [self.productDetailObj setStrOrderNo:ordersObj.orderNo];
                     [self.productDetailObj setOrderObj:ordersObj];
                     [self.productDetailObj AddProductIntoAnOrder];
                 } else {
                 //Move on product screen for choosing/Adding itmes(Order>Create>Select Product)
                 ProductListingViewController *productListObj = [[ProductListingViewController alloc]init];
                 [productListObj setStrCallerViewName:ORDERS_TITLE];
                 //Pass order object
                 [productListObj setStrOrderNo:ordersObj.orderNo];
                 [productListObj setOrderObj:ordersObj];
                 [self.navigationController pushViewController:productListObj animated:YES];
                     
                     [[[UIAlertView alloc] initWithTitle:@"" message:ORDER_CREATED delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                 }
                 }else{
                     
                     [[[UIAlertView alloc] initWithTitle:@"" message:kServerError delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                 }
             
         }failure:^(NSURLSessionDataTask *task, NSError *error)
         {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             NSLog(@"sales order: Failed");
             [self handleFailureResponseOrders];
         }
         ];
}

/**
 @brief: 
 - Creating new order list 
 - Setting orders in the open order list.
 - Calling servece with success & failure call backs.
 */

-(IBAction)addNeworderforOpen
{
    NSLog(@"Add New Order Screen: function called");
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[HumeApiClient SharedClient]AddAccessTokenInHeader];
    
    NSDateFormatter *dateForMatter  = [[NSDateFormatter alloc]init];
    [dateForMatter setDateFormat:@"dd/MM/yyyy"];
    NSString *strDate = [dateForMatter stringFromDate:[NSDate date]];
    NSDateFormatter *time  = [[NSDateFormatter alloc]init];
    [time setDateFormat:@"HH:mm"];
    NSString *strTime = [time stringFromDate:[NSDate date]];
    
    [[HumeApiClient SharedClient] POST :kOrderAddNewSaleOrder parameters:@{@"Type":@"SALES",@"OrderTime":strTime,@"OrderDate":strDate,@"synced":@"0",@"ToSync":@"1",@"Status":@"",@"username":[[NSUserDefaults standardUserDefaults] valueForKey:@"username"],@"Customer":[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMERNOKEY]} success:^(NSURLSessionDataTask *task, id ResponseObject)
     {
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         APIResponse *response = [APIResponse modelObjecttWithDictionary:ResponseObject];
         if(response.isSuccess==1)
         {
             NSLog(@"sales order: %@",ResponseObject);
             [self fetchDataFromServerOrders];
             
         }else{
             [[[UIAlertView alloc] initWithTitle:@"" message:kServerError delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
         }
     }failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         NSLog(@"sales order: Failed");
         [self handleFailureResponseOrders];
     }
     ];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [ordersAry count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Identifing here which cell will be load at runtime.
    
    static NSString *CellIdentifier = @"OrdersTableViewCell";
    OrdersTableViewCell *cell = (OrdersTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"OrdersTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.tag = indexPath.row;
    }
    
    Orders *ordersObj = [Orders modelObjectWithDictionary:[ordersAry objectAtIndex:indexPath.row]];
    [cell.lblOrderNo setText:[NSString stringWithFormat:@"Order# %@",ordersObj.orderNo]];
    [cell.lblCustomerNameValue setText:ordersObj.name];
    [cell.lblCustomerNoValue setText:ordersObj.customer];
    [cell.lblOrderDateValue setText:ordersObj.orderDate];
    //[cell.lblOrderTotalValue setText:ordersObj.amount];
    [cell.lblOrderTimeValue setText:ordersObj.orderTime];
    [cell.lblOrderTotalValue setText:[@"$" stringByAppendingString:ordersObj.amountExclGST]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    [cell.btnEdit setTag:indexPath.row];
    [cell.btnProcess setTag:indexPath.row];
    [cell.btnEdit addTarget:self action:@selector(EditButtonCall:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnProcess addTarget:self action:@selector(ProcessButtonCall:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([ordersObj.status isEqualToString:@"A"]) {
        [cell.imgaeViewPiChart setImage:[UIImage imageNamed:@"stat_a.png"]];
    }
    if ([ordersObj.status isEqualToString:@"B"] || [ordersObj.status isEqualToString:@"X"] || [ordersObj.status isEqualToString:@"Y"]) {
        [cell.imgaeViewPiChart setImage:[UIImage imageNamed:@"stat_b.png"]];
    }
    if ([ordersObj.status isEqualToString:@"C"]) {
        [cell.imgaeViewPiChart setImage:[UIImage imageNamed:@"stat_c.png"]];
    }
    if ([ordersObj.status isEqualToString:@"H"]) {
        [cell.imgaeViewPiChart setImage:[UIImage imageNamed:@"stat_h.png"]];
    }
    if (self.strCallerView == PRODUCT_DETAIL  || [ordersObj.status isEqualToString:@"H"] || [ordersObj.status isEqualToString:@"C"] || [ordersObj.status isEqualToString:@"B"] || [ordersObj.status isEqualToString:@"X"] || [ordersObj.status isEqualToString:@"Y"])
    {
        // user will see the list of open oder and as the user tap on any one  cell then add item() into that order.
        // Hide edit and process button
        [cell.btnEdit setHidden:true];
        [cell.btnProcess setHidden:true];
    }
    return cell;
}


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // SPecial case
    Orders *ordersObj = [Orders modelObjectWithDictionary:[ordersAry objectAtIndex:indexPath.row]];
    if (self.strCallerView == PRODUCT_DETAIL )
    {
        // Fetch order no from selected cell order and add item into that
         Orders *ordersObj = [Orders modelObjectWithDictionary:[ordersAry objectAtIndex:indexPath.row]];
        [self.productDetailObj setStrOrderNo:ordersObj.orderNo];
        [self.productDetailObj setOrderObj:ordersObj];
        [self.productDetailObj AddProductIntoAnOrder];
    }
    if (![ordersObj.status isEqualToString:@"A"]) {
        EditOrderTableViewController *editOrder = [[EditOrderTableViewController alloc] init];
        editOrder.ordersModelObj = ordersObj;
        editOrder.strOrderNo = ordersObj.orderNo;
        [self.navigationController pushViewController:editOrder animated:YES];
    }
    
}

/**
 @brief:
 
 Edit button action.
 
 pushiung to the EditOrderTableViewController with seelcted order number.
 */

-(IBAction)EditButtonCall:(id)sender
{
    //Move to detail page of selected order.
    UIButton *btn = (UIButton *)sender;
    Orders *ordersObj = [Orders modelObjectWithDictionary:[ordersAry objectAtIndex:btn.tag]];
    
    EditOrderTableViewController *editOrderObj = [[EditOrderTableViewController alloc]init];
    editOrderObj.ordersModelObj = ordersObj;
    editOrderObj.delegate = self;
    [editOrderObj setStrOrderNo:ordersObj.orderNo];
    [self.navigationController pushViewController:editOrderObj animated:YES];
   
}
/**
 @brief: Performing the button action for process order.
 - Calling the server for fetching the latest server list of orders.
 */
-(IBAction)ProcessButtonCall:(id)sender
{
    // if user want to process or repeate
    UIButton *btn = (UIButton *)sender;
    Orders *ship = [Orders modelObjectWithDictionary:[ordersAry objectAtIndex:btn.tag]];
        if ([ship.amountExclGST floatValue]<=0.00) {
        if ([UIAlertController class]) {
            UIAlertController *addProducts = [UIAlertController alertControllerWithTitle:@"" message:ITEMS_REQUIRED preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //open edit page
                //Move to detail page of selected order.
                UIButton *btn = (UIButton *)sender;
                Orders *ordersObj = [Orders modelObjectWithDictionary:[ordersAry objectAtIndex:btn.tag]];
                
                EditOrderTableViewController *editOrderObj = [[EditOrderTableViewController alloc]init];
                editOrderObj.ordersModelObj = ordersObj;
                editOrderObj.delegate = self;
                [editOrderObj setStrOrderNo:ordersObj.orderNo];
                [self.navigationController pushViewController:editOrderObj animated:YES];
            }];
            UIAlertAction *cancelBtn = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //do nothing
            }];
            [addProducts addAction:okBtn];
            [addProducts addAction:cancelBtn];
            [self presentViewController:addProducts animated:YES
                             completion:^{
                                 //
                             }];
        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"" message:ITEMS_REQUIRED delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:ALERT_BTN_PROCEED, nil] show];
            NSMutableArray *ord = [ordersAry mutableCopy];
            OrdersViewControllerTableViewController * __weak weakSelf = self;
            editCall = ^{
                
                UIButton *btn = (UIButton *)sender;
                Orders *ordersObj = [Orders modelObjectWithDictionary:[ord objectAtIndex:btn.tag]];
                
                EditOrderTableViewController *editOrderObj = [[EditOrderTableViewController alloc]init];
                editOrderObj.ordersModelObj = ordersObj;
                editOrderObj.delegate = weakSelf;
                [editOrderObj setStrOrderNo:ordersObj.orderNo];
                [weakSelf.navigationController pushViewController:editOrderObj animated:YES];
            };
        }
    }
    else {
    NSDateFormatter *dateForMatter  = [[NSDateFormatter alloc]init];
    [dateForMatter setDateFormat:@"dd/MM/yyyy"];
    NSString *strDate = [dateForMatter stringFromDate:[NSDate date]];
    NSDateFormatter *time  = [[NSDateFormatter alloc]init];
    [time setDateFormat:@"HH:mm"];
    NSString *strTime = [time stringFromDate:[NSDate date]];
    ship.orderDate = strDate;
    ship.orderTime = strTime;
    [[[UIAlertView alloc] initWithTitle:@"" message:R_U_SURE delegate:self cancelButtonTitle:ALERT_BTN_CANCEL otherButtonTitles:ALERT_BTN_OK, nil] show];
    
    OrdersViewControllerTableViewController * __weak weakSelf = self;
    someBlock = ^{
        //run webservice with know order number
        NSLog(@"block_check %li",(long)btn.tag);
        [MBProgressHUD showHUDAddedTo:weakSelf.view animated:YES];
        [[HumeApiClient SharedClient] POST:kOrderUpdateSaleOrderStatus parameters:[ship dictionaryRepresentation] success:^(NSURLSessionDataTask *task, id responseObject) {
            [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
            if ([[responseObject valueForKey:@"statusCode"]integerValue]==1) {
                [weakSelf fetchDataFromServerOrders];
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
        }];
        };
    }
    
}

/**
 @brief:
 
 Select button status
 
 - Filtering the array based on the different status
 
 - Performing the call back to notifying the source.
 */
- (IBAction)selectStatus:(id)sender{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if ([filterSegment selectedSegmentIndex]==0) {
        [self filterArrayByStatus:@"All" completion:^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
    }
    if ([filterSegment selectedSegmentIndex]==1) {
        [self filterArrayByStatus:@"A" completion:^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
    }
    if ([filterSegment selectedSegmentIndex]==2) {
        [self filterArrayByStatus:@"B" completion:^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
    }
    if ([filterSegment selectedSegmentIndex]==3) {
        [self filterArrayByStatus:@"C" completion:^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
    }
    if ([filterSegment selectedSegmentIndex]==4) {
        [self filterArrayByStatus:@"H" completion:^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
    }
}

/**
 @brief:
 Filtering data based on the different status
 
 @param: 
 (NSString*)status
 
 @callback:
 completion:(void(^)(void))callback
 - Returnig function complition sttus to the source.
 */

-(void)filterArrayByStatus:(NSString*)status completion:(void(^)(void))callback{
    if ([status isEqualToString:@"All"]) {
        ordersAry = ordersAryCopy;
        [self.tableview reloadData];
        callback();
    }
    else{
    NSPredicate *predStat = [NSPredicate predicateWithFormat:@"(Status==%@)",status];
    ordersAry = (NSMutableArray *)[ordersAryCopy filteredArrayUsingPredicate:predStat];
    [self.tableview reloadData];
    callback();
    }
}


#pragma mark UIAlertVIewDelegate methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSString *buttonText = [alertView buttonTitleAtIndex:buttonIndex];
    if ([buttonText isEqualToString:ALERT_BTN_OK]) {
        //call block
        someBlock();
    }
    if ([buttonText isEqualToString:ALERT_BTN_PROCEED]) {
        //call block
        editCall();
    }
    if ([buttonText isEqualToString:ALERT_BTN_CANCEL]) {
        
    }
    
}

#pragma mark UISEARCHBAR DELEGATE

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    //SEND  REQUEST ON SERVER FOR SEARCH AN ODER WITH ORDER NO
    //kSearchOrderNo
    [searchBar resignFirstResponder];
    if ([searchBar.text length]>0)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[HumeApiClient SharedClient]AddAccessTokenInHeader];
        NSString *customSearchURL = [NSString stringWithFormat:@"%@%@",kSearchOrderNo,[searchBar.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [[HumeApiClient SharedClient] GET:customSearchURL parameters:nil success:^(NSURLSessionDataTask *task, id ResponseObject)
         {
             NSLog(@"sales order: %@",ResponseObject);
             [self handleSuccessResponseForOrdersSearch:ResponseObject];
         }failure:^(NSURLSessionDataTask *task, NSError *error)
         {
             NSLog(@"sales order: Failed");
             [self handleFailureResponseOrders];
         }
         ];
  
    }else{
        [[[UIAlertView alloc]initWithTitle:@"" message:SEARCH_TEXT_REQUIRED delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        [searchBar resignFirstResponder];
         ordersAry = ordersAryCopy;
        [self.tableview reloadData];
    }
    
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{

     searchBar.showsCancelButton = YES;
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
     [searchBar resignFirstResponder];
     searchBar.text = @"";
     [self.lblSerachOrderResult setText:[NSString stringWithFormat:@"Order"]];
     searchBar.showsCancelButton=NO;
    [self.lblSerachOrderResult setText:[NSString stringWithFormat:@"Order"]];
    ordersAry = ordersAryCopy;
    [filterSegment setSelectedSegmentIndex:0];
    [self.tableview reloadData];
}

/**
 @brief: Success call back method for searched order.
 
 - based on the server response updating the table view data.
 
 */
- (void)handleSuccessResponseForOrdersSearch:(id)object {
    
    [filterSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
    
    // [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    APIResponse *response = [APIResponse modelObjecttWithDictionary:object];
    
    if(response.isSuccess==1)
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        ordersAry = [[NSMutableArray alloc]initWithArray:response.data];
        [self.lblSerachOrderResult setText:[NSString stringWithFormat:@"Order > Found %d Results",(int)[ordersAry count]]];
        [self.tableview reloadData];
        
    }else{
        
         [self.lblSerachOrderResult setText:[NSString stringWithFormat:@"Order"]];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:NO_RESULT_FOUND delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

- (void)handleFailureResponseOrdersSearch{
    [self.lblSerachOrderResult setText:[NSString stringWithFormat:@"Order"]];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [[[UIAlertView alloc] initWithTitle:@"" message:NETWORK_ERROR delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    return;
}



@end

//
//  ChangeURLViewController.m
//  Hume
//
//  Created by User on 10/14/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "ChangeURLViewController.h"



@interface ChangeURLViewController ()
@property (weak, nonatomic) IBOutlet UITextField *urlTF;
- (IBAction)saveUrl:(id)sender;

@end

@implementation ChangeURLViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Setting naviagtion bar tint color
    [Utility setTintColorOfNavigationBar:[UIColor whiteColor] OnNavigationController:self.navigationController];
    self.title = TITLE_CHANGE_URL;
    //current url checking if there then pick else take from saved once.
    self.urlTF.text =  self.currentUrl.length>0?self.currentUrl : [[NSUserDefaults standardUserDefaults] objectForKey:@"defaultUrl"];
	NSLog(@"url: %@", self.urlTF.text);
		
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = false;
    self.automaticallyAdjustsScrollViewInsets = NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark Private Methods

/**
 @brief: save Url
 @discussion: user can manualy update the default server base url.
 @param: sender(id)
 */
- (IBAction)saveUrl:(id)sender
{
    [self.view.window endEditing:true];
    if (self.urlTF.text.length >0) {
        //Confirming to UpdateUrlDelegate
        if ([self.delegate conformsToProtocol:@protocol(UpdateUrlDelegate)]) {
            [self.delegate didUrlUpdated:self.urlTF.text];
        }
        [[[UIAlertView alloc] initWithTitle:@"" message:URL_UPDATED delegate:self cancelButtonTitle:nil otherButtonTitles:ALERT_BTN_OK, nil] show];

    }else{
      [[[UIAlertView alloc] initWithTitle:@"" message:URL_BLANK delegate:self cancelButtonTitle:nil otherButtonTitles:ALERT_BTN_OK, nil] show];
        
    }
}

///Dismissing keyboard while user touch any where on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view.window endEditing:true];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    [textField resignFirstResponder];
    return true;
}

@end

//
//  EditCustomerInfoViewController.m
//  Hume
//
//  Created by user on 27/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "EditCustomerInfoViewController.h"
#import "HumeApiClient.h"
#import "APIResponse.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import "Customerinfo.h"

#define kOFFSET_FOR_KEYBOARD 80.0


@interface EditCustomerInfoViewController ()
{
    Customerinfo *customerInfo;
}
@end

@implementation EditCustomerInfoViewController
@synthesize dicInfo, scrollViewCust;

-(id)init
{
    self = [[EditCustomerInfoViewController alloc]initWithNibName:@"EditCustomerInfoViewController" bundle:nil];
    return  self;
}


// FIXME: Don't put random code directly. Add documented code in a method serving the purpose & call it here.
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    //Making scrollView as scrolable by encreasing content size.
    [self.scrollViewCust setContentSize:CGSizeMake(SCREEN_WIDTH, 800)];
    [self.lblTitle setText:EDIT_CUSTOMER_TITLE];
    //Creating a customer info model opbject from customer info dictionary.
    customerInfo = [Customerinfo   modelObjectWithDictionary:self.dicInfo];
    //Updating default value for email,contact, fax etc
    [self setValueOnUserInterface];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.scrollViewCust setScrollEnabled:YES];
    [self.scrollViewCust setContentSize:CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT+270)];
}

// FIXME: Don't put random code directly. Add documented code in a method serving the purpose & call it here.
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                   [UIColor whiteColor],
                                                                   NSForegroundColorAttributeName,
                                                                   [UIFont fontWithName:FONT_COND size:23],
                                                                   NSFontAttributeName,
                                                                   nil];
    [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:[UIColor whiteColor]];
    //self.automaticallyAdjustsScrollViewInsets = NO;
    [self.scrollViewCust setContentSize:CGSizeMake(SCREEN_WIDTH, 800)];
    //self.title = @"NEW SHIP ADDRESS";
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.title = @"";
    
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
#if !__has_feature(objc_arc)
    [super dealloc];
#endif
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Private Methods
/**
 @brief: Will update user interface filds with customer info values.
 */
-(void)setValueOnUserInterface
{
 
    [self.txtContactName setText:customerInfo.contact];
    [self.txtEmailAddress setText:customerInfo.email];
    [self.txtFax setText:customerInfo.fax];
    [self.txtTelex setText:customerInfo.phoneNo];
    //[self.txtContactNo setText:customerInfo.phoneNo];
}


/**
 @brief: Will update the shipping addrss in customer details.
 Null field checking for contact name & email
 server call for  updating shipping address with callbacks
 
 Success: With response 1 show updated shipping
 
 Failure: Show server error.
 */

//FIXME: WebService class should have wrappers on direct REST API methods
//FIXME: Request & Response should be handled by a Webservice class, only data should be provided to the calling class.
-(void)SendDataOnServerShippingAddress
{
    
    //Field should not be blank
    
    if ([self.txtTelex.text length]==0 || [self.txtEmailAddress.text length]==0 || [self.txtContactName.text length]==0 )
    {
        [[[UIAlertView alloc]initWithTitle:@"" message:ALL_FIELDS_ARE_REQUIRED delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        return;
    }
    //Before send a save request plz check the primary address field value.
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    //Adding the deveice token in header filed.
    [[HumeApiClient SharedClient]AddAccessTokenInHeader];
    //Here "Code" and "customer no" will not be editable.
    
    // As discussed with webserivce guy for new shipping address, code value will be "0".
        
        [[HumeApiClient SharedClient] POST:kUpdateContact parameters:@{@"No":customerInfo.noProperty,@"Contact":self.txtContactName.text,@"Email":self.txtEmailAddress.text,@"Fax":self.txtFax.text,@"Telex":@"",@"PhoneNo":self.txtTelex.text, @"synced":@"0",@"ToSync":@"1"} success:^(NSURLSessionDataTask *task, id ResponseObject)
         {
             NSLog(@"kAddShipToAddress %@",ResponseObject);
             //Move to previous view with success alert
             APIResponse *response = [APIResponse modelObjecttWithDictionary:ResponseObject];
             if (response.isSuccess==1)
             {
                 //Success Block
                 [[[UIAlertView alloc] initWithTitle:@"" message:UPDATEPROFILE delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                 [self.navigationController popViewControllerAnimated:YES];
                 return;
                 
             }else{
                
                 [[[UIAlertView alloc] initWithTitle:@"" message:kStandardErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                 return;
             }
             
             
             
         }failure:^(NSURLSessionDataTask *task, NSError *error)
         {   //Failure Block

             NSLog(@"kAddShipToAddress Failed");
             //Stay here and show error to user and try again
             [self handleFailureReqEditProfile];
         }
         ];
        
}

-(void)handleFailureReqEditProfile
{
    [[[UIAlertView alloc] initWithTitle:@"" message:kStandardErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}

/**
 @brief: Will call web api with save url to update shipping address.
 @param: (id)sender
 */
-(IBAction)save:(id)sender
{
    [self SendDataOnServerShippingAddress];
}

-(IBAction)clear:(id)sender
{
    [self.txtContactName setText:@""];
    [self.txtFax setText:@""];
    [self.txtTelex setText:@""];
    [self.txtEmailAddress setText:@""];
    [self.txtContactNo setText:@""];

}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    //[self.scrollViewCust setFrame:originalScrollViewFrame];
    return YES;
}


/**
 @brief: will show keyboard while textfield will start editing.
 */
-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (self.scrollViewCust.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.scrollViewCust.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

/**
 @brief: will hide keyboard while textfield will stop editing.
 */
-(void)keyboardWillHide {
    if (self.scrollViewCust.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.scrollViewCust.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isKindOfClass:[UITextField class]])
    {
    }
}

/**
 @brief: method to move the view up/down whenever the keyboard is shown/dismissed
 */
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.scrollViewCust.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.scrollViewCust.frame = rect;
    
    [UIView commitAnimations];
}


@end

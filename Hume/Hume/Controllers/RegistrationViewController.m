//
//  RegistrationViewController.m
//  Hume
//
//  Created by User on 5/18/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "RegistrationViewController.h"
#import "Constants.h"
#import "HumeApiClient.h"
#import "APIResponse.h"
#import "MBProgressHud.h"

#define BLANK stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]


@interface RegistrationViewController (){
    
    NSString *title;
    BOOL checked;

}

@end

@implementation RegistrationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [Utility setTintColorOfNavigationBar:[UIColor whiteColor] OnNavigationController:self.navigationController];
    
    [self.scrollViewCust setScrollEnabled:YES];
    
    //Iphone different sizes checking
    if(IS_IPHONE_4_OR_LESS|| IS_IPHONE_5) [self.scrollViewCust setContentSize:CGSizeMake(self.scrollViewCust.frame.size.width, SCREEN_HEIGHT+1400)];
    
    if(IS_IPHONE_6 || IS_IPHONE_6P || IS_IPAD) [self.scrollViewCust setContentSize:CGSizeMake(self.scrollViewCust.frame.size.width, SCREEN_HEIGHT+1000)];
  
    checked = NO;
    title = @"";
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Private Methods

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    //[self.scrollViewCust setFrame:originalScrollViewFrame];
    return YES;
}


///Function: Will pick name initial.
-(IBAction)selectTitle:(id)sender{
    
    UIButton *btn = (UIButton *)sender;
    switch (btn.tag) {
        case 1://Updating MR checkobox as highlited  and other none selected
            title = @"Mr.";
            [btnMr setBackgroundImage:[UIImage imageNamed:@"checkbox-active"] forState:UIControlStateNormal];
            [btnMrs setBackgroundImage:[UIImage imageNamed:@"checkbox-inactive"] forState:UIControlStateNormal];
            [btnMiss setBackgroundImage:[UIImage imageNamed:@"checkbox-inactive"] forState:UIControlStateNormal];
            break;
        case 2://Updating Mrs checkobox as highlited  and other none selected
            title = @"Mrs.";
            [btnMr setBackgroundImage:[UIImage imageNamed:@"checkbox-inactive"] forState:UIControlStateNormal];
            [btnMrs setBackgroundImage:[UIImage imageNamed:@"checkbox-active"] forState:UIControlStateNormal];
            [btnMiss setBackgroundImage:[UIImage imageNamed:@"checkbox-inactive"] forState:UIControlStateNormal];
            break;
            
        case 3://Updating Miss checkobox as highlited  and other none selected
            title = @"Miss";
            [btnMr setBackgroundImage:[UIImage imageNamed:@"checkbox-inactive"] forState:UIControlStateNormal];
            [btnMrs setBackgroundImage:[UIImage imageNamed:@"checkbox-inactive"] forState:UIControlStateNormal];
            [btnMiss setBackgroundImage:[UIImage imageNamed:@"checkbox-active"] forState:UIControlStateNormal];
            break;
        default:
            break;
    }
    
}

/**
 @brief: Will validate all fields entered by user before sending to server
*/
-(BOOL)validateForm
{
    //Confirming if any filed is balnk then return a alert
    if ([[txtState.text BLANK] length]<=0 || [[txtCity.text BLANK] length]<=0 || [[txtCompanyName.text BLANK] length]<=0 || [[txtFirstName.text BLANK] length]<=0  ||[[txtLastName.text BLANK] length]<=0 || [[txtEmail.text BLANK] length]<=0 || [[txtUserName.text BLANK] length]<=0 || [[txtPassword.text BLANK] length]<=0 || [title isEqualToString:@""]) {
        [[[UIAlertView alloc] initWithTitle:@"" message:FORM_BLANK delegate:self cancelButtonTitle:nil otherButtonTitles:ALERT_BTN_PROCEED, nil] show];
        return NO;
        
    } else {
        return YES;
    }
    
}

///@brief: Will send the form to web server & intract with user on success  failure.
-(IBAction)doneButton:(id)sender
{

    if ([self validateForm]) {
        if ([txtPassword.text length]<3) {
            //
            [[[UIAlertView alloc] initWithTitle:@"" message:PASS_VALIDATE delegate:self cancelButtonTitle:nil otherButtonTitles:ALERT_BTN_PROCEED, nil] show];
        } else {
            [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            // All validated fields Send to server.
            [[HumeApiClient SharedClient] POST:kRegister parameters:@{keyUsername:txtUserName.text,keyPassword:txtPassword.text,keyTitle:title,keyFirstName:txtFirstName.text,keyLastName:txtLastName.text,keyCompanyName:txtCompanyName.text,keyAddressLine1:txtADL1.text,keyAddressLine2:txtADL2.text,keyCity:txtCity.text,keyState:txtState.text,keyPostCode:txtPostCode.text,keyEmail:txtEmail.text,keyPhoneNumber:txtPhoneNo.text,keyFaxNumber:txtFaxNo.text,keyCustomerNo:txtUserName.text} success:^(NSURLSessionDataTask *task, id responseObject) {
                //succes
                [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
                APIResponse *response = [APIResponse modelObjecttWithDictionary:responseObject];
                if ([[responseObject valueForKey:keyStatusCode] integerValue]==1) {//Success
                    [[[UIAlertView alloc] initWithTitle:@"" message:ACCT_CREATED delegate:self cancelButtonTitle:nil otherButtonTitles:ALERT_BTN_OK, nil] show];
                } else {
                    [[[UIAlertView alloc] initWithTitle:@"" message:response.message delegate:self cancelButtonTitle:nil otherButtonTitles:ALERT_BTN_PROCEED, nil] show];
                }
                
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                //failure
                [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
            }];
        }
    }
    
}

#pragma mark UIAlerViewDelegate methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    NSString *titleAlert = [alertView buttonTitleAtIndex:buttonIndex];
    if ([titleAlert isEqualToString:ALERT_BTN_OK]) {
        [self.navigationController popViewControllerAnimated:YES];
    }


}


@end

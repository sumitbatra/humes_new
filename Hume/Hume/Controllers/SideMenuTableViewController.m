//
//  SideMenuTableViewController.m
//  Hume
//
//  Created by User on 4/1/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "SideMenuTableViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "ChangePasswordViewController.h"
#import "UIViewController+MFSideMenuAdditions.h"
#import "SIdeMenuTableViewCell.h"
#import "HomeViewController.h"
#import "AppDelegate.h"

#import "HumeApiClient.h"
#import "Constants.h"
#import "MBProgressHud.h"
#import "ViewController.h"

#define HOME @"Home"


@interface SideMenuTableViewController ()

@end

@implementation SideMenuTableViewController
{
    NSArray *menuItems;
}

#pragma mark View LifeCycle Methods
#pragma mark -
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    menuItems = @[@"Logout", @"Home", @"Change Password"];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSIndexSet *section = [NSIndexSet indexSetWithIndex:0];
        [self.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
    });
    
    [self performSelector:@selector(hideSearchBar) withObject:nil afterDelay:0.0f];
}

#pragma mark Private Methods
#pragma mark -
- (void)hideSearchBar {
    self.tableView.contentOffset = CGPointMake(0,-20);
}

#pragma mark - Table view data source
#pragma mark -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [menuItems count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"SideMenuCell";
    
    SIdeMenuTableViewCell *cell = (SIdeMenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {

        NSArray *nib;
        
            nib = [[NSBundle mainBundle] loadNibNamed:@"SIdeMenuTableViewCell" owner:self options:nil];

        cell = [nib objectAtIndex:0];
        
    }
    // Configure the cell...
    cell.menuCellTitle.text = [menuItems objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - TableView Delegate Methods
#pragma mark -
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([[menuItems objectAtIndex:indexPath.row] isEqualToString:HOME]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoToHome" object:nil];
        

    }
    if (indexPath.row==0) {
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCESSTOKEN];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:CUSTOMERNOKEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //[self presentViewController:[[ViewController alloc] init] animated:YES completion:nil];
        ViewController *homeObj = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
        
        UINavigationController *nvcontrol = [[UINavigationController alloc]initWithRootViewController:homeObj];
        if ([APP_DELEGATE applicationTheameSetting] != nil){
            nvcontrol.navigationBar.barTintColor = [Utility colorFromHexString:[[APP_DELEGATE applicationTheameSetting] navigationBarBackground]];
        }
        else{
            nvcontrol.navigationBar.barTintColor = [Utility colorFromHexString:knavigationBarBackground];//[UIColor colorWithRed:2.0/255.0 green:133.0/255.0 blue:62.0/255.0 alpha:1.0];
        }
        
        
        nvcontrol.navigationBar.translucent = NO;
        
        [[APP_DELEGATE window]  setRootViewController:nvcontrol];
        
    }
    if (indexPath.row == 2) {
        
        ChangePasswordViewController *changePasswordViewController = [[ChangePasswordViewController alloc] initWithNibName:@"ChangePasswordViewController" bundle:nil];
       // [[APP_DELEGATE window]  setRootViewController:changePasswordViewController];
        // navigationController pushViewController:changePasswordViewController animated:YES];
        
        
    [((MFSideMenuContainerViewController*)[[APP_DELEGATE window] rootViewController]).centerViewController pushViewController:changePasswordViewController animated:FALSE];
      
        
        ((MFSideMenuContainerViewController*)[[APP_DELEGATE window] rootViewController]).menuState = MFSideMenuStateClosed;
        
        
    }
    
}
#pragma mark - Callback Methods
#pragma mark -

/**
 @brief: This function is a callback for request success
 */
-(void)handleSuccess:(id)responseObject{
    
    [MBProgressHUD hideHUDForView:self.view.window animated:YES];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCESSTOKEN];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:CUSTOMERNOKEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //[self presentViewController:[[ViewController alloc] init] animated:YES completion:nil];
    ViewController *homeObj = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    
    UINavigationController *nvcontrol = [[UINavigationController alloc]initWithRootViewController:homeObj];
    if ([APP_DELEGATE applicationTheameSetting] != nil){
        nvcontrol.navigationBar.barTintColor = [Utility colorFromHexString:[[APP_DELEGATE applicationTheameSetting] navigationBarBackground]];
    }
    else{
        nvcontrol.navigationBar.barTintColor = [Utility colorFromHexString:knavigationBarBackground];//[UIColor colorWithRed:2.0/255.0 green:133.0/255.0 blue:62.0/255.0 alpha:1.0];
    }
    nvcontrol.navigationBar.translucent = NO;
    
    [[APP_DELEGATE window]  setRootViewController:nvcontrol];
    
}
/**
 @brief: This function is a callback for request failure.
 @param: (NSError*)error
 */
-(void)handleFailure:(NSError*)error{
    [MBProgressHUD hideHUDForView:self.view.window animated:YES];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCESSTOKEN];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:CUSTOMERNOKEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //[self presentViewController:[[ViewController alloc] init] animated:YES completion:nil];
    ViewController *homeObj = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    
    UINavigationController *nvcontrol = [[UINavigationController alloc]initWithRootViewController:homeObj];
    if ([APP_DELEGATE applicationTheameSetting] != nil){
        nvcontrol.navigationBar.barTintColor = [Utility colorFromHexString:[[APP_DELEGATE applicationTheameSetting] navigationBarBackground]];
    }
    else{
        nvcontrol.navigationBar.barTintColor = [Utility colorFromHexString:knavigationBarBackground];//[UIColor colorWithRed:2.0/255.0 green:133.0/255.0 blue:62.0/255.0 alpha:1.0];
    }
    nvcontrol.navigationBar.translucent = NO;
    
     [[APP_DELEGATE window]  setRootViewController:nvcontrol];
}

#pragma mark - Memory Management Methods
#pragma mark -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

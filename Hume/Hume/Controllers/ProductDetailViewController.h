//
//  ProductDetailViewController.h
//  Hume
//
//  Created by User on 1/27/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Orders.h"
#import "OrderDetails.h"
#import "PLData.h"
#import "PRDProductDetail.h"
#import "Pantrylist.h"


@protocol ProductDetailViewControllerDelegate;

typedef enum  ProductSelectionMode {
    ProductSelectionModeTypes = 0,
    ProductEditMode
}ProductSelectionFetchMode;


/**
 Class:ProductDetailViewController can perform to display product details like Quantity, UOM, Price etc.
 Can updated Quantity, UOM, Price.
 Can add product to cart & templete list.
 RelativeViewController: OrdersViewControllerTableViewController
 */
@interface ProductDetailViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate, UIAlertViewDelegate>
{
}

//Properties
@property (nonatomic,retain)IBOutlet UILabel *lblPrice;
@property(nonatomic,strong)NSString *str_CrossReference;
@property (nonatomic,retain)IBOutlet UIButton *addToCart;
@property (nonatomic,retain)IBOutlet UIButton *addToPantry;
@property (nonatomic,retain)IBOutlet UIButton *updatePantry;
@property (nonatomic, retain) IBOutlet UITextField *txtUom;
@property(nonatomic, retain) IBOutlet UILabel *lblUom;
@property(nonatomic,retain)IBOutlet UIPickerView *pkrUom;
@property(nonatomic, retain)IBOutlet UILabel *lblQuantity;
@property(nonatomic, retain)IBOutlet UIStepper *stpQuantity;
@property(nonatomic, retain) NSString *itemCode;
@property(nonatomic, retain)IBOutlet UILabel *itemCodeAndCategory;
@property(nonatomic, retain)IBOutlet UILabel *productGroup;
@property(nonatomic, retain)IBOutlet UILabel *productDescription;
@property(nonatomic, retain)IBOutlet UIImageView *productImage;
@property(nonatomic, retain)IBOutlet UIImageView *promoImage;
@property(nonatomic, retain) NSString *strOrderNo;
@property(nonatomic, retain) NSString *prvOrderNo;
@property(nonatomic,retain)NSString *strCallerViewName;
@property(nonatomic,retain)Orders *orderObj;
@property(nonatomic,retain)OrderDetails *orderDetailsObj;
@property(nonatomic,retain)PLData *productobj;
@property(nonatomic,retain)NSString *strLineNo;
@property(nonatomic,retain)PRDProductDetail *detail;
@property(nonatomic,assign)ProductSelectionFetchMode productSelectMode;
@property (nonatomic,strong)NSMutableArray *arrUom;
@property (assign,nonatomic) CGFloat itemPrice;
@property (assign,nonatomic) CGFloat basePrice;
@property (assign,nonatomic) BOOL baseSelected;
@property (nonatomic,strong) NSMutableDictionary *uomAndPrice;
@property(nonatomic, retain) Pantrylist *pList;
@property (nonatomic, assign) id<ProductDetailViewControllerDelegate>delegate;

//Methods
- (IBAction)valueChanged:(UIStepper *)sender;
- (IBAction)addToCart:(id)sender;
- (IBAction)updateOrder:(id)sender;
- (IBAction)pickUom:(id)sender;
-(void)AddProductIntoAnOrder;
@end

//Protocol
@protocol ProductDetailViewControllerDelegate<NSObject>
-(void)saveOrderNumber:(Orders*)orderDetails;
@end

//
//  AddressViewViewController.m
//  Hume
//
//  Created by user on 19/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "AddressViewViewController.h"
#import "ShippingAddress.h"
#import "Constants.h"
#import "Customerinfo.h"
#import "HumeApiClient.h"
#import "APIResponse.h"




@interface AddressViewViewController ()
{
    ShippingAddress *shippingObj;
    Customerinfo *customerInfo;
    BOOL primaryAddress;
    BOOL checked;
}
@end

@implementation AddressViewViewController
@synthesize dicData,callerViewName,scrollViewEdit,lblPrimary;

//FIXME:- Need better code organisation. Follow a modular approach.

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    //Setting navigation bar title color
    [Utility setTintColorOfNavigationBar:[UIColor whiteColor] OnNavigationController:self.navigationController];
    [self.scrollViewEdit setScrollEnabled:YES];
    
    //Encreasing scroll view content size
    [self.scrollViewEdit setContentSize:CGSizeMake(self.scrollViewEdit.frame.size.width, 800)];
    
    if ([self.callerViewName isEqualToString:NEW_SHIPPING_ADD_TITLE]) {//Setting default values for New Shipping Address
        
        //All the fields will be blank for fresh entry.
        [self.btnMakePrimary setEnabled:NO];
        primaryAddress = false;
        [self.lblTitle setText:@"  NEW SHIP ADDRESS"];
        [self.btnMakePrimary setHidden:false];
        [self.lblPrimary setHidden:false];
    }else if([self.callerViewName isEqualToString:BILLING_ADD_TITLE] )
    {//Setting default values for New Billing Address
        
        customerInfo = [Customerinfo modelObjectWithDictionary:self.dicData];
        
        [self.btnMakePrimary setHidden:true];
        [self.lblPrimary setHidden:true];
        
        [self.txtAddressLine1 setText:customerInfo.address];
        [self.txtAddressLine2 setText:customerInfo.address2];
        [self.txtCity setText:customerInfo.city];
        [self.txtPhoneNo setText:customerInfo.phoneNo];
        [self.txtState setText:customerInfo.state];
        [self.txtPostCode setText:customerInfo.postCode];
        [self.txtState setText:customerInfo.state];
        [self.txtContactName setText:customerInfo.name];
        [self.btnMakePrimary setHidden:true];
        self.title = @"  EDIT BILL ADDRESS";
        [self.lblTitle setText:@"  EDIT BILLING ADDRESS"];
        
    }else if([self.callerViewName isEqualToString:SHIPPING_ADD_TITLE])
    {//Setting default values for Shipping Address update
        
        shippingObj = [ShippingAddress modelObjectWithDictionary:self.dicData];
        [self.btnMakePrimary setHidden:false];
        [self.lblPrimary setHidden:false];

        [self.txtAddressLine1 setText:shippingObj.addressLine1];
        [self.txtAddressLine2 setText:shippingObj.addressLine2];
        [self.txtCity setText:shippingObj.city];
        [self.txtPhoneNo setText:shippingObj.phoneNo];
        [self.txtState setText:shippingObj.state];
        [self.txtPostCode setText:shippingObj.postcode];
        [self.txtState setText:shippingObj.state];
        [self.txtContactName setText:shippingObj.contactName];
         self.title = @"  EDIT SHIP ADDRESS";
        [self.lblTitle setText:@"  EDIT SHIPPING ADDRESS"];
        //Set primary checkbox
        if ([shippingObj.primary boolValue]==1)
        {
            [self.btnMakePrimary setBackgroundImage:[UIImage imageNamed:@"checkbox-active.png"] forState:UIControlStateNormal ];
            checked = YES;
            primaryAddress = YES;
        }else
        {
            [self.btnMakePrimary setBackgroundImage:[UIImage imageNamed:@"checkbox-inactive.png"] forState:UIControlStateNormal ];
            checked = NO;
            primaryAddress = NO;
            
        }
        
    }
    
}

//FIXME:- Need better code organisation. Follow a modular approach.
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                   [UIColor whiteColor],
                                                                   NSForegroundColorAttributeName,
                                                                   [UIFont fontWithName:FONT_COND size:23],
                                                                   NSFontAttributeName,
                                                                   nil];
    [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:[UIColor whiteColor]];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.title = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Private Methods

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


#pragma FETCHING DATA FROM SERVER
/**
 @brief: Will update shipping address on server with manual fields.
 
 @method:
 
      addShippingAddress
 
      addBillingAddress
 
      editShippingAddress
 */


-(void)SendDataOnServerShippingAddress
{
    //Field should not be blank
    if ([self.txtAddressLine1.text length]==0 || [self.txtCity.text length]==0 || [self.txtState.text length]==0 ||[self.txtPhoneNo.text length]==0)
    {
        [[[UIAlertView alloc]initWithTitle:@"" message:FILL_ALL delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        return;
    }
    
    //Before send a save request plz check the primary address field value.
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
     [[HumeApiClient SharedClient]AddAccessTokenInHeader];
    //Here "Code" and "customer no" will not be editable.
    
    //Calling different function based on caller view name like new shipping , update shipping, Billing address
    if ([self.callerViewName isEqualToString:NEW_SHIPPING_ADD_TITLE]) {
        [self addShippingAddress];
        
    }else if([self.callerViewName isEqualToString:BILLING_ADD_TITLE] )
    {
        [self addBillingAddress];
        
    }else if([self.callerViewName isEqualToString:SHIPPING_ADD_TITLE]){
        [self editShippingAddress];
    }
}

/**
 @brief: Will add a new shipping address in list
 */
- (void)addShippingAddress
{
    // As discussed with webserivce guy for new shipping address code value will be "0".
    NSString *customerNo = (NSString *)[[NSUserDefaults standardUserDefaults] valueForKey:CUSTOMERNOKEY];
    
    //Service call with default paremeter for adding new shipping address
    [[HumeApiClient SharedClient] POST:kAddShipToAddress parameters:@{keyCode:@"0",@"no":customerNo,keyAddressLine1:self.txtAddressLine1.text,keyAddressLine2:self.txtAddressLine2.text,keyCity:self.txtCity.text,keyContactName:self.txtContactName.text,keyState:self.txtState.text,keyPostCode:self.txtPostCode.text,@"PhoneNo":self.txtPhoneNo.text,keySynced:@"0",keyToSync:@"1",@"Primary":[NSString stringWithFormat:@"%@",checked==true?@"1":@"0"]} success:^(NSURLSessionDataTask *task, id ResponseObject)
     {
         NSLog(@"kAddShipToAddress %@",ResponseObject);
         //Move to previous view with success alert
         APIResponse *response = [APIResponse modelObjecttWithDictionary:ResponseObject];
         if (response.isSuccess==1)
         {
             
             [[[UIAlertView alloc] initWithTitle:@"" message:NEWADDRESSSUCCESSMSG delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
             [self.navigationController popViewControllerAnimated:YES];
             return;
             //[self handleSuccessResponseForShippingAddress:ResponseObject];
             
         }else{
             [[[UIAlertView alloc] initWithTitle:@"" message:kStandardErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
             return;
         }
         
     }failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"kAddShipToAddress Failed");
         //Stay here and show error to user and try again
         [self handleFailureResponseShippingAddress];
     }
     ];
    

}

/**
 @brief: Will update existing shipping address.
     */

//FIXME: Please use macros for hardcoded string getting passed to APIs
//FIXME: Request & Response should be handled by a Webservice class, only data should be provided to the calling class.
- (void)editShippingAddress
{
    //Service Call with default paremeter for updating existing shipping address.
    [[HumeApiClient SharedClient] POST:kUpdateShipToAddress parameters:@{@"Code":shippingObj.code,@"CustomerNo":shippingObj.customerNo,@"AddressLine1":self.txtAddressLine1.text,@"AddressLine2":self.txtAddressLine2.text,@"City":self.txtCity.text,@"ContactName":self.txtContactName.text,@"state":self.txtState.text,@"postcode":self.txtPostCode.text,@"phoneno":self.txtPhoneNo.text,@"synced":@"0",@"ToSync":@"1",@"primary":[NSString stringWithFormat:@"%@",checked==true?@"1":@"0"]} success:^(NSURLSessionDataTask *task, id ResponseObject)
     {
         NSLog(@"kUpdateShipToAddress %@",ResponseObject);
         //Move to previous view with success alert
         APIResponse *response = [APIResponse modelObjecttWithDictionary:ResponseObject];
         if (response.isSuccess==1)//Success Response checking
         {
             if (checked==true && primaryAddress==NO) {
                 //
                 [[HumeApiClient SharedClient] POST:kPrimaryShpiptoAddress parameters:@{@"Code":shippingObj.code,@"CustomerNo":shippingObj.customerNo,@"AddressLine1":self.txtAddressLine1.text,@"AddressLine2":self.txtAddressLine2.text,@"City":self.txtCity.text,@"ContactName":self.txtContactName.text,@"state":self.txtState.text,@"postcode":self.txtPostCode.text,@"phoneno":self.txtPhoneNo.text,@"synced":@"0",@"ToSync":@"1",@"primary":[NSString stringWithFormat:@"%@",checked==true?@"1":@"0"]} success:^(NSURLSessionDataTask *task, id responseObject) {
                     //
                     [[[UIAlertView alloc] initWithTitle:@"" message:SUCCESSMESSAGE delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                     [self.navigationController popViewControllerAnimated:YES];
                     return;
                 } failure:^(NSURLSessionDataTask *task, NSError *error) {
                     //
                     [self handleFailureResponseShippingAddress];
                     
                 }];
             }
             else {
                 [[[UIAlertView alloc] initWithTitle:@"" message:SUCCESSMESSAGE delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                 [self.navigationController popViewControllerAnimated:YES];
                 return;
             }
             //[self handleSuccessResponseForShippingAddress:ResponseObject];
             
         }else{
             [[[UIAlertView alloc] initWithTitle:@"" message:kStandardErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
             return;
         }
         
     }failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"kUpdateShipToAddress Failed");
         //Stay here and show error to user and try again
         [self handleFailureResponseShippingAddress];
     }
     ];

}

/**
    @brief: Will Add new billing address
 */

//FIXME: Please use macros for hardcoded string getting passed to APIs
//FIXME: Request & Response should be handled by a Webservice class, only data should be provided to the calling class.
- (void)addBillingAddress
{
    //Service Call for adding a new Billing address with default paremeters
    [[HumeApiClient SharedClient] POST:kUpdateToBillAddress parameters:@{@"no":customerInfo.noProperty,@"Address":self.txtAddressLine1.text,@"Address2":self.txtAddressLine2.text,keyCity:self.txtCity.text,keyContactName:self.txtContactName.text,@"state":self.txtState.text,@"postcode":self.txtPostCode.text,@"phoneno":self.txtPhoneNo.text,@"synced":@"0",@"ToSync":@"1"} success:^(NSURLSessionDataTask *task, id ResponseObject)
     {
         NSLog(@"kUpdateToBillAddress %@",ResponseObject);
         //Move to previous view with success alert
         //[self handleSuccessResponseForShippingAddress:ResponseObject];
         APIResponse *response = [APIResponse modelObjecttWithDictionary:ResponseObject];
         if (response.isSuccess==1)
         {
             [[[UIAlertView alloc] initWithTitle:@"" message:SUCCESSMESSAGE delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
             [self.navigationController popViewControllerAnimated:YES];
             return;
             //[self handleSuccessResponseForShippingAddress:ResponseObject];
             
         }else{
             [[[UIAlertView alloc] initWithTitle:@"" message:kStandardErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
             return;
         }
         
     }failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"kUpdateToBillAddress Faiconsled");
         //Stay here and show error to user and try again
         [self handleFailureResponseShippingAddress];
         
     }
     ];

}

///Function:Success shipping complition
- (void)handleSuccessResponseForShippingAddress:(id)object {
    
    // [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    APIResponse *response = [APIResponse modelObjecttWithDictionary:object];
    
    if(response.isSuccess==1)
    {
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        
      // address has successfully updated and move to previous page
        
    }else{
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        [[[UIAlertView alloc] initWithTitle:@"" message:response.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

- (void)handleFailureResponseShippingAddress{
    
    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    [[[UIAlertView alloc] initWithTitle:@"" message:kStandardErrorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    return;
}



-(IBAction)SaveCall:(id)sender
{
    [self SendDataOnServerShippingAddress];
}

///Function: Will reset all fields
-(IBAction)deleteCall:(id)sender
{
    [self.btnMakePrimary  setBackgroundImage:[UIImage imageNamed:@"checkbox-inactive.png"] forState:UIControlStateNormal];
    [self.txtAddressLine1 setText:@""];
    [self.txtAddressLine2 setText:@""];
    [self.txtCity setText:@""];
    [self.txtPhoneNo setText:@""];
    [self.txtState setText:@""];
    [self.txtPostCode setText:@""];
    [self.txtState setText:@""];
    [self.txtContactName setText:@""];
}

///Function: Will Add checked & unchecked option.
-(IBAction)PrimaryAddressCheckBox:(id)sender
{
    if (checked==false)
    {
        [self.btnMakePrimary  setBackgroundImage:[UIImage imageNamed:@"checkbox-active.png"] forState:UIControlStateNormal] ;
         checked = true;
    }else
    {    checked = false;
        [self.btnMakePrimary  setBackgroundImage:[UIImage imageNamed:@"checkbox-inactive.png"] forState:UIControlStateNormal];
    }
    
}

@end

//
//  PromotionTableViewController.m
//  Hume
//
//  Created by user on 03/02/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "PromotionTableViewController.h"
#import "Constants.h"
#import "Utility.h"
#import "HumeApiClient.h"


@interface PromotionTableViewController ()
{
    NSMutableArray *arryTitle;
}
@end

@implementation PromotionTableViewController

-(id)init
{

 self = [[PromotionTableViewController alloc]initWithNibName:@"PromotionTableViewController" bundle:nil];

    return  self;
}

#pragma mark - View LifeCycle Methods
#pragma mark -

- (void)viewDidLoad {
    [super viewDidLoad];
    

    arryTitle = [[NSMutableArray alloc]initWithObjects:@"Discounts",@"Multi Buy",@"Upsell",@"Rewards", nil];
    [[HumeApiClient SharedClient] AddAccessTokenInHeader];
    [[HumeApiClient SharedClient] POST:kGetAllPromotions parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {

        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {

        
    }];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.title = @"Promotions";
}

-(void)viewWillDisappear:(BOOL)animated
{
   self.title = @"";
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
#pragma mark -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [arryTitle count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"cell";

    
    UITableViewCell *cell; //= [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    // Configure the cell...
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier ];
    }
    
    [cell.textLabel setText:[arryTitle objectAtIndex:indexPath.row]];
    [cell.textLabel setFont:[UIFont fontWithName:FONT_REGULAR size:18]];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    //3c72be
    [cell setBackgroundColor:[Utility colorFromHexString:@"3c72BE"]];
    [cell.contentView setBackgroundColor:[Utility colorFromHexString:@"3c72BE"]];
    
    
    return  cell;

    
}

@end

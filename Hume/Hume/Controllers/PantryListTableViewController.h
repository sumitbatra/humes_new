//
//  PantryListTableViewController.h
//  Hume
//
//  Created by user on 12/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PantryListTableViewCell.h"
/**
 Class:PantryListTableViewController will display all available templates for a customer with different listing like Chilled, Frozen, Dry.
 Can filter with specific search criteria.
 Can Edit & Delete templates.
 Create a New order with Seleted templates.
 */
@interface PantryListTableViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UIAlertViewDelegate>{

    IBOutlet UISegmentedControl *filterSegment; 
    
}

//Properties
@property(nonatomic,retain) IBOutlet UITableView *tableview;
@property(nonatomic,retain) IBOutlet UILabel *lblCutomerID;
@property(nonatomic,retain) IBOutlet UISearchBar *searchBar;

//Methods
-(IBAction)selectStorage:(id)sender;

@end

//
//  NSDictionary+KeyExists.h
//  Test
//
//  Created by Developer on 3/17/10.
//  Copyright 2010 . All rights reserved.
//

/**
 @class:A catagory class for NSDictionary with custom methods.
 @brief: 
   - Can peform key existance in a dictionary.
   - Can validate a null string
 @function:
 - (BOOL) keyExists:(NSString *) key;
 - (BOOL) valueForKeyIsNull:(NSString *) key;

 */

@interface NSDictionary (KeyExists) 

- (BOOL) keyExists:(NSString *) key;
- (BOOL) valueForKeyIsNull:(NSString *) key;

@end

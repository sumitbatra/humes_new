//
//  NSDictionary+KeyExists.m
//  Test
//
//  Created by Developer on 3/17/10.
//  Copyright 2010 . All rights reserved.
//

#import "NSDictionary+KeyExists.h"


@implementation NSDictionary (KeyExists)

/**
 @brief: Will validate a key inside dictionary
 @function:  keyExists
 @param: key string type.
 */
-(BOOL) keyExists:(NSString *)key {
	
	NSArray *anArray = [self allKeys];
	for (int i=0;i< [anArray count];i++)
	{
		if ([[anArray objectAtIndex:i] caseInsensitiveCompare:key] == 0) {
			return YES;
		}
    }
	return NO;
}

/**
 @brief: Will validate a Null string
 @function:  valueForKeyIsNull
 @param: key string type.
 */
- (BOOL) valueForKeyIsNull:(NSString *) key {
	return ![self keyExists:key] || ((NSNull *)[self valueForKey:key] == [NSNull null]);
}
@end

//
//  UIButton+Position.h
//  Hume
//
//  Created by User on 1/22/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 @class: A category class for button.
 @brief: Setting up the image edges with providing some spaces with title & background image.
 @function:
 centerButtonAndImageWithSpacing
 @param: (CGFloat)spacing
 */

@interface UIButton (Position)

-(void) centerButtonAndImageWithSpacing:(CGFloat)spacing;

@end

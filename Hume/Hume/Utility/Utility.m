//
//  Utility.m
//  Hume
//
//  Created by User on 12/30/14.
//  Copyright (c) 2014 Damco. All rights reserved.
//

#import "Utility.h"
#import "CustomLabel.h"

@implementation Utility


/**
 @brief: A class method
  - Update a default navigation bar tint color.
 @param:(UIColor *)colorObj ,(UINavigationController *)navControllerObj
 */
+(void) setTintColorOfNavigationBar:(UIColor *)colorObj OnNavigationController:(UINavigationController *)navControllerObj
{
    if (!colorObj) {
        return;
    }
    [navControllerObj.navigationBar setTintColor:colorObj];
}

/**
 @brief: A Class method
    - Will update a default navigation bar title label with custom label
 @param: (UINavigationController *)navControllerObj ,(NSString *)titleString
 */
+ (void) setTitleOnNavigationControllerObj:(UINavigationController *)navControllerObj WithString:(NSString *)titleString
{
    CustomLabel *titleLabel = [[CustomLabel alloc]initWithFrame:CGRectMake(120, 12, 180, 20)];
    [titleLabel setText:titleString];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [navControllerObj.navigationBar addSubview:titleLabel];
}

/**
 @brief: Validating null value inside a dic by enumrating each & every object.
 @param:(NSDictionary *)responseDictionaryData
 */
+(NSDictionary *)parseNullValuesInDictionary:(NSDictionary *)responseDictionaryData
{
    NSArray *allDicKeys = [responseDictionaryData allKeys];
    
    NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc]initWithDictionary :responseDictionaryData];
    
    for (int i = 0 ; i < [allDicKeys count ]; i++) {
        if ([[responseDictionary valueForKey:[allDicKeys objectAtIndex:i]] isEqual:[NSNull null]]) {
            NSString *tempKeyString = [allDicKeys objectAtIndex:i];
            [responseDictionary setValue:@"" forKey:tempKeyString];
        }
        else if ([[responseDictionary valueForKey :[allDicKeys objectAtIndex:i]] isKindOfClass:[NSNull class]]) {
            NSString *tempKeyString = [allDicKeys objectAtIndex:i];
            [responseDictionary setValue:@"" forKey:tempKeyString];
        }
        else if ([responseDictionary valueForKey :[allDicKeys objectAtIndex:i]] == nil) {
            NSString *tempKeyString = [allDicKeys objectAtIndex:i];
            [responseDictionary setValue:@"" forKey:tempKeyString];
        }
        else if (![responseDictionary valueForKey :[allDicKeys objectAtIndex:i]]) {
            NSString *tempKeyString = [allDicKeys objectAtIndex:i];
            [responseDictionary setValue:@"" forKey:tempKeyString];
        }
        else if ([responseDictionary valueForKey :[allDicKeys objectAtIndex:i]] == NULL) {
            NSString *tempKeyString = [allDicKeys objectAtIndex:i];
            [responseDictionary setValue:@"" forKey:tempKeyString];
        }
        else if ([[responseDictionary valueForKey :[allDicKeys objectAtIndex:i]] isKindOfClass:[NSDictionary class]])
        {
            NSMutableDictionary *tempDictionary = [responseDictionary valueForKey:[allDicKeys objectAtIndex:i]];
            tempDictionary = (NSMutableDictionary *)[self parseNullValuesInDictionary:tempDictionary];
            NSString *tempKeyString = [allDicKeys objectAtIndex:i];
            [responseDictionary setValue:tempDictionary forKey :tempKeyString];
        }
        else if ([[responseDictionary valueForKey :[allDicKeys objectAtIndex:i]] isKindOfClass:[NSArray class]])
        {
            NSArray *tempDictionary = [responseDictionary valueForKey :[allDicKeys objectAtIndex:i]];
            tempDictionary = [self parseNullValuesInArray :tempDictionary];
            NSString *tempKeyString = [allDicKeys objectAtIndex:i];
            [responseDictionary setValue:tempDictionary forKey :tempKeyString];
        }
    }
    
    return ( NSDictionary *)responseDictionary;
}

/**
 @brief: A class method
    - Validating a null object inside a array
 @param: (NSArray *)responseArray
 */
+(NSArray *)parseNullValuesInArray:(NSArray *)responseArray
{
    NSMutableArray *tempMutableArray = [[NSMutableArray alloc]initWithArray:responseArray];
    for (int index = 0; index < [responseArray count]; index++) {
        NSDictionary *tempDic = [self parseNullValuesInDictionary:[tempMutableArray objectAtIndex:index]];
        [tempMutableArray replaceObjectAtIndex:index withObject :tempDic];
    }
    return ( NSArray *)tempMutableArray;
}

/**
 @brief: A Class method
    - Converting a Hex string into a Color value
 @param: (NSString *)hexString
 */
+ (UIColor *)colorFromHexString:(NSString *)hexString {
    
    if ([hexString length] <= 0) {
        return [UIColor greenColor];
    }
    else{
        unsigned rgbValue = 0;
        NSScanner *scanner = [NSScanner scannerWithString:hexString];
        [scanner setScanLocation:1]; // bypass '#' character
        [scanner scanHexInt:&rgbValue];
        
        return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
    }
}


@end

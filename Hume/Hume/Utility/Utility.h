//
//  Utility.h
//  Hume
//
//  Created by User on 12/30/14.
//  Copyright (c) 2014 Damco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 @class: Utility class
 @brief: Can perform different operation navigation color, navigation bar title. 
 - Null value checking in dic
 - Null value checking in Array
 - Color from Hex string
 @function:
  - setTintColorOfNavigationBar
    @param: (UIColor *)colorObj ,(UINavigationController *)navControllerObj
 
  - setTitleOnNavigationControllerObj 
    @param: (UINavigationController *)navControllerObj ,(NSString *)titleString

  - parseNullValuesInDictionary
    @param: (NSDictionary *)responseDictionaryData

  - parseNullValuesInArray
    @param: (NSArray *)responseArray

  - colorFromHexString
    @param:(NSString *)hexString

 */

@interface Utility : NSObject

+(void) setTintColorOfNavigationBar:(UIColor *)colorObj OnNavigationController:(UINavigationController *)navControllerObj;

+ (void) setTitleOnNavigationControllerObj:(UINavigationController *)navControllerObj WithString:(NSString *)titleString;

+(NSDictionary *)parseNullValuesInDictionary:(NSDictionary *)responseDictionaryData;
+(NSArray *)parseNullValuesInArray:(NSArray *)responseArray;
+ (UIColor *)colorFromHexString:(NSString *)hexString;
@end

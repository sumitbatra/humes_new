//
//  UIButton+Position.m
//  Hume
//
//  Created by User on 1/22/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "UIButton+Position.h"

@implementation UIButton (Position)

/**
 @brief: Setting up the image edges with providing some spaces with title & background image.
 */
-(void) centerButtonAndImageWithSpacing:(CGFloat)spacing
{
    self.imageEdgeInsets = UIEdgeInsetsMake(-16, self.frame.size.width/2-spacing, 0, 0);
}

@end

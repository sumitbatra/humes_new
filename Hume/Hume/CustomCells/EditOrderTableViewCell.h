//
//  EditOrderTableViewCell.h
//  Hume
//
//  Created by user on 02/02/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @brief:
 Is a custom table be class
 - Can use by calling initWithFrame with passing cell identifier & custom frame
 - Having number of labels and buttons for shgowing user interface of 
 @property
 skul number
 product name, quantity
 price
 delete
 amount
 total
 address
 invoice discount
 contact name
 */
@interface EditOrderTableViewCell : UITableViewCell

{
    
}

-(id)initWithFrame:(CGRect)frame withIdentifier:(NSString *)identifierString;

//Properties
@property(nonatomic,retain) IBOutlet  UILabel *lblSkuNo;
@property(nonatomic,retain) IBOutlet  UILabel *lblProductsName;
@property(nonatomic,retain) IBOutlet  UILabel *lblRatePerCartoon;
@property(nonatomic,retain) IBOutlet  UILabel *lblQuantity;
@property(nonatomic,retain) IBOutlet  UILabel *lblPrice;
@property(nonatomic,retain) IBOutlet  UIImageView *imgViewProduct;
@property(nonatomic,retain) IBOutlet UIButton *btnEdit;
@property(nonatomic,retain) IBOutlet UIButton *btnDelete;
@property(nonatomic,retain) IBOutlet UILabel *lblAmountExcluding;
@property(nonatomic,retain) IBOutlet UILabel *lblGSTAmount;
@property(nonatomic,retain) IBOutlet UILabel *lblTotalExcuding;
@property(nonatomic,retain) IBOutlet UILabel *lblAddress;
@property(nonatomic,retain) IBOutlet UILabel *lblInvoiceDiscAmount;
@property(nonatomic,retain) IBOutlet UILabel *lblInvoiceDiscPrecentage;
@property(nonatomic,retain) IBOutlet UILabel *lblContactName;



@end

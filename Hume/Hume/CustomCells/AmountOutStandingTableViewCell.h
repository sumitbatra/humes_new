//
//  AmountOutStandingTableViewCell.h
//  Hume
//
//  Created by user on 15/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @brief: Custom tableview cell class with couple of labels like amount,current balance,balance value
 showing 3 different types of balance like >30,60,90
 @property:
 -  UILabel *lblAmountOustanding;
 -  UILabel *lblCurrentBalance;
 -  UILabel *lblBalance1Value;
 -  UILabel *lblBalance2Value;
 -  UILabel *lblBalance3Value;
 -  UILabel *lblTotal;
 -  UILabel *lbl30;
 -  UILabel *lbl60;
 -  UILabel *lbl90;
 -  UIView  * uiViewEnxpandableView;
 -  UIImageView *imgaeViewArrow;
- */
@interface AmountOutStandingTableViewCell : UITableViewCell

//Properties
@property(nonatomic,retain) IBOutlet  UILabel *lblAmountOustanding;
@property(nonatomic,retain) IBOutlet  UILabel *lblCurrentBalance;
@property(nonatomic,retain) IBOutlet  UILabel *lblBalance1Value;
@property(nonatomic,retain) IBOutlet  UILabel *lblBalance2Value;
@property(nonatomic,retain) IBOutlet  UILabel *lblBalance3Value;
@property(nonatomic,retain) IBOutlet  UILabel *lblTotal;
@property(nonatomic,retain) IBOutlet  UILabel *lbl30;
@property(nonatomic,retain) IBOutlet  UILabel *lbl60;
@property(nonatomic,retain) IBOutlet  UILabel *lbl90;
@property(nonatomic,retain) IBOutlet  UIView  * uiViewEnxpandableView;
@property(nonatomic,retain) IBOutlet  UIImageView *imgaeViewArrow;

@end

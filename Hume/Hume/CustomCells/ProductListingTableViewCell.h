//
//  ProductListingTableViewCell.h
//  Hume
//
//  Created by User on 1/20/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 @brief: ProductListingTableViewCell custom cell class with property
 @property:
 - UILabel *lblProductDescription;
 
 - UILabel *lblItemNo;
 
 - UILabel *lblUom;
 
 - UILabel *lblListPrice;
 
 - UILabel *lblCategory;
 
 - UIImageView *imgPicture;
 
 - UIButton *btnAddToCart;
 
 - UIButton *btnAddToPantry;
  
 Simply we can set cell property by using ProductListingTableViewCell object
 */
@interface ProductListingTableViewCell : UITableViewCell

//Properties
@property(nonatomic, retain) IBOutlet UILabel *lblProductDescription;
@property(nonatomic, retain) IBOutlet UILabel *lblItemNo;
@property(nonatomic, retain) IBOutlet UILabel *lblUom;
@property(nonatomic, retain) IBOutlet UILabel *lblListPrice;
@property(nonatomic, retain) IBOutlet UILabel *lblCategory;
@property(nonatomic, retain) IBOutlet UIImageView *imgPicture;
@property(nonatomic, retain) IBOutlet UIButton *btnAddToCart;
@property(nonatomic, retain) IBOutlet UIButton *btnAddToPantry;

@end

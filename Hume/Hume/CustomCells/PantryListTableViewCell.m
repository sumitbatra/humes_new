//
//  PantryListTableViewCell.m
//  Hume
//
//  Created by user on 12/02/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "PantryListTableViewCell.h"

@implementation PantryListTableViewCell

- (void)awakeFromNib {
    // Initialization code
    if (IS_IPAD) {
        [self.lblCategory setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblDescription setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblGroup setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblItemGroup setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblItemNo setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblqty setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblUnitPrice setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblUOM setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)checkBoxSelected:(id)sender{

    id<PantryListTableViewCellDelegate> strongDelegate = self.delegate;
    if ([strongDelegate respondsToSelector:@selector(addToCart:)]) {
        [self.delegate addToCart:sender];
    }
      [self.delegate addToCart:sender];
}

@end

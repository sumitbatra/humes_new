//
//  MyAccountTableViewCell1.h
//  Hume
//
//  Created by user on 08/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @brief: Custem tableview cell 
  - Using to display user account info with
 Customes labels
 Email, Telex, Fax, cell heading, address, contact name
 Customes buttons
 Edit
 View
 Edit imageview
 */

@interface MyAccountTableViewCell1 : UITableViewCell
{
    
    
    
}

//Properties
@property(nonatomic,retain) IBOutlet UIView  * uiViewEnxpandableView;
@property(nonatomic,retain) IBOutlet  UILabel *lblCustomerName;
@property(nonatomic,retain) IBOutlet  UILabel *lblEmail;
@property(nonatomic,retain) IBOutlet  UILabel *lblTelex;
@property(nonatomic,retain) IBOutlet  UILabel *lblFax;
@property(nonatomic,retain) IBOutlet  UILabel *lblContactNo;
@property(nonatomic,retain) IBOutlet  UILabel *lblCellHeading;
@property(nonatomic,retain) IBOutlet  UILabel *lblRewardsValue;
@property(nonatomic,retain) IBOutlet UITextView *txtViewAddress;
@property(nonatomic,retain) IBOutlet UIImageView *imgaeViewArrow;
@property(nonatomic,retain) IBOutlet UIButton *btnEdit;
@property(nonatomic,retain) IBOutlet UIImageView *imgViewEdit;
@end

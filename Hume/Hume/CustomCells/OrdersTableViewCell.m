//
//  OrdersTableViewCell.m
//  Hume
//
//  Created by user on 15/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "OrdersTableViewCell.h"

@implementation OrdersTableViewCell

- (void)awakeFromNib {
    // Initialization code
    if (IS_IPAD) {
        [self.lblOrderNo setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblCustomerNameValue setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblCustomerNoValue setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblEachesQTYValue setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblOrderDateValue setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblOrderTimeValue setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblOrderTotalValue setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
    }
}
@synthesize lblCustomerNameValue,
lblCustomerNameTxt,
lblCustomerNoTxt,
lblCustomerNoValue,
lblOrderDateTxt,
lblOrderDateValue,
lblOrderTotalTxt,
lblOrderTotalValue,
lblEachesQTYTxt,
lblEachesQTYValue,
lblOrderTimeTxt,
lblOrderTimeValue,
imgaeViewPiChart,lblOrderNo;
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

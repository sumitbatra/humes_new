//
//  EditOrderTableViewCell.m
//  Hume
//
//  Created by user on 02/02/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "EditOrderTableViewCell.h"
#import "AppDelegate.h"

@implementation EditOrderTableViewCell

- (void)awakeFromNib {
    // Initialization code
    if (IS_IPAD) {
         [self.lblProductsName setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
         [self.lblQuantity setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
         [self.lblPrice setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
         [self.lblRatePerCartoon setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
         [self.lblSkuNo setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0]];
    }
   
}

-(id)initWithFrame:(CGRect)frame withIdentifier:(NSString *)identifierString
{
    self = [super initWithFrame:frame];
    if (!self) {
        NSArray *nibArray = [[NSBundle mainBundle]loadNibNamed:@"EditOrderTableViewCell" owner:self options:nil];
        
        
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

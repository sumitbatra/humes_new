//
//  AmountOutStandingTableViewCell.m
//  Hume
//
//  Created by user on 15/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "AmountOutStandingTableViewCell.h"

@implementation AmountOutStandingTableViewCell
@synthesize lblAmountOustanding,
lblCurrentBalance,
lblBalance1Value,
lblBalance2Value,
lblBalance3Value,
lblTotal,
lbl30,
lbl60,
lbl90,uiViewEnxpandableView;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

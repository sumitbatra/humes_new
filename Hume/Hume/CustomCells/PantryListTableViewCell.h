//
//  PantryListTableViewCell.h
//  Hume
//
//  Created by user on 12/02/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 @brief: PantryListTableViewCell custom cell class with property
 @property:
 
 - UILabel *lblqty;
 
 - UILabel *lblUOM;
 
 - UILabel *lblItemNo;
 
 - UILabel *lblDescription;
 
 - UILabel *lblUnitPrice;
 
 - UIImageView *imgExcludefromPantry;
 
 - UIImageView *checkBox;
 
 - UIButton *btnEdit;
 
 - UIButton *btnDelete;
 
 - UILabel *lblCategory;
 
 - UILabel *lblGroup;
 
 - UILabel *lblItemGroup;
 
 - UIButton *btnCheckBox;
 
 Simply we can set cell property by using PantryListTableViewCell object
 */

@protocol PantryListTableViewCellDelegate;

@interface PantryListTableViewCell : UITableViewCell

//Properties
@property(nonatomic, retain) IBOutlet UILabel *lblqty;
@property(nonatomic, retain) IBOutlet UILabel *lblUOM;
@property(nonatomic, retain) IBOutlet UILabel *lblItemNo;
@property(nonatomic, retain) IBOutlet UILabel *lblDescription;
@property(nonatomic, retain) IBOutlet UILabel *lblUnitPrice;
@property(nonatomic, retain) IBOutlet UIImageView *imgExcludefromPantry;
@property(nonatomic, retain) IBOutlet UIImageView *checkBox;
@property(nonatomic, retain) IBOutlet UIButton *btnEdit;
@property(nonatomic, retain) IBOutlet UIButton *btnDelete;
@property(nonatomic, retain) IBOutlet UILabel *lblCategory;
@property(nonatomic, retain) IBOutlet UILabel *lblGroup;
@property(nonatomic, retain) IBOutlet UILabel *lblItemGroup;
@property(nonatomic, retain) IBOutlet UIButton *btnCheckBox;
@property(nonatomic,assign) id <PantryListTableViewCellDelegate> delegate;

//Methods
-(IBAction)checkBoxSelected:(id)sender;
@end

//Protocol
@protocol PantryListTableViewCellDelegate <NSObject>
-(void)addToCart:(id)sender;
@end

//
//  SIdeMenuTableViewCell.h
//  Hume
//
//  Created by User on 4/1/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @brief: SIdeMenuTableViewCell custom cell class
  - Setup a menu cell title using property menuCellTitle label
 */

@interface SIdeMenuTableViewCell : UITableViewCell

//Properties
@property (nonatomic, retain) IBOutlet UILabel *menuCellTitle;
@end

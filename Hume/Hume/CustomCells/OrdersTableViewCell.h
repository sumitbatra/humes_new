//
//  OrdersTableViewCell.h
//  Hume
//
//  Created by user on 15/01/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @brief: Custom tableview cell class
 @discussion: for showing order list in tableview with default values like
 customer name
 customer number
 order date
 order time value
 order quantity value
 
 Some button actions like
 Editbutton,
 process button will be added at controller leve whoever want ot use custom cell
 
 */
@interface OrdersTableViewCell : UITableViewCell

//Properties
@property(nonatomic,retain) IBOutlet  UILabel *lblCustomerNameValue;
@property(nonatomic,retain) IBOutlet  UILabel *lblCustomerNameTxt;
@property(nonatomic,retain) IBOutlet  UILabel *lblCustomerNoTxt;
@property(nonatomic,retain) IBOutlet  UILabel *lblCustomerNoValue;
@property(nonatomic,retain) IBOutlet  UILabel *lblOrderDateTxt;
@property(nonatomic,retain) IBOutlet  UILabel *lblOrderDateValue;
@property(nonatomic,retain) IBOutlet  UILabel *lblOrderTotalTxt;
@property(nonatomic,retain) IBOutlet  UILabel *lblOrderTotalValue;
@property(nonatomic,retain) IBOutlet  UILabel *lblEachesQTYTxt;
@property(nonatomic,retain) IBOutlet  UILabel *lblEachesQTYValue;
@property(nonatomic,retain) IBOutlet  UILabel *lblOrderTimeTxt;
@property(nonatomic,retain) IBOutlet  UILabel *lblOrderTimeValue;
@property(nonatomic,retain) IBOutlet UIImageView *imgaeViewPiChart;
@property(nonatomic,retain) IBOutlet  UILabel *lblOrderNo;
@property(nonatomic,retain) IBOutlet UIButton *btnEdit;
@property(nonatomic,retain) IBOutlet UIButton *btnProcess;

@end

//
//  ProductListingTableViewCell.m
//  Hume
//
//  Created by User on 1/20/15.
//  Copyright (c) 2015 Damco. All rights reserved.
//

#import "ProductListingTableViewCell.h"

@implementation ProductListingTableViewCell

- (void)awakeFromNib {
    // Initialization code
    if (IS_IPAD) {
        [self.lblProductDescription setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblCategory setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblItemNo setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblListPrice setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        [self.lblUom setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18.0]];
        
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

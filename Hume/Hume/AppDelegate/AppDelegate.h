//
//  AppDelegate.h
//  Hume
//
//  Created by User on 12/29/14.
//  Copyright (c) 2014 Damco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideMenuTableViewController.h"
#import <CoreData/CoreData.h>
#import "Settings.h"


#define APP_DELEGATE (AppDelegate*)[UIApplication sharedApplication].delegate

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(strong, nonatomic) SideMenuTableViewController *leftMenuViewController;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (retain, nonatomic)Settings *applicationTheameSetting;



@end

